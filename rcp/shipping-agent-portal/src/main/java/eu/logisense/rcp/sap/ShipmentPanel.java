/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.sap;

import eu.logisense.api.session.client.Connections;
import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.data.ShipmentManager;
import eu.logisense.rcp.data.ShipmentManagerImpl;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javax.swing.JPanel;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kenrik
 */
public class ShipmentPanel extends JPanel {

    public static final int SCENE_WIDTH = 750;
    public static final int SCENE_HEIGHT = 300;

    private JFXPanel fxPanel;
    private Consumer<JFXPanel> addNotifyListener;

    ShipmentTable<ObservableShipment> orderTable;
    ShipmentTableButtons buttons;

    private static final Logger log = LoggerFactory.getLogger(ShipmentPanel.class);

    public ShipmentPanel() {
        initComponents();
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        fxPanel = new JFXPanel() {

            @Override
            public void addNotify() {
                super.addNotify();
                if (addNotifyListener != null) {
                    addNotifyListener.accept(fxPanel);
                }
            }
        };
        //Scene dimensions not used by Dialog as it is created later, set on fxPanel as well:
        fxPanel.setPreferredSize(new Dimension(SCENE_WIDTH, SCENE_HEIGHT));

        Platform.setImplicitExit(false);
        Platform.runLater(() -> {
            Scene scene = new Scene(new AnchorPane());
            scene.getStylesheets().addAll(ShipmentPanel.class.getResource("sap.css").toExternalForm());

            //
            // left side, source table
            //
            AnchorPane leftSidePane = new AnchorPane();

            //
            // filling the table
            //
            
            orderTable = new ShipmentTable(Lookup.getDefault().lookup(ShipmentManager.class).getShipments());

            leftSidePane.getChildren().addAll(orderTable);

            //
            // bind overviewTable to parent anchorpane for resizing.
            // gap at bottom for filterField and buttons
            //
            AnchorPane.setBottomAnchor(orderTable, 0.0);
            AnchorPane.setTopAnchor(orderTable, 0.0);
            AnchorPane.setRightAnchor(orderTable, 0.0);
            AnchorPane.setLeftAnchor(orderTable, 64.0);

            //
            // add/delete buttons for tableView
            //
            buttons = new ShipmentTableButtons(orderTable);
            leftSidePane.getChildren().addAll(buttons);

            AnchorPane.setTopAnchor(buttons, 0.0);
            AnchorPane.setLeftAnchor(buttons, 0.0);

            ((AnchorPane) scene.getRoot()).getChildren().addAll(leftSidePane);
            AnchorPane.setBottomAnchor(leftSidePane, 10.0);
            AnchorPane.setTopAnchor(leftSidePane, 10.0);
            AnchorPane.setRightAnchor(leftSidePane, 10.0);
            AnchorPane.setLeftAnchor(leftSidePane, 10.0);

            fxPanel.setScene(scene);
        });

        add(fxPanel, BorderLayout.CENTER);
    }

    //FIXME: 
    // workaround to keep using our swing droptarget impls on jfxpanels (see #144);
    // remove when DnD handling has been converted from swing to javafx
    // (looks like this route is only needed in CropProductionUnitPropertiesView).
    public void setAddNotifyListener(Consumer<JFXPanel> l) {
        addNotifyListener = l;
    }
}
