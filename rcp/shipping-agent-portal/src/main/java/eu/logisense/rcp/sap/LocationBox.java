/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.sap;

import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ShipmentManagerImpl;
import eu.logisense.rcp.util.AutoCompleteComboBoxListener;
import eu.logisense.rcp.util.CountryComboBox;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import lombok.Getter;
import org.controlsfx.control.RangeSlider;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author kenrik
 */
@Messages({
    "locationBox email prompt=email",
    "locationBox phone prompt=phone",
    "locationBox sender equals load location=sender is load"
})
public class LocationBox extends VBox {

    private static final double TA_HEIGHT = 40.0;
    private static final double LABEL_WIDTH = 100.0;
    private static final double CHECKBOX_WIDTH = 160.0;
    private static final double SPACING = 5.0;

    private String title;

    //
    // top row
    //
    private HBox orderLocationHBox;
    private Label orderLocationLabel;
    @Getter
    private ComboBox clientNameComboBox;
    @Getter
    private ComboBox clientLocationComboBox;
    @Getter
    private CheckBox newLocationCheck;
    private HBox newLocationBox;

    //
    // second row
    //
    private HBox shipmentLocationHBox;
    private Label shipmentLocationLabel;
    @Getter
    private TextArea addressTA;
    @Getter
    private TextField postalCodeTF;
    @Getter
    private TextField cityTF;
    @Getter
    private ComboBox locationNameComboBox;
    @Getter
    private CountryComboBox countryComboBox;

    //
    // third row
    //
    private HBox emailPhoneHBox;
    private Label emailPhoneLabel;
    @Getter
    private TextField emailTF;
    @Getter
    private TextField phoneTF;

    //
    // bottom row
    //
    private HBox timeWindowHBox;
    private Label timeWindowLabel;
    @Getter
    private DatePicker datePicker;
    @Getter
    private RangeSlider timePicker;

    public LocationBox(String title) {
        this.title = title;
        setPadding(new Insets(0.0, 0.0, 10.0, 0.0));
        setSpacing(SPACING);

        initComponents(title);
    }

    private void initComponents(String title) {

        //
        // top row
        //
        orderLocationLabel = new Label(title);
        clientNameComboBox = new ComboBox();
        clientLocationComboBox = new ComboBox();
        newLocationCheck = new CheckBox();
        newLocationBox = new HBox(newLocationCheck);
        newLocationBox.setAlignment(Pos.BOTTOM_RIGHT);

        orderLocationHBox = new HBox(orderLocationLabel, clientNameComboBox, clientLocationComboBox, newLocationBox);

        //
        // second row
        //
        shipmentLocationLabel = new Label();
        addressTA = new TextArea();
        postalCodeTF = new TextField();
        cityTF = new TextField();
        locationNameComboBox = new ComboBox();
        countryComboBox = new CountryComboBox();

        shipmentLocationHBox = new HBox(shipmentLocationLabel,
                new VBox(locationNameComboBox, new HBox(postalCodeTF, cityTF, countryComboBox)), addressTA);

        //
        // third row
        //
        emailPhoneLabel = new Label();
        emailTF = new TextField();
        phoneTF = new TextField();

        emailPhoneHBox = new HBox(emailPhoneLabel, emailTF, phoneTF);

        //
        // bottom row
        //
        timeWindowLabel = new Label(Bundle.time_window_label());
        datePicker = new DatePicker();
        //
        // create timepicker with range 0-23.99 and default value at 12
        //
        timePicker = new RangeSlider(0.0, 23.99, 12.0, 12.0);

        timeWindowHBox = new HBox(timeWindowLabel, datePicker, timePicker);

        //
        // set Title and load/unload label, dependent on title
        // also only add orderLocationHBox when is sender/origin
        //
        switch (title.toLowerCase()) {
            case "origin":
            case "sender":
                shipmentLocationLabel.setText(Bundle.load_label());
                newLocationCheck.setText(Bundle.locationBox_sender_equals_load_location());
                getChildren().addAll(orderLocationHBox, shipmentLocationHBox, emailPhoneHBox, timeWindowHBox);
                break;
            case "destination":
                shipmentLocationLabel.setText(Bundle.unload_label());
                getChildren().addAll(shipmentLocationHBox, emailPhoneHBox, timeWindowHBox);
                break;
            default:
                break;
        }

        configComponents();
    }

    private void configComponents() {

        orderLocationLabel.setPrefWidth(LABEL_WIDTH);
        //
        // bind to screen width but subtract width of label and two times the amount of spacing
        //
        clientNameComboBox.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5));
        clientNameComboBox.setEditable(true);
        clientNameComboBox.setPromptText(Bundle.client_name_label());
        clientLocationComboBox.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5).subtract(CHECKBOX_WIDTH + SPACING));
        clientLocationComboBox.setPromptText(Bundle.client_location_label());
        newLocationCheck.setSelected(true);
        newLocationCheck.setPrefWidth(CHECKBOX_WIDTH);
        newLocationCheck.setPrefHeight(clientNameComboBox.getPrefHeight());

        //
        // set location detail fields disabled as default
        //
        disableFields(true);

        orderLocationHBox.setSpacing(SPACING);

        //
        // fill clientName and Location boxes
        //
        clientNameComboBox.setItems(ClientManager.getInstance().getUsableClients());

        //
        // fill location name box
        //
        locationNameComboBox.setItems(ClientManager.getInstance().getLocations());

        shipmentLocationLabel.setPrefWidth(LABEL_WIDTH);
        addressTA.setPromptText(Bundle.address_label());
        addressTA.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5));
        addressTA.setPrefHeight(TA_HEIGHT);
        //
        // bind to address width and divide between postal, city and country
        // clunky but only way to get it correct most of the times.
        // most of the times because there are some issues with rounding
        // numbers (caused by divide(3))
        //
        locationNameComboBox.setPromptText(Bundle.name_label());
        locationNameComboBox.setEditable(true);
        locationNameComboBox.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5));
        postalCodeTF.setPromptText(Bundle.postal_code_label());
        postalCodeTF.prefWidthProperty().bind(locationNameComboBox.prefWidthProperty().divide(3));
        cityTF.setPromptText(Bundle.city_label());
        cityTF.prefWidthProperty().bind(locationNameComboBox.prefWidthProperty().divide(3));
        countryComboBox.setPromptText(Bundle.country_label());
        countryComboBox.prefWidthProperty().bind(locationNameComboBox.prefWidthProperty().divide(3));
        shipmentLocationHBox.setSpacing(SPACING);

        //
        // Looks weird but this adds the autocomplete functionality to the comboboxes
        // FIXME: ugly code
        //
        new AutoCompleteComboBoxListener<>(clientNameComboBox);
        new AutoCompleteComboBoxListener<>(locationNameComboBox);
//        countryBox.setSpacing(SPACING);

        emailPhoneLabel.setPrefWidth(LABEL_WIDTH);
        emailTF.setPromptText(Bundle.locationBox_email_prompt());
        emailTF.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5));
        phoneTF.setPromptText(Bundle.locationBox_phone_prompt());
        phoneTF.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5));
        emailPhoneHBox.setSpacing(SPACING);

        //
        // TimeWindow DateTimePickers
        //
        timeWindowLabel.setPrefWidth(LABEL_WIDTH);
        datePicker.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5));
        timePicker.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5));

        //
        // set DatePicker format to dd/MM/yyyy
        //
        datePicker.setConverter(new StringConverter<LocalDate>() {
            private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null) {
                    return "";
                }
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString) {
                if (dateString == null || dateString.trim().isEmpty()) {
                    return null;
                }
                return LocalDate.parse(dateString, dateTimeFormatter);
            }
        });
        // 
        // set timepicker 
        //
        timePicker.setShowTickMarks(true);
        timePicker.setShowTickLabels(true);
        timePicker.setMinorTickCount(1);
        timePicker.setMajorTickUnit(1.0);
        timePicker.setBlockIncrement(1.0);
        timePicker.setSnapToTicks(true);

        timeWindowHBox.setSpacing(SPACING);

    }

    public void disableFields(boolean bool) {
        addressTA.setDisable(bool);
        postalCodeTF.setDisable(bool);
        cityTF.setDisable(bool);
        countryComboBox.setDisable(bool);
        phoneTF.setDisable(bool);
        emailTF.setDisable(bool);
    }

}
