/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.sap;

import eu.logisense.api.Location;
import eu.logisense.api.OrderLine;
import eu.logisense.api.OrderLineType;
import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableClient;
import eu.logisense.rcp.data.ObservableLocation;
import eu.logisense.rcp.data.ObservableOrderLine;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.util.AutoCompleteComboBoxListener;
import eu.logisense.rcp.util.DebitorCreditorBox;
import eu.logisense.rcp.util.IsoNameConverter;
import eu.logisense.rcp.util.MoneyStringConverter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Currency;
import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kenrik
 */
@Messages({
    "sender.label=sender",
    "destination.label=destination",
    "load.label=load",
    "unload.label=unload",
    "date.label=date",
    "from.label=from",
    "until.label=until",
    "name.label=name",
    "length.label=length",
    "width.label=width",
    "height.label=height",
    "volume.label=volume",
    "weight.label=weight",
    "size.label=size",
    "note.label=note",
    "street.label=street",
    "region.label=region",
    "country.label=country",
    "address.label=address",
    "city.label=city",
    "postal.code.label=postal code",
    "time.window.label=time window",
    "client.name.label=client name",
    "client.location.label=client location",
    "total.label=total",
    "customer.label=customer",
    "order.reference.label=order reference",
    "debitor.label=debitor",
    "reference label for shipment detail popup=reference",
    "shipment details validator message client=enter client",
    "shipment details validator message client name=enter client name",
    "shipment details validator message client postal code=enter client postal code",
    "shipment details validator message client city=enter client city",
    "shipment details validator message client phone=enter client phone number",
    "shipment details validator message client email=enter client email address",
    "shipment details validator message client country=enter client country",
    "shipment details validator message client address=enter client address",
    "shipment details validator message client price=enter price",
    "shipment details validator message sender not equals debitor=debitor is not the same as sender"
})
public class ShipmentDetails extends VBox {

    private static final double ORDERLINES_HEIGHT = 160.0;
    private static final double NOTE_HEIGHT = 60.0;
    private static final double LABEL_WIDTH = 100.0;
    private static final double SPACING = 20.0;
    private static final double PADDING = 5.0;
    private static final double INSETS = PADDING * 2;
    private static final String EMPTY_SHIPMENT_NAME = "";

    private TextArea noteTA;
    private Label errorLabel;
    private HBox errorHBox;

    private VBox locationBoxes;
    private LocationBox load;
    private LocationBox unload;
    private DebitorCreditorBox debitor;

    private Label referenceLabel;
    private TextField referenceTF;
    private HBox referenceBox;

    private OrderLinesTable<ObservableOrderLine> orderLinesTable;

    private VBox orderLinesBox;
    private Button addOrderLine;
    private Button removeOrderLine;
    private HBox buttonBox;

    private Label volumeLabel;
    private TextField volumeNTF;
    private HBox volumeBox;
    private Label weightLabel;
    private TextField weightNTF;
    private HBox weightBox;

    private final ValidationSupport validationSupport = new ValidationSupport();

    private static final Logger log = LoggerFactory.getLogger(ShipmentDetails.class);

    public ShipmentDetails(ObservableShipment shipment) {
        setSpacing(SPACING);
        setPadding(new Insets(PADDING));
        initComponents(shipment);
    }

    private void initComponents(ObservableShipment shipment) {

        load = new LocationBox(Bundle.sender_label());
        unload = new LocationBox(Bundle.destination_label());
        debitor = new DebitorCreditorBox(Bundle.debitor_label());
        locationBoxes = new VBox(load, unload, debitor);

        debitor.getClientNameComboBox().setItems(ClientManager.getInstance().getClients());

        //
        // Looks weird but this adds the autocomplete functionality to the comboboxes
        // FIXME: ugly code
        //
        new AutoCompleteComboBoxListener<>(debitor.getClientNameComboBox());

        addOrderLine = new Button("+");
        removeOrderLine = new Button("-");
        buttonBox = new HBox(addOrderLine, removeOrderLine);
        buttonBox.setSpacing(PADDING);

        //
        // fill orderlines table with orderlineslist from observable shipment
        //
        orderLinesTable = new OrderLinesTable<>(shipment.getOrderLineList());

        volumeLabel = new Label(Bundle.total_label() + " " + Bundle.volume_label());
        volumeLabel.setPrefWidth(LABEL_WIDTH);
        volumeNTF = new TextField();
        volumeBox = new HBox(volumeLabel, volumeNTF);
        volumeNTF.setEditable(false);

        weightLabel = new Label(Bundle.total_label() + " " + Bundle.weight_label());
        weightLabel.setPrefWidth(LABEL_WIDTH);
        weightNTF = new TextField();
        weightBox = new HBox(weightLabel, weightNTF);
        weightNTF.setEditable(false);

        orderLinesBox = new VBox(orderLinesTable, buttonBox, volumeBox, weightBox);
        orderLinesBox.setSpacing(PADDING);

        noteTA = new TextArea();

        referenceLabel = new Label(Bundle.reference_label_for_shipment_detail_popup());
        referenceTF = new TextField();
        referenceTF.setPromptText(Bundle.reference_label_for_shipment_detail_popup());
        referenceBox = new HBox(referenceLabel, referenceTF);

        //
        // error message when load client is not the same as debitor
        // wrap in hbox to be able to display at right side of the stage.
        // only show label when text in sender combo != text in debitor
        //
        errorLabel = new Label(Bundle.shipment_details_validator_message_sender_not_equals_debitor());
        errorLabel.setTextFill(Color.RED);
        errorHBox = new HBox(errorLabel);
        errorHBox.setAlignment(Pos.CENTER_RIGHT);
        errorLabel.visibleProperty().bind(Bindings.notEqual(
                load.getClientNameComboBox().getEditor().textProperty(),
                debitor.getClientNameComboBox().getEditor().textProperty()));

        this.getChildren().addAll(/*nameNTF, */locationBoxes, referenceBox, orderLinesBox, noteTA, errorHBox);

        //
        // button action listeners
        //
        addOrderLine.setOnAction((ActionEvent e) -> {
            try {
                ObservableOrderLine ol = new ObservableOrderLine(OrderLine.builder()
                        .amount(0)
                        .length(OrderLineType.EURO_PALLET.length)
                        .width(OrderLineType.EURO_PALLET.width)
                        .height(0.0)
                        .weight(0.0)
                        .type(OrderLineType.EURO_PALLET)
                        .stackable(Boolean.FALSE)
                        .swapPallets(Boolean.FALSE)
                        .paymentOnDeliveryAmount(0.0)
                        .paymentOnDeliveryCurrency("EUR")
                        .build());
                orderLinesTable.getItems().add(ol);
                
                
            } catch (NoSuchMethodException ex) {
                Exceptions.printStackTrace(ex);
            }
        });

        removeOrderLine.setOnAction((ActionEvent e) -> {
            orderLinesTable.getItems().remove((ObservableOrderLine) orderLinesTable.getSelectionModel().getSelectedItem());
        });

        //
        // disable removeOrder button when selection is null
        //
        removeOrderLine.disableProperty().bind(Bindings.isEmpty(orderLinesTable.getSelectionModel().getSelectedItems()));

        setSizes();
        setBindings(shipment);
        setValidationBindings();
    }

    private void setSizes() {

        load.prefWidthProperty().bind(widthProperty().subtract(INSETS).multiply(0.5));
        unload.prefWidthProperty().bind(widthProperty().subtract(INSETS).multiply(0.5));
        locationBoxes.setSpacing(SPACING);

        referenceLabel.setPrefWidth(LABEL_WIDTH);
        referenceTF.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH));
        referenceBox.setSpacing(SPACING);

        orderLinesTable.setPrefHeight(ORDERLINES_HEIGHT);
        orderLinesTable.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        noteTA.setPromptText(Bundle.note_label());
        noteTA.setPrefHeight(NOTE_HEIGHT);
        noteTA.prefWidthProperty().bind(widthProperty().subtract(INSETS));
    }

    private void setBindings(ObservableShipment shipment) {

        /**
         * LOAD DETAILS - load address, load postal code, load city, load country, load date, load time, load phone, load email
         */
        load.getAddressTA().textProperty().bindBidirectional(shipment.loadAddressProperty());
        load.getPostalCodeTF().textProperty().bindBidirectional(shipment.loadPostalCodeProperty());
        load.getCityTF().textProperty().bindBidirectional(shipment.loadCityProperty());

        Bindings.bindBidirectional(load.getCountryComboBox().valueProperty(),
                shipment.loadCountryProperty(), new IsoNameConverter());

        //
        // check value before setting
        // set to existing date/time if available, else, set value to now
        //
        load.getDatePicker().setValue((shipment.loadTimeFrameFromProperty().getValue() == null)
                ? LocalDate.now() : shipment.loadTimeFrameFromProperty().getValue().toLocalDate());
        load.getTimePicker().setLowValue((shipment.loadTimeFrameFromProperty().getValue() == null)
                ? LocalTime.now().getHour() + LocalTime.now().getMinute() / 60
                : shipment.loadTimeFrameFromProperty().getValue().getHour()
                + shipment.loadTimeFrameFromProperty().getValue().getMinute() / 60);
        load.getTimePicker().setHighValue((shipment.loadTimeFrameToProperty().getValue() == null)
                ? LocalTime.now().getHour() + LocalTime.now().getMinute() / 60
                : shipment.loadTimeFrameToProperty().getValue().getHour()
                + shipment.loadTimeFrameToProperty().getValue().getMinute() / 60);

        //
        // add changelisteners to datepicker and timepicker
        //
        load.getDatePicker().valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
            if (newValue != null) {
                shipment.loadTimeFrameFromProperty().setValue(shipment.loadTimeFrameFromProperty().getValue().toLocalTime().atDate(newValue));
                shipment.loadTimeFrameToProperty().setValue(shipment.loadTimeFrameToProperty().getValue().toLocalTime().atDate(newValue));
            }
        });
        load.getTimePicker().lowValueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                int hours = newValue.intValue();
                int minutes = (int) (((double) (newValue.doubleValue() - hours)) * 60);
                LocalTime newTime = LocalTime.of(hours, minutes, 0, 0);
                //
                // set new LocalDateTime to be existing date with the double
                // from the sliders translated into a new LocalTime
                //
                shipment.loadTimeFrameFromProperty().setValue(
                        shipment.loadTimeFrameFromProperty().getValue().toLocalDate().atTime(newTime));
            }
        });
        load.getTimePicker().highValueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                int hours = newValue.intValue();
                int minutes = (int) (((double) (newValue.doubleValue() - hours)) * 60);
                LocalTime newTime = LocalTime.of(hours, minutes, 0, 0);
                shipment.loadTimeFrameToProperty().setValue(
                        shipment.loadTimeFrameToProperty().getValue().toLocalDate().atTime(newTime));
            }
        });
        load.getPhoneTF().textProperty().bindBidirectional(shipment.loadPhoneProperty());
        load.getEmailTF().textProperty().bindBidirectional(shipment.loadEmailProperty());

        /**
         * UNLOAD DETAILS - unload address, unload postal code, unload city, unload country, unload date, unload time, unload
         * phone, unload email
         */
        unload.getAddressTA().textProperty().bindBidirectional(shipment.unloadAddressProperty());
        unload.getPostalCodeTF().textProperty().bindBidirectional(shipment.unloadPostalCodeProperty());
        unload.getCityTF().textProperty().bindBidirectional(shipment.unloadCityProperty());

        Bindings.bindBidirectional(unload.getCountryComboBox().valueProperty(),
                shipment.unloadCountryProperty(), new IsoNameConverter());

        unload.getDatePicker().setValue((shipment.unloadTimeFrameFromProperty().getValue() == null)
                ? LocalDate.now() : shipment.unloadTimeFrameFromProperty().getValue().toLocalDate());
        unload.getTimePicker().setLowValue((shipment.unloadTimeFrameFromProperty().getValue() == null)
                ? LocalTime.now().getHour() + LocalTime.now().getMinute() / 60
                : shipment.unloadTimeFrameFromProperty().getValue().getHour()
                + shipment.unloadTimeFrameFromProperty().getValue().getMinute() / 60);
        unload.getTimePicker().setHighValue((shipment.unloadTimeFrameToProperty().getValue() == null)
                ? LocalTime.now().getHour() + LocalTime.now().getMinute() / 60
                : shipment.unloadTimeFrameToProperty().getValue().getHour()
                + shipment.unloadTimeFrameToProperty().getValue().getMinute() / 60);

        //
        // add changelisteners to datepicker and timepicker
        //
        unload.getDatePicker().valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
            if (newValue != null) {
                shipment.unloadTimeFrameFromProperty().setValue(shipment.unloadTimeFrameFromProperty().getValue().toLocalTime().atDate(newValue));
                shipment.unloadTimeFrameToProperty().setValue(shipment.unloadTimeFrameToProperty().getValue().toLocalTime().atDate(newValue));
            }
        });
        unload.getTimePicker().lowValueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                int hours = newValue.intValue();
                int minutes = (int) (((double) (newValue.doubleValue() - hours)) * 60);
                LocalTime newTime = LocalTime.of(hours, minutes, 0, 0);
                shipment.unloadTimeFrameFromProperty().setValue(
                        shipment.unloadTimeFrameFromProperty().getValue().toLocalDate().atTime(newTime));
            }
        });
        unload.getTimePicker().highValueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                int hours = newValue.intValue();
                int minutes = (int) (((double) (newValue.doubleValue() - hours)) * 60);
                LocalTime newTime = LocalTime.of(hours, minutes, 0, 0);
                shipment.unloadTimeFrameToProperty().setValue(
                        shipment.unloadTimeFrameToProperty().getValue().toLocalDate().atTime(newTime));
            }
        });
        unload.getPhoneTF().textProperty().bindBidirectional(shipment.unloadPhoneProperty());
        unload.getEmailTF().textProperty().bindBidirectional(shipment.unloadEmailProperty());

        /**
         * WEIGHT, VOLUME & SHIPPED UNITS
         */
        //
        // bind sums of table to total textFields and to shipment weight/volume
        //
        weightNTF.textProperty().bind(shipment.weightProperty().asString());
        volumeNTF.textProperty().bind(shipment.volumeProperty().asString());
        
        /**
         * LOAD LOCATIONS- load client, load location, load location name, senderIsLoad true/false
         */
        //
        // following is executed when an existing shipment is opened for editing
        //
        if (shipment.ordererProperty().getValue() != null
                && !"".equals(shipment.ordererProperty().getValue().getName())) {
            //
            // set value in client name box if not null
            //
            load.getClientNameComboBox().getSelectionModel().select(shipment.ordererProperty().getValue().getName());

            //
            // FIXME: this will cause problems when client database gets bigger
            // check if orderer name is available in observableClient list
            // because we need to fill locationBox.
            //
            ClientManager.getInstance().getClients()
                    .stream()
                    .filter((ObservableClient oc)
                            -> oc.getClient().getId().equals(shipment.ordererProperty().getValue().getId())
                            && oc.getLocations() != null)
                    .forEach((ObservableClient oc) -> {
                        //
                        // fill locationbox with locations
                        //
                        load.getClientLocationComboBox().setItems(oc.getLocations());
                        //
                        // if there's already a location name available,
                        // set that value to the boxes
                        //
                        if (shipment.loadNameProperty() != null) {

                            load.getClientLocationComboBox().getSelectionModel().select(
                                    shipment.loadNameProperty().getValue());

                            load.getLocationNameComboBox().getSelectionModel().select(
                                    shipment.loadNameProperty().getValue());
                            //
                            // else, select the first location in the list
                            //
                        } else {
                            load.getClientLocationComboBox().getSelectionModel().select(0);
                        }
                    });
        }

        //
        // on startup, check loadName to set locationName / clientLocation boxes
        //
        if (shipment.loadNameProperty() != null) {
            load.getClientLocationComboBox().getSelectionModel().select(shipment.loadNameProperty().getValue());
            load.getLocationNameComboBox().getSelectionModel().select(shipment.loadNameProperty().getValue());
        }

        //
        // listen to selected client to set loadLocation values
        //
        load.getClientNameComboBox().getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    log.debug("from changed");
                    //
                    // update locations comboBox
                    //
                    if (newValue instanceof ObservableClient) {
                        shipment.ordererProperty().setValue(((ObservableClient) newValue).getClient());
                        shipment.ordererNameProperty().setValue(((ObservableClient) newValue).getClient().getName());

                        if (((ObservableClient) newValue).getLocations() != null
                        && !((ObservableClient) newValue).getLocations().isEmpty()) {
                            load.getClientLocationComboBox().setItems(((ObservableClient) newValue).getLocations());

                            //
                            // select the first location in the list
                            //
                            load.getClientLocationComboBox().getSelectionModel().select(0);
                        }

                        //
                        // find out oldDebitor name
                        //
                        String oldDebitor = shipment.debitorIdProperty().getValue() == null ? ""
                                : ClientManager.getInstance().getClient(shipment.debitorIdProperty().getValue()) == null ? ""
                                        : ClientManager.getInstance().getClient(shipment.debitorIdProperty().getValue()).getClient().getName();

                        String oldClient = oldValue == null ? "" : oldValue.toString();

                        //
                        // set debtor if empty or same as old value
                        //
                        if (shipment.debitorIdProperty().getValue() == null
                        || "".equals(shipment.debitorIdProperty().getValue())
                        || oldDebitor.equals(oldClient)) {
                            debitor.getClientNameComboBox().getSelectionModel().select((ObservableClient) newValue);
                        }
                    }
                });

        //
        // listen to loadLocation combobox
        //
        load.getClientLocationComboBox().getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof ObservableLocation) {
                        updateLocationDetails(shipment, (ObservableLocation) newValue, true);
                        load.getLocationNameComboBox().getSelectionModel().select(((ObservableLocation) newValue).nameProperty().getValue());
                    }
                });

        //
        // listen to loadLocation name combobox
        //
        load.getLocationNameComboBox().getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof ObservableLocation) {
                        updateLocationDetails(shipment, (ObservableLocation) newValue, true);
                    }
                });

        //
        // add listener to location name combo textfield
        // enable detail fields when typed location name is not already known
        //
        load.getLocationNameComboBox().getEditor().textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            final BooleanProperty foundProperty = new SimpleBooleanProperty(false);
            ClientManager.getInstance().getLocations()
                    .stream()
                    .filter(l -> l.nameProperty().getValue() != null && l.nameProperty().getValue().equals(newValue))
                    .forEach((l) -> {
                        updateLocationDetails(shipment, l, true);
                        foundProperty.setValue(true);
                    });
            load.disableFields(foundProperty.getValue());

            if (!foundProperty.getValue()) {
                shipment.loadIdProperty().setValue(null);
                shipment.loadNameProperty().setValue(newValue);
                updateLocationDetails(shipment, null, true);
            }

            shipment.loadNameProperty().setValue(newValue);
        });

        //
        // set checkBox to stored value
        // disable when 'sender is load' is checked
        // bind checking of 'sender is load' to stored value
        //
        if (shipment.senderIsLoadProperty() != null && shipment.senderIsLoadProperty().getValue() != null) {
            load.getNewLocationCheck().setSelected(shipment.senderIsLoadProperty().getValue());
        }
        load.getLocationNameComboBox().disableProperty().bind(load.getNewLocationCheck().selectedProperty());
        shipment.senderIsLoadProperty().bind(load.getNewLocationCheck().selectedProperty());

        //
        // when checkBox is unticked, select newLoadLocation
        //
        load.getNewLocationCheck().selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {

                load.getLocationNameComboBox().getSelectionModel().select(EMPTY_SHIPMENT_NAME);
            } else {
                //
                // when checkbox is 'reticked', set value from client location
                // to location name
                //
                load.getLocationNameComboBox().getSelectionModel().select(load.getClientLocationComboBox().getValue());
                //
                // set fields disabled to be sure, because ClientLocation combo
                // may be empty
                //
                load.disableFields(newValue);
            }

        });

        /**
         * UNLOAD LOCATION- unload location name
         */
        if (shipment.unloadNameProperty() != null) {
            unload.getLocationNameComboBox().getSelectionModel().select(shipment.unloadNameProperty().getValue());
        }

        //
        // listen to loadLocation name combobox
        //
        unload.getLocationNameComboBox().getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof ObservableLocation) {
                        updateLocationDetails(shipment, (ObservableLocation) newValue, false);
                    }
                });

        //
        // add listener to location name combo textfield
        // enable detail fields when typed location name is not already known
        //
        unload.getLocationNameComboBox().getEditor().textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            final BooleanProperty foundProperty = new SimpleBooleanProperty(false);
            ClientManager.getInstance().getLocations()
                    .stream()
                    .filter(l -> l.nameProperty().getValue() != null && l.nameProperty().getValue().equals(newValue))
                    .forEach((l) -> {
                        updateLocationDetails(shipment, l, false);
                        foundProperty.setValue(true);
                    });
            unload.disableFields(foundProperty.getValue());

            if (!foundProperty.getValue()) {

                shipment.unloadIdProperty().setValue(null);
                shipment.unloadNameProperty().setValue(newValue);
                updateLocationDetails(shipment, null, false);

            }

            shipment.unloadNameProperty().setValue(newValue);
        });

        /**
         * DEBITOR - debitor name, currency & price
         */
        //
        // executed at startup, fill debitor if there's a value available
        //
        if (shipment.debitorIdProperty() != null && shipment.debitorIdProperty().getValue() != null) {
            ObservableClient debitorClient = ClientManager.getInstance().getClient(shipment.debitorIdProperty().getValue());
            if (debitorClient != null) {
                debitor.getClientNameComboBox().getSelectionModel().select(debitorClient);
            }
        }

        //
        // listen to debitor selection
        //
        debitor.getClientNameComboBox().getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof ObservableClient) {
                        shipment.debitorIdProperty().setValue(((ObservableClient) newValue).getClient().getId());
                    }
                });

        //
        // executed at startup, fill currency if there's a value available,
        // else, set to EUR
        //
        if (shipment.currencyProperty() != null) {
            debitor.getCurrencyComboBox().getSelectionModel().select(shipment.currencyProperty().getValue());
        } else {
            debitor.getCurrencyComboBox().getSelectionModel().select("EUR");
        }

        //
        // listen to currency selection
        //
        debitor.getCurrencyComboBox().getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof Currency) {
                        shipment.currencyProperty().setValue(newValue.toString());
                    }
                });

        Bindings.bindBidirectional(debitor.getPriceTF().textProperty(),
                shipment.sellPriceProperty(), new MoneyStringConverter());
        
        noteTA.textProperty().bindBidirectional(shipment.noteProperty());
        referenceTF.textProperty().bindBidirectional(shipment.orderReferenceProperty());
    }

    /**
     * set values from ObservableLocation to shipment
     *
     * @param shipment
     * @param newValue
     * @param isLoad
     */
    private void updateLocationDetails(ObservableShipment shipment, ObservableLocation newValue, boolean isLoad) {
        if (isLoad) {
            shipment.loadIdProperty().setValue(newValue == null ? null : newValue.idProperty().getValue());
            shipment.loadNameProperty().setValue(newValue == null ? null : newValue.nameProperty().getValue());
            shipment.loadAddressProperty().setValue(newValue == null ? null : newValue.addressProperty().getValue());
            shipment.loadCityProperty().setValue(newValue == null ? null : newValue.cityProperty().getValue());
            shipment.loadCountryProperty().setValue(newValue == null ? null : newValue.countryProperty().getValue());
            shipment.loadPostalCodeProperty().setValue(newValue == null ? null : newValue.postalcodeProperty().getValue());
            shipment.loadPhoneProperty().setValue(newValue == null ? null : newValue.phoneProperty().getValue());
            shipment.loadEmailProperty().setValue(newValue == null ? null : newValue.emailProperty().getValue());
        } else {
            shipment.unloadIdProperty().setValue(newValue == null ? null : newValue.idProperty().getValue());
            shipment.unloadNameProperty().setValue(newValue == null ? null : newValue.nameProperty().getValue());
            shipment.unloadAddressProperty().setValue(newValue == null ? null : newValue.addressProperty().getValue());
            shipment.unloadCityProperty().setValue(newValue == null ? null : newValue.cityProperty().getValue());
            shipment.unloadCountryProperty().setValue(newValue == null ? null : newValue.countryProperty().getValue());
            shipment.unloadPostalCodeProperty().setValue(newValue == null ? null : newValue.postalcodeProperty().getValue());
            shipment.unloadPhoneProperty().setValue(newValue == null ? null : newValue.phoneProperty().getValue());
            shipment.unloadEmailProperty().setValue(newValue == null ? null : newValue.emailProperty().getValue());
        }
    }

    /**
     * initiate and set validation binding
     */
    private void setValidationBindings() {
        // https://bitbucket.org/controlsfx/controlsfx/issues/539/multiple-dialog-fields-with-validation
        Platform.runLater(() -> {

            //
            // add validator to nodes, shows small error icon in bottomleft corner
            // when empty
            //
            validationSupport.registerValidator(load.getClientNameComboBox(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_name()));
            validationSupport.registerValidator(load.getLocationNameComboBox(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_name()));
            validationSupport.registerValidator(load.getAddressTA(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_address()));
            validationSupport.registerValidator(load.getPostalCodeTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_postal_code()));
            validationSupport.registerValidator(load.getCityTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_city()));
            validationSupport.registerValidator(load.getPhoneTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_phone()));
            validationSupport.registerValidator(load.getEmailTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_email()));
            validationSupport.registerValidator(load.getCountryComboBox(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_country()));
            validationSupport.registerValidator(unload.getAddressTA(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_address()));
            validationSupport.registerValidator(unload.getPostalCodeTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_postal_code()));
            validationSupport.registerValidator(unload.getCityTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_city()));
            validationSupport.registerValidator(unload.getPhoneTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_phone()));
            validationSupport.registerValidator(unload.getEmailTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_email()));
            validationSupport.registerValidator(unload.getCountryComboBox(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_country()));
            validationSupport.registerValidator(debitor.getClientNameComboBox(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_name()));
            validationSupport.registerValidator(debitor.getPriceTF(), Validator.createEmptyValidator(Bundle.shipment_details_validator_message_client_price()));
          
          
        });
    }

    public ReadOnlyBooleanProperty invalidProperty() {
        return validationSupport.invalidProperty();
    }

    private ObservableLocation newObservableLocation() throws NoSuchMethodException {
        String locationId = UUID.randomUUID().toString();
        Location location = Location.buildLocation()
                .address("")
                .city("")
                .country("")
                .id(locationId)
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .name("")
                .postalcode("")
                .build();

        ObservableLocation oLocation = new ObservableLocation(location);
        return oLocation;
    }

}
