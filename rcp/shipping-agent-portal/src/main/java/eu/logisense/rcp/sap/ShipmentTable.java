/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.sap;

import eu.logisense.api.Status;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.data.Savable;
import eu.logisense.rcp.util.LocalDateTimeTableCell;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.openide.util.NbBundle.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kenrik
 * @param <OS>
 */
@Messages({
    "shipped.units.label=shipped units",
    "total.sell.price.label=total sell price",
    "total.buy.price.label=total buy price",
    "creditor.label=creditor",
    "creditor.contact.name.label=email",
    "creditor.contact.phone.number.label=phone",
    "status header label for shipmentTable=status",
    "external id header label for shipmentTable=external id"
})
public class ShipmentTable<OS extends ObservableShipment> extends TableView {

    private static final Logger log = LoggerFactory.getLogger(ShipmentTable.class);

    private final double standardHeight = 540.0;

    private TableColumn<ObservableShipment, String> nameCol;
//    private TableColumn<ObservableShipment, String> statusCol;
    private TableColumn<ObservableShipment, Status> shipmentStatusCol;

    private TableColumn<ObservableShipment, LocalDateTime> loadDateCol;
    private TableColumn<ObservableShipment, LocalDateTime> loadTimeFrameFromCol;
    private TableColumn<ObservableShipment, LocalDateTime> loadTimeFrameToCol;
    private TableColumn<ObservableShipment, LocalDateTime> unloadDateCol;
    private TableColumn<ObservableShipment, LocalDateTime> unloadTimeFrameFromCol;
    private TableColumn<ObservableShipment, LocalDateTime> unloadTimeFrameToCol;

    private TableColumn<ObservableShipment, String> customerNameCol;
    private TableColumn<ObservableShipment, String> externalIdCol;
    private TableColumn<ObservableShipment, String> orderReferenceCol;

    private TableColumn<ObservableShipment, String> loadAddressCol;
    private TableColumn<ObservableShipment, String> loadPostalCodeCol;
    private TableColumn<ObservableShipment, String> loadCityCol;
    private TableColumn<ObservableShipment, String> loadCountryCol;

    private TableColumn<ObservableShipment, String> unloadAddressCol;
    private TableColumn<ObservableShipment, String> unloadPostalCodeCol;
    private TableColumn<ObservableShipment, String> unloadCityCol;
    private TableColumn<ObservableShipment, String> unloadCountryCol;

    private TableColumn<ObservableShipment, Double> weightCol;
    private TableColumn<ObservableShipment, Double> sizeCol;
    private TableColumn<ObservableShipment, Integer> shippedUnitsCol;

    private TableColumn<ObservableShipment, Double> totalSellPriceCol;
    private TableColumn<ObservableShipment, Double> totalBuyPriceCol;
    private TableColumn<ObservableShipment, String> creditorNameCol;
    private TableColumn<ObservableShipment, String> creditorEmailCol;
    private TableColumn<ObservableShipment, String> creditorPhoneNumberCol;

    private TableColumn<ObservableShipment, String> noteCol;

    public ShipmentTable(ObservableList<ObservableShipment> list) {
        super(list);
        this.setPrefHeight(standardHeight);
        this.setEditable(true);
        this.setTableMenuButtonVisible(true);

        setupTable();
    }

    private void setupTable() {
        nameCol = new TableColumn(Bundle.name_label());

        nameCol.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        //
        // not needed at the moment, so set invisible
        //
        nameCol.setVisible(false);

//        statusCol = new TableColumn(Bundle.status_header_label_for_shipmentTable());
//        statusCol.setCellValueFactory(cellData -> cellData.getValue().statusProperty());
        shipmentStatusCol = new TableColumn(Bundle.status_header_label_for_shipmentTable());
        shipmentStatusCol.setCellValueFactory(cellData -> cellData.getValue().shipmentStatusProperty());

        loadDateCol = new TableColumn(Bundle.load_label() + " " + Bundle.date_label());
        loadTimeFrameFromCol = new TableColumn(Bundle.load_label() + " " + Bundle.from_label());
        loadTimeFrameToCol = new TableColumn(Bundle.load_label() + " " + Bundle.until_label());

        loadDateCol.setCellValueFactory(cellData -> cellData.getValue().loadTimeFrameFromProperty());
        loadDateCol.setCellFactory(column -> {
            return new LocalDateTimeTableCell<>("dd/MM/yy");
        });

        loadTimeFrameFromCol.setCellValueFactory(cellData -> cellData.getValue().loadTimeFrameFromProperty());
        loadTimeFrameFromCol.setCellFactory(column -> {
            return new LocalDateTimeTableCell<>("HH:mm:ss");
        });

        loadTimeFrameToCol.setCellValueFactory(cellData -> cellData.getValue().loadTimeFrameToProperty());
        loadTimeFrameToCol.setCellFactory(column -> {
            return new LocalDateTimeTableCell<>("HH:mm:ss");
        });

        unloadDateCol = new TableColumn(Bundle.unload_label() + " " + Bundle.date_label());
        unloadTimeFrameFromCol = new TableColumn(Bundle.unload_label() + " " + Bundle.from_label());
        unloadTimeFrameToCol = new TableColumn(Bundle.unload_label() + " " + Bundle.until_label());

        unloadDateCol.setCellValueFactory(cellData -> cellData.getValue().unloadTimeFrameFromProperty());
        unloadDateCol.setCellFactory(column -> {
            return new LocalDateTimeTableCell<>("dd/MM/yy");
        });

        unloadTimeFrameFromCol.setCellValueFactory(cellData -> cellData.getValue().unloadTimeFrameFromProperty());
        unloadTimeFrameFromCol.setCellFactory(column -> {
            return new LocalDateTimeTableCell<>("HH:mm:ss");
        });

        unloadTimeFrameToCol.setCellValueFactory(cellData -> cellData.getValue().unloadTimeFrameToProperty());
        unloadTimeFrameToCol.setCellFactory(column -> {
            return new LocalDateTimeTableCell<>("HH:mm:ss");
        });

        loadAddressCol = new TableColumn(Bundle.address_label());
        loadPostalCodeCol = new TableColumn(Bundle.postal_code_label());
        loadCityCol = new TableColumn(Bundle.city_label());
        loadCountryCol = new TableColumn(Bundle.country_label());

        loadAddressCol.setCellValueFactory(cellData -> cellData.getValue().loadAddressProperty());
        loadPostalCodeCol.setCellValueFactory(cellData -> cellData.getValue().loadPostalCodeProperty());
        loadCityCol.setCellValueFactory(cellData -> cellData.getValue().loadCityProperty());
        loadCountryCol.setCellValueFactory(cellData -> cellData.getValue().loadCountryProperty());

        unloadAddressCol = new TableColumn(Bundle.address_label());
        unloadPostalCodeCol = new TableColumn(Bundle.postal_code_label());
        unloadCityCol = new TableColumn(Bundle.city_label());
        unloadCountryCol = new TableColumn(Bundle.country_label());

        unloadAddressCol.setCellValueFactory(cellData -> cellData.getValue().unloadAddressProperty());
        unloadPostalCodeCol.setCellValueFactory(cellData -> cellData.getValue().unloadPostalCodeProperty());
        unloadCityCol.setCellValueFactory(cellData -> cellData.getValue().unloadCityProperty());
        unloadCountryCol.setCellValueFactory(cellData -> cellData.getValue().unloadCountryProperty());

        customerNameCol = new TableColumn<>(Bundle.customer_label());
        externalIdCol = new TableColumn<>(Bundle.external_id_header_label_for_shipmentTable());
        orderReferenceCol = new TableColumn<>(Bundle.order_reference_label());

        customerNameCol.setCellValueFactory(cellData -> cellData.getValue().ordererNameProperty());
        externalIdCol.setCellValueFactory(cellData -> cellData.getValue().externalIdProperty());
        orderReferenceCol.setCellValueFactory(cellData -> cellData.getValue().orderReferenceProperty());

        weightCol = new TableColumn(Bundle.weight_label());
        sizeCol = new TableColumn(Bundle.size_label());
        shippedUnitsCol = new TableColumn(Bundle.shipped_units_label());

        weightCol.setCellValueFactory(cellData -> cellData.getValue().weightProperty().asObject());
        sizeCol.setCellValueFactory(cellData -> cellData.getValue().volumeProperty().asObject());
        shippedUnitsCol.setCellValueFactory(cellData -> cellData.getValue().shippedUnitsProperty());

        totalSellPriceCol = new TableColumn<>(Bundle.total_sell_price_label());
        totalBuyPriceCol = new TableColumn<>(Bundle.total_buy_price_label());
        creditorNameCol = new TableColumn<>(Bundle.creditor_label());
        creditorEmailCol = new TableColumn<>(Bundle.creditor_contact_name_label());
        creditorPhoneNumberCol = new TableColumn<>(Bundle.creditor_contact_phone_number_label());

        totalSellPriceCol.setCellValueFactory(cellData -> cellData.getValue().sellPriceProperty());
        totalBuyPriceCol.setCellValueFactory(cellData -> cellData.getValue().buyPriceProperty().asObject());
        creditorNameCol.setCellValueFactory(cellData -> cellData.getValue().creditorNameProperty());
        creditorEmailCol.setCellValueFactory(cellData -> cellData.getValue().creditorEmailProperty());
        creditorPhoneNumberCol.setCellValueFactory(cellData -> cellData.getValue().creditorPhoneProperty());

        //
        // TODO: retrieve and bind creditor properties
        //
        noteCol = new TableColumn(Bundle.note_label());

        noteCol.setCellValueFactory(cellData -> cellData.getValue().noteProperty());

        getColumns().addAll(/*statusCol, */shipmentStatusCol, loadDateCol, loadTimeFrameFromCol, loadTimeFrameToCol,
                unloadDateCol, unloadTimeFrameFromCol, unloadTimeFrameToCol, customerNameCol,
                externalIdCol,
                orderReferenceCol, loadAddressCol, loadPostalCodeCol, loadCityCol,
                loadCountryCol, unloadAddressCol, unloadPostalCodeCol, unloadCityCol,
                unloadCountryCol, weightCol, sizeCol, shippedUnitsCol, totalSellPriceCol,
                totalBuyPriceCol, creditorNameCol, creditorEmailCol,
                creditorPhoneNumberCol, noteCol);

        getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        //
        // open up details when row is clicked twice
        //
        this.setOnMousePressed((MouseEvent event) -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                System.out.println("selected " + getSelectionModel().getSelectedItem());
                ShipmentDetailPopup sdp = new ShipmentDetailPopup();
                if (getSelectionModel().getSelectedItem() instanceof ObservableShipment) {
                    sdp.showUpdateShipmentDetail((ObservableShipment) this.getSelectionModel().getSelectedItem());
                } else {
                    sdp.showNewShipmentDetail(this);
                }
            }
        });

        //
        // set all columns min width and editable
        //
        getColumns().stream().map((tc) -> {
            ((TableColumn) tc).setMinWidth(100.0);
            ((TableColumn) tc).setEditable(true);
            return tc;
            //
            // set nested columns min width
            //
        }).filter((tc) -> (!((TableColumn) tc).getColumns().isEmpty())).forEach((tc) -> {
            ((TableColumn) tc).getColumns().stream().forEach((tcc) -> {
                ((TableColumn) tcc).setMinWidth(100.0);
            });
        });

        setRowFactory((tableview) -> {
            TableRow<ObservableShipment> row = new TableRow<>();
            ChangeListener<String> changeListener = (obs, oldStatus, newStatus) -> {
                Savable.ClientStatus status = Savable.ClientStatus.from(newStatus);
                setPseudoClass(row, status);
            };

            row.itemProperty().addListener((obs, previousShipment, currentShipment) -> {
                if (previousShipment != null) {
                    previousShipment.statusProperty().removeListener(changeListener);
                }
                if (currentShipment != null) {
                    currentShipment.statusProperty().addListener(changeListener);
                    Savable.ClientStatus status = Savable.ClientStatus.from(currentShipment.statusProperty().getValue());
                    setPseudoClass(row, status);
                }
            });
            return row;
        });
    }

    private void setPseudoClass(TableRow<ObservableShipment> row, Status status) {
        PseudoClass statusNew = PseudoClass.getPseudoClass("statusNew");
        PseudoClass statusUnsaved = PseudoClass.getPseudoClass("statusUnsaved");
        PseudoClass statusPlanned = PseudoClass.getPseudoClass("statusPlanned");
        PseudoClass statusLoaded = PseudoClass.getPseudoClass("statusLoaded");
        PseudoClass statusUnloaded = PseudoClass.getPseudoClass("statusUnloaded");
        
        log.debug("shipment status {}", status);

        row.pseudoClassStateChanged(statusNew, Savable.ClientStatus.NEW.equals(status));
        row.pseudoClassStateChanged(statusUnsaved, Savable.ClientStatus.UNSAVED.equals(status) || Savable.ClientStatus.CHANGED.equals(status));
        row.pseudoClassStateChanged(statusPlanned, Savable.ClientStatus.PLANNED.equals(status));
        row.pseudoClassStateChanged(statusLoaded, Savable.ClientStatus.LOADED.equals(status) || Savable.ClientStatus.EN_ROUTE.equals(status));
        row.pseudoClassStateChanged(statusUnloaded, Savable.ClientStatus.UNLOADED.equals(status));
    }

}
