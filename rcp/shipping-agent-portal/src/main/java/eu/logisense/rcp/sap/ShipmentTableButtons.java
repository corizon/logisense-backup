/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.sap;

import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.data.ShipmentManagerImpl;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import org.openide.util.NbBundle;

/**
 *
 * @author kenrik
 */
@NbBundle.Messages({
    "delete.label=delete",
    "new.label=new"
})
public class ShipmentTableButtons extends VBox {

    Button createButton;
    Button deleteButton;

    public ShipmentTableButtons(ShipmentTable table) {
        initComponents(table);
    }

    private void initComponents(ShipmentTable table) {
        createButton = new Button();
        createButton.setTooltip(new Tooltip(Bundle.new_label()));
        createButton.setPrefHeight(64.0);
        createButton.setPrefWidth(64.0);
        createButton.setStyle("-fx-background-image: url('/eu/logisense/rcp/sap/shipment_add64.png')");        
        
        
        deleteButton = new Button();
        deleteButton.setTooltip(new Tooltip(Bundle.delete_label()));
        deleteButton.setPrefHeight(64.0);
        deleteButton.setPrefWidth(64.0);
        deleteButton.setStyle("-fx-background-image: url('/eu/logisense/rcp/sap/shipment_remove64.png')");                
        
        this.getChildren().addAll(createButton, deleteButton);
        this.setAlignment(Pos.TOP_LEFT);

        //
        // disable button when nothing is selecred
        //
        deleteButton.disableProperty().bind(Bindings.isEmpty(table.getSelectionModel().getSelectedItems()));

        //
        // add new empty order to list
        //
        createButton.setOnAction(((ActionEvent e) -> {
            ShipmentDetailPopup sdp = new ShipmentDetailPopup();
            sdp.showNewShipmentDetail(table);
        }));

        //
        // delete selected item
        //
        deleteButton.setOnAction(((ActionEvent e) -> {
            table.getSelectionModel().getSelectedItems().stream().forEach((selected) -> {
                ShipmentManagerImpl.getInstance().removeShipment((ObservableShipment) selected);
            });

        }));
    }

}
