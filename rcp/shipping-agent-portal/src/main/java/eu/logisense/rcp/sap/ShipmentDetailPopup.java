/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.sap;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Client;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.Origin;
import eu.logisense.api.Shipment;
import eu.logisense.api.UnloadLocation;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.data.Savable;
import eu.logisense.rcp.data.ShipmentManagerImpl;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.openide.util.NbBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
@NbBundle.Messages({
    "create.shipment=create shipment",
    "update.shipment=update shipment"
})
public class ShipmentDetailPopup extends Alert {

    private static final Logger log = LoggerFactory.getLogger(ShipmentDetailPopup.class);

    private static final String POPUP_WIDTH_STYLE = "-fx-pref-width:1050px;";
    private static final Double INSET = 40.0;

    private ScrollPane scrollPane = new ScrollPane();

    private ShipmentDetailPopup(AlertType alertType) {
        super(alertType);
    }

    public ShipmentDetailPopup() {
        this(AlertType.CONFIRMATION);

        scrollPane.hbarPolicyProperty().set(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.vbarPolicyProperty().set(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    }

    public void showNewShipmentDetail(TableView table) {

        try {

            ObservableShipment shipment = newEmptyShipment();

            //
            // open popup
            //
            this.setTitle(Bundle.create_shipment());
            this.setResizable(true);
            //
            // normal way of setting minWidth not working
            //
            this.getDialogPane().setStyle(POPUP_WIDTH_STYLE);

            //
            // open details for new ObservableShipment
            //
            ShipmentDetails shipmentDetails = new ShipmentDetails(shipment);
            shipmentDetails.prefWidthProperty().bind(scrollPane.widthProperty().subtract(INSET));
            scrollPane.setContent(shipmentDetails);
            this.getDialogPane().setContent(scrollPane);

            final Button btOk = (Button) this.getDialogPane().lookupButton(ButtonType.OK);

            // set action event filter for custom dialog validation and storing the result 
            // => don't close the dialog on errors 
            btOk.addEventFilter(ActionEvent.ACTION, (event) -> {
                if (!isValidShipment(shipment.getShipment())) {
                    log.debug("shipment not valid");
                    event.consume();
                } else {
                    log.debug("shipment valid");

                }
            });

            Optional<ButtonType> result = this.showAndWait();

            //
            // only add to table if 'ok' pressed
            //
            if (result.get() == ButtonType.OK) {
                shipment.statusProperty().setValue(Savable.ClientStatus.UNSAVED.name());
                ShipmentManagerImpl.getInstance().newShipment(shipment);
                table.getSelectionModel().select(table.getItems().indexOf(shipment));

            } else {
                this.close();
            }

        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        }
    }

    public void showUpdateShipmentDetail(ObservableShipment shipment) {
        //
        // open popup
        //
        this.setTitle(Bundle.update_shipment());
        //
        // remove cancel button
        //
        this.getDialogPane().getButtonTypes().remove(ButtonType.CANCEL);
        this.setResizable(true);
        //
        // normal way of setting minWidth not working
        //
        this.getDialogPane().setStyle(POPUP_WIDTH_STYLE);

        //
        // open details for existing ObservableShipment
        //
        ShipmentDetails shipmentDetails = new ShipmentDetails(shipment);
        shipmentDetails.prefWidthProperty().bind(scrollPane.widthProperty().subtract(INSET));
        scrollPane.setContent(shipmentDetails);
        this.getDialogPane().setContent(scrollPane);

        final Button btOk = (Button) this.getDialogPane().lookupButton(ButtonType.OK);

            // set action event filter for custom dialog validation and storing the result 
        // => don't close the dialog on errors 
        btOk.addEventFilter(ActionEvent.ACTION, (event) -> {
            if (!isValidShipment(shipment.getShipment())) {
                log.debug("shipment  not valid");
                event.consume();
            } else {
                log.debug("shipment valid");

            }
        });

        this.showAndWait();
    }

    private ObservableShipment newEmptyShipment() throws NoSuchMethodException {
        LocalTime eightAM = LocalTime.of(8, 0, 0, 0);
        LocalTime fivePM = LocalTime.of(17, 0, 0, 0);

        LoadLocation emptyFrom = LoadLocation.builder()
                .address("")
                .city("")
                .country("")
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .name("")
                .postalcode("")
                .timeFrameFrom(LocalDateTime.now().with(eightAM))
                .timeFrameTo(LocalDateTime.now().with(fivePM))
                .id(UUID.randomUUID().toString())
                .build();

        UnloadLocation emptyTo = UnloadLocation.builder()
                .address("")
                .city("")
                .country("")
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .name("")
                .postalcode("")
                .timeFrameFrom(LocalDateTime.now().with(eightAM))
                .timeFrameTo(LocalDateTime.now().with(fivePM))
                .build();

        List<OrderLine> lines = new ArrayList<>();

        Shipment emptyShipment = Shipment.builder()
                .deadline(emptyTo.getTimeFrameTo())
                .loadLocation(emptyFrom)
                .id(UUID.randomUUID().toString())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .currency("EUR")
                                .build())
                        .destination(Destination.builder()
                                .build())
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .name("")
                        .orderedBy(Client.builder()
                                .status(ApprovalStatus.NEW)
                                .name("")
                                .build())
                        .origin(Origin.builder()
                                .build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .status(ApprovalStatus.NEW)
                .unloadLocation(emptyTo)
                .senderIsLoad(Boolean.TRUE)
                .destinationIsUnload(Boolean.TRUE)
                .build();

        ObservableShipment newOrder = new ObservableShipment(emptyShipment, Savable.ClientStatus.ENTRY);

        return newOrder;
    }

    protected boolean isValidShipment(Shipment shipment) {

        log.debug("validate name {}" + shipment.getName());
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        // Create a bean validator and check for issues.
        Set<ConstraintViolation<Object>> violations = validator.validate(shipment);

        if (!violations.isEmpty()) {
            String message = "";
            for (ConstraintViolation violation : violations) {
                if (violation.getConstraintDescriptor() != null && violation.getConstraintDescriptor().getMessageTemplate() != null) {
                    message += violation.getConstraintDescriptor().getMessageTemplate() + "\n";
                }
            }

            log.error("message {}" + message);

        }

        log.debug("validate {}" + violations.isEmpty());
        return violations.isEmpty();
    }
}
