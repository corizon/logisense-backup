/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.sap;

import eu.logisense.rcp.util.DoubleTextFieldCellFactory;
import eu.logisense.rcp.util.StringTextFieldCellFactory;
import eu.logisense.rcp.util.IntegerTextFieldCellFactory;
import eu.logisense.api.OrderLineType;
import eu.logisense.rcp.data.ObservableOrderLine;
import eu.logisense.rcp.util.CheckBoxCellFactory;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
@Messages({
    "description column in orderLinesTable=description",
    "remarks column in orderLinesTable=remarks",
    "amount column in orderLinesTable=amount",
    "type column in orderLinesTable=type",
    "swap pallets column in orderLinesTable=swap pallets",
    "length column in orderLinesTable=length in m",
    "width column in orderLinesTable=width in m",
    "height column in orderLinesTable=height in m",
    "weight column in orderLinesTable=weight in kg",
    "stackable column in orderLinesTable=stackable",
    "pod amount column in orderLinesTable=pod amount",
    "pod currency column in orderLinesTable=pod currency"

})
public class OrderLinesTable<OOL extends ObservableOrderLine> extends TableView {

    private static final double COLUMN_WIDTH = 100.0;

    private TableColumn<ObservableOrderLine, String> descriptionCol;
    private TableColumn<ObservableOrderLine, String> remarksCol;
    private TableColumn<ObservableOrderLine, Integer> amountCol;
    private TableColumn<ObservableOrderLine, OrderLineType> typeCol;
    private TableColumn<ObservableOrderLine, Boolean> swapPalletsCol;
    private TableColumn<ObservableOrderLine, Double> lengthCol;
    private TableColumn<ObservableOrderLine, Double> widthCol;
    private TableColumn<ObservableOrderLine, Double> heightCol;
    private TableColumn<ObservableOrderLine, Double> weightCol;
    private TableColumn<ObservableOrderLine, Boolean> stackableCol;
    private TableColumn<ObservableOrderLine, Double> podAmountCol;
    private TableColumn<ObservableOrderLine, String> podCurrencyCol;

    public OrderLinesTable(ObservableList<ObservableOrderLine> list) {
        super(list);
        this.setEditable(true);
        this.setTableMenuButtonVisible(true);

        setUpTable();
    }

    private void setUpTable() {

        descriptionCol = new TableColumn(Bundle.description_column_in_orderLinesTable());
        remarksCol = new TableColumn(Bundle.remarks_column_in_orderLinesTable());
        amountCol = new TableColumn(Bundle.amount_column_in_orderLinesTable());
        typeCol = new TableColumn(Bundle.type_column_in_orderLinesTable());
        swapPalletsCol = new TableColumn(Bundle.swap_pallets_column_in_orderLinesTable());
        lengthCol = new TableColumn(Bundle.length_column_in_orderLinesTable());
        widthCol = new TableColumn(Bundle.width_column_in_orderLinesTable());
        heightCol = new TableColumn(Bundle.height_column_in_orderLinesTable());
        weightCol = new TableColumn(Bundle.weight_column_in_orderLinesTable());
        stackableCol = new TableColumn(Bundle.stackable_column_in_orderLinesTable());
        podAmountCol = new TableColumn(Bundle.pod_amount_column_in_orderLinesTable());
        podCurrencyCol = new TableColumn(Bundle.pod_currency_column_in_orderLinesTable());

        descriptionCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, String> data) -> data.getValue().descriptionProperty());
        remarksCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, String> data) -> data.getValue().remarksProperty());
        amountCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, Integer> data) -> data.getValue().amountProperty());
        typeCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, OrderLineType> data) -> data.getValue().typeProperty());
        swapPalletsCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, Boolean> data) -> data.getValue().swapPalletsProperty());
        lengthCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, Double> data) -> data.getValue().lengthProperty());
        widthCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, Double> data) -> data.getValue().widthProperty());
        heightCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, Double> data) -> data.getValue().heightProperty());
        weightCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, Double> data) -> data.getValue().weightProperty());
        stackableCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, Boolean> data) -> data.getValue().stackableProperty());
        podAmountCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, Double> data) -> data.getValue().podAmountProperty());
        podCurrencyCol.setCellValueFactory((TableColumn.CellDataFeatures<ObservableOrderLine, String> data) -> data.getValue().podCurrencyProperty());

        //
        // set tablecells factories to be able to edit them in table itself
        //
        descriptionCol.setCellFactory(new StringTextFieldCellFactory(true));
        remarksCol.setCellFactory(new StringTextFieldCellFactory(false));
        amountCol.setCellFactory(new IntegerTextFieldCellFactory(true));
        typeCol.setCellFactory(ComboBoxTableCell.forTableColumn(OrderLineType.values()));
        swapPalletsCol.setCellFactory(new CheckBoxCellFactory());
        lengthCol.setCellFactory(new DoubleTextFieldCellFactory(true));
        widthCol.setCellFactory(new DoubleTextFieldCellFactory(true));
        heightCol.setCellFactory(new DoubleTextFieldCellFactory(true));
        weightCol.setCellFactory(new DoubleTextFieldCellFactory(true));
        stackableCol.setCellFactory(new CheckBoxCellFactory());
        podAmountCol.setCellFactory(new DoubleTextFieldCellFactory(false));
        podCurrencyCol.setCellFactory(new StringTextFieldCellFactory(false));

        getColumns().addAll(descriptionCol, remarksCol, amountCol, typeCol,
                swapPalletsCol, lengthCol, widthCol, heightCol, weightCol,
                stackableCol, podAmountCol, podCurrencyCol);

        getColumns().stream().forEach((tc) -> {
            ((TableColumn) tc).setPrefWidth(COLUMN_WIDTH);
        });

        typeCol.setOnEditCommit((TableColumn.CellEditEvent<ObservableOrderLine, OrderLineType> event) -> {
            ObservableOrderLine line = ((ObservableOrderLine) event.getTableView()
                    .getItems().get(event.getTablePosition().getRow()));

            line.typeProperty().setValue(event.getNewValue());
            //
            // set width/length according to value from OrderLineType selected
            //
            line.widthProperty().setValue(event.getNewValue().width);
            line.lengthProperty().setValue(event.getNewValue().length);

            //
            // TODO: disable length/width when EURO_PALLET or BLOCK_PALLEt selected
            // regular disable isn't working with current cellfactories
            //
            // TODO: disable swapPallets when EURO_PALLET not selected
            //
            if (event.getNewValue() == OrderLineType.EURO_PALLET) {
            } else {
            }

        });
    }
}
