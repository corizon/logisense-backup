/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.clients;

import eu.logisense.rcp.data.ObservableClient;
import eu.logisense.rcp.data.ObservableLocation;
import java.util.Iterator;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;

/**
 *
 * @author kenrik
 */
public class LocationsAccordion extends Accordion {

    private ObjectProperty<ObservableClient> selectedClientProperty = new SimpleObjectProperty();
    private BooleanProperty isClientSelected = new SimpleBooleanProperty(Boolean.FALSE);

    public LocationsAccordion() {

    }

    public void addLocationTitledPane(ObservableLocation location) {
        ClientDetails details = new ClientDetails(location);
        TitledPane titledPane = new TitledPane(location.getLocation().getName(), details);

        getPanes().add(titledPane);
        titledPane.setExpanded(true);

    }

    public void removeLocationTitledPane() {
        ClientDetails details = (ClientDetails) this.getExpandedPane().getContent();
        ObservableClient client = this.getSelectedClient();

        Iterator<ObservableLocation> iterator = client.getLocations().iterator();

        //
        // if found, remove from clients location list
        //
        while (iterator.hasNext()) {
            ObservableLocation location = iterator.next();
            if (location.getLocation().getName().equals(details.getLocationName())) {
                iterator.remove();
            }
        }

        updateLocationTitledPanesForClient(client);

    }

    public void clear() {
        getPanes().clear();
    }

    public void updateLocationTitledPanesForClient(ObservableClient client) {
        selectedClientProperty.setValue(client);

        this.clear();
        
        //
        // prevent nullpointer when location titled pane is shown and client is
        // removed from sortedlist due to filtering
        //
        if (client != null && !client.getLocations().isEmpty()) {
            client.getLocations().stream().forEach(ol -> {
                this.addLocationTitledPane(ol);
            });
        }
    }

    public ObjectProperty<ObservableClient> selectedClientProperty() {
        return selectedClientProperty;
    }

    public ObservableClient getSelectedClient() {
        return selectedClientProperty.getValue();
    }

}
