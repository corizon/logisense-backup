/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.clients;

import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableClient;
import eu.logisense.rcp.data.ObservableLocation;
import eu.logisense.rcp.data.Savable;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.css.PseudoClass;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.openide.util.NbBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@NbBundle.Messages({
    "client label=client",
    "name label=name",
    "city label=city",
    "postal code label=postal code",
    "address label=address",
    "clients table phone column=phone",
    "clients table email column=email",
    "country label=country",
    "external id label=external id"
})
public class ClientsTableView<OC extends ObservableClient> extends TableView {

    private final double standardHeight = 540.0;
    private static final Logger log = LoggerFactory.getLogger(ClientsTableView.class);

    private TableColumn<ObservableLocation, String> locationNameCol;
    private TableColumn<ObservableLocation, String> addressCol;
    private TableColumn<ObservableLocation, String> postalCodeCol;
    private TableColumn<ObservableLocation, String> cityCol;
    private TableColumn<ObservableLocation, String> countryCol;
    private TableColumn<ObservableLocation, String> phoneCol;
    private TableColumn<ObservableLocation, String> emailCol;
    private TableColumn<ObservableLocation, String> externalIdCol;

    private LocationsAccordion accordion;

    public ClientsTableView(LocationsAccordion accordion) {
        this.setPrefHeight(standardHeight);
        this.setEditable(true);
        this.setTableMenuButtonVisible(true);

        this.accordion = accordion;

        setupTable();
    }

    private void setupTable() {

        //
        // allow just one selection at any time
        //
        getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            accordion.updateLocationTitledPanesForClient((ObservableClient) newValue);
        });

        locationNameCol = new TableColumn(Bundle.name_label());
        locationNameCol.setCellValueFactory(cellData -> cellData.getValue().nameProperty());

        addressCol = new TableColumn(Bundle.address_label());
        addressCol.setCellValueFactory(cellData -> cellData.getValue().addressProperty());

        postalCodeCol = new TableColumn(Bundle.postal_code_label());
        postalCodeCol.setCellValueFactory(cellData -> cellData.getValue().postalcodeProperty());

        cityCol = new TableColumn(Bundle.city_label());
        cityCol.setCellValueFactory(cellData -> cellData.getValue().cityProperty());

        countryCol = new TableColumn(Bundle.country_label());
        countryCol.setCellValueFactory(cellData -> cellData.getValue().countryProperty());

        phoneCol = new TableColumn(Bundle.clients_table_phone_column());
        phoneCol.setCellValueFactory(cellData -> cellData.getValue().phoneProperty());

        emailCol = new TableColumn(Bundle.clients_table_email_column());
        emailCol.setCellValueFactory(cellData -> cellData.getValue().emailProperty());

        externalIdCol = new TableColumn(Bundle.external_id_label());
        externalIdCol.setCellValueFactory(cellData -> cellData.getValue().externalIdProperty());

        getColumns().addAll(locationNameCol, addressCol, postalCodeCol, cityCol, countryCol, phoneCol, emailCol, externalIdCol);

        //
        // open up details when row is clicked twice
        //
        this.setOnMousePressed((MouseEvent event) -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                if (this.getSelectionModel().getSelectedItem() != null) {
                    ClientDetailsPopup sdp = new ClientDetailsPopup();
                    sdp.showUpdateClientDetail((ObservableClient) this.getSelectionModel().getSelectedItem());
                }
            }
        });

        //
        // set all columns min width and editable
        //
        getColumns().stream().map((ttc) -> {
            ((TableColumn) ttc).setMinWidth(100.0);
            ((TableColumn) ttc).setEditable(true);
            return ttc;
            //
            // set nested columns min width
            //
        }).filter((ttc) -> (!((TableColumn) ttc).getColumns().isEmpty())).forEach((ttc) -> {
            ((TableColumn) ttc).getColumns().stream().forEach((ttcc) -> {
                ((TableColumn) ttcc).setMinWidth(100.0);
            });
        });

        PseudoClass statusNew = PseudoClass.getPseudoClass("statusNew");
        PseudoClass statusUnsaved = PseudoClass.getPseudoClass("statusUnsaved");
        PseudoClass statusRejected = PseudoClass.getPseudoClass("statusRejected");
        PseudoClass statusApproved = PseudoClass.getPseudoClass("statusApproved");

        setRowFactory((tableview) -> {
            TableRow<ObservableLocation> row = new TableRow<>();
            ChangeListener<String> changeListener = (obs, oldStatus, newStatus) -> {
                log.debug("client status change from {} to {}", oldStatus, newStatus);
                Savable.ClientStatus status = Savable.ClientStatus.from(newStatus);
                row.pseudoClassStateChanged(statusNew, Savable.ClientStatus.NEW.equals(status));
                row.pseudoClassStateChanged(statusRejected, Savable.ClientStatus.REJECTED.equals(status));
                row.pseudoClassStateChanged(statusApproved, Savable.ClientStatus.APPROVED.equals(status));
                row.pseudoClassStateChanged(statusUnsaved, Savable.ClientStatus.CHANGED.equals(status) || Savable.ClientStatus.UNSAVED.equals(status));
            };

            row.itemProperty().addListener((obs, previousLocation, currentLocation) -> {
                if (previousLocation != null) {
                    if (previousLocation.getLocation() != null) {
                        log.debug("previousLocation with id {}", previousLocation.getLocation().getId());
                    }
                    previousLocation.statusProperty().removeListener(changeListener);
                }
                if (currentLocation != null) {
                    if (currentLocation.getLocation() != null) {
                        log.debug("currentLocation with id {}", currentLocation.getLocation().getId());
                    }

                    currentLocation.statusProperty().addListener(changeListener);
                    Savable.ClientStatus status = Savable.ClientStatus.from(currentLocation.statusProperty().getValue());
                    log.debug("Savable.ClientStatus.from(currentLocation.statusProperty().getValue() {}", Savable.ClientStatus.from(currentLocation.statusProperty().getValue()));
                    log.debug("client status {}", status);
                    row.pseudoClassStateChanged(statusNew, Savable.ClientStatus.NEW.equals(status));
                    row.pseudoClassStateChanged(statusRejected, Savable.ClientStatus.REJECTED.equals(status));
                    row.pseudoClassStateChanged(statusApproved, Savable.ClientStatus.APPROVED.equals(status));
                    row.pseudoClassStateChanged(statusUnsaved, Savable.ClientStatus.CHANGED.equals(status) || Savable.ClientStatus.UNSAVED.equals(status));
                } else {
                    row.pseudoClassStateChanged(statusNew, false);
                    row.pseudoClassStateChanged(statusRejected, false);
                    row.pseudoClassStateChanged(statusApproved, false);
                    row.pseudoClassStateChanged(statusUnsaved, false);
                }
            });
            return row;
        });
    }

}
