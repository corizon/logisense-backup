/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.clients;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import org.openide.util.NbBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@NbBundle.Messages({
    "clientsTableButtons tooltip create client=create client"
})
public class ClientsTableButtons extends VBox {
    
    private static final double BUTTON_WIDTH = 64.0;

    private static final Logger log = LoggerFactory.getLogger(ClientsTableButtons.class);

    private Button createClientButton;
//    private Button deleteClientButton;

    public ClientsTableButtons(ClientsTableView table) {
        initComponents(table);
        
        this.setMinWidth(BUTTON_WIDTH);
    }

    private void initComponents(ClientsTableView table) {
        createClientButton = new Button();
        createClientButton.setTooltip(new Tooltip(Bundle.clientsTableButtons_tooltip_create_client()));
        createClientButton.setPrefHeight(BUTTON_WIDTH);
        createClientButton.setPrefWidth(BUTTON_WIDTH);
        createClientButton.setStyle("-fx-background-image: url('/eu/logisense/rcp/clients/client_add64.png')");

//        deleteClientButton = new Button();
//        deleteClientButton.setTooltip(new Tooltip(Bundle.delete_label() + " " + Bundle.client_label()));
//        deleteClientButton.setPrefHeight(64.0);
//        deleteClientButton.setPrefWidth(64.0);
//        deleteClientButton.setStyle("-fx-background-image: url('/eu/logisense/rcp/clients/client_remove64.png')");

        this.getChildren().addAll(createClientButton/*, deleteClientButton*/);
        this.setAlignment(Pos.TOP_LEFT);

        //
        // add new client to list
        //
        createClientButton.setOnAction(((ActionEvent e) -> {
            ClientDetailsPopup sdp = new ClientDetailsPopup();
            sdp.showNewClientDetail();
        }));
    }
}
