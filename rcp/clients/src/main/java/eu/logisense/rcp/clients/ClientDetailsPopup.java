/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.clients;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Client;
import eu.logisense.api.Location;
import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableClient;
import eu.logisense.rcp.data.ObservableLocation;
import eu.logisense.rcp.data.Savable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javafx.scene.text.Text;
import javafx.scene.paint.Color;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@NbBundle.Messages({
    "create.client=create client",
    "create.location=create location",
    "update.client=update client",
    "empty form during create.client=Location can not be empty"
})
public class ClientDetailsPopup extends Alert {

    private static final Logger log = LoggerFactory.getLogger(ClientDetailsPopup.class);

    private static final String POPUP_WIDTH_STYLE = "-fx-pref-width:600px;";

    private ObservableLocation observableLocation;

    private final Text validation_message = new Text();

    private ClientDetailsPopup(AlertType alertType) {
        super(alertType);
    }

    public ClientDetailsPopup() {
        this(AlertType.CONFIRMATION);
    }

    public void showNewClientDetail() {

        try {

            ObservableClient client = newClient();

            //
            // open popup
            //
            this.setTitle(Bundle.create_client());
            this.setResizable(true);
            //
            // normal way of setting minWidth not working
            //
            this.getDialogPane().setStyle(POPUP_WIDTH_STYLE);

            //
            // open details for new ObservableShipment
            //
            this.getDialogPane().setContent(new VBox(validation_message, new ClientDetails(client, false)));

            final Button btOk = (Button) this.getDialogPane().lookupButton(ButtonType.OK);

            // set action event filter for custom dialog validation and storing the result 
            // => don't close the dialog on errors 
            btOk.addEventFilter(ActionEvent.ACTION, (event) -> {
                if (!validateClient(client.getClient())) {
                    log.debug("client  valid");
                    event.consume();
                } else {
                    log.debug("client not valid");

                }
            });

            Optional<ButtonType> result = this.showAndWait();
            //
            // only add to table if 'ok' pressed
            //
            if (result.get() == ButtonType.OK) {

                client.statusProperty().setValue(Savable.ClientStatus.UNSAVED.name());
                client.getLocations().forEach((location) -> {
                    if (location.nameProperty().getValue() == null || location.nameProperty().getValue().isEmpty()) {
                        location.nameProperty().setValue(client.nameProperty().getValue());
                    }
                    location.statusProperty().setValue(Savable.ClientStatus.UNSAVED.name());
                });
                ClientManager.getInstance().newClient(client);
            } else {
                client.cancel();
                this.close();
            }

        } catch (NoSuchMethodException ex) {

        }
    }

    public void showNewLocationDetail(ObservableClient client) {
        try {
            //
            // open popup
            //
            this.setTitle(Bundle.create_location());
            this.setResizable(true);
            //
            // normal way of setting minWidth not working
            //
            this.getDialogPane().setStyle(POPUP_WIDTH_STYLE);

            //
            // open details for new ObservableLocation
            //
            ObservableLocation emptyLocation = newLocation();

            ClientDetails clientDetails = new ClientDetails(emptyLocation);

            this.getDialogPane().setContent(new VBox(validation_message, clientDetails));
            this.observableLocation = emptyLocation;

            final Button btOk = (Button) this.getDialogPane().lookupButton(ButtonType.OK);

            // set action event filter for custom dialog validation and storing the result 
            // => don't close the dialog on errors 
            btOk.addEventFilter(ActionEvent.ACTION, (event) -> {
                if (!validateAtLeastOneChangeLocation()) {
                    event.consume();
                }
            });

            Optional<ButtonType> result = this.showAndWait();

            //
            // only add to table if 'ok' pressed
            //
            if (result.get() == ButtonType.OK) {
                log.debug("ok is pressed {}");

                log.debug("validator say OK");
                client.getLocations().add(emptyLocation);

                client.statusProperty().setValue(Savable.ClientStatus.CHANGED.name());
                emptyLocation.statusProperty().setValue(Savable.ClientStatus.UNSAVED.name());
            } else {
                this.close();
            }
        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void showUpdateClientDetail(ObservableClient client) {
        //
        // open popup
        //
        this.setTitle(Bundle.update_client());
        //
        // remove cancel button
        //
        this.getDialogPane().getButtonTypes().remove(ButtonType.CANCEL);
        this.setResizable(true);
        //
        // normal way of setting minWidth not working
        //
        this.getDialogPane().setStyle(POPUP_WIDTH_STYLE);

        //
        // open details for existing ObservableShipment
        //
        this.getDialogPane().setContent(new VBox(validation_message, new ClientDetails(client, true)));
        final Button btOk = (Button) this.getDialogPane().lookupButton(ButtonType.OK);

        // set action event filter for custom dialog validation and storing the result 
        // => don't close the dialog on errors 
        btOk.addEventFilter(ActionEvent.ACTION, (event) -> {
            if (!validateClient(client.getClient())) {
                event.consume();
            }
        });

        this.showAndWait();
    }

    public void showUpdateLocationDetail(ObservableLocation location) {
        //
        // open popup
        //
        this.setTitle(Bundle.update_client());
        //
        // remove cancel button
        //
        this.getDialogPane().getButtonTypes().remove(ButtonType.CANCEL);
        this.setResizable(true);
        //
        // normal way of setting minWidth not working
        //
        this.getDialogPane().setStyle(POPUP_WIDTH_STYLE);

        //
        // open details for existing ObservableShipment
        //
        this.getDialogPane().setContent(new VBox(validation_message, new ClientDetails(location)));
        final Button btOk = (Button) this.getDialogPane().lookupButton(ButtonType.OK);

        // set action event filter for custom dialog validation and storing the result 
        // => don't close the dialog on errors 
        btOk.addEventFilter(ActionEvent.ACTION, (event) -> {
            if (!validateAtLeastOneChangeLocation()) {
                event.consume();
            }
        });

        this.showAndWait();
    }

    private ObservableLocation newLocation() throws NoSuchMethodException {
        String locationId = UUID.randomUUID().toString();
        Location location = Location.buildLocation()
                .address("")
                .city("")
                .country("")
                .id(locationId)
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .name("")
                .postalcode("")
                .build();

        ObservableLocation oLocation = new ObservableLocation(location);

        return oLocation;
    }

    private ObservableClient newClient() throws NoSuchMethodException {
        String clientId = UUID.randomUUID().toString();
        String locationId = UUID.randomUUID().toString();

        Location location = Location.buildLocation()
                .address("")
                .city("")
                .country("")
                .id(locationId)
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .name("")
                .status(ApprovalStatus.NEW)
                .postalcode("")
                .build();

        List<Location> locations = new ArrayList<>();
        locations.add(location);

        Client client = Client.builder()
                .id(clientId)
                .name("")
                .currency("EUR")
                .status(ApprovalStatus.NEW)
                .locations(locations)
                .build();

        ObservableClient oClient = new ObservableClient(client, Savable.ClientStatus.ENTRY);

        return oClient;
    }

    public ObservableLocation getObservableLocation() {
        return observableLocation;
    }

    protected boolean validateAtLeastOneChangeLocation() {
        boolean valid = true;

        if ((observableLocation.getLocation().getAddress() == null || observableLocation.getLocation().getAddress().equals(""))
                && (observableLocation.getLocation().getName() == null || observableLocation.getLocation().getName().equals(""))
                && (observableLocation.getLocation().getCity() == null || observableLocation.getLocation().getCity().equals(""))
                && (observableLocation.getLocation().getCountry() == null || observableLocation.getLocation().getCountry().equals(""))
                && (observableLocation.getLocation().getEmail() == null || observableLocation.getLocation().getEmail().equals(""))
                && (observableLocation.getLocation().getExternalId() == null || observableLocation.getLocation().getExternalId().equals(""))
                // && (observableLocation.getLocation().getLatitude() == null || observableLocation.getLocation().getLatitude().equals(""))
                // && (observableLocation.getLocation().getLongitude() == null || observableLocation.getLocation().getLongitude().equals(""))
                && (observableLocation.getLocation().getPhone() == null || observableLocation.getLocation().getPhone().equals(""))
                && (observableLocation.getLocation().getPostalcode() == null || observableLocation.getLocation().getPostalcode().equals(""))) {
            valid = false;
            validation_message.setText(Bundle.empty_form_during_create_client());
            validation_message.setFill(Color.RED);
        } else {
            validation_message.setText("");
        }
        return valid;
    }

    protected boolean validateClient(Client client) {

        log.debug("validateClient name {}" + client.getName());
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        // Create a bean validator and check for issues.
        Set<ConstraintViolation<Object>> violations = validator.validate(client);

        if (!violations.isEmpty()) {
            String message = "";
            for (ConstraintViolation violation : violations) {
                if (violation.getConstraintDescriptor() != null && violation.getConstraintDescriptor().getMessageTemplate() != null) {
                    message += violation.getConstraintDescriptor().getMessageTemplate()+"\n";
                }
            }
            validation_message.setText(message);
            validation_message.setFill(Color.RED);

            log.debug("message {}" + message);

        }

        log.debug("validateClient {}" + violations.isEmpty());
        return violations.isEmpty();
    }

    protected boolean validateAndStore() {
        boolean valid = true;

        if ((observableLocation.getLocation().getAddress() == null || observableLocation.getLocation().getAddress().equals(""))
                && (observableLocation.getLocation().getName() == null || observableLocation.getLocation().getName().equals(""))
                && (observableLocation.getLocation().getCity() == null || observableLocation.getLocation().getCity().equals(""))
                && (observableLocation.getLocation().getCountry() == null || observableLocation.getLocation().getCountry().equals(""))
                && (observableLocation.getLocation().getEmail() == null || observableLocation.getLocation().getEmail().equals(""))
                && (observableLocation.getLocation().getExternalId() == null || observableLocation.getLocation().getExternalId().equals(""))
                // && (observableLocation.getLocation().getLatitude() == null || observableLocation.getLocation().getLatitude().equals(""))
                // && (observableLocation.getLocation().getLongitude() == null || observableLocation.getLocation().getLongitude().equals(""))
                && (observableLocation.getLocation().getPhone() == null || observableLocation.getLocation().getPhone().equals(""))
                && (observableLocation.getLocation().getPostalcode() == null || observableLocation.getLocation().getPostalcode().equals(""))) {
            valid = false;
            validation_message.setText(Bundle.empty_form_during_create_client());
            validation_message.setStyle(POPUP_WIDTH_STYLE);
        } else {
            validation_message.setText("");
        }
        return valid;
    }

}
