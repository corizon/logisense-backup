/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.clients;

import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableClient;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javax.swing.JPanel;
import org.openide.util.NbBundle;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@NbBundle.Messages({
    "clients.panel.loading.clients.from.manager.label=Loading"
})
public class ClientsPanel extends JPanel {

    public static final int SCENE_WIDTH = 750;
    public static final int SCENE_HEIGHT = 300;
    public static final double SPACING = 2.0;
    public static final double BUTTON_WIDTH = 64.0;
    public static final double FILTERFIELD_HEIGHT = 32.0;
    public static final double INDICITATOR_SIZE = 100.0;

    private JFXPanel fxPanel;
    private Consumer<JFXPanel> addNotifyListener;

    ClientsTableView<ObservableClient> clientsTable;
    ClientsTableButtons buttons;
    LocationsAccordion accordion = new LocationsAccordion();
    LocationsButtons locButtons = new LocationsButtons(accordion);
    ClientsTableFilterTextField filterField = new ClientsTableFilterTextField();

    //
    // indicator shown when clientmanager is not yet ready to fill table
    // -1.0 sets the indicator in indeterminate mode
    //
    ProgressIndicator indicator = new ProgressIndicator(-1.0);

    public ClientsPanel() {
        initComponents();
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        fxPanel = new JFXPanel() {

            @Override
            public void addNotify() {
                super.addNotify();
                if (addNotifyListener != null) {
                    addNotifyListener.accept(fxPanel);
                }
            }
        };
        //Scene dimensions not used by Dialog as it is created later, set on fxPanel as well:
        fxPanel.setPreferredSize(new Dimension(SCENE_WIDTH, SCENE_HEIGHT));

        Platform.setImplicitExit(false);
        Platform.runLater(() -> {
            Scene scene = new Scene(new AnchorPane());
            scene.getStylesheets().addAll(ClientsPanel.class.getResource("clients.css").toExternalForm());

            SplitPane splitPane = new SplitPane();

            //
            // left side, source table
            //
            clientsTable = new ClientsTableView(accordion);
            buttons = new ClientsTableButtons(clientsTable);

            AnchorPane leftSidePane = new AnchorPane();
            
            //
            // box is needed to be able to position indicator correctly
            //
            HBox indicatorBox = new HBox(indicator);
            indicatorBox.setAlignment(Pos.CENTER);
            indicatorBox.setMouseTransparent(true);
            leftSidePane.getChildren().addAll(buttons, filterField, clientsTable, indicatorBox);

            //
            // show indicator when ClientManager is not done
            //
            indicator.visibleProperty().bind(ClientManager.getInstance().isDoneLoadingProperty().not());
            indicator.setPrefSize(INDICITATOR_SIZE, INDICITATOR_SIZE);
            indicator.setMinSize(INDICITATOR_SIZE, INDICITATOR_SIZE);
            indicator.setMaxSize(INDICITATOR_SIZE, INDICITATOR_SIZE);

            //
            // change Table text when loading
            //
            clientsTable.setPlaceholder(new Label(Bundle.clients_panel_loading_clients_from_manager_label()));
            
            //
            // set filtering and items for clients table
            //
            filterField.getSortedData().comparatorProperty().bind(clientsTable.comparatorProperty());
            clientsTable.setItems(filterField.getSortedData());

            //
            // set all textfield heights, was a small bug that caused field to collapse
            //
            filterField.setPrefHeight(FILTERFIELD_HEIGHT);
            filterField.setMinHeight(FILTERFIELD_HEIGHT);
            filterField.setMaxHeight(FILTERFIELD_HEIGHT);

            //
            // set sizes
            // buttons
            //
            AnchorPane.setBottomAnchor(buttons, 0.0);
            AnchorPane.setTopAnchor(buttons, 0.0);
            AnchorPane.setLeftAnchor(buttons, 0.0);

            //
            // textfield
            //
            AnchorPane.setTopAnchor(filterField, 0.0);
            AnchorPane.setRightAnchor(filterField, 0.0);
            AnchorPane.setLeftAnchor(filterField, BUTTON_WIDTH + SPACING);

            // 
            // table
            //
            AnchorPane.setBottomAnchor(clientsTable, 0.0);
            AnchorPane.setTopAnchor(clientsTable, FILTERFIELD_HEIGHT + SPACING);
            AnchorPane.setRightAnchor(clientsTable, 0.0);
            AnchorPane.setLeftAnchor(clientsTable, BUTTON_WIDTH + SPACING);

            // 
            // indicator
            //
            AnchorPane.setBottomAnchor(indicatorBox, 0.0);
            AnchorPane.setTopAnchor(indicatorBox, BUTTON_WIDTH + SPACING);
            AnchorPane.setRightAnchor(indicatorBox, 0.0);
            AnchorPane.setLeftAnchor(indicatorBox, BUTTON_WIDTH + SPACING);

            //
            // right side, details table
            //
            AnchorPane rightSidePane = new AnchorPane();
            rightSidePane.getChildren().addAll(locButtons, accordion);

            //
            // set sizes
            // buttons
            //
            AnchorPane.setBottomAnchor(locButtons, 0.0);
            AnchorPane.setTopAnchor(locButtons, 0.0);
            AnchorPane.setLeftAnchor(locButtons, 0.0);

            //
            // accordion
            //
            AnchorPane.setBottomAnchor(accordion, 0.0);
            AnchorPane.setTopAnchor(accordion, 0.0);
            AnchorPane.setRightAnchor(accordion, 0.0);
            AnchorPane.setLeftAnchor(accordion, BUTTON_WIDTH);

            //
            // add all to splitpane and root anchorpane
            //
            splitPane.getItems().addAll(leftSidePane, rightSidePane);
            ((AnchorPane) scene.getRoot()).getChildren().addAll(splitPane);

            AnchorPane.setBottomAnchor(splitPane, 10.0);
            AnchorPane.setTopAnchor(splitPane, 10.0);
            AnchorPane.setRightAnchor(splitPane, 10.0);
            AnchorPane.setLeftAnchor(splitPane, 10.0);

            fxPanel.setScene(scene);
        });

        add(fxPanel, BorderLayout.CENTER);
    }

    //FIXME: 
    // workaround to keep using our swing droptarget impls on jfxpanels (see #144);
    // remove when DnD handling has been converted from swing to javafx
    // (looks like this route is only needed in CropProductionUnitPropertiesView).
    public void setAddNotifyListener(Consumer<JFXPanel> l) {
        addNotifyListener = l;
    }

}
