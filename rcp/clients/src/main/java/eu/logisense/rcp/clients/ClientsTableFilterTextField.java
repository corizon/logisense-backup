/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.clients;

import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableClient;
import eu.logisense.rcp.data.ObservableLocation;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.TextField;
import org.openide.util.NbBundle;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
@NbBundle.Messages({
    "clients.table.filter.textfield.prompt=filter"
})
public class ClientsTableFilterTextField extends TextField {

    private ObservableList<ObservableClient> clientsList = ClientManager.getInstance().getClients();

    //
    // filtered list with all clients from manager
    //
    private FilteredList<ObservableClient> filteredData = new FilteredList<>(clientsList, client -> true);
    private SortedList<ObservableClient> sortedData;

    public ClientsTableFilterTextField() {
        
        this.disableProperty().bind(ClientManager.getInstance().isDoneLoadingProperty().not());

        this.setPromptText(Bundle.clients_table_filter_textfield_prompt());

        this.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(client -> {

                //
                // if textfield is empty, display all clients
                //
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                //
                // check values of first (default) location of the client
                // TODO: also check secondary addresses
                //
                return matchesFilter(client.getLocations().get(0), lowerCaseFilter);
            });
        });

        sortedData = new SortedList<>(filteredData);

    }

    /**
     * checks all location values like address, name etc, to see if that value,
     * or a part of it, corresponds with the given string
     *
     * @param location
     * @param filter
     * @return
     */
    private boolean matchesFilter(ObservableLocation location, String filter) {

        if (location.addressProperty().getValue() != null && location.addressProperty().getValue().toLowerCase().contains(filter)) {
            return true;
        }
        if (location.cityProperty().getValue() != null && location.cityProperty().getValue().toLowerCase().contains(filter)) {
            return true;
        }
        if (location.countryProperty().getValue() != null && location.countryProperty().getValue().toLowerCase().contains(filter)) {
            return true;
        }
        if (location.emailProperty().getValue() != null && location.emailProperty().getValue().toLowerCase().contains(filter)) {
            return true;
        }
        if (location.idProperty().getValue() != null && location.idProperty().getValue().toLowerCase().contains(filter)) {
            return true;
        }
        if (location.nameProperty().getValue() != null && location.nameProperty().getValue().toLowerCase().contains(filter)) {
            return true;
        }
        if (location.phoneProperty().getValue() != null && location.phoneProperty().getValue().toLowerCase().contains(filter)) {
            return true;
        }
        if (location.postalcodeProperty().getValue() != null && location.postalcodeProperty().getValue().toLowerCase().contains(filter)) {
            return true;
        }

        //
        // false if no match found
        //
        return false;
    }

    public SortedList<ObservableClient> getSortedData() {
        return sortedData;
    }

}
