/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.clients;

import eu.logisense.rcp.data.ObservableClient;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author kenrik
 */
@Messages({
    "locationButtons.tooltip.create.location=create location",
    "locationButtons.tooltip.delete.location=delete location"
})
public class LocationsButtons extends VBox {
    
    private static final double BUTTON_WIDTH = 64.0;

    private Button createLocationButton = new Button();
    private Button deleteRecordButton = new Button();

    private LocationsAccordion accordion;

    public LocationsButtons(LocationsAccordion accordion) {
        this.accordion = accordion;

        initComponents();
        this.setMinWidth(BUTTON_WIDTH);
    }

    private void initComponents() {
        createLocationButton.setTooltip(new Tooltip(Bundle.locationButtons_tooltip_create_location()));
        createLocationButton.setPrefHeight(BUTTON_WIDTH);
        createLocationButton.setPrefWidth(BUTTON_WIDTH);
        createLocationButton.setStyle("-fx-background-image: url('/eu/logisense/rcp/clients/client_add64.png')");

        deleteRecordButton.setTooltip(new Tooltip(Bundle.locationButtons_tooltip_delete_location()));
        deleteRecordButton.setPrefHeight(BUTTON_WIDTH);
        deleteRecordButton.setPrefWidth(BUTTON_WIDTH);
        deleteRecordButton.setStyle("-fx-background-image: url('/eu/logisense/rcp/clients/client_remove64.png')");

        this.getChildren().addAll(createLocationButton, deleteRecordButton);

        this.setAlignment(Pos.TOP_LEFT);

        //
        // disable buttons when no client selected
        // disable delete when no location is expanded aswel
        //
        createLocationButton.disableProperty().bind(accordion.selectedClientProperty().isNull());
        deleteRecordButton.disableProperty().bind(Bindings.or(
                accordion.selectedClientProperty().isNull(),
                accordion.expandedPaneProperty().isNull()));

        //
        // add new location to client
        //
        createLocationButton.setOnAction(((ActionEvent e) -> {
            ClientDetailsPopup sdp = new ClientDetailsPopup();
            ObservableClient client = accordion.getSelectedClient();

            sdp.showNewLocationDetail(client);

            accordion.updateLocationTitledPanesForClient(client);
        }));

        //
        // delete selected item
        //
        deleteRecordButton.setOnAction(((ActionEvent e) -> {
            accordion.removeLocationTitledPane();
        }));
    }
}
