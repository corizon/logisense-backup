/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.clients;

import eu.logisense.rcp.data.ObservableClient;
import eu.logisense.rcp.data.ObservableLocation;
import eu.logisense.rcp.util.AutoCompleteComboBoxListener;
import eu.logisense.rcp.util.CountryComboBox;
import eu.logisense.rcp.util.IsoNameConverter;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@Messages({
    "client name label=name",
    "client location address label=address",
    "client location city label=city",
    "client location country label=country",
    "client location phone label=phone",
    "client location email label=email",
    "client location charge vat label=charge vat",
    "client location vat number label=vat number",
    "client location chamber of commerce label=chamber of commerce",
    "client location chamber of commerce city label=chamber of commerce city",
    "client location bic code label=bic code",
    "client location currency label=currency",
    "client location iban code label=iban code",
    "client location postal code label=postalcode",
    "client location validator message name=enter name",
    "client location validator message address=enter address",
    "client location validator message city=enter city",
    "client location validator message country=enter country",
    "client location validator message postal code=enter postal code",
    "client location validator message phone=enter phone number",
    "client location validator message email=enter email address",
    "client location validator message vat number=enter vat number",
    "client location validator message chamber of commerce=enter chamber of commerce",
    "client location validator message chamber of commerce city=enter chamber of commerce city",
    "client location validator message bic code=enter bic code",
    "client location validator message iban=enter iban number",
    "client location validator message currency=enter currency"
})
public class ClientDetails extends VBox {

    private static final double TA_HEIGHT = 40.0;
    private static final double SPACING = 10.0;
    private static final double PADDING = 5.0;
    private static final double INSETS = PADDING * 2;

    private TextField nameTF;

    public TextField getNameTF() {
        return nameTF;
    }
    private TextField addressTF;
    private TextField cityTF;
    private CountryComboBox countryComboBox;
    private TextField postalcodeTF;
    private TextField phoneTF;
    private TextField emailTF;
    private CheckBox chargeVatCheck;
    private TextField vatNumberTF;
    private TextField chamberOfCommerceTF;
    private TextField chamberOfCommerceCityTF;
    private TextField bicCodeTF;
    private TextField ibanCodeTF;
    private ComboBox currencyComboBox;

    public ClientDetails(ObservableLocation location) {
        setSpacing(SPACING);
        setPadding(new Insets(PADDING));
        initLocationComponents(location);
    }

    public ClientDetails(ObservableClient client, boolean isUpdate) {
        setSpacing(SPACING);
        setPadding(new Insets(PADDING));
        initClientComponents(client, isUpdate);
    }

    private void initClientComponents(ObservableClient client, boolean isUpdate) {
        List<Node> nodes = new ArrayList<>();

        nameTF = new TextField();
        nodes.add(nameTF);

        if (!isUpdate) {
            addressTF = new TextField();
            nodes.add(addressTF);
            cityTF = new TextField();
            nodes.add(cityTF);
            countryComboBox = new CountryComboBox();
            nodes.add(countryComboBox);
            postalcodeTF = new TextField();
            nodes.add(postalcodeTF);
            phoneTF = new TextField();
            nodes.add(phoneTF);
            emailTF = new TextField();
            nodes.add(emailTF);
        }
        chargeVatCheck = new CheckBox();
        nodes.add(chargeVatCheck);
        chamberOfCommerceTF = new TextField();
        nodes.add(chamberOfCommerceTF);
        chamberOfCommerceCityTF = new TextField();
        nodes.add(chamberOfCommerceCityTF);
        vatNumberTF = new TextField();
        nodes.add(vatNumberTF);
        bicCodeTF = new TextField();
        nodes.add(bicCodeTF);
        ibanCodeTF = new TextField();
        nodes.add(ibanCodeTF);

        ObservableList<Currency> currencies = FXCollections.observableArrayList();
        Currency.getAvailableCurrencies().stream().forEach((c) -> {
            currencies.add(c);
        });
        currencyComboBox = new ComboBox(currencies.sorted());
        currencyComboBox.setEditable(true);
        nodes.add(currencyComboBox);
        //
        // Looks weird but this adds the autocomplete functionality to the comboboxes
        // FIXME: ugly code
        //
        new AutoCompleteComboBoxListener<>(currencyComboBox);

        this.getChildren().addAll(nodes);

        if (!isUpdate) {
            setLocationSizes();
            setLocationBindings(client);
        }
        setClientSizes();
        setClientBindings(client);
        setValidationBindings();
    }

    private void initLocationComponents(ObservableLocation location) {
        nameTF = new TextField();
        addressTF = new TextField();
        cityTF = new TextField();
        countryComboBox = new CountryComboBox();
        postalcodeTF = new TextField();
        phoneTF = new TextField();
        emailTF = new TextField();

        this.getChildren().addAll(nameTF, addressTF, cityTF, countryComboBox,
                postalcodeTF, phoneTF, emailTF);

        setLocationSizes();
        setLocationBindings(location);
        setValidationBindings();
    }

    private void setLocationSizes() {
        nameTF.setPromptText(Bundle.client_name_label());
        nameTF.setTooltip(new Tooltip(Bundle.client_name_label()));
        nameTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));
        nameTF.setPrefHeight(TA_HEIGHT);

        addressTF.setPromptText(Bundle.client_location_address_label());
        addressTF.setTooltip(new Tooltip(Bundle.client_location_address_label()));
        addressTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        cityTF.setPromptText(Bundle.client_location_city_label());
        cityTF.setTooltip(new Tooltip(Bundle.client_location_city_label()));
        cityTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        countryComboBox.setPromptText(Bundle.client_location_country_label());
        countryComboBox.setTooltip(new Tooltip(Bundle.client_location_country_label()));
        countryComboBox.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        postalcodeTF.setPromptText(Bundle.client_location_postal_code_label());
        postalcodeTF.setTooltip(new Tooltip(Bundle.client_location_postal_code_label()));
        postalcodeTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        phoneTF.setPromptText(Bundle.client_location_phone_label());
        phoneTF.setTooltip(new Tooltip(Bundle.client_location_phone_label()));
        phoneTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        emailTF.setPromptText(Bundle.client_location_email_label());
        emailTF.setTooltip(new Tooltip(Bundle.client_location_email_label()));
        emailTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));
    }

    private void setClientSizes() {
        nameTF.setPromptText(Bundle.client_name_label());
        nameTF.setTooltip(new Tooltip(Bundle.client_name_label()));
        nameTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));
        nameTF.setPrefHeight(TA_HEIGHT);

        chargeVatCheck.setText(Bundle.client_location_charge_vat_label());
        chargeVatCheck.setTooltip(new Tooltip(Bundle.client_location_charge_vat_label()));
        chargeVatCheck.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        vatNumberTF.setPromptText(Bundle.client_location_vat_number_label());
        vatNumberTF.setTooltip(new Tooltip(Bundle.client_location_vat_number_label()));
        vatNumberTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        chamberOfCommerceTF.setPromptText(Bundle.client_location_chamber_of_commerce_label());
        chamberOfCommerceTF.setTooltip(new Tooltip(Bundle.client_location_chamber_of_commerce_label()));
        chamberOfCommerceTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        chamberOfCommerceCityTF.setPromptText(Bundle.client_location_chamber_of_commerce_city_label());
        chamberOfCommerceCityTF.setTooltip(new Tooltip(Bundle.client_location_chamber_of_commerce_city_label()));
        chamberOfCommerceCityTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        bicCodeTF.setPromptText(Bundle.client_location_bic_code_label());
        bicCodeTF.setTooltip(new Tooltip(Bundle.client_location_bic_code_label()));
        bicCodeTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        currencyComboBox.setPromptText(Bundle.client_location_currency_label());
        currencyComboBox.setTooltip(new Tooltip(Bundle.client_location_currency_label()));
        currencyComboBox.prefWidthProperty().bind(widthProperty().subtract(INSETS));

        ibanCodeTF.setPromptText(Bundle.client_location_iban_code_label());
        ibanCodeTF.setTooltip(new Tooltip(Bundle.client_location_iban_code_label()));
        ibanCodeTF.prefWidthProperty().bind(widthProperty().subtract(INSETS));
    }

    private void setLocationBindings(ObservableLocation location) {
        nameTF.textProperty().bindBidirectional(location.nameProperty());
        addressTF.textProperty().bindBidirectional(location.addressProperty());
        cityTF.textProperty().bindBidirectional(location.cityProperty());
        postalcodeTF.textProperty().bindBidirectional(location.postalcodeProperty());
        phoneTF.textProperty().bindBidirectional(location.phoneProperty());
        emailTF.textProperty().bindBidirectional(location.emailProperty());

        Bindings.bindBidirectional(countryComboBox.valueProperty(),
                location.countryProperty(), new IsoNameConverter());

    }

    private void setClientBindings(ObservableClient client) {
        nameTF.textProperty().bindBidirectional(client.nameProperty());
        chargeVatCheck.selectedProperty().bindBidirectional(client.chargeVatProperty());
        vatNumberTF.textProperty().bindBidirectional(client.vatNumberProperty());
        chamberOfCommerceTF.textProperty().bindBidirectional(client.chamberOfCommerceProperty());
        chamberOfCommerceCityTF.textProperty().bindBidirectional(client.chamberOfCommerceCityProperty());
        bicCodeTF.textProperty().bindBidirectional(client.bicCodeProperty());
        ibanCodeTF.textProperty().bindBidirectional(client.ibanCodeProperty());

        if (client.currencyProperty() != null) {
            currencyComboBox.getSelectionModel().select(client.currencyProperty().getValue());
        } else {
            currencyComboBox.getSelectionModel().select("EUR");
        }

        currencyComboBox.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof Currency) {
                        client.currencyProperty().setValue(((Currency) newValue).toString());
                    }
                });
    }

    private void setValidationBindings() {
        // https://bitbucket.org/controlsfx/controlsfx/issues/539/multiple-dialog-fields-with-validation
        Platform.runLater(() -> {

            //
            // add validator to nodes, shows small error icon in bottomleft corner
            // when empty
            //
            ValidationSupport validationSupport = new ValidationSupport();

            //
            // set validationBinding to true when either of the bound nodes is empty
            //
            if (vatNumberTF != null) {
                validationSupport.registerValidator(vatNumberTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_vat_number()));
            }
            if (chamberOfCommerceTF != null) {
                validationSupport.registerValidator(chamberOfCommerceTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_chamber_of_commerce()));
            }
            if (chamberOfCommerceCityTF != null) {
                validationSupport.registerValidator(chamberOfCommerceCityTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_chamber_of_commerce_city()));
            }
            if (bicCodeTF != null) {
                validationSupport.registerValidator(bicCodeTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_bic_code()));
            }
            if (ibanCodeTF != null) {
                validationSupport.registerValidator(ibanCodeTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_iban()));
            }
            if (currencyComboBox != null) {
                validationSupport.registerValidator(currencyComboBox, Validator.createEmptyValidator(Bundle.client_location_validator_message_currency()));
            }
            if (nameTF != null) {
                validationSupport.registerValidator(nameTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_name()));
            }
            if (addressTF != null) {
                validationSupport.registerValidator(addressTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_address()));
            }
            if (cityTF != null) {
                validationSupport.registerValidator(cityTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_city()));
            }
            if (postalcodeTF != null) {
                validationSupport.registerValidator(postalcodeTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_postal_code()));
            }
            if (phoneTF != null) {
                validationSupport.registerValidator(phoneTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_phone()));
            }
            if (emailTF != null) {
                validationSupport.registerValidator(emailTF, Validator.createEmptyValidator(Bundle.client_location_validator_message_email()));
            }
            if (countryComboBox != null) {
                validationSupport.registerValidator(countryComboBox, Validator.createEmptyValidator(Bundle.client_location_validator_message_country()));
            }
        });
    }

    public String getLocationName() {
        return nameTF.getText();
    }

}
