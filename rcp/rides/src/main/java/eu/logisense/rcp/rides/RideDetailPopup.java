/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.rides;

import eu.logisense.rcp.data.ObservableRide;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author kenrik
 */
@Messages({
    "update ride popup title=update ride"
})
public class RideDetailPopup extends Alert {

    private static final String POPUP_WIDTH_STYLE = "-fx-pref-width:600px;";

    private RideDetailPopup(Alert.AlertType alertType) {
        super(alertType);
    }

    public RideDetailPopup() {
        this(Alert.AlertType.CONFIRMATION);
    }

    public void showUpdateRideDetail(ObservableRide ride) {
        //
        // open popup
        //
        this.setTitle(Bundle.update_ride_popup_title());
        //
        // remove cancel button
        //
        this.getDialogPane().getButtonTypes().remove(ButtonType.CANCEL);
        this.setResizable(true);
        //
        // normal way of setting minWidth not working
        //
        this.getDialogPane().setStyle(POPUP_WIDTH_STYLE);

        //
        // open details for existing ObservableShipment
        //
        this.getDialogPane().setContent(new RideDetails(ride));

        this.showAndWait();
    }

}
