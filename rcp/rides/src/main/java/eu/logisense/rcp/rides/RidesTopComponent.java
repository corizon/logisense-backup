/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.rides;

import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.RideManager;
import java.awt.BorderLayout;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;
import org.openide.util.lookup.Lookups;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
        dtd = "-//eu.logisense.rcp.rides//Rides//EN",
        autostore = false
)
@TopComponent.Description(
        preferredID = "RidesTopComponent",
        iconBase = "eu/logisense/rcp/rides/rides.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "editor", openAtStartup = false)
@ActionID(category = "Window", id = "eu.logisense.rcp.rides.RidesTopComponent")
@ActionReference(path = "Menu/LogiSense" , position = 30)
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_RidesAction",
        preferredID = "RidesTopComponent"
)
@Messages({
    "CTL_RidesAction=Rides",
    "CTL_RidesTopComponent=Rides",
    "HINT_RidesTopComponent=This is a Rides window"
})
public final class RidesTopComponent extends TopComponent {

    public RidesTopComponent() {
        initComponents();
        setName(Bundle.CTL_RidesTopComponent());
        associateLookup(Lookups.proxy(RideManager.getInstance().getRideSaveManager()));
        setToolTipText(Bundle.HINT_RidesTopComponent());

    }

    private void initComponents() {
        setLayout(new BorderLayout());
        add(new RidesPanel());
    }

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }
}
