/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.rides;

import eu.logisense.api.Creditor;
import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import eu.logisense.rcp.data.Savable;
import eu.logisense.rcp.data.ObservableRide;
import eu.logisense.rcp.util.LocalDateTimeTableCell;
import java.time.LocalDateTime;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.openide.util.NbBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@NbBundle.Messages({
    "rides.table.status.col.header=status",
    "rides.table.id.col.header=id",
    "rides.table.load.city.col.header=load city",
    "rides.table.unload.city.col.header=unload city",
    "rides.table.etd.col.header=etd",
    "rides.table.creditor.name.col.header=creditor",
    "rides.table.creditor.email.col.header=email",
    "rides.table.creditor.phone.col.header=phone",
    "rides.table.sellprice.col.header=sell price",
    "rides.table.buyprice.col.header=buy price",
    "rides.table.currency.col.header=currency",
    "rides.table.margin.col.header=margin",
    "rides.table.buy.margin.col.header=buy margin %",
    "rides.table.sell.margin.col.header=sell margin %"
})
public class RidesTableView<R extends ObservableRide> extends TableView {

    private static final Logger log = LoggerFactory.getLogger(RidesTableView.class);

    private final double standardHeight = 540.0;

    private TableColumn<ObservableRide, Status> statusCol;
//    private TableColumn<ObservableRide, String> idCol;
    private TableColumn<ObservableRide, String> loadCityCol;
    private TableColumn<ObservableRide, String> unloadCityCol;
    private TableColumn<ObservableRide, LocalDateTime> etdCol;
//    private TableColumn<ObservableRide, String> customerCol;
    private TableColumn<ObservableRide, String> creditorNameCol;
    private TableColumn<ObservableRide, String> creditorEmailCol;
    private TableColumn<ObservableRide, String> creditorPhoneCol;
    private TableColumn<ObservableRide, Double> buyPriceCol;
    private TableColumn<ObservableRide, Double> sellPriceCol;
    private TableColumn<ObservableRide, String> currencyCol;
    private TableColumn<ObservableRide, Double> marginCol;
    private TableColumn<ObservableRide, Double> marginBuyCol;
    private TableColumn<ObservableRide, Double> marginSellCol;

    public RidesTableView(ObservableList<ObservableRide> list) {
        super(list);
        this.setPrefHeight(standardHeight);
        this.setEditable(true);
        this.setTableMenuButtonVisible(true);

        setupTable();
    }

    private void setupTable() {
        statusCol = new TableColumn(Bundle.rides_table_status_col_header());
        statusCol.setCellValueFactory(cellData -> cellData.getValue().transportStatusProperty());

//        idCol = new TableColumn(Bundle.rides_table_id_col_header());
//        idCol.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        loadCityCol = new TableColumn(Bundle.rides_table_load_city_col_header());
        loadCityCol.setCellValueFactory(cellData -> cellData.getValue().loadCityProperty());

        unloadCityCol = new TableColumn(Bundle.rides_table_unload_city_col_header());
        unloadCityCol.setCellValueFactory(cellData -> cellData.getValue().unloadCityProperty());

        etdCol = new TableColumn(Bundle.rides_table_etd_col_header());
        etdCol.setCellValueFactory(cellData -> cellData.getValue().etdProperty());
        etdCol.setCellFactory(column -> {
            return new LocalDateTimeTableCell<>("dd/MM/yy HH:mm:ss");
        });

        creditorNameCol = new TableColumn(Bundle.rides_table_creditor_name_col_header());
        creditorNameCol.setCellValueFactory(cellData -> cellData.getValue().creditorNameProperty());

        creditorEmailCol = new TableColumn(Bundle.rides_table_creditor_email_col_header());
        creditorEmailCol.setCellValueFactory(cellData -> cellData.getValue().creditorContactEmailProperty());

        creditorPhoneCol = new TableColumn(Bundle.rides_table_creditor_phone_col_header());
        creditorPhoneCol.setCellValueFactory(cellData -> cellData.getValue().creditorContactPhoneProperty());

        buyPriceCol = new TableColumn(Bundle.rides_table_buyprice_col_header());
        buyPriceCol.setCellValueFactory(cellData -> cellData.getValue().priceProperty());

        sellPriceCol = new TableColumn(Bundle.rides_table_sellprice_col_header());
        sellPriceCol.setCellValueFactory(cellData -> cellData.getValue().totalSellPriceProperty().asObject());

        currencyCol = new TableColumn(Bundle.rides_table_currency_col_header());
        currencyCol.setCellValueFactory(cellData -> cellData.getValue().currencyProperty());

        marginCol = new TableColumn(Bundle.rides_table_margin_col_header());
        marginCol.setCellValueFactory(cellData -> cellData.getValue().marginProperty());

        marginBuyCol = new TableColumn(Bundle.rides_table_buy_margin_col_header());
        marginBuyCol.setCellValueFactory(cellData -> cellData.getValue().marginBuyProperty());

        marginSellCol = new TableColumn(Bundle.rides_table_sell_margin_col_header());
        marginSellCol.setCellValueFactory(cellData -> cellData.getValue().marginSellProperty());

        getColumns().addAll(statusCol, /*idCol,*/ loadCityCol, unloadCityCol,
                etdCol, creditorNameCol, creditorEmailCol, creditorPhoneCol,
                buyPriceCol, sellPriceCol, currencyCol, marginCol, marginBuyCol,
                marginSellCol);
        getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        //
        // open up details when row is clicked twice
        //
        this.setOnMousePressed((MouseEvent event) -> {
            log.info("MouseEvent");
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                log.info("Double click");
                log.info("selected " + getSelectionModel().getSelectedItem());
                if (getSelectionModel().getSelectedItem() != null) {
                    RideDetailPopup rdp = new RideDetailPopup();
                    log.info("RideDetailPopup");
                    rdp.showUpdateRideDetail((ObservableRide) this.getSelectionModel().getSelectedItem());
                }
            }
        });

        //
        // set all columns min width and editable
        //
        getColumns().stream().map((tc) -> {
            ((TableColumn) tc).setMinWidth(100.0);
            ((TableColumn) tc).setEditable(true);
            return tc;
        });

        setRowFactory((tableview) -> {
            TableRow<ObservableRide> row = new TableRow<>();
            ChangeListener<String> changeListener = (obs, oldStatus, newStatus) -> {
                Savable.ClientStatus status = Savable.ClientStatus.from(newStatus);
                setPseudoClass(row, status);
            };

            row.itemProperty().addListener((obs, previousRide, currentRide) -> {
                if (previousRide != null) {
                    previousRide.statusProperty().removeListener(changeListener);
                }
                if (currentRide != null) {
                    currentRide.statusProperty().addListener(changeListener);
                    Savable.ClientStatus status = Savable.ClientStatus.from(currentRide.statusProperty().getValue());
                    setPseudoClass(row, status);
                }
            });
            return row;
        });
    }

    private void setPseudoClass(TableRow<ObservableRide> row, Status status) {
        //
        // change colour of row depending on status
        //
        PseudoClass statusNew = PseudoClass.getPseudoClass("statusNew");
        PseudoClass statusUnsaved = PseudoClass.getPseudoClass("statusUnsaved");
        PseudoClass statusPlanned = PseudoClass.getPseudoClass("statusPlanned");
        PseudoClass statusLoaded = PseudoClass.getPseudoClass("statusLoaded");
        PseudoClass statusUnloaded = PseudoClass.getPseudoClass("statusUnloaded");

        row.pseudoClassStateChanged(statusNew, Savable.ClientStatus.NEW.equals(status));
        row.pseudoClassStateChanged(statusUnsaved, Savable.ClientStatus.UNSAVED.equals(status) || Savable.ClientStatus.CHANGED.equals(status));
        row.pseudoClassStateChanged(statusPlanned, Savable.ClientStatus.PLANNED.equals(status));
        row.pseudoClassStateChanged(statusLoaded, Savable.ClientStatus.LOADED.equals(status) || Savable.ClientStatus.EN_ROUTE.equals(status));
        row.pseudoClassStateChanged(statusUnloaded, Savable.ClientStatus.UNLOADED.equals(status));
    }

}
