/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.rides;

import eu.logisense.api.Creditor;
import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableClient;
import eu.logisense.rcp.data.ObservableRide;
import eu.logisense.rcp.util.AutoCompleteComboBoxListener;
import eu.logisense.rcp.util.DateTimePicker;
import eu.logisense.rcp.util.MoneyStringConverter;
import eu.logisense.rcp.util.NumberTextField;
import java.util.Currency;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.converter.DoubleStringConverter;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.openide.util.NbBundle.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kenrik
 */
@Messages({
    "ride details status tooltip=status",
    "ride details etd tooltip=load date",
    "ride details creditor tooltip=creditor",
    "ride details price tooltip=price",
    "ride details currency tooltip=currency",
    "ride details margin tooltip=margin",
    "ride details margin buy tooltip=buy margin %",
    "ride details margin sell tooltip=sell margin %",
    "ride details validator message etd picker=enter load date",
    "ride details validator message price=enter price",
    "ride details validator message currency=enter currency",
    "ride details validator message creditor=enter creditor"
})
public class RideDetails extends VBox {

    private static final double SPACING = 20.0;
    private static final double PADDING = 5.0;
    private static final double INSETS = PADDING * 2;
    private static final double LABEL_WIDTH = 100.0;

    private ComboBox statusComboBox = new ComboBox();
    private Label statusLabel = new Label(Bundle.ride_details_status_tooltip());
    private DateTimePicker etdPicker = new DateTimePicker();
    private Label etdLabel = new Label(Bundle.ride_details_etd_tooltip());
    private ComboBox creditorComboBox = new ComboBox(ClientManager.getInstance().getClients());
    private Label creditorLabel = new Label(Bundle.ride_details_creditor_tooltip());
    private NumberTextField priceTF = new NumberTextField();
    private Label priceLabel = new Label(Bundle.ride_details_price_tooltip());
    private ComboBox currencyComboBox = new ComboBox();
    private Label currencyLabel = new Label(Bundle.ride_details_currency_tooltip());
    private NumberTextField marginTF = new NumberTextField();
    private Label marginLabel = new Label(Bundle.ride_details_margin_tooltip());
    private NumberTextField marginBuyTF = new NumberTextField();
    private Label marginBuyLabel = new Label(Bundle.ride_details_margin_buy_tooltip());
    private NumberTextField marginSellTF = new NumberTextField();
    private Label marginSellLabel = new Label(Bundle.ride_details_margin_sell_tooltip());

    private static final Logger log = LoggerFactory.getLogger(RideDetails.class);

    public RideDetails(ObservableRide shipment) {
        setSpacing(SPACING);
        setPadding(new Insets(PADDING));
        initComponents(shipment);
    }

    private void initComponents(ObservableRide shipment) {

        statusComboBox.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + INSETS + PADDING));
        statusComboBox.setPromptText(Bundle.ride_details_status_tooltip());
        statusComboBox.setTooltip(new Tooltip(Bundle.ride_details_status_tooltip()));

        ObservableList<Status> rideStatus = FXCollections.observableArrayList();
        rideStatus.add(TransportStatus.LOADED);
        rideStatus.add(TransportStatus.EN_ROUTE);
        rideStatus.add(TransportStatus.UNLOADED);

        statusComboBox.setItems(rideStatus);
        statusLabel.setPrefWidth(LABEL_WIDTH);

        etdPicker.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + INSETS + PADDING));
        etdPicker.setPromptText(Bundle.ride_details_etd_tooltip());
        etdPicker.setTooltip(new Tooltip(Bundle.ride_details_etd_tooltip()));
        etdLabel.setPrefWidth(LABEL_WIDTH);

        creditorComboBox.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + INSETS + PADDING));
        creditorComboBox.setPromptText(Bundle.ride_details_creditor_tooltip());
        creditorComboBox.setTooltip(new Tooltip(Bundle.ride_details_creditor_tooltip()));
        creditorComboBox.setEditable(true);
        creditorLabel.setPrefWidth(LABEL_WIDTH);

        priceTF.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + INSETS + PADDING));
        priceTF.setPromptText(Bundle.ride_details_price_tooltip());
        priceTF.setTooltip(new Tooltip(Bundle.ride_details_price_tooltip()));
        priceLabel.setPrefWidth(LABEL_WIDTH);

        currencyComboBox.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + INSETS + PADDING));
        currencyComboBox.setPromptText(Bundle.ride_details_currency_tooltip());
        currencyComboBox.setTooltip(new Tooltip(Bundle.ride_details_currency_tooltip()));
        currencyComboBox.setEditable(true);

        ObservableList<Currency> currencies = FXCollections.observableArrayList();
        Currency.getAvailableCurrencies().stream().forEach((c) -> {
            currencies.add(c);
        });

        currencyComboBox.setItems(currencies.sorted());
        currencyLabel.setPrefWidth(LABEL_WIDTH);
        //
        // Looks weird but this adds the autocomplete functionality to the combobox
        // FIXME: ugly code
        //
        new AutoCompleteComboBoxListener<>(creditorComboBox);
        new AutoCompleteComboBoxListener<>(currencyComboBox);

        marginTF.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + INSETS + PADDING));
        marginTF.setPromptText(Bundle.ride_details_margin_tooltip());
        marginTF.setTooltip(new Tooltip(Bundle.ride_details_margin_tooltip()));
        marginTF.setEditable(false);
        marginLabel.setPrefWidth(LABEL_WIDTH);

        marginBuyTF.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + INSETS + PADDING));
        marginBuyTF.setPromptText(Bundle.ride_details_margin_buy_tooltip());
        marginBuyTF.setTooltip(new Tooltip(Bundle.ride_details_margin_buy_tooltip()));
        marginBuyTF.setEditable(false);
        marginBuyLabel.setPrefWidth(LABEL_WIDTH);

        marginSellTF.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + INSETS + PADDING));
        marginSellTF.setPromptText(Bundle.ride_details_margin_sell_tooltip());
        marginSellTF.setTooltip(new Tooltip(Bundle.ride_details_margin_sell_tooltip()));
        marginSellTF.setEditable(false);
        marginSellLabel.setPrefWidth(LABEL_WIDTH);

        getChildren().addAll(new HBox(SPACING, statusLabel, statusComboBox),
                new HBox(SPACING, etdLabel, etdPicker),
                new HBox(SPACING, creditorLabel, creditorComboBox),
                new HBox(SPACING, priceLabel, priceTF),
                new HBox(SPACING, currencyLabel, currencyComboBox),
                new HBox(SPACING, marginLabel, marginTF),
                new HBox(SPACING, marginBuyLabel, marginBuyTF),
                new HBox(SPACING, marginSellLabel, marginSellTF));

        setBindings(shipment);
        setValidationBindings();
    }

    private void setBindings(ObservableRide ride) {

        if (ride.transportStatusProperty() != null) {
            statusComboBox.getSelectionModel().select(ride.transportStatusProperty().getValue());
        }

        statusComboBox.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof Status) {
                        ride.transportStatusProperty().setValue(TransportStatus.valueOf(newValue.toString()));
                        log.debug("setting ride status to {}", newValue);
                    }
                });

        etdPicker.dateTimeValueProperty().bindBidirectional(ride.etdProperty());

        if (ride.creditorNameProperty().getValue() != null) {
            creditorComboBox.getSelectionModel().select(ride.creditorNameProperty().getValue());
        }

        creditorComboBox.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof ObservableClient) {
                        ride.creditorIdProperty().setValue(((ObservableClient) newValue).getClient().getId());
                    }
                });

        Bindings.bindBidirectional(priceTF.textProperty(), ride.priceProperty(), new MoneyStringConverter());

        if (ride.currencyProperty() == null || "".equals(ride.currencyProperty().getValue())) {
            currencyComboBox.getSelectionModel().select("EUR");
        } else {
            currencyComboBox.getSelectionModel().select(ride.currencyProperty().getValue());
        }

        currencyComboBox.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observable, Object oldValue, Object newValue) -> {
                    if (newValue instanceof Currency) {
                        ride.currencyProperty().setValue(newValue.toString());
                    }
                });

        Bindings.bindBidirectional(marginTF.textProperty(), ride.marginProperty(), new DoubleStringConverter());
        Bindings.bindBidirectional(marginBuyTF.textProperty(), ride.marginBuyProperty(), new DoubleStringConverter());
        Bindings.bindBidirectional(marginSellTF.textProperty(), ride.marginSellProperty(), new DoubleStringConverter());
    }

    private void setValidationBindings() {
        // https://bitbucket.org/controlsfx/controlsfx/issues/539/multiple-dialog-fields-with-validation
        Platform.runLater(() -> {
            //
            // add validator to nodes, shows small error icon in bottomleft corner
            // when empty
            //
            ValidationSupport validationSupport = new ValidationSupport();

            validationSupport.registerValidator(creditorComboBox, Validator.createEmptyValidator(Bundle.ride_details_validator_message_creditor()));
            validationSupport.registerValidator(priceTF, Validator.createEmptyValidator(Bundle.ride_details_validator_message_price()));
            validationSupport.registerValidator(currencyComboBox, Validator.createEmptyValidator(Bundle.ride_details_validator_message_currency()));
        });
    }
}
