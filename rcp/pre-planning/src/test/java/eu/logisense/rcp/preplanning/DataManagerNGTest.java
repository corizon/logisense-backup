/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.preplanning;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Client;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Order;
import eu.logisense.api.Origin;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.data.Savable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class DataManagerNGTest {
    
    public DataManagerNGTest() {
    }

    @org.testng.annotations.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.testng.annotations.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.testng.annotations.BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @org.testng.annotations.AfterMethod
    public void tearDownMethod() throws Exception {
    }





    /**
     * Test of saveRide method, of class DataManager.
     */
    @org.testng.annotations.Test
    public void testSaveRide() throws IOException, NoSuchMethodException {
        System.out.println("saveRide");
        
//        RideCardVM rideCard = RideCardVM.builder()
//                .customer("PRONORM")
//                .etd(LocalDateTime.now())
//                .note("insert note")
//                .reference("insert reference")
//                .rideId(UUID.randomUUID().toString())
//                .build();   
//        
//        rideCard.getContainers().add(Container.builder()
//                .capacity(40.0)
//                .maxCapacity(40.0)
//                .name("010")
//                .build());


        Origin emptyFrom = Origin.builder()
                .address("")
                .city("")
                .country("")
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .postalcode("")
                .build();

        Destination emptyTo = Destination.builder()
                .address("")
                .city("")
                .country("")
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .postalcode("")
                .build();
        
        LoadLocation emptyLoad = LoadLocation.builder()
                .address("")
                .city("")
                .country("")
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .postalcode("")
                .build();

        UnloadLocation emptyUnload = UnloadLocation.builder()
                .address("")
                .city("")
                .country("")
                .latitude(Double.NaN)
                .longitude(Double.NaN)
                .postalcode("")
                .build();

        Order order = Order.builder()
                .batch("")
                .debitor(Debitor.builder().build())
                .destination(Destination.builder().build())
                .href("")
                .id("")
                .invoiceReference("")
                .lines(new ArrayList<>())
                .loadingList("")
                .name("")
                .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                .origin(Origin.builder().build())
                .unloadReference("")
                .otherReference("")
                .build();
        
        Shipment emptyShipment = Shipment.builder()
                .id(UUID.randomUUID().toString())
                .name("")
                .note("")
                .status(TransportStatus.NEW)
                .order(order)
                .loadLocation(emptyLoad)
                .unloadLocation(emptyUnload)
                .status(Savable.ClientStatus.ENTRY)
                .build();

        ObservableShipment newOrder = new ObservableShipment(emptyShipment, Savable.ClientStatus.ENTRY);

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JSR310Module());
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

        jsonProvider.writeTo(emptyShipment, Shipment.class, null, null, MediaType.APPLICATION_JSON_TYPE, null, System.out);
        System.out.println();
        
        
//        ClientUtil.proxy(RideCardService.class).store(rideCard);
    }
    
    
    
}
