/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.preplanning;

import eu.logisense.api.TransportStatus;
import eu.logisense.api.session.client.Connections;
import eu.logisense.rcp.data.ObservableRide;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.data.RideManager;
import java.util.Iterator;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kenrik
 */
public class RidesAccordion extends Accordion {

    private static final Logger log = LoggerFactory.getLogger(RidesAccordion.class);

    private ObservableList<ObservableRide> rides;
    private PlanRidesTable<ObservableShipment> overviewTable;

    public RidesAccordion(PlanRidesTable<ObservableShipment> overviewTable) {
        this.overviewTable = overviewTable;

        Connections.onConnection(() -> {
            Platform.runLater(() -> {

                try {
                    rides = RideManager.getInstance().getRidesForStatus(TransportStatus.NEW);

                    //
                    // initial fill from plannable rides
                    //
                    rides.stream().forEach(or -> {
                        this.addRideContent(new RideContent(or, overviewTable));
                    });

                    rides.addListener((ListChangeListener.Change<? extends ObservableRide> c) -> {
                        while (c.next()) {

                            if (c.wasAdded()) {
                                c.getAddedSubList().stream().forEach(or -> {
                                    log.debug("---------------");
                                    log.debug("RIDE WAS ADDED TO PLANNABLE RIDES, ID:{}", or.idProperty().getValue());

                                    final SimpleBooleanProperty found = new SimpleBooleanProperty(Boolean.FALSE);

                                    //
                                    // check if not already in accordion
                                    //
                                    this.getPanes().stream().forEach(pane -> {
                                        if (((RideTitledPane) pane).rideIdProperty.get().equals(or.idProperty().getValue())) {
                                            found.setValue(Boolean.TRUE);
                                        }
                                    });

                                    if (!found.get()) {
                                        this.addRideContent(new RideContent(or, overviewTable));
                                    }

                                });
                            }

                            if (c.wasRemoved()) {
                                c.getRemoved().stream().forEach(or -> {
                                    log.debug("---------------");
                                    log.debug("RIDE WAS REMOVED FROM PLANNABLE RIDES, ID:{}", or.idProperty().getValue());
                                    this.removeRideContent(or);
                                });
                            }
                        }
                    });

                } catch (NoSuchMethodException ex) {
                    Exceptions.printStackTrace(ex);
                }
            });
        });
    }

    private void addRideContent(RideContent rideContent) {
        RideTitledPane titledPane = new RideTitledPane(rideContent.getObservableRide());
        titledPane.setContent(rideContent);

        //
        // prevents java.lang.IllegalStateException
        //
        Platform.runLater(() -> {
            getPanes().add(titledPane);
            titledPane.setExpanded(true);
        });
    }

    private void removeRideContent(ObservableRide ride) {
        //
        // prevents java.lang.IllegalStateException
        //
        Platform.runLater(() -> {
            Iterator<TitledPane> iterator = this.getPanes().iterator();

            while (iterator.hasNext()) {
                RideTitledPane pane = (RideTitledPane) iterator.next();

                if (pane.rideIdProperty.get().equals(ride.idProperty().getValue())) {
                    iterator.remove();
                }
            }
        });

    }

    class RideTitledPane extends TitledPane {

        private final SimpleStringProperty rideIdProperty = new SimpleStringProperty();

        /**
         * Custom titled pane with bound id String
         *
         * @param idProperty
         */
        public RideTitledPane(ObservableRide ride) {
            rideIdProperty.bind(ride.idProperty());

            //
            // bind pane title to display name of ride, consists of load + unload city
            //
            this.textProperty().bind(ride.displayNameProperty());
        }

        public SimpleStringProperty rideIdProperty() {
            return rideIdProperty;
        }

    }
}
