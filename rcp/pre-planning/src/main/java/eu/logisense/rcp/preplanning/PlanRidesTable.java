/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.rcp.preplanning;

import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.sap.ShipmentDetailPopup;
import java.time.LocalDateTime;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 * @param <Shipments>
 */
@Messages({
    "load.timeframefrom.label=load timeframe from",
    "load.timeframeto.label=load timeframe to",
    "name.label=name",
    "country.label=country",
    "postal.code.label=postal code",
    "city.label=city",
    "unload.timeframefrom.label=unload timeframe from",
    "unload.timeframeto.label=unload timeframe to",
    "volume.label=volume",
    "weight.label=weight",
    "note.label=note",
    "index.label=#"
})
public class PlanRidesTable<Shipments> extends TableView {

    private static final double STANDARD_HEIGHT = 540.0;

    private TableColumn<ObservableShipment, LocalDateTime> loadTimeframeFromColumn;
    private TableColumn<ObservableShipment, LocalDateTime> loadTimeframeToColumn;
    private TableColumn<ObservableShipment, String> nameColumn;
    private TableColumn<ObservableShipment, String> countryColumn;
    private TableColumn<ObservableShipment, String> postalCodeColumn;
    private TableColumn<ObservableShipment, String> cityColumn;
    private TableColumn<ObservableShipment, LocalDateTime> unloadTimeframeFromColumn;
    private TableColumn<ObservableShipment, LocalDateTime> unloadTimeframeToColumn;
    private TableColumn<ObservableShipment, Double> weightColumn;
    private TableColumn<ObservableShipment, Double> volumeColumn;
    private TableColumn<ObservableShipment, String> noteColumn;
    private TableColumn<ObservableShipment, Integer> indexColumn;

    private String name;

    public PlanRidesTable(String name) {
        this.name = name;
        this.setPrefHeight(STANDARD_HEIGHT);
        this.setEditable(true);
        this.setTableMenuButtonVisible(true);

        setUpTable();
    }

    public PlanRidesTable(ObservableList items) {
        super(items);
        this.setPrefHeight(STANDARD_HEIGHT);
        this.setEditable(true);
        this.setTableMenuButtonVisible(true);

        setUpTable();
    }

    private void setUpTable() {

        loadTimeframeFromColumn = new TableColumn(Bundle.load_timeframefrom_label());
        loadTimeframeFromColumn.setPrefWidth(120.0);
        loadTimeframeToColumn = new TableColumn(Bundle.load_timeframeto_label());
        loadTimeframeToColumn.setPrefWidth(120.0);
        nameColumn = new TableColumn(Bundle.name_label());
        countryColumn = new TableColumn(Bundle.country_label());
        postalCodeColumn = new TableColumn(Bundle.postal_code_label());
        cityColumn = new TableColumn(Bundle.city_label());
        unloadTimeframeFromColumn = new TableColumn(Bundle.unload_timeframefrom_label());
        unloadTimeframeToColumn = new TableColumn(Bundle.unload_timeframeto_label());
        weightColumn = new TableColumn(Bundle.weight_label());
        volumeColumn = new TableColumn(Bundle.volume_label());
        noteColumn = new TableColumn(Bundle.note_label());
        noteColumn.setPrefWidth(300.0);
        indexColumn = new TableColumn(Bundle.index_label());
        indexColumn.setPrefWidth(20.0);
        indexColumn.setSortable(false);

        loadTimeframeFromColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, LocalDateTime> data) -> data.getValue().loadTimeFrameFromProperty());
        loadTimeframeToColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, LocalDateTime> data) -> data.getValue().loadTimeFrameToProperty());
        nameColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, String> data) -> data.getValue().nameProperty());
        countryColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, String> data) -> data.getValue().unloadCountryProperty());
        postalCodeColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, String> data) -> data.getValue().unloadPostalCodeProperty());
        cityColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, String> data) -> data.getValue().unloadCityProperty());
        unloadTimeframeFromColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, LocalDateTime> data) -> data.getValue().unloadTimeFrameFromProperty());
        unloadTimeframeToColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, LocalDateTime> data) -> data.getValue().unloadTimeFrameToProperty());
        volumeColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, Double> data) -> data.getValue().volumeProperty().asObject());
        noteColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, String> data) -> data.getValue().noteProperty());
        weightColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, Double> data) -> data.getValue().weightProperty().asObject());
        indexColumn.setCellValueFactory((CellDataFeatures<ObservableShipment, Integer> data) -> data.getValue().indexProperty());

        getColumns().addAll(indexColumn, loadTimeframeFromColumn, loadTimeframeToColumn, nameColumn, postalCodeColumn, cityColumn, countryColumn, unloadTimeframeFromColumn, unloadTimeframeToColumn, volumeColumn, weightColumn, noteColumn);

        
         
        //
        // hide index and loadDate by default, can be made visible by user by enabling in tableViewMenu
        //
        loadTimeframeFromColumn.setVisible(false);
        loadTimeframeToColumn.setVisible(false);
        indexColumn.setVisible(false);

        getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
                //
        // open up details when row is clicked twice
        //
        this.setOnMousePressed((MouseEvent event) -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                System.out.println("selected " + getSelectionModel().getSelectedItem());
                ShipmentDetailPopup sdp = new ShipmentDetailPopup();
                if (getSelectionModel().getSelectedItem() instanceof ObservableShipment) {
                    sdp.showUpdateShipmentDetail((ObservableShipment) this.getSelectionModel().getSelectedItem());
                } else {
                    sdp.showNewShipmentDetail(this);
                }
            }
        });
        

    }

    public void setIndexColumnVisible(boolean bool) {
        indexColumn.setVisible(bool);
    }

}
