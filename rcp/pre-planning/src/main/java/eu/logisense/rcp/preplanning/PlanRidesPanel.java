/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.preplanning;

import eu.logisense.api.TransportStatus;
import eu.logisense.api.session.client.Connections;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.data.Savable;
import eu.logisense.rcp.data.ShipmentManager;
import eu.logisense.rcp.data.ShipmentManagerImpl;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javax.swing.JPanel;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class PlanRidesPanel extends JPanel {

    public static final int SCENE_WIDTH = 750;
    public static final int SCENE_HEIGHT = 300;

    private JFXPanel fxPanel;
    private Consumer<JFXPanel> addNotifyListener;

    PlanRidesTable<ObservableShipment> overviewTable;

    public PlanRidesPanel() {
        initComponents();
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        fxPanel = new JFXPanel() {

            @Override
            public void addNotify() {
                super.addNotify();
                if (addNotifyListener != null) {
                    addNotifyListener.accept(fxPanel);
                }
            }
        };
        //Scene dimensions not used by Dialog as it is created later, set on fxPanel as well:
        fxPanel.setPreferredSize(new Dimension(SCENE_WIDTH, SCENE_HEIGHT));

        Platform.setImplicitExit(false);
        Platform.runLater(() -> {
            Scene scene = new Scene(new AnchorPane());
            scene.getStylesheets().add("/eu/logisense/rcp/preplanning/PrePlanningStyles.css");

            SplitPane splitPane = new SplitPane();
            //
            // left side, source table
            //
//            HBox horizontalSpacer = new HBox(20.0);
            AnchorPane overview = new AnchorPane();

            overviewTable = new PlanRidesTable<>("main");

            Connections.onConnection(() -> {
                Platform.runLater(() -> {
                    try {
                        overviewTable.setItems(Lookup.getDefault().lookup(ShipmentManager.class)
                                .getShipmentsForStatus(TransportStatus.NEW));
//                        ShipmentManagerImpl.getInstance().getUnLinkedShipments().comparatorProperty().bind(overviewTable.comparatorProperty());
                    } catch (NoSuchMethodException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                });
            });
            overview.getChildren().addAll(overviewTable);

            // bind overviewTable to parent anchorpane for resizing
            AnchorPane.setBottomAnchor(overviewTable, 0.0);
            AnchorPane.setTopAnchor(overviewTable, 0.0);
            AnchorPane.setRightAnchor(overviewTable, 0.0);
            AnchorPane.setLeftAnchor(overviewTable, 0.0);
//            AnchorPane.setBottomAnchor(reloadDBButton, 0.0);

            splitPane.getItems().add(overview);
            HBox.setHgrow(overview, Priority.ALWAYS);
            //
            // right side, rides 
            //
            RidesScrollPane rsp = new RidesScrollPane(overviewTable);

            splitPane.getItems().add(rsp);
            HBox.setHgrow(rsp, Priority.ALWAYS);

            ((AnchorPane) scene.getRoot()).getChildren().addAll(splitPane);
            // bind horizontalSpacer to parent anchorpane for resizing
            AnchorPane.setBottomAnchor(splitPane, 10.0);
            AnchorPane.setTopAnchor(splitPane, 10.0);
            AnchorPane.setRightAnchor(splitPane, 10.0);
            AnchorPane.setLeftAnchor(splitPane, 10.0);

            fxPanel.setScene(scene);
        });

        add(fxPanel, BorderLayout.CENTER);
    }

    //FIXME: 
    // workaround to keep using our swing droptarget impls on jfxpanels (see #144);
    // remove when DnD handling has been converted from swing to javafx
    // (looks like this route is only needed in CropProductionUnitPropertiesView).
    public void setAddNotifyListener(Consumer<JFXPanel> l) {
        addNotifyListener = l;
    }
}
