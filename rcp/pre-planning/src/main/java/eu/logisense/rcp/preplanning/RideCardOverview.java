/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.rcp.preplanning;

import eu.logisense.api.Ride;

import eu.logisense.api.session.client.Connections;
import eu.logisense.rcp.data.ObservableRide;
import eu.logisense.rcp.data.RideManager;
import eu.logisense.rcp.data.Savable;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
public class RideCardOverview extends ScrollPane {

    private VBox total = new VBox();
    private static RideCardOverviewColumn monday, tuesday, wednesday, thursday, friday;
    private HBox weekAdjusters;
    private HBox weekBox;
    private Button minusWeek, plusWeek;
    private static TextField weekNumberTF;
//    private static List<RideCardVM> list;

    SimpleObjectProperty<LocalDate> week = new SimpleObjectProperty<>(LocalDate.now());
    TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();

    private static final Logger log = LoggerFactory.getLogger(RideCardOverview.class);

    RideCardOverview() {
//        this.list = list;
        setHbarPolicy(ScrollBarPolicy.NEVER);

        addWeekAdjusters();
        addTable();

        setContent(total);

        Connections.onConnection(() -> {
            Platform.runLater(() -> {
                allocateRideCards();
                plusWeek.setDisable(false);
                minusWeek.setDisable(false);
            });
        });
    }

    private void addTable() {
        monday = new RideCardOverviewColumn(week, DayOfWeek.MONDAY);
        tuesday = new RideCardOverviewColumn(week, DayOfWeek.TUESDAY);
        wednesday = new RideCardOverviewColumn(week, DayOfWeek.WEDNESDAY);
        thursday = new RideCardOverviewColumn(week, DayOfWeek.THURSDAY);
        friday = new RideCardOverviewColumn(week, DayOfWeek.FRIDAY);

        weekBox = new HBox();
        weekBox.setSpacing(10.0);
        weekBox.getChildren().addAll(monday,
                new Separator(Orientation.VERTICAL), tuesday,
                new Separator(Orientation.VERTICAL), wednesday,
                new Separator(Orientation.VERTICAL), thursday,
                new Separator(Orientation.VERTICAL), friday);

        total.getChildren().add(weekBox);
        total.prefWidthProperty().bind(this.widthProperty());
        weekBox.prefWidthProperty().bind(total.widthProperty());

        monday.initWidthListener();
        tuesday.initWidthListener();
        wednesday.initWidthListener();
        thursday.initWidthListener();
        friday.initWidthListener();

    }

    private void addWeekAdjusters() {

        weekAdjusters = new HBox();
        minusWeek = new Button("<");
        plusWeek = new Button(">");
        weekNumberTF = new TextField();
        weekNumberTF.setText("week " + week.get().get(woy));

        minusWeek.setOnAction((ActionEvent event) -> {
            week.set(week.get().minusWeeks(1));
            weekNumberTF.setText("week " + week.get().get(woy));
            monday.setDate(week.get());
            tuesday.setDate(week.get());
            wednesday.setDate(week.get());
            thursday.setDate(week.get());
            friday.setDate(week.get());

            allocateRideCards();
        });

        plusWeek.setOnAction((ActionEvent event) -> {
            week.set(week.get().plusWeeks(1));
            weekNumberTF.setText("week " + week.get().get(woy));
            monday.setDate(week.get());
            tuesday.setDate(week.get());
            wednesday.setDate(week.get());
            thursday.setDate(week.get());
            friday.setDate(week.get());

            allocateRideCards();
        });

        // disable until connection available:
        plusWeek.setDisable(true);
        minusWeek.setDisable(true);

        weekNumberTF.setMouseTransparent(true);
        weekNumberTF.setFocusTraversable(false);
        weekNumberTF.setEditable(false);

        weekAdjusters.setSpacing(10.0);
        weekAdjusters.getChildren().addAll(minusWeek, weekNumberTF, plusWeek);
        weekAdjusters.setAlignment(Pos.TOP_CENTER);

        total.setSpacing(10.0);
        total.getChildren().add(weekAdjusters);
    }

    public static void allocateRideCards() {
        monday.getChildren().removeIf((n) -> n instanceof RideCard);
        tuesday.getChildren().removeIf((n) -> n instanceof RideCard);
        wednesday.getChildren().removeIf((n) -> n instanceof RideCard);
        thursday.getChildren().removeIf((n) -> n instanceof RideCard);
        friday.getChildren().removeIf((n) -> n instanceof RideCard);

        RideManager.getInstance().getRideCards().stream().forEach((r) -> {
            Ride model = r.getRide();
            RideCard rc = new RideCard(model);

            if (model.getEtd().toLocalDate().equals(monday.getDate())) {
                monday.getChildren().add(rc);
                rc.prefWidthProperty().bind(monday.prefWidthProperty());
            } else if (model.getEtd().toLocalDate().equals(tuesday.getDate())) {
                tuesday.getChildren().add(rc);
                rc.prefWidthProperty().bind(tuesday.prefWidthProperty());
            } else if (model.getEtd().toLocalDate().equals(wednesday.getDate())) {
                wednesday.getChildren().add(rc);
                rc.prefWidthProperty().bind(wednesday.prefWidthProperty());
            } else if (model.getEtd().toLocalDate().equals(thursday.getDate())) {
                thursday.getChildren().add(rc);
                rc.prefWidthProperty().bind(thursday.prefWidthProperty());
            } else if (model.getEtd().toLocalDate().equals(friday.getDate())) {
                friday.getChildren().add(rc);
                rc.prefWidthProperty().bind(friday.prefWidthProperty());
            }
            rc.refreshRideCardStyleBasedOnStatus();
            
            
        });
    }
}
