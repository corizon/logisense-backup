/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.rcp.preplanning;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjuster;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
public class RideCardOverviewColumn extends VBox {

    private SimpleObjectProperty<LocalDate> dateObject = new SimpleObjectProperty<>(LocalDate.now());
    private TemporalAdjuster dayOfWeek;
    private TextField dayHeader;
    
    
    
    RideCardOverviewColumn(SimpleObjectProperty<LocalDate> date, TemporalAdjuster dayOfWeek) {
        this.dateObject.set(date.get().with(dayOfWeek));
        this.dayOfWeek = dayOfWeek;
        
        
        
//        this.setPrefWidth(width);

        setSpacing(20.0);

        dayHeader = new TextField();
        dayHeader.setMouseTransparent(true);
        dayHeader.setFocusTraversable(false);
        dayHeader.setEditable(false);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        dayHeader.textProperty().bind(Bindings.createStringBinding(()
                -> dtf.format(date.get().with(dayOfWeek)), date)
        );

        getChildren().add(dayHeader);
    }
    
    void initWidthListener(){
        this.prefWidthProperty().bind(((HBox)getParent()).widthProperty().multiply(0.2));
    }

    public SimpleObjectProperty<LocalDate> getDateProperty() {
        return dateObject;
    }

    public LocalDate getDate() {
        return dateObject.get();
    }

    public void setDate(LocalDate date) {
        this.dateObject.set(date.with(dayOfWeek));
    }
}
