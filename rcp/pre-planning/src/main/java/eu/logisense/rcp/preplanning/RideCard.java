/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.preplanning;

import eu.logisense.api.Container;
import eu.logisense.api.Ride;
import eu.logisense.api.TransportStatus;
import eu.logisense.rcp.data.Savable;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import javafx.css.PseudoClass;
import java.util.Observable;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import static javax.ws.rs.core.Response.status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.scene.paint.Color;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.LocalDateTimeStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kenrik
 */
public class RideCard extends FlowPane {

    private Ride ride;

    private static final double VGAP = 0.0;
    private static final double HGAP = 0.0;

    private static final String RIDECARD_CLASS = "rideCard_class";
    private static final Logger log = LoggerFactory.getLogger(RideCard.class);

    RideCard(Ride ride) {
        this.ride = ride;
        //
        // margins
        //
        setVgap(VGAP);
        setHgap(HGAP);
        setPadding(Insets.EMPTY);

        initComponents(ride);
        setSizes();

    }

    TextField customer;
    TextField cont;
    TextField dgn;
    TextArea destinationCity;
    TextField rideId;
    TextField comment;
    TextField containers;
    FlowPane bPane;
    TextField tourOne;
    TextField loadDateOne;
    TextField tourTwo;

    private void setSizes() {
        //
        // row 1
        //
        customer.prefWidthProperty().bind(widthProperty().multiply(0.5).add(-2));
        customer.setPrefHeight(32.0);
        cont.prefWidthProperty().bind(widthProperty().multiply(0.25));
        cont.setPrefHeight(32.0);
        dgn.prefWidthProperty().bind(widthProperty().multiply(0.25));
        dgn.setPrefHeight(32.0);
        //
        // row 2
        // text + container
        //
        // text area left
        destinationCity.prefWidthProperty().bind(widthProperty().multiply(0.5).add(-2));
        destinationCity.setPrefHeight(64.0);
        // rest right
        rideId.prefWidthProperty().bind(widthProperty().multiply(0.247));
        rideId.setPrefHeight(32.0);
        comment.prefWidthProperty().bind(widthProperty().multiply(0.247));;
        comment.setPrefHeight(32.0);
        containers.prefWidthProperty().bind(widthProperty().multiply(0.5));
        containers.setPrefHeight(32.0);
        bPane.prefWidthProperty().bind(widthProperty().multiply(0.5));
        bPane.setPrefHeight(64.0);
        //
        // row 3
        //
        tourOne.prefWidthProperty().bind(widthProperty().multiply(0.75).add(-2));
        tourOne.setPrefHeight(32.0);
        loadDateOne.prefWidthProperty().bind(widthProperty().multiply(0.25));
        loadDateOne.setPrefHeight(32.0);
        //
        // row 4
        //
        tourTwo.prefWidthProperty().bind(widthProperty().add(-64.0));
        tourTwo.setPrefHeight(32.0);
    }

    private void initComponents(Ride ride) {
//        getStyleClass().add("RideCard");

        customer = new TextField("");
        cont = new TextField("Cont + R");
        cont.setDisable(true);
        dgn = new TextField("Dgn");
        dgn.setDisable(true);

        List<String> destinations = new ArrayList<>();
        //FIXME fill with destinations

        StringBuilder dsb = new StringBuilder();
        for (String s : destinations) {
            dsb.append(s);
            dsb.append("\n");
        }

        destinationCity = new TextArea(dsb.toString());
        rideId = new TextField(ride.getId());
        comment = new TextField();
        //FIXME add note field to ride
        comment.setPromptText("");

        StringBuilder csb = new StringBuilder();
        if (ride.getContainers() != null) {
            for (Container c : ride.getContainers()) {
                csb.append(c.getName());
                csb.append(" ");
            }
        }

        containers = new TextField(csb.toString());

        bPane = new FlowPane();
        bPane.setVgap(VGAP);
        bPane.setHgap(HGAP);

        bPane.getChildren().addAll(rideId, comment, containers);

        String batches = "";
        //FIXME fill with batches from orders

        tourOne = new TextField("tour : " + batches);
        if (ride.getEtd() == null) {
            ride.setEtd(LocalDateTime.now());
        }
        loadDateOne = new TextField(((String) Integer.toString(this.ride.getEtd().getDayOfMonth()) + "|"
                + ((String) Integer.toString(this.ride.getEtd().getMonthValue()))));
        tourTwo = new TextField("tour : " + batches);

        Button minusDate = new Button();
        minusDate.setPrefSize(32.0, 32.0);
        minusDate.setStyle("-fx-background-image: url('/eu/logisense/rcp/preplanning/move_left32.png')");

        Button plusDate = new Button();
        plusDate.setStyle("-fx-background-image: url('/eu/logisense/rcp/preplanning/move_right32.png')");
        plusDate.setPrefSize(32.0, 32.0);

        minusDate.setOnAction((ActionEvent e) -> {
            if (this.ride.getEtd().getDayOfWeek() == DayOfWeek.MONDAY) {
                this.ride.setEtd(this.ride.getEtd().minusDays(3));
            } else {
                this.ride.setEtd(this.ride.getEtd().minusDays(1));
            }
            RideCardOverview.allocateRideCards();
            System.out.println("ride.getLoadingDate() = " + this.ride.getEtd());
        });

        plusDate.setOnAction((ActionEvent e) -> {
            if (this.ride.getEtd().getDayOfWeek() == DayOfWeek.FRIDAY) {
                this.ride.setEtd(this.ride.getEtd().plusDays(3));
            } else {
                this.ride.setEtd(this.ride.getEtd().plusDays(1));
            }
            RideCardOverview.allocateRideCards();
            System.out.println("ride.getLoadingDate() = " + this.ride.getEtd());
        });

        getChildren().addAll(customer, cont, dgn, destinationCity, bPane, tourOne, loadDateOne, tourTwo, minusDate, plusDate);

        // set all TextField to not be selectable with mouse
        for (Node s : getChildren()) {
            if (s instanceof TextField) {
                s.setMouseTransparent(true);
                s.setFocusTraversable(false);
                ((TextField) s).setEditable(false);
            }
            s.getStyleClass().add(RIDECARD_CLASS);

        }
        for (Node s : bPane.getChildren()) {
            if (s instanceof TextField) {
                s.setMouseTransparent(true);
                s.setFocusTraversable(false);
                ((TextField) s).setEditable(false);
            }

            s.getStyleClass().add(RIDECARD_CLASS);
        }
        destinationCity.setMouseTransparent(true);
        destinationCity.setFocusTraversable(false);
        destinationCity.setEditable(false);

    }

    public void refreshRideCardStyleBasedOnStatus() {

        PseudoClass statusNew = PseudoClass.getPseudoClass("statusNew");
        PseudoClass statusLoaded = PseudoClass.getPseudoClass("statusLoaded");
        PseudoClass statusPlanningInAdvance = PseudoClass.getPseudoClass("statusPlanningInAdvance");

        for (Node s : bPane.getChildren()) {

            log.debug("Is new? {}", TransportStatus.NEW.equals(ride.getStatus()));

            boolean isNew = TransportStatus.NEW.equals(ride.getStatus());
            s.pseudoClassStateChanged(statusNew, isNew);

            log.debug("Is loaded? {}", TransportStatus.LOADED.equals(ride.getStatus()));
            boolean isLoaded = TransportStatus.LOADED.equals(ride.getStatus());

            s.pseudoClassStateChanged(statusLoaded, TransportStatus.LOADED.equals(ride.getStatus()));
            if (!isNew && !isLoaded) {
                long planningGapInDays = ChronoUnit.DAYS.between(ride.getEtd(), LocalDateTime.now());
                log.debug("Is  planningGapInDays < 7? {}", planningGapInDays < 7);
                s.pseudoClassStateChanged(statusPlanningInAdvance, planningGapInDays < 7);
            }

        }
    }
}
