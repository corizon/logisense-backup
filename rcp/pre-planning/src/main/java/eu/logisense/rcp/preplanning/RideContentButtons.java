/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.preplanning;

import eu.logisense.api.Container;
import eu.logisense.api.Creditor;
import eu.logisense.api.Ride;
import eu.logisense.api.TransportStatus;
import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableContainer;
import eu.logisense.rcp.data.Savable;
import eu.logisense.rcp.data.ObservableRide;
import eu.logisense.rcp.data.RideManager;
import java.util.ArrayList;
import java.util.UUID;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author kenrik
 */
@NbBundle.Messages({"planning screen create ride tooltip=create new ride", "planning screen create container tooltip=create new container"})
public class RideContentButtons extends VBox {

    private final Button addRideContentButton = new Button();
    private final Button addContainerButton = new Button();

    private Ride ride;
    private ObservableRide observableRide;

    public RideContentButtons(RidesScrollPane ridesPane) {
        setComponentSpecs();
        initActionListeners(ridesPane);

        setSpacing(40.0);
        //
        // VBoxes to separate Ride and Container buttons
        //
        getChildren().addAll(new VBox(addRideContentButton), new VBox(addContainerButton));
    }

    private void setComponentSpecs() {
        addContainerButton.setPrefHeight(64.0);
        addContainerButton.setPrefWidth(64.0);
        addContainerButton.setStyle("-fx-background-image: url('/eu/logisense/rcp/preplanning/container_add64.png')");
        addContainerButton.setTooltip(new Tooltip(Bundle.planning_screen_create_container_tooltip()));

        addRideContentButton.setPrefHeight(64.0);
        addRideContentButton.setPrefWidth(64.0);
        addRideContentButton.setMinWidth(64.0);
        addRideContentButton.setStyle("-fx-background-image: url('/eu/logisense/rcp/preplanning/ride_add64.png')");
        addRideContentButton.setTooltip(new Tooltip(Bundle.planning_screen_create_ride_tooltip()));

    }

    private void initActionListeners(RidesScrollPane ridesPane) {
        addRideContentButton.setOnAction((ActionEvent event) -> {
            try {
                //
                // create new RideCardVM
                //
                String id = UUID.randomUUID().toString();
                ride = Ride.builder()
                        .id(id)
                        .status(TransportStatus.NEW)
                        .creditor(Creditor.builder()
                                .currency("EUR")
                                .build())
                        .containers(new ArrayList<>())
                        .build();
                observableRide = new ObservableRide(ride, Savable.ClientStatus.UNSAVED);
                //
                // add to datamanager
                //
                RideManager.getInstance().newRide(observableRide);

            } catch (NoSuchMethodException ex) {
                Exceptions.printStackTrace(ex);
            }

        });

        addContainerButton.setOnAction((ActionEvent e) -> {
            //
            // create new Container
            //
            Container container = Container.builder()
                    .width(0.0)
                    .length(0.0)
                    .height(0.0)
                    .maxWeight(0.0)
                    .id(UUID.randomUUID().toString())
                    .build();

            RideContent rideContent = (RideContent) ridesPane.getRidesAccordion().getExpandedPane().getContent();

            //
            // add to ridecard and table
            //
            ObservableContainer oc=  rideContent.getObservableRide().addContainer(container);
            rideContent.addContainerCard(oc);

            //
            // Disable button when size gets to 2 containers
            //
            if (rideContent.getObservableRide().getRide().getContainers().size() == 2) {
                addContainerButton.setDisable(true);
            }
        });

        //
        // Disable button when selection changed to null or selection already has 2 containers 
        //
        ridesPane.getRidesAccordion().expandedPaneProperty().addListener((ObservableValue<? extends TitledPane> observable, TitledPane oldValue, TitledPane newValue) -> {
            if (newValue == null
                    || ((RideContent) newValue.getContent()).getObservableRide().getRide().getContainers() != null
                    && ((RideContent) newValue.getContent()).getObservableRide().getRide().getContainers().size() == 2) {
                addContainerButton.setDisable(true);
            } else {
                addContainerButton.setDisable(false);
            }
        });

    }
}
