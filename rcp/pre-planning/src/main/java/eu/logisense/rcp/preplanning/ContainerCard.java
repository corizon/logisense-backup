/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.rcp.preplanning;

import eu.logisense.api.ContainerType;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import eu.logisense.rcp.data.ObservableContainer;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.util.DateTimePicker;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.NumberStringConverter;
import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationResult;
import org.openide.util.NbBundle.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
@Messages({
    "containerCard container name label=container",
    "containerCard total volume of container label=total volume",
    "containerCard total weight of container label=total weight",
    "containerCard total loadingmeters of container label=total loading meters",
    "containerCard volume error label=volume exceeds maximum",
    "containerCard weight error label=weight exceeds maximum",
    "containerCard loading meters error label=loading meters exceed maximum",
    "containerCard type of container label=Container Type",
    "containerCard width of container label=Width",
    "containerCard height of container label=Height",
    "containerCard length of container label=Length",
    "containerCard max weight of container label=Max weight",
    "containerCard validation message when no match departure and ETA=departure and ETA doesnt match",
    "containerCard validator message container type=please select container type",
    "containerCard validator message width=please set width",
    "containerCard validator message height=please set height",
    "containerCard validator message length=please set length",
    "containerCard validator message weight=please set weight"
})
public class ContainerCard extends VBox {

    private static final double STANDARD_HEIGHT = 160.0;
    private static final double LABEL_WIDTH = 140.0;
    private static final double SPACING = 5.0;
    private static final double COMPONENT_SPACING = 10.0;

    private final Button addToRide = new Button();
    private final Button removeFromRide = new Button();
    private final VBox buttonBox = new VBox();

    // Top side of the table
    private final ComboBox<String> pickTypeComboBox = new ComboBox();
    private final TextField containerWidthTF = new TextField();
    private final TextField containerHeightTF = new TextField();
    private final TextField containerLengthTF = new TextField();
    private final TextField containerMaxWeightTF = new TextField();

    // Center
    private final PlanRidesTable<ObservableShipment> rideTable = new PlanRidesTable<>(Bundle.containerCard_container_name_label());
    private final HBox buttonsAndTableHBox = new HBox(SPACING, buttonBox, rideTable);

    // Total Text Fields Bottom side of the table
    private final TextField totalShipmentsVolumeTF = new TextField();
    private final TextField totalShipmentsWeightTF = new TextField();
    private final TextField totalShipmentsLoadingMetersTF = new TextField();
    private final Label totalShipmentsVolumeLabel = new Label(Bundle.containerCard_total_volume_of_container_label());
    private final Label totalShipmentsWeightLabel = new Label(Bundle.containerCard_total_weight_of_container_label());
    private final Label totalShipmentsLoadingMetersLabel = new Label(Bundle.containerCard_total_loadingmeters_of_container_label());
    private final Label volumeErrorLabel = new Label(Bundle.containerCard_volume_error_label());
    private final Label weightErrorLabel = new Label(Bundle.containerCard_weight_error_label());
    private final Label loadingMetersErrorLabel = new Label(Bundle.containerCard_loading_meters_error_label());

    private final ObservableContainer container;
    private static final Logger log = LoggerFactory.getLogger(ContainerCard.class);

    public ContainerCard(ObservableContainer container) {
        this.container = container;

        setSpacing(COMPONENT_SPACING);

        //
        // <- and -> buttons
        //
        addToRide.setPrefWidth(32.0);
        addToRide.setMinWidth(32.0);
        addToRide.setPrefHeight(32.0);
        addToRide.setStyle("-fx-background-image: url('/eu/logisense/rcp/preplanning/right32.png')");

        removeFromRide.setPrefWidth(32.0);
        removeFromRide.setMinWidth(32.0);
        removeFromRide.setPrefHeight(32.0);
        removeFromRide.setStyle("-fx-background-image: url('/eu/logisense/rcp/preplanning/left32.png')");

        buttonBox.getChildren().addAll(addToRide, removeFromRide);
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(10.0);

        //
        // shipments list for ride
        //
        rideTable.setItems(container.getShipmentsList());
        rideTable.setPrefHeight(STANDARD_HEIGHT);
        rideTable.setIndexColumnVisible(true);
        VBox.setVgrow(rideTable, Priority.SOMETIMES);

        //
        // totals and error messages for them
        //
        totalShipmentsVolumeTF.setEditable(false);
        totalShipmentsVolumeLabel.setPrefWidth(LABEL_WIDTH);
        volumeErrorLabel.setTextFill(Color.RED);
        volumeErrorLabel.setVisible(false);

        totalShipmentsLoadingMetersTF.setEditable(false);
        totalShipmentsLoadingMetersLabel.setPrefWidth(LABEL_WIDTH);
        loadingMetersErrorLabel.setTextFill(Color.RED);
        loadingMetersErrorLabel.setVisible(false);

        totalShipmentsWeightTF.setEditable(false);
        totalShipmentsWeightLabel.setPrefWidth(LABEL_WIDTH);
        weightErrorLabel.setTextFill(Color.RED);
        weightErrorLabel.setVisible(false);

        VBox totalHBox = new VBox(SPACING,
                new HBox(SPACING, totalShipmentsWeightLabel, totalShipmentsWeightTF, weightErrorLabel),
                new HBox(SPACING, totalShipmentsLoadingMetersLabel, totalShipmentsLoadingMetersTF, loadingMetersErrorLabel),
                new HBox(SPACING, totalShipmentsVolumeLabel, totalShipmentsVolumeTF, volumeErrorLabel));

        //
        // combobox to choose Container Type
        //
        pickTypeComboBox.setItems(FXCollections.observableArrayList(
                ContainerType.SEMITRAILER.getDisplayName(),
                ContainerType.JK_CONTAINER.getDisplayName(),
                ContainerType.CUSTOM.getDisplayName()));
        pickTypeComboBox.setPrefWidth(LABEL_WIDTH);

        pickTypeComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                String selected = (String) t1;
                if (ContainerType.SEMITRAILER.getDisplayName().equals(selected)) {

                    containerWidthTF.setDisable(true);
                    containerHeightTF.setDisable(true);
                    containerLengthTF.setDisable(true);
                    container.widthProperty().setValue(ContainerType.SEMITRAILER.getWidth());
                    container.heightProperty().setValue(ContainerType.SEMITRAILER.getHeigth());
                    container.lengthProperty().setValue(ContainerType.SEMITRAILER.getLength());
                    container.maxWeightProperty().setValue(ContainerType.SEMITRAILER.getMaxWeight());

                } else if (ContainerType.JK_CONTAINER.getDisplayName().equals(selected)) {

                    containerWidthTF.setDisable(true);
                    containerHeightTF.setDisable(true);
                    containerLengthTF.setDisable(true);
                    container.widthProperty().setValue(ContainerType.JK_CONTAINER.getWidth());
                    container.heightProperty().setValue(ContainerType.JK_CONTAINER.getHeigth());
                    container.lengthProperty().setValue(ContainerType.JK_CONTAINER.getLength());
                    container.maxWeightProperty().setValue(ContainerType.JK_CONTAINER.getMaxWeight());

                } else if (ContainerType.CUSTOM.getDisplayName().equals(selected)) {

                    containerWidthTF.setDisable(false);
                    containerHeightTF.setDisable(false);
                    containerLengthTF.setDisable(false);
                }
                // Update load meter total in case of dimension change due to the change of container type
                log.debug("updateTotals because change in container type");
            }
        });

        //
        // current container specifics
        //
        containerWidthTF.setDisable(true);
        containerHeightTF.setDisable(true);
        containerLengthTF.setDisable(true);

        //
        // bind to parent's width
        //
        containerWidthTF.prefWidthProperty().bind(this.widthProperty().subtract(LABEL_WIDTH + SPACING * 3).divide(4));
        containerHeightTF.prefWidthProperty().bind(this.widthProperty().subtract(LABEL_WIDTH + SPACING * 3).divide(4));
        containerLengthTF.prefWidthProperty().bind(this.widthProperty().subtract(LABEL_WIDTH + SPACING * 3).divide(4));
        containerMaxWeightTF.prefWidthProperty().bind(this.widthProperty().subtract(LABEL_WIDTH + SPACING * 3).divide(4));

        VBox containerType1Box = new VBox(new Label(Bundle.containerCard_type_of_container_label()), pickTypeComboBox);
        VBox containerType2Box = new VBox(new Label(Bundle.containerCard_length_of_container_label()), containerLengthTF);
        VBox containerType3Box = new VBox(new Label(Bundle.containerCard_width_of_container_label()), containerWidthTF);
        VBox containerType4Box = new VBox(new Label(Bundle.containerCard_height_of_container_label()), containerHeightTF);
        VBox containerType5Box = new VBox(new Label(Bundle.containerCard_max_weight_of_container_label()), containerMaxWeightTF);

        // HBox with container type combo
        HBox containerTypeHeaderHBox = new HBox(SPACING,
                containerType1Box,
                containerType2Box,
                containerType3Box,
                containerType4Box,
                containerType5Box);

        getChildren().addAll(containerTypeHeaderHBox, buttonsAndTableHBox, totalHBox, new Separator(Orientation.HORIZONTAL));

        setBindings();
        setValidationBindings();
    }

    private void setBindings() {
        Bindings.bindBidirectional(totalShipmentsVolumeTF.textProperty(), container.totalShipmentsVolumeProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(totalShipmentsLoadingMetersTF.textProperty(), container.totalShipmentsLoadingMetersProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(totalShipmentsWeightTF.textProperty(), container.totalShipmentsWeightProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(containerMaxWeightTF.textProperty(), container.maxWeightProperty(), new DoubleStringConverter());
        Bindings.bindBidirectional(containerWidthTF.textProperty(), container.widthProperty(), new DoubleStringConverter());
        Bindings.bindBidirectional(containerHeightTF.textProperty(), container.heightProperty(), new DoubleStringConverter());
        Bindings.bindBidirectional(containerLengthTF.textProperty(), container.lengthProperty(), new DoubleStringConverter());

        //
        // listeners instead of bindings because BooleanBinding and JavaBeanProperty
        // don't work together, you'd have to create SimpleDoubleProperties
        // for each value and bind those
        //
        SimpleDoubleProperty maxVolume = new SimpleDoubleProperty();
        maxVolume.bind(container.maxVolumeProperty());
        volumeErrorLabel.visibleProperty().bind(Bindings.greaterThan(container.totalShipmentsVolumeProperty(), maxVolume));

        SimpleDoubleProperty length = new SimpleDoubleProperty();
        length.bind(container.lengthProperty());
        loadingMetersErrorLabel.visibleProperty().bind(Bindings.greaterThan(container.totalShipmentsLoadingMetersProperty(), length));

        SimpleDoubleProperty maxWeight = new SimpleDoubleProperty();
        maxWeight.bind(container.maxWeightProperty());
        weightErrorLabel.visibleProperty().bind(Bindings.greaterThan(container.totalShipmentsWeightProperty(), maxWeight));
    }

    void configureButtonActions(PlanRidesTable<ObservableShipment> overviewTable, DateTimePicker departureDatePicker) {

        //
        // selection model for total / master list
        //
        TableView.TableViewSelectionModel<ObservableShipment> totalSM = overviewTable.getSelectionModel();

        //
        // selection model for ride
        //
        TableView.TableViewSelectionModel<ObservableShipment> rideSM = rideTable.getSelectionModel();

        addToRide.setOnAction((ActionEvent event)
                -> {
            if (!totalSM.isEmpty()) {

                ObservableList<ObservableShipment> selectedItems = FXCollections.observableArrayList(totalSM.getSelectedItems());

                selectedItems.stream().forEach((s) -> {
                    // sometimes selected items contains nulls
                    if (s != null) {
                        log.debug("adding shipment to ride");
                        container.getShipmentsList().add(s);
                        s.containerProperty().setValue(
                                Resource.buildResource()
                                .id(container.getContainer().getId())
                                .href(container.getContainer().getHref())
                                .name(container.getContainer().getName())
                                .build()
                        );
                        updateIndexes(rideTable);
                    }
                });

            }

            departureDatePickerCheck(departureDatePicker);

        });

        removeFromRide.setOnAction((ActionEvent event)
                -> {
            if (container.getShipmentsList().size() > 0 && !rideSM.isEmpty()) {
                ObservableList<ObservableShipment> selectedItems = FXCollections.observableArrayList(rideSM.getSelectedItems());
                // sometimes selected items contains nulls
                selectedItems.stream().forEach((s) -> {
                    if (s != null) {
                        log.debug("removing shipment {} from ride", s.getShipment());
                        container.getShipmentsList().remove(s);
                        s.containerProperty().setValue(null);
                        updateIndexes(rideTable);
                    }
                });
            }

            departureDatePickerCheck(departureDatePicker);
        });
    }

    private void departureDatePickerCheck(DateTimePicker departureDatePicker) {

        ValidationSupport support = new ValidationSupport();

        Validator<java.time.LocalDate> validator = new ValidatorImpl(departureDatePicker);

        support.registerValidator(departureDatePicker, true, validator);
    }

    private void updateIndexes(PlanRidesTable<ObservableShipment> rideTable) {
        rideTable.getItems().stream().forEach((os) -> {
            ((ObservableShipment) os).indexProperty().setValue(rideTable.getItems().indexOf(os) + 1);
        });
    }

    private void setValidationBindings() {
        // https://bitbucket.org/controlsfx/controlsfx/issues/539/multiple-dialog-fields-with-validation
        Platform.runLater(() -> {

            //
            // add validator to nodes, shows small error icon in bottomleft corner
            // when empty
            //
            ValidationSupport validationSupport = new ValidationSupport();
            validationSupport.registerValidator(pickTypeComboBox, Validator.createEmptyValidator(Bundle.containerCard_validator_message_container_type()));
            validationSupport.registerValidator(containerWidthTF, Validator.createEmptyValidator(Bundle.containerCard_validator_message_width()));
            validationSupport.registerValidator(containerHeightTF, Validator.createEmptyValidator(Bundle.containerCard_validator_message_height()));
            validationSupport.registerValidator(containerLengthTF, Validator.createEmptyValidator(Bundle.containerCard_validator_message_length()));
            validationSupport.registerValidator(containerMaxWeightTF, Validator.createEmptyValidator(Bundle.containerCard_validator_message_weight()));

        });
    }

    private class ValidatorImpl implements Validator<LocalDate> {

        private final DateTimePicker departureDatePicker;

        public ValidatorImpl(DateTimePicker departureDatePicker) {
            this.departureDatePicker = departureDatePicker;
        }

        @Override
        public ValidationResult apply(Control control, java.time.LocalDate value) {

            Iterator<ObservableShipment> it = container.getShipmentsList().iterator();
            boolean riseUpWarning = false;

            while (!riseUpWarning && it.hasNext()) {
                Shipment shipment = it.next().getShipment();
                if (shipment.getUnloadLocation() != null && shipment.getUnloadLocation().getTimeFrameTo()!= null
                        && departureDatePicker.getDateTimeValue() != null) {
                    LocalDateTime eta = shipment.getUnloadLocation().getTimeFrameTo();
                    LocalDateTime departure = departureDatePicker.getDateTimeValue();
                    if (eta.compareTo(departure) > 0) {
                        // at least one shipment ETA doesnt match with ride departure
                        riseUpWarning = true;
                    }
                }

            }

            return ValidationResult.fromMessageIf(control, Bundle.containerCard_validation_message_when_no_match_departure_and_ETA(), Severity.ERROR, riseUpWarning);
        }
    }

}
