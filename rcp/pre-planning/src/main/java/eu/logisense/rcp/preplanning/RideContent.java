/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.rcp.preplanning;

import eu.logisense.api.Container;
import eu.logisense.api.Creditor;
import eu.logisense.rcp.data.ClientManager;
import eu.logisense.rcp.data.ObservableClient;
import eu.logisense.rcp.data.ObservableContainer;
import eu.logisense.rcp.data.ObservableShipment;
import eu.logisense.rcp.data.ObservableRide;
import eu.logisense.rcp.util.AutoCompleteComboBoxListener;
import eu.logisense.rcp.util.DateTimePicker;
import eu.logisense.rcp.util.DebitorCreditorBox;
import eu.logisense.rcp.util.MoneyStringConverter;
import java.beans.PropertyChangeListener;
import java.util.Currency;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
@Messages({
    "ride content departure label=departure",
    "ride content creditor label=creditor",
    "ride content license plate prompt=license plate"
})
public class RideContent extends VBox {
    
    private static final double LABEL_WIDTH = 100.0;
    private static final double SPACING = 5.0;
    private static final double COMPONENT_SPACING = 10.0;
    
    private DateTimePicker departureDateTimePicker = new DateTimePicker();
    private Label departureLabel = new Label(Bundle.ride_content_departure_label());
    
    private TextField licensePlateTF = new TextField();
    private Label licensePlateLabel = new Label(Bundle.ride_content_license_plate_prompt());
    
    private PlanRidesTable<ObservableShipment> overviewTable;
    private ObservableRide observableRide;
    private DebitorCreditorBox creditor = new DebitorCreditorBox(Bundle.ride_content_creditor_label());
    
    private static final Logger log = LoggerFactory.getLogger(RideCardOverview.class);
    
    public RideContent(ObservableRide observableRide, PlanRidesTable<ObservableShipment> overviewTable) {
        this.overviewTable = overviewTable;
        this.observableRide = observableRide;
        initcomponents();

        //
        // add containerCards for linked containers, if any
        //
        if (observableRide.getContainers() != null) {
            observableRide.getContainers().stream().forEach((c) -> {
                addContainerCard(c);
            });
        }
        
        setValidationBindings();
    }
    
    private void initcomponents() {
        setSpacing(COMPONENT_SPACING);
        setPadding(new Insets(SPACING));
        getStyleClass().add("RideContent");
        setAlignment(Pos.CENTER_RIGHT);
        
        departureLabel.setPrefWidth(LABEL_WIDTH);
        licensePlateTF.setPromptText(Bundle.ride_content_license_plate_prompt());
        licensePlateLabel.setPrefWidth(LABEL_WIDTH);
        
        creditor.getClientNameComboBox().setItems(ClientManager.getInstance().getClients());

        //
        // Looks weird but this adds the autocomplete functionality to the comboboxes
        // FIXME: ugly code
        //
        new AutoCompleteComboBoxListener<>(creditor.getClientNameComboBox());
        
        getChildren().addAll(new VBox(SPACING,
                new HBox(SPACING, departureLabel, departureDateTimePicker),
                new HBox(SPACING, licensePlateLabel, licensePlateTF),
                creditor),
                new Separator(Orientation.HORIZONTAL));
        
        setBindings();
    }
    
    private void setBindings() {
        //
        // update DateTimePicker and bind value to savableRideCard loading date
        //
        if (observableRide.getRide().getEtd() != null) {
            departureDateTimePicker.setDateTimeValue(observableRide.getRide().getEtd());
        }
        departureDateTimePicker.dateTimeValueProperty().bindBidirectional(observableRide.etdProperty());
        
        departureDateTimePicker.addEventFilter(EventType.ROOT, new EventHandler() {
            
            @Override
            public void handle(Event t) {
                
                log.debug("reallocate RideCards");
                RideCardOverview.allocateRideCards();
            }
        });
        
        licensePlateTF.textProperty().bindBidirectional(observableRide.licensePlateProperty());
        
        if (observableRide.creditorNameProperty() != null) {
            creditor.getClientNameComboBox().getSelectionModel().select(observableRide.creditorNameProperty().getValue());
        }
        if (observableRide.currencyProperty() != null) {
            creditor.getCurrencyComboBox().getSelectionModel().select(observableRide.currencyProperty().getValue());
        }
        
        creditor.getClientNameComboBox().getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            if (newValue instanceof ObservableClient) {
                observableRide.creditorIdProperty().setValue(((ObservableClient) newValue).getClient().getId());
            }
        });
        
        creditor.getCurrencyComboBox().getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            if (newValue instanceof Currency) {
                observableRide.currencyProperty().setValue(((Currency) newValue).toString());
            }
        });
        
        Bindings.bindBidirectional(creditor.getPriceTF().textProperty(),
                observableRide.priceProperty(), new MoneyStringConverter());
        
    }
    
    final void addContainerCard(ObservableContainer container) {
        ContainerCard containerCard;
        containerCard = new ContainerCard(container);
        containerCard.configureButtonActions(overviewTable, departureDateTimePicker);
        getChildren().add(getChildren().size(), containerCard);
        
    }
    
    public ObservableRide getObservableRide() {
        return observableRide;
    }
    
    private void setValidationBindings() {
        // https://bitbucket.org/controlsfx/controlsfx/issues/539/multiple-dialog-fields-with-validation
        Platform.runLater(() -> {

            //
            // add validator to nodes, shows small error icon in bottomleft corner
            // when empty
            //
            ValidationSupport validationSupport = new ValidationSupport();
            validationSupport.registerValidator(creditor.getClientNameComboBox(), Validator.createEmptyValidator(""));
            validationSupport.registerValidator(creditor.getPriceTF(), Validator.createEmptyValidator(""));
            validationSupport.registerValidator(creditor.getCurrencyComboBox(), Validator.createEmptyValidator(""));
            
        });
    }
}
