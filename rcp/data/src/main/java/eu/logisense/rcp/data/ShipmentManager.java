/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public interface ShipmentManager {

    void deleteShipment(String id);


    ObservableShipment getShipment(String id);

    ObservableShipment getShipmentByHref(String href);

    SaveManager getShipmentSaveManager();

    //TODO accept predicate as filter?
    ObservableList<ObservableShipment> getShipments();

    /**
     * Get an observable list of shipments for one or more statuses
     *
     * @param forStatus
     * @return
     */
    ObservableList<ObservableShipment> getShipmentsForStatus(TransportStatus... forStatus) throws NoSuchMethodException;

    /**
     * register new shipment, not saved to server yet
     *
     * @param observableShipment
     */
    void newShipment(ObservableShipment observableShipment);

    ObservableShipment postShipment(ObservableShipment template) throws NoSuchMethodException;

    void removeShipment(ObservableShipment observableShipment);

    void saveShipment(ObservableShipment observableShipment);

    ObservableShipment updateShipment(ObservableShipment template) throws NoSuchMethodException;
    
}
