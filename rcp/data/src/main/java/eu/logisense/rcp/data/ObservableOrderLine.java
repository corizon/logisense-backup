/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Container;
import eu.logisense.api.OrderLine;
import eu.logisense.api.OrderLineType;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanProperty;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
public class ObservableOrderLine {

    private final OrderLine orderLine;

    private final JavaBeanProperty<String> descriptionProperty;
    private final JavaBeanProperty<String> remarksProperty;
    private final JavaBeanProperty<Integer> amountProperty;
    private final JavaBeanProperty<OrderLineType> typeProperty;
    private final JavaBeanProperty<Boolean> swapPalletsProperty;
    private final JavaBeanProperty<Double> lengthProperty;
    private final JavaBeanProperty<Double> widthProperty;
    private final JavaBeanProperty<Double> heightProperty;
    private final JavaBeanProperty<Double> weightProperty;
    private final SimpleDoubleProperty volumeProperty;
    private final JavaBeanProperty<Boolean> stackableProperty;
    private final JavaBeanProperty<Double> podAmountProperty;
    private final JavaBeanProperty<String> podCurrencyProperty;
    private final SimpleDoubleProperty loadingMetersProperty;

    private final IntegerProperty simpleAmount = new SimpleIntegerProperty();

    private final DoubleProperty simpleLength = new SimpleDoubleProperty();

    private final DoubleProperty simpleWidth = new SimpleDoubleProperty();

    private final DoubleProperty simpleHeight = new SimpleDoubleProperty();

    private final DoubleBinding loadingMetersBinding = simpleLength.multiply(simpleWidth)
            .multiply(simpleAmount)
            .divide(Container.STANDARD_WIDTH);

    private final DoubleBinding volumeBinding = simpleLength.multiply(simpleWidth)
            .multiply(simpleHeight)
            .multiply(simpleAmount);

    public ObservableOrderLine(OrderLine orderLine) throws NoSuchMethodException {
        this.orderLine = orderLine;

        this.descriptionProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_DESCRIPTION).build();
        this.remarksProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_REMARKS).build();
        this.amountProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_AMOUNT).build();
        this.typeProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_TYPE).build();
        this.swapPalletsProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_SWAP_PALLETS).build();
        this.lengthProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_LENGTH).build();
        this.widthProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_WIDTH).build();
        this.heightProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_HEIGHT).build();
        this.weightProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_WEIGHT).build();
        this.stackableProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_STACKABLE).build();
        this.podAmountProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT).build();
        this.podCurrencyProperty = JavaBeanObjectPropertyBuilder.create().bean(orderLine).name(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY).build();
        this.loadingMetersProperty = new SimpleDoubleProperty();
        this.volumeProperty = new SimpleDoubleProperty();

        setVolumeBinding();
    }

    private void setVolumeBinding() {
        //
        // create new SimpleProperties to be able to bind to volume, 
        // have not found a different solution besides actionlisteners
        // on each separate value.
        //
        simpleAmount.bind(amountProperty);
        simpleLength.bind(lengthProperty);
        simpleWidth.bind(widthProperty);
        simpleHeight.bind(heightProperty);
        volumeProperty.bind(volumeBinding);
        loadingMetersProperty.bind(loadingMetersBinding);

    }

    public JavaBeanProperty<String> descriptionProperty() {
        return descriptionProperty;
    }

    public JavaBeanProperty<String> remarksProperty() {
        return remarksProperty;
    }

    public JavaBeanProperty<Integer> amountProperty() {
        return amountProperty;
    }

    public SimpleDoubleProperty loadingMetersProperty() {
        return loadingMetersProperty;
    }

    public double getLoadingMeters() {
        return loadingMetersProperty.get();
    }

    public final int getAmount() {
        if (amountProperty.getValue() != null) {
            return amountProperty.getValue();
        } else {
            return 0;
        }
    }

    public JavaBeanProperty<OrderLineType> typeProperty() {
        return typeProperty;
    }

    public JavaBeanProperty<Boolean> swapPalletsProperty() {
        return swapPalletsProperty;
    }

    public SimpleDoubleProperty volumeProperty() {
        return volumeProperty;
    }

    public final double getVolume() {
        if (volumeProperty.getValue() != null) {
            return volumeProperty.getValue();
        } else {
            return 0.0;
        }
    }

    public JavaBeanProperty<Double> lengthProperty() {
        return lengthProperty;
    }

    public final double getLength() {
        if (lengthProperty.getValue() != null) {
            return lengthProperty.getValue();
        } else {
            return 0.0;
        }
    }

    public JavaBeanProperty<Double> widthProperty() {
        return widthProperty;
    }

    public final double getWidth() {
        if (widthProperty.getValue() != null) {
            return widthProperty.getValue();
        } else {
            return 0.0;
        }
    }

    public JavaBeanProperty<Double> heightProperty() {
        return heightProperty;
    }

    public final double getHeight() {
        if (heightProperty.getValue() != null) {
            return heightProperty.getValue();
        } else {
            return 0.0;
        }
    }

    public JavaBeanProperty<Double> weightProperty() {
        return weightProperty;
    }

    public final double getWeight() {
        if (weightProperty.getValue() != null) {
            return weightProperty.getValue();
        } else {
            return 0.0;
        }
    }

    public JavaBeanProperty<Boolean> stackableProperty() {
        return stackableProperty;
    }

    public JavaBeanProperty<Double> podAmountProperty() {
        return podAmountProperty;
    }

    public JavaBeanProperty<String> podCurrencyProperty() {
        return podCurrencyProperty;
    }

    public OrderLine getOrderLine() {
        return orderLine;
    }

}
