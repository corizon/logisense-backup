/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Location;
import eu.logisense.api.Status;
import java.io.IOException;
import java.util.Objects;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanProperty;
import org.openide.util.NbBundle.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@Messages({
    "locationNamePrefix=location"
})
public class ObservableLocation extends Savable {
    
    private static final Logger log = LoggerFactory.getLogger(ObservableLocation.class);
    
    private final Location location;
    
    private final JavaBeanProperty<String> idProperty;
    private final JavaBeanProperty<String> LocationNameProperty;
    private final JavaBeanProperty<String> addressProperty;
    private final JavaBeanProperty<String> cityProperty;
    private final JavaBeanProperty<String> countryProperty;
    private final JavaBeanProperty<String> postalcodeProperty;
    private final JavaBeanProperty<String> phoneProperty;
    private final JavaBeanProperty<String> emailProperty;
    private final JavaBeanProperty<String> externalIdProperty;
    
    public ObservableLocation(Status status, String id, Location location) throws NoSuchMethodException {
        super(status, id);
        this.location = location;
        
        this.idProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_ID).build();
        this.LocationNameProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_NAME).build();
        this.addressProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_ADDRESS).build();
        this.cityProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_CITY).build();
        this.countryProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_COUNTRY).build();
        this.postalcodeProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_POSTALCODE).build();
        this.phoneProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_PHONE).build();
        this.emailProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_EMAIL).build();
        this.externalIdProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_EXTERNAL_ID).build();
        
        this.idProperty.addListener(updateListener);
        this.LocationNameProperty.addListener(updateListener);
        this.addressProperty.addListener(updateListener);
        this.cityProperty.addListener(updateListener);
        this.countryProperty.addListener(updateListener);
        this.postalcodeProperty.addListener(updateListener);
        this.phoneProperty.addListener(updateListener);
        this.emailProperty.addListener(updateListener);
    }
    
    public ObservableLocation(Location location) throws NoSuchMethodException {
        super(location.getStatus(), location.getId());
        Objects.requireNonNull(location);
        this.location = location;
        
        this.idProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_ID).build();
        this.LocationNameProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_NAME).build();
        this.addressProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_ADDRESS).build();
        this.cityProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_CITY).build();
        this.countryProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_COUNTRY).build();
        this.postalcodeProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_POSTALCODE).build();
        this.phoneProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_PHONE).build();
        this.emailProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_EMAIL).build();
        this.externalIdProperty = JavaBeanObjectPropertyBuilder.create().bean(location).name(Location.PROP_EXTERNAL_ID).build();
        
        this.idProperty.addListener(updateListener);
        this.LocationNameProperty.addListener(updateListener);
        this.addressProperty.addListener(updateListener);
        this.cityProperty.addListener(updateListener);
        this.countryProperty.addListener(updateListener);
        this.postalcodeProperty.addListener(updateListener);
        this.phoneProperty.addListener(updateListener);
        this.emailProperty.addListener(updateListener);        
    }
    
    public Location getLocation() {
        return location;
    }
    
    public JavaBeanProperty<String> nameProperty() {
        return LocationNameProperty;
    }
    
    public JavaBeanProperty<String> addressProperty() {
        return addressProperty;
    }
    
    public JavaBeanProperty<String> cityProperty() {
        return cityProperty;
    }
    
    public JavaBeanProperty<String> countryProperty() {
        return countryProperty;
    }
    
    public JavaBeanProperty<String> postalcodeProperty() {
        return postalcodeProperty;
    }
    
    public JavaBeanProperty<String> phoneProperty() {
        return phoneProperty;
    }
    
    public JavaBeanProperty<String> emailProperty() {
        return emailProperty;
    }
    
    public JavaBeanProperty<String> idProperty() {
        return idProperty;
    }
    
    public JavaBeanProperty<String> externalIdProperty() {
        return externalIdProperty;
    }
    
    @Override
    protected String findDisplayName() {
        return location.getName();
    }
    
    @Override
    protected void handleSave() throws IOException {
        log.debug(" handleSave   {}", this.getLocation().getId());
        ClientManager.getInstance().saveClient(this);
    }
    
    @Override
    SaveManager getSaveManager() {
        return ClientManager.getInstance().getClientLocationSaveManager();
    }    
}
