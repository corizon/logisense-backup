/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import java.util.function.Predicate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RidesFilterUpdater implements ListChangeListener<ObservableRide>, ChangeListener<String> {

    private static final Logger log = LoggerFactory.getLogger(RidesFilterUpdater.class);

    private final ObjectProperty<Predicate<? super ObservableRide>> plannableRidesPredicateProperty;

    public RidesFilterUpdater(ObjectProperty<Predicate<? super ObservableRide>> plannableRidesPredicateProperty) {
        this.plannableRidesPredicateProperty = plannableRidesPredicateProperty;
    }

    /**
     * fire on every add or remove from the shipments list
     *
     * @param c
     */
    @Override
    public void onChanged(ListChangeListener.Change<? extends ObservableRide> c) {
        while (c.next()) {
            if (c.wasAdded()) {
                c.getAddedSubList().stream().forEach((observableRide) -> {
                    log.debug("adding listener to new observableRide {}",observableRide);
                    observableRide.statusProperty().addListener(this);
                });
            }
            if (c.wasRemoved()) {
                c.getRemoved().stream().forEach((observableRide) -> {
                    log.debug("removing listener from removed observableRide {}",observableRide);
                    observableRide.statusProperty().removeListener(this);
                });
            }
        }
    }

    /**
     * fire on every change of container property of a shipment.
     *
     * @param observable
     * @param oldValue
     * @param newValue
     */
    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        updatePlannableRidesFilter();
    }

    /**
     * sets predicate to "new" value so filtered list gets refreshed
     */
    private void updatePlannableRidesFilter() {
        log.warn("updateFilter");
        //
        // setting first to null because there is no other way to update a filtered list :S
        //
        plannableRidesPredicateProperty.setValue(null);
        plannableRidesPredicateProperty.setValue(ride
                -> Savable.ClientStatus.NEW.equals(Savable.ClientStatus.from(ride.statusProperty().getValue()))
                || Savable.ClientStatus.UNSAVED.equals(Savable.ClientStatus.from(ride.statusProperty().getValue()))
                || (Savable.ClientStatus.CHANGED.equals(Savable.ClientStatus.from(ride.statusProperty().getValue()))
                && Savable.ClientStatus.NEW.equals(Savable.ClientStatus.from(ride.getRide().getStatus())))
        );
    }

}
