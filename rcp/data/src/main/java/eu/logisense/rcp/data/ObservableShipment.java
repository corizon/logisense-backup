/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Client;
import eu.logisense.api.Container;
import eu.logisense.api.Debitor;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Order;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import eu.logisense.api.session.client.Connections;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@NbBundle.Messages({
    "shipmentNamePrefix=Shipment:"
})
public class ObservableShipment extends Savable {

    private final Shipment shipment;

    private final JavaBeanProperty<LocalDateTime> loadTimeFrameFromProperty;
    private final JavaBeanProperty<LocalDateTime> loadTimeFrameToProperty;

    private final JavaBeanProperty<LocalDateTime> unloadTimeFrameFromProperty;
    private final JavaBeanProperty<LocalDateTime> unloadTimeFrameToProperty;

    private final JavaBeanProperty<String> nameProperty;
    private final JavaBeanProperty<String> loadAddressProperty;
    private final JavaBeanProperty<String> loadPostalCodeProperty;
    private final JavaBeanProperty<String> loadCityProperty;
    private final JavaBeanProperty<String> loadCountryProperty;
    private final JavaBeanProperty<String> loadNameProperty;
    private final JavaBeanProperty<String> loadIdProperty;
    private final JavaBeanProperty<String> loadPhoneProperty;
    private final JavaBeanProperty<String> loadEmailProperty;

    private final JavaBeanProperty<String> unloadAddressProperty;
    private final JavaBeanProperty<String> unloadPostalCodeProperty;
    private final JavaBeanProperty<String> unloadCityProperty;
    private final JavaBeanProperty<String> unloadCountryProperty;
    private final JavaBeanProperty<String> unloadNameProperty;
    private final JavaBeanProperty<String> unloadIdProperty;
    private final JavaBeanProperty<String> unloadPhoneProperty;
    private final JavaBeanProperty<String> unloadEmailProperty;

    private final JavaBeanProperty<Boolean> senderIsLoadProperty;

    private final JavaBeanProperty<String> ordererNameProperty;
    private final JavaBeanProperty<String> orderReferenceProperty;

    private final SimpleDoubleProperty weightProperty;
    private final SimpleDoubleProperty volumeProperty;
    private final JavaBeanProperty<Integer> shippedUnitsProperty;

    private final JavaBeanProperty<String> noteProperty;
    private final JavaBeanProperty<String> externalIdProperty;

    private final JavaBeanProperty<Integer> indexProperty;
    private final JavaBeanProperty<Resource> containerProperty;
    private final JavaBeanProperty<Client> ordererProperty;

    private final JavaBeanProperty<String> debitorIdProperty;

    private final JavaBeanProperty<Double> sellPriceProperty;
    private final JavaBeanProperty<String> currencyProperty;
    private final JavaBeanProperty<Status> shipmentStatusProperty;

    private final SimpleStringProperty creditorNameProperty;
    private final SimpleStringProperty creditorEmailProperty;
    private final SimpleStringProperty creditorPhoneProperty;
    private final SimpleDoubleProperty buyPriceProperty;

    private final SimpleDoubleProperty loadingMetersProperty;
    private final DoubleBinding loadingMetersBinding;
    private final DoubleBinding volumeBinding;
    private final DoubleBinding weightBinding;
    //
    // construct list with extractor (the Observable[]) to be able to live
    // update amount, weight and volume
    //
    private final ObservableList<ObservableOrderLine> orderLineList
            = FXCollections.observableArrayList(observableOrderLine
                    -> new Observable[]{
                observableOrderLine.amountProperty(),
                observableOrderLine.lengthProperty(),
                observableOrderLine.widthProperty(),
                observableOrderLine.heightProperty(),
                observableOrderLine.volumeProperty(),
                observableOrderLine.weightProperty()});

    private static final Logger log = LoggerFactory.getLogger(ObservableShipment.class);

    public Shipment getShipment() {
        return shipment;
    }

    public ObservableShipment(Shipment shipment, ClientStatus clientStatus) throws NoSuchMethodException {
        super(clientStatus, shipment.getId());
        Objects.requireNonNull(shipment);
        Objects.requireNonNull(shipment.getId());
        Objects.requireNonNull(shipment.getOrder());
        Objects.requireNonNull(clientStatus);

        this.shipment = shipment;

        if (shipment.getOrder().getLines() != null) {
            log.debug("creating shipment with {} order lines", shipment.getOrder().getLines().size());
            shipment.getOrder().getLines().stream().forEach((orderLine) -> {
                try {
                    orderLineList.add(new ObservableOrderLine(orderLine));
                } catch (NoSuchMethodException ex) {
                    log.error("unable to add location:{}", ex);
                    Exceptions.printStackTrace(ex);
                }
            });
        }
        //
        // sync observable list with orderlines with the orderlines in the shipment object
        //
        this.orderLineList.addListener((ListChangeListener.Change<? extends ObservableOrderLine> change) -> {
            while (change.next()) {
                change.getAddedSubList().stream().forEach((orderLine) -> {
                    shipment.getOrder().getLines().add(orderLine.getOrderLine());
                });
                change.getRemoved().stream().forEach((orderLine) -> {
                    shipment.getOrder().getLines().remove(orderLine.getOrderLine());
                });
            }
        });

//        this.nameProperty = new SimpleStringProperty(shipment, Shipment.PROP_NAME, shipment.getName());
        this.nameProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_NAME).build();
        this.shipmentStatusProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_STATUS).build();

        this.ordererNameProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getOrder().getOrderedBy()).name(Resource.PROP_NAME).build();
        this.orderReferenceProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getOrder()).name(Order.PROP_INVOICE_REFERENCE).build();

        this.loadAddressProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_ADDRESS).build();
        this.loadPostalCodeProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_POSTALCODE).build();
        this.loadCityProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_CITY).build();
        this.loadCountryProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_COUNTRY).build();
        this.loadNameProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_NAME).build();
        this.loadIdProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_ID).build();
        this.loadPhoneProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_PHONE).build();
        this.loadEmailProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_EMAIL).build();

        this.loadTimeFrameFromProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_TIME_FRAME_FROM).build();
        this.loadTimeFrameToProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getLoadLocation()).name(LoadLocation.PROP_TIME_FRAME_TO).build();

        this.unloadAddressProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_ADDRESS).build();
        this.unloadPostalCodeProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_POSTALCODE).build();
        this.unloadCityProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_CITY).build();
        this.unloadCountryProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_COUNTRY).build();
        this.unloadNameProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_NAME).build();
        this.unloadIdProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_ID).build();
        this.unloadPhoneProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_PHONE).build();
        this.unloadEmailProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_EMAIL).build();

        this.unloadTimeFrameFromProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_TIME_FRAME_FROM).build();
        this.unloadTimeFrameToProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getUnloadLocation()).name(UnloadLocation.PROP_TIME_FRAME_TO).build();

        this.senderIsLoadProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_SENDER_IS_LOAD).build();

        this.shippedUnitsProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_SHIPPED_UNITS).build();

        this.noteProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_NOTE).build();
        this.externalIdProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getOrder()).name(Order.PROP_EXTERNAL_ID).build();

        this.indexProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_INDEX).build();
        this.containerProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_CONTAINER).build();

        this.ordererProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getOrder()).name(Order.PROP_ORDERED_BY).build();

        this.debitorIdProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getOrder().getDebitor()).name(Debitor.PROP_ID).build();
        this.sellPriceProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getOrder().getDebitor()).name(Debitor.PROP_PRICE).build();
        this.currencyProperty = JavaBeanObjectPropertyBuilder.create().bean(shipment.getOrder().getDebitor()).name(Debitor.PROP_CURRENCY).build();

        this.buyPriceProperty = new SimpleDoubleProperty();
        this.creditorNameProperty = new SimpleStringProperty();
        this.creditorEmailProperty = new SimpleStringProperty();
        this.creditorPhoneProperty = new SimpleStringProperty();

        this.loadingMetersBinding = Bindings.createDoubleBinding(() -> orderLineList.stream().collect(Collectors.summingDouble(ObservableOrderLine::getLoadingMeters)), orderLineList);
        this.loadingMetersProperty = new SimpleDoubleProperty();
        this.loadingMetersProperty.bind(loadingMetersBinding);

        this.volumeBinding = Bindings.createDoubleBinding(() -> orderLineList.stream().collect(Collectors.summingDouble(ObservableOrderLine::getVolume)), orderLineList);
        this.volumeProperty = new SimpleDoubleProperty();
        this.volumeProperty.bind(volumeBinding);

        this.weightBinding = Bindings.createDoubleBinding(() -> orderLineList.stream().collect(Collectors.summingDouble(ObservableOrderLine::getWeight)), orderLineList);
        this.weightProperty = new SimpleDoubleProperty();
        this.weightProperty.bind(weightBinding);

        this.containerProperty.addListener((ObservableValue<? extends Resource> observable, Resource oldValue, Resource newValue) -> {
            TransportStatus currentStatus = TransportStatus.valueOf(this.shipment.getStatus().name());
            log.debug("container property for shipment {} changed from {} to {} . Current status = {}", observable, oldValue, newValue, currentStatus);

            if (oldValue == null && newValue != null) {
                //LINKED
                this.shipmentStatusProperty.setValue(TransportStatus.PLANNED);
            }
            if (oldValue != null && newValue == null) {
                //UNLINKED
                this.shipmentStatusProperty.setValue(TransportStatus.NEW);
            }
        });

        //
        // add listeners to detect changes to be able to save
        //
        this.nameProperty.addListener(updateListener);
        this.shipmentStatusProperty.addListener(updateListener);
        this.ordererNameProperty.addListener(updateListener);
        this.orderReferenceProperty.addListener(updateListener);
        this.ordererProperty.addListener(updateListener);

        this.loadAddressProperty.addListener(updateListener);
        this.loadPostalCodeProperty.addListener(updateListener);
        this.loadCityProperty.addListener(updateListener);
        this.loadCountryProperty.addListener(updateListener);
        this.loadNameProperty.addListener(updateListener);
        this.loadIdProperty.addListener(updateListener);
        this.loadPhoneProperty.addListener(updateListener);
        this.loadEmailProperty.addListener(updateListener);
        this.loadTimeFrameFromProperty.addListener(updateListener);
        this.loadTimeFrameToProperty.addListener(updateListener);

        this.unloadAddressProperty.addListener(updateListener);
        this.unloadPostalCodeProperty.addListener(updateListener);
        this.unloadCityProperty.addListener(updateListener);
        this.unloadCountryProperty.addListener(updateListener);
        this.unloadNameProperty.addListener(updateListener);
        this.unloadIdProperty.addListener(updateListener);
        this.unloadPhoneProperty.addListener(updateListener);
        this.unloadEmailProperty.addListener(updateListener);
        this.unloadTimeFrameFromProperty.addListener(updateListener);
        this.unloadTimeFrameToProperty.addListener(updateListener);
        this.senderIsLoadProperty.addListener(updateListener);

        this.volumeProperty.addListener(updateListener);
        this.weightProperty.addListener(updateListener);
        this.shippedUnitsProperty.addListener(updateListener);
        this.noteProperty.addListener(updateListener);
        this.externalIdProperty.addListener(updateListener);

//        this.indexProperty.addListener(updateListener);
//        this.containerProperty.addListener(updateListener);
        this.debitorIdProperty.addListener(updateListener);
        this.sellPriceProperty.addListener(updateListener);
        this.currencyProperty.addListener(updateListener);

    }

    @Override
    protected String findDisplayName() {
        return Bundle.shipmentNamePrefix() + shipment.getId();
    }

    @Override
    protected void handleSave() throws IOException {
        Lookup.getDefault().lookup(ShipmentManager.class).saveShipment(this);
//        ShipmentManager.getInstance().saveShipment(this);
    }

    public JavaBeanProperty<Resource> containerProperty() {
        return containerProperty;
    }

    public JavaBeanProperty<String> ordererNameProperty() {
        return ordererNameProperty;
    }

    public JavaBeanProperty<Status> shipmentStatusProperty() {
        return shipmentStatusProperty;
    }

    public JavaBeanProperty<String> loadNameProperty() {
        return loadNameProperty;
    }

    public JavaBeanProperty<String> loadIdProperty() {
        return loadIdProperty;
    }

    public JavaBeanProperty<String> loadPhoneProperty() {
        return loadPhoneProperty;
    }

    public SimpleDoubleProperty loadingMetersProperty() {
        return loadingMetersProperty;
    }

    public double getLoadingMeters() {
        return loadingMetersProperty().get();
    }

    public JavaBeanProperty<String> loadEmailProperty() {
        return loadEmailProperty;
    }

    public JavaBeanProperty<String> unloadNameProperty() {
        return unloadNameProperty;
    }

    public JavaBeanProperty<String> unloadIdProperty() {
        return unloadIdProperty;
    }

    public JavaBeanProperty<String> unloadPhoneProperty() {
        return unloadPhoneProperty;
    }

    public JavaBeanProperty<String> unloadEmailProperty() {
        return unloadEmailProperty;
    }

    public JavaBeanProperty<LocalDateTime> unloadTimeFrameFromProperty() {
        return unloadTimeFrameFromProperty;
    }

    public JavaBeanProperty<LocalDateTime> unloadTimeFrameToProperty() {
        return unloadTimeFrameToProperty;
    }

    public JavaBeanProperty<String> loadAddressProperty() {
        return loadAddressProperty;
    }

    public JavaBeanProperty<String> loadCityProperty() {
        return loadCityProperty;
    }

    public JavaBeanProperty<String> loadCountryProperty() {
        return loadCountryProperty;
    }

    public JavaBeanProperty<String> loadPostalCodeProperty() {
        return loadPostalCodeProperty;
    }

    public JavaBeanProperty<String> nameProperty() {
        return nameProperty;
    }

    public JavaBeanProperty<String> noteProperty() {
        return noteProperty;
    }

    public JavaBeanProperty<String> externalIdProperty() {
        return externalIdProperty;
    }

    public JavaBeanProperty<LocalDateTime> loadTimeFrameFromProperty() {
        return loadTimeFrameFromProperty;
    }

    public JavaBeanProperty<LocalDateTime> loadTimeFrameToProperty() {
        return loadTimeFrameToProperty;
    }

    public SimpleDoubleProperty volumeProperty() {
        return volumeProperty;
    }

    public double getVolume() {
        return volumeProperty().getValue();
    }

    public JavaBeanProperty<String> unloadAddressProperty() {
        return unloadAddressProperty;
    }

    public JavaBeanProperty<String> unloadCityProperty() {
        return unloadCityProperty;
    }

    public JavaBeanProperty<String> unloadCountryProperty() {
        return unloadCountryProperty;
    }

    public JavaBeanProperty<String> unloadPostalCodeProperty() {
        return unloadPostalCodeProperty;
    }

    public SimpleDoubleProperty weightProperty() {
        return weightProperty;
    }

    public double getWeight() {
        return weightProperty().getValue();
    }

    public JavaBeanProperty<Integer> indexProperty() {
        return indexProperty;
    }

    public JavaBeanProperty<Client> ordererProperty() {
        return ordererProperty;
    }

    public JavaBeanProperty<String> debitorIdProperty() {
        return debitorIdProperty;
    }

    public JavaBeanProperty<Double> sellPriceProperty() {
        return sellPriceProperty;
    }

    public SimpleDoubleProperty buyPriceProperty() {
        return buyPriceProperty;
    }

    public SimpleStringProperty creditorNameProperty() {
        return creditorNameProperty;
    }

    public SimpleStringProperty creditorEmailProperty() {
        return creditorEmailProperty;
    }

    public SimpleStringProperty creditorPhoneProperty() {
        return creditorPhoneProperty;
    }

    public double getPrice() {
        return sellPriceProperty() == null || sellPriceProperty().getValue() == null ? 0.0 : sellPriceProperty().getValue();
    }

    public JavaBeanProperty<String> currencyProperty() {
        return currencyProperty;
    }

    public JavaBeanProperty<String> orderReferenceProperty() {
        return orderReferenceProperty;
    }

    public JavaBeanProperty<Integer> shippedUnitsProperty() {
        return shippedUnitsProperty;
    }

    public JavaBeanProperty<Boolean> senderIsLoadProperty() {
        return senderIsLoadProperty;
    }

    public ObservableList<ObservableOrderLine> getOrderLineList() {
        return orderLineList;
    }

    @Override
    public SaveManager getSaveManager() {
        return Lookup.getDefault().lookup(ShipmentManager.class).getShipmentSaveManager();
    }

//    @Override
//    public int hashCode() {
//        return shipment.hashCode();
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        return shipment.equals(obj);
//    }
}
