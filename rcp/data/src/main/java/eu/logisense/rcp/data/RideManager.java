/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.logisense.api.Ride;
import eu.logisense.api.RideService;
import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.session.client.Connections;
import eu.logisense.api.util.ClientUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javax.ws.rs.core.MediaType;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class RideManager /* implements RideService /* (can only implement it if ObservableShipment would extend Shipment instead of wrapping it)*/ {

    private static final Logger log = LoggerFactory.getLogger(RideManager.class);

    private final ObservableMap<String, ObservableRide> managedRides = FXCollections.observableHashMap();
    private final RideService rideService = ClientUtil.proxy(RideService.class);

    private static final RideManager INSTANCE = new RideManager();

    // Ride circle
    //
    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    private final RideSaveManager rideSaveManager = new RideSaveManager();

    public RideSaveManager getRideSaveManager() {
        return rideSaveManager;
    }

    /**
     * Get an observable list of rides for one or more statuses
     *
     * @param forStatus
     * @return
     * @throws java.lang.NoSuchMethodException
     */
    public ObservableList<ObservableRide> getRidesForStatus(TransportStatus... forStatus) throws NoSuchMethodException {

        //
        // create predicate from status to filter on
        //
        Predicate<ObservableRide> filter = (ObservableRide t) -> {
            boolean found = false;
            for (Status status : forStatus) {
                if (status.equals(TransportStatus.valueOf(t.transportStatusProperty().getValue().name()))
                        && !Savable.ClientStatus.ENTRY.equals(Savable.ClientStatus.from(t.statusProperty().getValue()))) {
                    found = true;
                }
            }
            return found;
        };

        //
        // get a list of rides
        //
        ObservableList<ObservableRide> list = getRides();

        //
        // filter the list based on statuses provided
        //
        FilteredList<ObservableRide> filteredList = new FilteredList<>(list, filter);

        //
        // create sorted list from filtered list
        //
        SortedList<ObservableRide> sorted = new SortedList<>(filteredList);

        sorted.addListener((ListChangeListener.Change<? extends ObservableRide> c) -> {
            while (c.next()) {
                log.debug("sorted.addListener {} added, {} removed", c.getAddedSize(), c.getRemovedSize());
            }
        });

        return sorted;
    }

    public ObservableList<ObservableRide> getRideCards() {
        return getRides();
    }

    /**
     * register new ride, not saved to server yet
     *
     * @param savableRide
     */
    public void newRide(ObservableRide savableRide) {
        rideSaveManager.notifyChanged(savableRide);
        managedRides.put(savableRide.getRide().getId(), savableRide);
    }

    public static RideManager getInstance() {
        return INSTANCE;
    }

    private void logDebug(String message, Object o, Class c) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JSR310Module());
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            jsonProvider.writeTo(o, c, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
            log.debug(message, baos.toString());

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void saveRide(ObservableRide observableRide) {
        executor.submit(() -> {
            switch (Savable.ClientStatus.from(observableRide.statusProperty().getValue())) {
                case UNSAVED:
                    logDebug("new ride {}", observableRide.getRide(), Ride.class);
                    try {
                        rideService.postRide(observableRide.getRide());
                        observableRide.statusProperty().setValue(observableRide.getRide().getStatus().name());
                        log.debug("ride saved");
                    } catch (Exception e) {
                        log.error("Error saving ride{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    break;
                case CHANGED:
                    logDebug("update ride {}", observableRide.getRide(), Ride.class);
                    try {
                        rideService.updateRide(observableRide.getRide());
                        observableRide.statusProperty().setValue(observableRide.getRide().getStatus().name());
                        log.debug("ride saved");
                    } catch (Exception e) {
                        log.error("Error saving ride{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    break;
                default:
                    log.warn("trying to save observableRide with unsavable status {} ", observableRide.statusProperty().getValue());
                    break;
            }
        });
        getRideSaveManager().notifySaved(observableRide);
    }

    public ObservableList<ObservableRide> getRides() {
        ObservableList<ObservableRide> list = FXCollections.observableArrayList(or -> new Observable[]{or.statusProperty(), or.transportStatusProperty()});

        managedRides.addListener((MapChangeListener.Change<? extends String, ? extends ObservableRide> change) -> {
            if (change.wasAdded()) {

                if (!list.contains(change.getValueAdded())) {
                    list.add(change.getValueAdded());
                }
            }
            if (change.wasRemoved()) {
                list.remove(change.getValueRemoved());
            }
            //TODO need to update filters?
        });

        Connections.onConnection(() -> {
//        executor.submit(() -> {
            log.info("initializing rides");
            List<Ride> ridesList = rideService.getRides();
            log.debug("server returned {} rides", ridesList.size());

            ridesList.stream().forEach((ride) -> {
                log.debug("about to add a ride");
                try {

                    if (!managedRides.containsKey(ride.getId())) {
                        managedRides.put(ride.getId(), new ObservableRide(ride, Savable.ClientStatus.from(ride.getStatus())));
                    }

                    log.info("adding ride {}", ride);
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                    Exceptions.printStackTrace(ex);
                }
            });
        });

        list.addAll(managedRides.values());

        return list;
    }

    public ObservableRide getRide(String id) throws NoSuchMethodException {
        ObservableRide observableRide = null;
        if (!managedRides.containsKey(id)) {
            Ride ride = rideService.getRide(id);
            observableRide = new ObservableRide(ride, Savable.ClientStatus.from(ride.getStatus()));
            managedRides.put(ride.getId(), observableRide);
        }
        return managedRides.get(id);
    }

    public ObservableRide updateRide(ObservableRide template) throws NoSuchMethodException {
        Ride saved = rideService.updateRide(template.getRide());
        ObservableRide savedOS = new ObservableRide(saved, Savable.ClientStatus.from(saved.getStatus()));
        managedRides.put(saved.getId(), savedOS);
        return savedOS;
    }

    public void deleteRide(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<ObservableRide> getRides(LocalDate etd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
