/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.logisense.api.Location;
import eu.logisense.api.LocationService;
import eu.logisense.api.Shipment;
import eu.logisense.api.ShipmentService;
import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.session.client.Connections;
import eu.logisense.api.util.ClientUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.core.MediaType;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author perezdf
 */
@ServiceProvider(service = ShipmentManager.class)
public class ShipmentManagerImpl implements ShipmentManager {

    private static final Logger log = LoggerFactory.getLogger(ShipmentManagerImpl.class);

    protected final ObservableMap<String, ObservableShipment> managedShipments = FXCollections.observableHashMap();

    private final ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
    private final Validator validator = vf.getValidator();

    //
    // Services
    //
    protected final ShipmentService shipmentService = ClientUtil.proxy(ShipmentService.class);
    protected final LocationService locationService = ClientUtil.proxy(LocationService.class);

    //TODO accept predicate as filter?
    @Override
    public ObservableList<ObservableShipment> getShipments() {
        log.debug("getShipments");
        ObservableList<ObservableShipment> shipments = FXCollections.observableArrayList(os -> new Observable[]{os.statusProperty(), os.containerProperty(), os.shipmentStatusProperty()});

        managedShipments.addListener((MapChangeListener.Change<? extends String, ? extends ObservableShipment> change) -> {
            log.debug("change in managedShipments added? {}, removed? {}", change.wasAdded(), change.wasRemoved());
            if (change.wasAdded()) {
                //
                // only add if not already in list,
                //
                if (!shipments.contains(change.getValueAdded())) {
                    shipments.add(change.getValueAdded());
                }
            }
            if (change.wasRemoved()) {
                shipments.remove(change.getValueRemoved());
            }
            //TODO need to update filters?
        });
        //TODO only retrieve every x seconds, use timer
        Connections.onConnection(() -> {
            List<Shipment> shipmentList = shipmentService.getShipments();
            log.debug("retrieved {} shipments from the server", shipmentList.size());
            shipmentList.stream().forEach((shipment) -> {
                try {
                    //TODO support updated coming from the server, need not to teplace the shipments, but update the attributes
                    // otherwise listeners and bindings get lost
                    if (!managedShipments.containsKey(shipment.getId())) {
                        log.debug("adding shipment {} to managed shipments", shipment.getId());
                        managedShipments.put(shipment.getId(), new ObservableShipment(shipment, Savable.ClientStatus.from(shipment.getStatus())));
                    }
                } catch (NoSuchMethodException ex) {
                    log.warn("Could not create observable shipment from shipment {}:{}", shipment, ex.getMessage());
                }
            });
        });

        shipments.addListener((ListChangeListener.Change<? extends ObservableShipment> c) -> {
            while (c.next()) {
                log.debug("shipments.addListener {} added, {} removed", c.getAddedSize(), c.getRemovedSize());
            }
        });

        shipments.addAll(managedShipments.values());
//        managedShipments.values().forEach(os -> {
//            if (!shipments.contains(os)) {
//                shipments.add(os);
//            }
//        });
        log.debug("returning {} shipments", shipments.size());
        return shipments;
    }

    /**
     * Get an observable list of shipments for one or more statuses
     *
     * @param forStatus
     * @return
     */
    @Override
    public ObservableList<ObservableShipment> getShipmentsForStatus(TransportStatus... forStatus) throws NoSuchMethodException {
        //
        // create predicate from status to filter on
        //
        Predicate<ObservableShipment> filter = (ObservableShipment t) -> {
            boolean found = false;
            for (Status status : forStatus) {
                if (status.equals(TransportStatus.valueOf(t.shipmentStatusProperty().getValue().name()))
                        && !Savable.ClientStatus.ENTRY.equals(Savable.ClientStatus.from(t.statusProperty().getValue()))) {
                    found = true;
                }
            }
            return found;
        };

        // get a list of shipments
        ObservableList<ObservableShipment> list = getShipments();
        // filter the list based on statuses provided
        FilteredList<ObservableShipment> filtered = list.filtered(filter);
        // create sorted list from filtered list
        SortedList<ObservableShipment> sorted = new SortedList<>(filtered);
        // listener for changes
        ChangeListener<String> listener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            filtered.predicateProperty().setValue(null);
            filtered.predicateProperty().setValue(filter);
        };
        // add listener for all the shipments in the list
        list.stream().forEach((observableShipment) -> {
            observableShipment.statusProperty().addListener(listener);
        });
        // add listener to all future shipments and remove when shipment is removed
        list.addListener((ListChangeListener.Change<? extends ObservableShipment> c) -> {

            while (c.next()) {
                if (c.wasAdded()) {
                    c.getAddedSubList().stream().forEach((observableShipment) -> {
                        log.debug("adding listener to new shipment {}", observableShipment);
                        observableShipment.statusProperty().addListener(listener);
                    });
                }
                if (c.wasRemoved()) {
                    c.getRemoved().stream().forEach((observableShipment) -> {
                        log.debug("removing listener from removed shipment {}", observableShipment);
                        observableShipment.statusProperty().removeListener(listener);
                    });
                }
            }
        });

        sorted.addListener((ListChangeListener.Change<? extends ObservableShipment> c) -> {
            while (c.next()) {
                log.debug("sorted.addListener {} added, {} removed", c.getAddedSize(), c.getRemovedSize());
            }
        });

        return sorted;
    }

    @Override
    public ObservableShipment getShipment(String id) {
        log.debug("get shipment {}", id);
        ObservableShipment observableShipment = null;
        if (!managedShipments.containsKey(id)) {
            log.debug("shipment not managed, retrieving from server");
            try {
                Shipment shipment = shipmentService.getShipment(id);
                observableShipment = new ObservableShipment(shipment, Savable.ClientStatus.from(shipment.getStatus()));
                managedShipments.put(shipment.getId(), observableShipment);
            } catch (NoSuchMethodException ex) {
                log.error("unable to create observable shipment from shipment", ex);
                Exceptions.printStackTrace(ex);
            }
        }
        return managedShipments.get(id);
    }

    @Override
    public ObservableShipment getShipmentByHref(String href) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableShipment postShipment(ObservableShipment template) throws NoSuchMethodException {

        Set<ConstraintViolation<Shipment>> violations = validator.validate(template.getShipment());
        if (violations != null) {
            throw new ConstraintViolationException(violations);
        }

        Shipment saved = shipmentService.postShipment(template.getShipment());
        ObservableShipment savedOS = new ObservableShipment(saved, Savable.ClientStatus.from(saved.getStatus()));
        managedShipments.put(saved.getId(), savedOS);
        log.debug("shipment id ({}) put ", saved.getId());

        log.info("reset shipment list");
        shipmentService.getShipments().stream().forEach((shipment) -> {
            try {

                log.debug("shipment.getId():" + shipment.getId());
                managedShipments.put(shipment.getId(), new ObservableShipment(shipment, Savable.ClientStatus.from(shipment.getStatus())));
                log.info("adding shipment {}", shipment);
            } catch (Exception ex) {
                log.error(ex.getMessage());
                Exceptions.printStackTrace(ex);
            }
        });

        return savedOS;
    }

    @Override
    public ObservableShipment updateShipment(ObservableShipment template) throws NoSuchMethodException {
        Set<ConstraintViolation<Shipment>> violations = validator.validate(template.getShipment());
        if (violations != null) {
            throw new ConstraintViolationException(violations);
        }
        Shipment saved = shipmentService.updateShipment(template.getShipment());
        ObservableShipment savedOS = new ObservableShipment(saved, Savable.ClientStatus.from(saved.getStatus()));
        managedShipments.put(saved.getId(), savedOS);
        return savedOS;
    }

    @Override
    public void deleteShipment(String id) {
        shipmentService.deleteShipment(id);
        managedShipments.remove(id);
    }

    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    public static ShipmentManager getInstance() {
        return Lookup.getDefault().lookup(ShipmentManager.class);
    }

    @Override
    public SaveManager getShipmentSaveManager() {
        return Lookup.getDefault().lookup(ShipmentSaveManager.class);
    }

    /**
     * register new shipment, not saved to server yet
     *
     * @param observableShipment
     */
    @Override
    public void newShipment(ObservableShipment observableShipment) {
        //
        // Fix for LOG-217 add newly created locations for load and unload to the global locations list
        //
        final BooleanProperty foundProperty = new SimpleBooleanProperty(false);
        ClientManager.getInstance().getLocations()
                .stream()
                .filter(l -> l.nameProperty().getValue() != null
                        && l.nameProperty().getValue().equals(observableShipment.unloadNameProperty().getValue()))
                .forEach((l) -> {
                    foundProperty.setValue(true);
                });

        if (!foundProperty.getValue()) {
            try {
                ClientManager.getInstance().getLocations().add(new ObservableLocation(observableShipment.getShipment().getUnloadLocation()));
            } catch (NoSuchMethodException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        foundProperty.setValue(Boolean.FALSE);
        ClientManager.getInstance().getLocations()
                .stream()
                .filter(l -> l.nameProperty().getValue() != null
                        && l.nameProperty().getValue().equals(observableShipment.loadNameProperty().getValue()))
                .forEach((l) -> {
                    foundProperty.setValue(true);
                });

        if (!foundProperty.getValue()) {
            try {
                ClientManager.getInstance().getLocations().add(new ObservableLocation(observableShipment.getShipment().getLoadLocation()));
            } catch (NoSuchMethodException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        getShipmentSaveManager().notifyChanged(observableShipment);
        managedShipments.put(observableShipment.getId(), observableShipment);

    }

    @Override
    public void removeShipment(ObservableShipment observableShipment) {
        executor.submit(() -> {
            try {
                Savable.ClientStatus status = Savable.ClientStatus.from(observableShipment.statusProperty().getValue());
                if (!Savable.ClientStatus.NEW.equals(status) && !Savable.ClientStatus.ENTRY.equals(status)) {
                    shipmentService.deleteShipment(observableShipment.getId());
                }
                managedShipments.remove(observableShipment.getId());
                getShipmentSaveManager().notifySaved(observableShipment);
            } catch (Exception e) {
                log.error("Error removing shipment{}", e);
                Exceptions.printStackTrace(e);
            }
        });
    }

    @Override
    public void saveShipment(ObservableShipment observableShipment) {
        log.info("saveShipment with id {}", observableShipment.getId());
        executor.submit(() -> {
            switch (Savable.ClientStatus.from(observableShipment.statusProperty().getValue())) {
                case UNSAVED:
                    logDebug("new shipment {}", observableShipment.getShipment(), Shipment.class);
                    try {
                        Shipment shipment = shipmentService.postShipment(observableShipment.getShipment());
                        //
                        // update id's from server
                        //
                        observableShipment.getShipment().setId(shipment.getId());
                        observableShipment.getShipment().setHref(shipment.getHref());
                        observableShipment.getShipment().getOrder().setId(shipment.getOrder().getId());
                        observableShipment.getShipment().getOrder().setHref(shipment.getOrder().getHref());
                        observableShipment.statusProperty().setValue(observableShipment.getShipment().getStatus().name());
                        log.debug("shipment saved");
                    } catch (Exception e) {
                        log.error("Error saving shipment{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    break;
                case CHANGED:
                    try {
                        logDebug("update shipment {}", observableShipment.getShipment(), Shipment.class);
                        shipmentService.updateShipment(observableShipment.getShipment());
                        observableShipment.statusProperty().setValue(observableShipment.getShipment().getStatus().name());
                        log.debug("shipment saved");
                    } catch (Exception e) {
                        log.error("Error saving shipment{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    break;
                default:
                    log.warn("trying to save observableShipment with unsavable status {} ", observableShipment.statusProperty().getValue());
                    break;
            }
            getShipmentSaveManager().notifySaved(observableShipment);
        });
    }

    private void logDebug(String message, Object o, Class c) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JSR310Module());
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            jsonProvider.writeTo(o, c, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
            log.debug(message, baos.toString());

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    void saveLocation(ObservableLocation location) {
        log.debug("saveLocation");
        executor.submit(() -> {
            Savable.ClientStatus status = Savable.ClientStatus.from(location.statusProperty().getValue());
            log.debug("saving location {} with status {} which resolves to status {}", location, location.statusProperty().getValue(), status);
            switch (status) {
                case UNSAVED:
                    logDebug("new location {}", location.getLocation(), Location.class);
                    try {
                        locationService.postLocation(location.getLocation());
                        location.statusProperty().setValue(location.getLocation().getStatus().name());
                    } catch (Exception e) {
                        log.error("Error saving location{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    log.debug("location saved");
                    break;
                case CHANGED:
                    logDebug("update location {}", location.getLocation(), Location.class);
                    locationService.updateLocation(location.getLocation().getId(), location.getLocation());
                    try {
                        locationService.updateLocation(location.getId(), location.getLocation());
                        location.statusProperty().setValue(location.getLocation().getStatus().name());
                    } catch (Exception e) {
                        log.error("Error updating location{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    log.debug("location saved");
                    break;
                default:
                    log.warn("trying to save location with unsavable status {} ", location.statusProperty().getValue());
                    break;
            }
        });
    }
}
