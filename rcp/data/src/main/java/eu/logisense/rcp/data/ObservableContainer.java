/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Container;
import eu.logisense.api.Resource;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
public class ObservableContainer/* extends Savable*/ {

    private static final String SEPARATOR = " to ";

    private final Container container;

    private final DoubleBinding priceBinding;
    private final DoubleBinding volumeBinding;
    private final DoubleBinding weightBinding;
    private final DoubleBinding loadingMetersBinding;

    private final SimpleStringProperty displayNameProperty;
    private final SimpleStringProperty loadCityProperty;
    private final SimpleStringProperty unloadCityProperty;

    private final SimpleStringProperty creditorNameProperty;
    private final SimpleStringProperty creditorEmailProperty;
    private final SimpleStringProperty creditorPhoneProperty;
    private final SimpleDoubleProperty buyPriceProperty;

    private final SimpleDoubleProperty sellPriceProperty;
    private final JavaBeanProperty<Double> widthProperty;
    private final JavaBeanProperty<Double> heightProperty;
    private final JavaBeanProperty<Double> lengthProperty;
    private final SimpleDoubleProperty _widthProperty;
    private final SimpleDoubleProperty _heightProperty;
    private final SimpleDoubleProperty _lengthProperty;
    private final JavaBeanProperty<Double> maxWeightProperty;
    private final SimpleDoubleProperty maxLoadingMetersProperty;
    private final SimpleDoubleProperty maxVolumeProperty;
    private final SimpleDoubleProperty totalShipmentsWeightProperty;
    private final SimpleDoubleProperty totalShipmentsVolumeProperty;
    private final SimpleDoubleProperty totalShipmentsLoadingMetersProperty;

//    private final ObservableList<ObservableShipment> shipmentsList = FXCollections.observableArrayList();
    private final ObservableList<ObservableShipment> shipmentsList = FXCollections.observableArrayList(shipment -> new Observable[]{
        shipment.sellPriceProperty(), shipment.weightProperty(), shipment.volumeProperty(), shipment.loadingMetersProperty()
    });

    private static final Logger log = LoggerFactory.getLogger(ObservableContainer.class);

    ObservableContainer(Container container) throws NoSuchMethodException {
        this.container = container;
        log.debug("container with {} shipments", container.getShipments().size());

        this.widthProperty = JavaBeanObjectPropertyBuilder.create().bean(container).name(Container.PROP_WIDTH).build();
        this.heightProperty = JavaBeanObjectPropertyBuilder.create().bean(container).name(Container.PROP_HEIGHT).build();
        this.lengthProperty = JavaBeanObjectPropertyBuilder.create().bean(container).name(Container.PROP_LENGTH).build();
        this.maxWeightProperty = JavaBeanObjectPropertyBuilder.create().bean(container).name(Container.PROP_MAX_WEIGHT).build();

        this.priceBinding = Bindings.createDoubleBinding(() -> shipmentsList.stream().collect(Collectors.summingDouble(ObservableShipment::getPrice)), shipmentsList);
        this.sellPriceProperty = new SimpleDoubleProperty();
        this.sellPriceProperty.bind(priceBinding);

        this.volumeBinding = Bindings.createDoubleBinding(() -> shipmentsList.stream().collect(Collectors.summingDouble(ObservableShipment::getVolume)), shipmentsList);
        this.totalShipmentsVolumeProperty = new SimpleDoubleProperty();
        this.totalShipmentsVolumeProperty.bind(volumeBinding);

        this.weightBinding = Bindings.createDoubleBinding(() -> shipmentsList.stream().collect(Collectors.summingDouble(ObservableShipment::getWeight)), shipmentsList);
        this.totalShipmentsWeightProperty = new SimpleDoubleProperty();
        this.totalShipmentsWeightProperty.bind(weightBinding);

        this.loadingMetersBinding = Bindings.createDoubleBinding(() -> shipmentsList.stream().collect(Collectors.summingDouble(ObservableShipment::getLoadingMeters)), shipmentsList);
        this.totalShipmentsLoadingMetersProperty = new SimpleDoubleProperty();
        this.totalShipmentsLoadingMetersProperty.bind(loadingMetersBinding);

        _widthProperty = new SimpleDoubleProperty();
        _heightProperty = new SimpleDoubleProperty();
        _lengthProperty = new SimpleDoubleProperty();

        _widthProperty.bind(widthProperty);
        _heightProperty.bind(heightProperty);
        _lengthProperty.bind(lengthProperty);

        maxVolumeProperty = new SimpleDoubleProperty();
        maxVolumeProperty.bind(_widthProperty.multiply(_heightProperty).multiply(_lengthProperty));

        maxLoadingMetersProperty = new SimpleDoubleProperty();
        maxLoadingMetersProperty.bind(_lengthProperty.multiply(_widthProperty).divide(Container.STANDARD_WIDTH));

        this.loadCityProperty = new SimpleStringProperty("");
        this.unloadCityProperty = new SimpleStringProperty("");
        this.displayNameProperty = new SimpleStringProperty("");
        displayNameProperty.bind(Bindings.concat(loadCityProperty, SEPARATOR, unloadCityProperty));

        this.buyPriceProperty = new SimpleDoubleProperty();
        this.creditorNameProperty = new SimpleStringProperty();
        this.creditorEmailProperty = new SimpleStringProperty();
        this.creditorPhoneProperty = new SimpleStringProperty();

        initListeners();
    }

    public SimpleDoubleProperty sellPriceProperty() {
        return sellPriceProperty;
    }

    public Double getSellPrice() {
        return sellPriceProperty.get();
    }

    private void initListeners() {

        //
        // may contain nulls if container was constructed without these values
        //
        if (widthProperty.getValue() == null) {
            widthProperty.setValue(0.0);
        }
        if (heightProperty.getValue() == null) {
            heightProperty.setValue(0.0);
        }
        if (lengthProperty.getValue() == null) {
            lengthProperty.setValue(0.0);
        }
        if (maxWeightProperty.getValue() == null) {
            maxWeightProperty.setValue(0.0);
        }
        if (maxVolumeProperty.getValue() == null) {
            maxVolumeProperty.setValue(0.0);
        }

        if (container.getShipments() != null) {
            container.getShipments().stream().forEach((shipment) -> {
                ObservableShipment observableShipment = ShipmentManagerImpl.getInstance().getShipment(shipment.getId());
                shipmentsList.add(observableShipment);

                //
                // bind to propagate creditor values
                //
                observableShipment.buyPriceProperty().bind(buyPriceProperty);
                observableShipment.creditorNameProperty().bind(creditorNameProperty);
                observableShipment.creditorEmailProperty().bind(creditorEmailProperty);
                observableShipment.creditorPhoneProperty().bind(creditorPhoneProperty);
            });

            //
            // set display name for this container
            //
            if (!this.shipmentsList.isEmpty()) {
                loadCityProperty.bind(this.shipmentsList.get(0).loadCityProperty());
                unloadCityProperty.bind(this.shipmentsList.get(0).unloadCityProperty());
            } else {
                loadCityProperty.unbind();
                unloadCityProperty.unbind();
                loadCityProperty.setValue("");
                unloadCityProperty.setValue("");
            }
        }
        //
        // sync observable list with shipments with the shipments in the container object
        //
        this.shipmentsList.addListener((ListChangeListener.Change<? extends ObservableShipment> change) -> {
            while (change.next()) {
                log.debug("change in shipments list detected, {} added and {} removed", change.getAddedSize(), change.getRemovedSize());
                change.getAddedSubList().stream().forEach((shipment) -> {
                    log.debug("adding shipment {} to container underneath", shipment.getId());
                    container.getShipments().add(Resource.buildResource()
                            .href(shipment.getShipment().getHref())
                            .id(shipment.getShipment().getId())
                            .name(shipment.getShipment().getName())
                            .status(shipment.getShipment().getStatus())
                            .index(shipment.getShipment().getIndex())
                            .build());

                    shipment.buyPriceProperty().bind(buyPriceProperty);
                    shipment.creditorNameProperty().bind(creditorNameProperty);
                    shipment.creditorEmailProperty().bind(creditorEmailProperty);
                    shipment.creditorPhoneProperty().bind(creditorPhoneProperty);

                });
                change.getRemoved().stream().forEach((shipment) -> {
                    log.debug("removing shipment {} from container underneath", shipment.getId());
                    Optional<Resource> found = container.getShipments().stream().filter((r) -> r.getId().equals(shipment.getId())).findFirst();
                    if (found.get() != null) {
                        container.getShipments().remove(found.get());
                    }

                    shipment.buyPriceProperty().unbind();
                    shipment.creditorNameProperty().unbind();
                    shipment.creditorEmailProperty().unbind();
                    shipment.creditorPhoneProperty().unbind();
                    
                    //
                    // have to explicitly reset values, else they remain visible
                    //
                    shipment.buyPriceProperty().setValue(0.0);
                    shipment.creditorNameProperty().setValue("");
                    shipment.creditorEmailProperty().setValue("");
                    shipment.creditorPhoneProperty().setValue("");

                });

                //
                // update display name for this container
                //
                if (!this.shipmentsList.isEmpty()) {
                    loadCityProperty.bind(this.shipmentsList.get(0).loadCityProperty());
                    unloadCityProperty.bind(this.shipmentsList.get(0).unloadCityProperty());
                } else {
                    loadCityProperty.unbind();
                    unloadCityProperty.unbind();
                    loadCityProperty.setValue("");
                    unloadCityProperty.setValue("");
                }
            }
        });

    }

    public Container getContainer() {
        return container;
    }

    public SimpleDoubleProperty maxLoadingMetersProperty() {
        return maxLoadingMetersProperty;
    }

    public JavaBeanProperty<Double> maxWeightProperty() {
        return maxWeightProperty;
    }

    public SimpleDoubleProperty maxVolumeProperty() {
        return maxVolumeProperty;
    }

    public SimpleDoubleProperty totalShipmentsWeightProperty() {
        return totalShipmentsWeightProperty;
    }

    public SimpleDoubleProperty totalShipmentsVolumeProperty() {
        return totalShipmentsVolumeProperty;
    }

    public SimpleDoubleProperty totalShipmentsLoadingMetersProperty() {
        return totalShipmentsLoadingMetersProperty;
    }

    public ObservableList<ObservableShipment> getShipmentsList() {
        return shipmentsList;
    }

    public JavaBeanProperty<Double> widthProperty() {
        return widthProperty;
    }

    public JavaBeanProperty<Double> heightProperty() {
        return heightProperty;
    }

    public JavaBeanProperty<Double> lengthProperty() {
        return lengthProperty;
    }

    public SimpleStringProperty displayNameProperty() {
        return displayNameProperty;
    }

    public SimpleStringProperty loadCityProperty() {
        return loadCityProperty;
    }

    public SimpleStringProperty unloadCityProperty() {
        return unloadCityProperty;
    }

    public SimpleStringProperty creditorNameProperty() {
        return creditorNameProperty;
    }

    public SimpleStringProperty creditorEmailProperty() {
        return creditorEmailProperty;
    }

    public SimpleStringProperty creditorPhoneProperty() {
        return creditorPhoneProperty;
    }

    public SimpleDoubleProperty buyPriceProperty() {
        return buyPriceProperty;
    }

}
