/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Container;
import eu.logisense.api.Creditor;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Ride;
import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@Messages({
    "rideCardNamePrefix=Ride:"
})
public class ObservableRide extends Savable {

    private static final String SEPARATOR = " to ";
    private static final Logger log = LoggerFactory.getLogger(ObservableRide.class);

    private Ride ride;

//    private final ObservableList<ObservableContainer> containers = FXCollections.observableArrayList();
    private final ObservableList<ObservableContainer> containers
            = FXCollections.observableArrayList(container
                    -> new Observable[]{
                container.sellPriceProperty()});

//    private final JavaBeanProperty<String> customerProperty;
    private final SimpleDoubleProperty totalSellPrice;
    DoubleBinding totalSellPriceBinding = Bindings.createDoubleBinding(() -> containers.stream().collect(Collectors.summingDouble(ObservableContainer::getSellPrice)), containers);

    private final SimpleStringProperty displayNameProperty;
    private final SimpleStringProperty loadCityProperty;
    private final SimpleStringProperty unloadCityProperty;
    private final JavaBeanProperty<String> nameProperty;
    private final JavaBeanProperty<String> idProperty;
    private final JavaBeanProperty<LocalDateTime> EtdProperty;
    private final JavaBeanProperty<String> creditorIdProperty;
    private final SimpleStringProperty creditorNameProperty;
    private final SimpleStringProperty creditorContactEmailProperty;
    private final SimpleStringProperty creditorContactPhoneProperty;
    private final JavaBeanProperty<Double> marginProperty;
    private final SimpleObjectProperty<Double> marginBuyProperty;
    private final SimpleObjectProperty<Double> marginSellProperty;
    private final JavaBeanProperty<String> licensePlateProperty;
    private final JavaBeanProperty<Status> transportStatusProperty;

    private JavaBeanProperty<LoadLocation> loadLocation;
    private JavaBeanProperty<UnloadLocation> unloadLocation;

    private final JavaBeanProperty<Double> priceProperty;
    private final JavaBeanProperty<String> currencyProperty;

    public ObservableRide(Ride ride, ClientStatus clientStatus) throws NoSuchMethodException {
        super(clientStatus, ride.getId());
        Objects.requireNonNull(ride);
        Objects.requireNonNull(ride.getId());
        Objects.requireNonNull(ride.getStatus());

        if (ride.getCreditor() == null) {
            ride.setCreditor(Creditor.builder().build());
        }

        this.displayNameProperty = new SimpleStringProperty();
        this.loadCityProperty = new SimpleStringProperty();
        this.unloadCityProperty = new SimpleStringProperty();
        this.displayNameProperty.bind(Bindings.concat(loadCityProperty, SEPARATOR, unloadCityProperty));

        this.ride = ride;

        //
        // properties to calculate margins with
        //
        totalSellPrice = new SimpleDoubleProperty();
        totalSellPrice.bind(totalSellPriceBinding);

        this.priceProperty = JavaBeanObjectPropertyBuilder.create().bean(ride.getCreditor()).name(Creditor.PROP_PRICE).build();
        this.currencyProperty = JavaBeanObjectPropertyBuilder.create().bean(ride.getCreditor()).name(Creditor.PROP_CURRENCY).build();

//        DoubleProperty totalDebtorPriceProperty = new SimpleDoubleProperty();
        DoubleProperty creditorPriceProperty = new SimpleDoubleProperty();
        creditorPriceProperty.bind(priceProperty);
        DoubleBinding marginBinding = totalSellPrice.subtract(creditorPriceProperty);
        DoubleBinding marginBuyBinding = marginBinding.divide(totalSellPrice).multiply(100);
        DoubleBinding marginSellBinding = marginBinding.divide(creditorPriceProperty).multiply(100);
        this.marginBuyProperty = new SimpleObjectProperty<>();
        this.marginBuyProperty.bind(marginBuyBinding.asObject());
        this.marginSellProperty = new SimpleObjectProperty<>();
        this.marginSellProperty.bind(marginSellBinding.asObject());

//        this.customerProperty = JavaBeanObjectPropertyBuilder.create().bean(ride).name(Ride.PROP_CUSTOMER).build();
        this.nameProperty = JavaBeanObjectPropertyBuilder.create().bean(ride).name(Ride.PROP_NAME).build();
        this.idProperty = JavaBeanObjectPropertyBuilder.create().bean(ride).name(Ride.PROP_ID).build();
        this.EtdProperty = JavaBeanObjectPropertyBuilder.create().bean(ride).name(Ride.PROP_ETD).build();
        this.creditorIdProperty = JavaBeanObjectPropertyBuilder.create().bean(ride.getCreditor()).name(Creditor.PROP_ID).build();
        this.creditorNameProperty = new SimpleStringProperty();
        this.creditorContactEmailProperty = new SimpleStringProperty();
        this.creditorContactPhoneProperty = new SimpleStringProperty();

        this.marginProperty = JavaBeanObjectPropertyBuilder.create().bean(ride).name(Ride.PROP_MARGIN).build();
        this.licensePlateProperty = JavaBeanObjectPropertyBuilder.create().bean(ride).name(Ride.PROP_LICENSE_PLATE).build();
        this.transportStatusProperty = JavaBeanObjectPropertyBuilder.create().bean(ride).name(Ride.PROP_STATUS).build();

        // First LoadLocation based on the first shipment in the first container
        if (ride.getContainers() != null && !ride.getContainers().isEmpty()) {
            Container firstContainer = ride.getContainers().get(0);
            // container has been initialized
            // TODO pending to retrieve Shipment object by the Shipment Link
            /* if (firstContainer.getShipments() != null && !firstContainer.getShipments().isEmpty()) {
             Shipment shipment = firstContainer.getShipments().get(0);
             this.loadLocation = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_LOAD_LOCATION).build();
             }*/
        }

        // Last UnloadLocation based on the last shipment in the last container
        if (ride.getContainers() != null && !ride.getContainers().isEmpty()) {
            Container lastContainer = ride.getContainers().get(ride.getContainers().size() - 1);
            // container has been initialized
            // TODO pending to retrieve Shipment object by the Shipment Link
            /*
             if (lastContainer.getShipments() != null && !lastContainer.getShipments().isEmpty()) {
             Shipment shipment = lastContainer.getShipments().get(lastContainer.getShipments().size() - 1);
             this.unloadLocation = JavaBeanObjectPropertyBuilder.create().bean(shipment).name(Shipment.PROP_UNLOAD_LOCATION).build();
             }*/
        }

        if (this.ride.getContainers() != null) {
            this.ride.getContainers().stream().forEach((container) -> {
                try {
                    ObservableContainer oc = new ObservableContainer(container);
                    containers.add(oc);

                    oc.buyPriceProperty().bind(priceProperty);
                    oc.creditorNameProperty().bind(creditorNameProperty);
                    oc.creditorEmailProperty().bind(creditorContactEmailProperty);
                    oc.creditorPhoneProperty().bind(creditorContactPhoneProperty);

                } catch (NoSuchMethodException ex) {
                    log.warn("unable to create observable container from container {}:{}", container, ex);
                }
            });

            //
            // set display name for ride with display name for first container
            // which in turn gets it from the first shipment
            //
            if (!this.ride.getContainers().isEmpty()) {
                this.loadCityProperty.bind(this.containers.get(0).loadCityProperty());
                this.unloadCityProperty.bind(this.containers.get(0).unloadCityProperty());
                //
                // if first container empty, bind to second if available
                //
                if (this.getContainers().get(0).getShipmentsList().isEmpty() && this.ride.getContainers().size() > 1) {
                    this.loadCityProperty.unbind();
                    this.unloadCityProperty.unbind();
                    this.loadCityProperty.bind(this.containers.get(1).loadCityProperty());
                    this.unloadCityProperty.bind(this.containers.get(1).unloadCityProperty());
                }
            } else {
                this.loadCityProperty.unbind();
                this.unloadCityProperty.unbind();
                this.loadCityProperty.setValue("");
                this.unloadCityProperty.setValue("");
            }

        } else {
            //
            // when ride is saved without any containers, it sets containers to null
            //
            this.ride.setContainers(FXCollections.observableArrayList());
        }

        marginProperty.bind(marginBinding.asObject());

        initListeners();
    }

    private void initListeners() {

        this.containers.addListener((ListChangeListener.Change<? extends ObservableContainer> c) -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    c.getAddedSubList().stream().forEach(oc -> {
                        this.getRide().getContainers().add(oc.getContainer());

                        oc.buyPriceProperty().bind(priceProperty);
                        oc.creditorNameProperty().bind(creditorNameProperty);
                        oc.creditorEmailProperty().bind(creditorContactEmailProperty);
                        oc.creditorPhoneProperty().bind(creditorContactPhoneProperty);
                    });
                }

                //
                // TODO: added for testing, needs implementation on rcp side
                //
                if (c.wasRemoved()) {
                    c.getRemoved().stream().forEach(oc -> {
                        this.getRide().getContainers().remove(oc.getContainer());

                        oc.buyPriceProperty().unbind();
                        oc.creditorNameProperty().unbind();
                        oc.creditorEmailProperty().unbind();
                        oc.creditorPhoneProperty().unbind();
                    });
                }

                //
                // update display name for ride
                //
                if (!this.ride.getContainers().isEmpty()) {
                    this.loadCityProperty.bind(this.containers.get(0).loadCityProperty());
                    this.unloadCityProperty.bind(this.containers.get(0).unloadCityProperty());
                    //
                    // if first container empty, bind to second if available
                    //
                    if (this.getContainers().get(0).getShipmentsList().isEmpty() && this.ride.getContainers().size() > 1
                            && !this.ride.getContainers().get(1).getShipments().isEmpty()) {
                        this.loadCityProperty.unbind();
                        this.unloadCityProperty.unbind();
                        this.loadCityProperty.bind(this.containers.get(1).loadCityProperty());
                        this.unloadCityProperty.bind(this.containers.get(1).unloadCityProperty());
                    }
                } else {
                    this.loadCityProperty.unbind();
                    this.unloadCityProperty.unbind();
                    this.loadCityProperty.setValue("");
                    this.unloadCityProperty.setValue("");
                }
            }
        });

        this.creditorIdProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !"".equals(newValue)) {
                log.debug("updating creditor");
                updateCreditor(newValue);
            }
        });

        //
        // fill on creation
        //
        if (this.creditorIdProperty.getValue() != null) {
            updateCreditor(this.creditorIdProperty.getValue());
        }

        this.nameProperty.addListener(updateListener);
        this.EtdProperty.addListener(updateListener);
        this.creditorIdProperty.addListener(updateListener);
        this.priceProperty.addListener(updateListener);
        this.currencyProperty.addListener(updateListener);
        this.licensePlateProperty.addListener(updateListener);
        this.transportStatusProperty.addListener(updateListener);
        this.containers.addListener(listUpdateListener);

        this.transportStatusProperty.addListener((ObservableValue<? extends Status> observable, Status oldValue, Status newValue) -> {

            ClientStatus currentClientStatus = ClientStatus.from(this.statusProperty().getValue());
            this.ride.setStatus(newValue);

            if (!currentClientStatus.isSavable()) {
                this.statusProperty().setValue(newValue.name());
            }

            if (newValue == TransportStatus.LOADED
                    || newValue == TransportStatus.EN_ROUTE
                    || newValue == TransportStatus.UNLOADED) {

                this.containers.stream().forEach(oc -> {
                    oc.getShipmentsList().stream().forEach(os -> {
                        ObservableShipment observableShipment = Lookup.getDefault().lookup(ShipmentManager.class).getShipment(os.getId());
                        observableShipment.shipmentStatusProperty().setValue(newValue);
                    });
                });
            }
        });

    }

    private void updateCreditor(String id) {
        ObservableClient client = ClientManager.getInstance().getClient(id);

        if (client != null) {

            //
            // reset name binding
            //
            creditorContactEmailProperty.unbind();
            creditorContactPhoneProperty.unbind();
            creditorNameProperty.unbind();
            creditorNameProperty.bind(client.nameProperty());

            //
            // set phone / email from first location if available
            //
            if (!client.getLocations().isEmpty()) {
                creditorContactEmailProperty.bind(client.getLocations().get(0).emailProperty());
                creditorContactPhoneProperty.bind(client.getLocations().get(0).phoneProperty());
            }

        }
    }

    public Ride getRide() {
        return ride;
    }

    public ObservableList<ObservableContainer> getContainers() {
        return containers;
    }

    public SimpleDoubleProperty totalSellPriceProperty() {
        return totalSellPrice;
    }

    public ObservableContainer addContainer(Container container) {
        try {
            ObservableContainer oc = new ObservableContainer(container);
            containers.add(oc);
            return oc;
        } catch (NoSuchMethodException ex) {
            log.warn("unable to create observable container from container {}:{}", container, ex);
            Exceptions.printStackTrace(ex);
            return null;
        }
    }

    @Override
    protected String findDisplayName() {
        return Bundle.rideCardNamePrefix() + displayNameProperty.get();
    }

    @Override
    protected void handleSave() throws IOException {
        log.info("saving ridecard");
        RideManager.getInstance().saveRide(this);
    }

    public SimpleStringProperty displayNameProperty() {
        return displayNameProperty;
    }

    public SimpleStringProperty loadCityProperty() {
        return loadCityProperty;
    }

    public SimpleStringProperty unloadCityProperty() {
        return unloadCityProperty;
    }

    public JavaBeanProperty<LocalDateTime> etdProperty() {
        return EtdProperty;
    }

    public JavaBeanProperty<LoadLocation> loadLocationProperty() {
        return loadLocation;
    }

    public JavaBeanProperty<UnloadLocation> unloadLocationProperty() {
        return unloadLocation;
    }

    public JavaBeanProperty<String> creditorIdProperty() {
        return creditorIdProperty;
    }

    public SimpleStringProperty creditorNameProperty() {
        return creditorNameProperty;
    }

    public SimpleStringProperty creditorContactEmailProperty() {
        return creditorContactEmailProperty;
    }

    public SimpleStringProperty creditorContactPhoneProperty() {
        return creditorContactPhoneProperty;
    }

    public JavaBeanProperty<Double> priceProperty() {
        return priceProperty;
    }

    public JavaBeanProperty<String> currencyProperty() {
        return currencyProperty;
    }

    public JavaBeanProperty<String> idProperty() {
        return idProperty;
    }

    public JavaBeanProperty<String> nameProperty() {
        return nameProperty;
    }

    public JavaBeanProperty<Double> marginProperty() {
        return marginProperty;
    }

    public SimpleObjectProperty<Double> marginBuyProperty() {
        return marginBuyProperty;
    }

    public SimpleObjectProperty<Double> marginSellProperty() {
        return marginSellProperty;
    }

    public JavaBeanProperty<String> licensePlateProperty() {
        return licensePlateProperty;
    }

    @Override
    SaveManager getSaveManager() {
        return RideManager.getInstance().getRideSaveManager();
    }

    public JavaBeanProperty<Status> transportStatusProperty() {
        return transportStatusProperty;
    }
}
