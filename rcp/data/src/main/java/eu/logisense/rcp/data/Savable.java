/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Status;
import java.util.Objects;
import java.util.UUID;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import org.netbeans.spi.actions.AbstractSavable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public abstract class Savable extends AbstractSavable implements ChangeListener<String> {

    private static final Logger log = LoggerFactory.getLogger(Savable.class);
    private final String id;

    protected final ListChangeListener listUpdateListener = (ListChangeListener) (ListChangeListener.Change c) -> {
        if (c.next()) {
            log.debug("change in child list {} detected: {} added, {} removed ",c.getList(),c.getAddedSize(),c.getRemovedSize());
            ClientStatus status = ClientStatus.from(Savable.this.statusProperty().getValue());
            if (!status.isSavable() && !ClientStatus.ENTRY.equals(status)) {
                log.debug("status is not savabale and not entry so setting to changed");
                Savable.this.statusProperty().setValue(ClientStatus.CHANGED.name());
            }
        }
    };

    protected final ChangeListener updateListener = (ChangeListener) (ObservableValue observable, Object oldValue, Object newValue) -> {
        log.debug("property value {} changed from {} to {}", observable, oldValue, newValue);
        ClientStatus status = ClientStatus.from(Savable.this.statusProperty().getValue());
        if (!status.isSavable() && !ClientStatus.ENTRY.equals(status)) {
            log.debug("status is not savabale and not entry so setting to changed");
            Savable.this.statusProperty().setValue(ClientStatus.CHANGED.name());
        }
    };

    public Savable(Status status, String id) {
        Objects.requireNonNull(status);
//        Objects.requireNonNull(id);
        this.id = id == null ? UUID.randomUUID().toString() : id;
        statusProperty.addListener(this);
        statusProperty.setValue(status.name());
    }

    abstract SaveManager getSaveManager();

    String getId() {
        return id;
    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        ClientStatus from = ClientStatus.from(oldValue);
        ClientStatus to = ClientStatus.from(newValue);

        if (to.isSavable() && !from.isSavable()) {
            log.debug("registering savable");
            register();
            getSaveManager().notifyChanged(this);
        } else if (!to.isSavable() && from.isSavable()) {
            log.debug("unregistering savable");
            unregister();
            getSaveManager().notifySaved(this);
        }
    }

    public StringProperty statusProperty() {
        return statusProperty;
    }

    /**
     * cancel creation of new object, needed to remove it from the save manager
     */
    public void cancel() {
        unregister();
    }

    public enum ClientStatus implements Status {
        /**
         * stage where new object is being entered
         */
        /**
         * stage where new object is being entered
         */
        ENTRY(false),
        /**
         * new object saved to backend
         */
        NEW(false),
        /**
         * new object not yet saved to backend
         */
        UNSAVED(true),
        /**
         * existing object with changes, ready to be saved
         */
        CHANGED(true),
        APPROVED(false),
        REJECTED(false),
        DISPATCHED(false),
        CANCELLED(false),
        LOADED(false),
        UNLOADED(false),
        EN_ROUTE(false),
        PLANNED(false),
        UNKNOWN(false);

        private final boolean savable;

        public boolean isSavable() {
            return savable;
        }

        private ClientStatus(boolean savable) {
            this.savable = savable;
        }

        public static ClientStatus from(String value) {
            if (value == null) {
                return UNKNOWN;
            }
            try {
                return ClientStatus.valueOf(value);
            } catch (IllegalArgumentException e) {
                return UNKNOWN;
            }
        }

        public static ClientStatus from(Status value) {
            if (value == null) {
                return UNKNOWN;
            }
            try {
                return ClientStatus.valueOf(value.name());
            } catch (IllegalArgumentException e) {
                return UNKNOWN;
            }
        }

    }

    private final StringProperty statusProperty = new SimpleStringProperty();

    /**
     * equals and hashcode should not change when object is changed, otherwise abstracysavable cannot find it in the registry
     * anymore and will complain about it and not handle the save
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Savable) {
            return id.equals(((Savable) obj).getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
