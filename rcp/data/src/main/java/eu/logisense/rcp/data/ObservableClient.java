/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Client;
import java.io.IOException;
import java.util.Objects;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@NbBundle.Messages(value = {"clientNamePrefix=Client"})
public class ObservableClient extends ObservableLocation {

    private static final Logger log = LoggerFactory.getLogger(ObservableClient.class);
    private final Client client;

    private final JavaBeanProperty<String> nameProperty;
    private final ObservableList<ObservableLocation> locations = FXCollections.observableArrayList();

    private final JavaBeanProperty<Boolean> chargeVatProperty;
    private final JavaBeanProperty<String> chamberOfCommerceCityProperty;
    private final JavaBeanProperty<String> chamberOfCommerceProperty;
    private final JavaBeanProperty<String> vatNumberProperty;
    private final JavaBeanProperty<String> bicCodeProperty;
    private final JavaBeanProperty<String> ibanCodeProperty;
    private final JavaBeanProperty<String> currencyProperty;
    private final JavaBeanProperty<String> externalIdProperty;

    public ObservableClient(Client client,  ClientStatus clientStatus) throws NoSuchMethodException {
        //FIXME set default location object directly on the client (public Location client.getDefaultLocation()) and use here
        super(clientStatus, client.getId(), client.getLocations().get(0));
        Objects.requireNonNull(client);
        Objects.requireNonNull(client.getLocations());
        Objects.requireNonNull(client.getStatus());

        this.client = client;
        client.getLocations().stream().forEach((location) -> {
            try {
                locations.add(new ObservableLocation(location));
            } catch (NoSuchMethodException ex) {
                Exceptions.printStackTrace(ex);
            }
            
        });
        //
        // sync observable list with location with the locations in the client object
        //
        this.locations.addListener((ListChangeListener.Change<? extends ObservableLocation> change) -> {
            while (change.next()) {
                change.getAddedSubList().stream().forEach((loc) -> {
                    client.getLocations().add(loc.getLocation());
                });
                change.getRemoved().stream().forEach((loc) -> {
                    client.getLocations().remove(loc.getLocation());
                });
            }
        });
        
        this.locations.addListener(listUpdateListener);

        this.nameProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_NAME).build();

        this.chargeVatProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_CHARGE_VAT).build();
        this.vatNumberProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_VAT_NUMBER).build();
        this.chamberOfCommerceProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_CHAMBER_OF_COMMERCE).build();
        this.chamberOfCommerceCityProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_CHAMBER_OF_COMMERCE_CITY).build();
        this.bicCodeProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_BIC_CODE).build();
        this.ibanCodeProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_IBAN_CODE).build();
        this.currencyProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_CURRENCY).build();
        this.externalIdProperty = JavaBeanObjectPropertyBuilder.create().bean(client).name(Client.PROP_EXTERNAL_ID).build();

        this.nameProperty.addListener(updateListener);
        this.chargeVatProperty.addListener(updateListener);
        this.vatNumberProperty.addListener(updateListener);
        this.chamberOfCommerceProperty.addListener(updateListener);
        this.chamberOfCommerceCityProperty.addListener(updateListener);
        this.bicCodeProperty.addListener(updateListener);
        this.ibanCodeProperty.addListener(updateListener);
        this.currencyProperty.addListener(updateListener);

    }

    public Client getClient() {
        return client;
    }

    public ObservableList<ObservableLocation> getLocations() {
        return locations;
    }

    @Override
    protected String findDisplayName() {
        return client.getName();
    }

    @Override
    protected void handleSave() throws IOException {
        ClientManager.getInstance().saveClient(this);
    }

    @Override
    public JavaBeanProperty<String> nameProperty() {
        return nameProperty;
    }

    @Override
    public JavaBeanProperty<String> externalIdProperty() {
        return externalIdProperty;
    }

    public JavaBeanProperty<Boolean> chargeVatProperty() {
        return chargeVatProperty;
    }

    public JavaBeanProperty<String> chamberOfCommerceCityProperty() {
        return chamberOfCommerceCityProperty;
    }

    public JavaBeanProperty<String> chamberOfCommerceProperty() {
        return chamberOfCommerceProperty;
    }

    public JavaBeanProperty<String> vatNumberProperty() {
        return vatNumberProperty;
    }

    public JavaBeanProperty<String> bicCodeProperty() {
        return bicCodeProperty;
    }

    public JavaBeanProperty<String> ibanCodeProperty() {
        return ibanCodeProperty;
    }

    public JavaBeanProperty<String> currencyProperty() {
        return currencyProperty;
    }

}
