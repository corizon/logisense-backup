/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.logisense.api.Client;
import eu.logisense.api.ClientService;
import eu.logisense.api.Location;
import eu.logisense.api.LocationService;
import eu.logisense.api.RideService;
import eu.logisense.api.util.ClientUtil;
import eu.logisense.api.session.client.Connections;
import eu.logisense.rcp.data.Savable.ClientStatus;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javax.ws.rs.core.MediaType;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ClientManager {

    private static final Logger log = LoggerFactory.getLogger(ClientManager.class);

    // Shipment circle
    //
    private final ObservableList<ObservableLocation> locations = FXCollections.observableArrayList();

    // Client circle
    //
    private final ObservableList<ObservableClient> clients = FXCollections.observableArrayList();
    private final ObjectProperty<Predicate<? super ObservableClient>> usableClientsPredicateProperty;
    private final SortedList<ObservableClient> usableClients;
    private final UsableClientsFilterUpdater usableClientsFilterUpdater = new UsableClientsFilterUpdater();

    // Services
    //
    private final ClientService clientService = ClientUtil.proxy(ClientService.class);
    private final LocationService locationService = ClientUtil.proxy(LocationService.class);

    // Managers
    //
    private final ClientLocationSaveManager clientLocationSaveManager = new ClientLocationSaveManager();

    // Instace
    //
    private static final ClientManager INSTANCE = new ClientManager();
    
    //
    // BooleanProperty to expose if initial client load is done
    //
    private SimpleBooleanProperty isDoneLoadingProperty = new SimpleBooleanProperty(Boolean.FALSE);
    
    public static ClientManager getInstance() {
        return INSTANCE;
    }

    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    /**
     * sets predicate to "new" value so filtered list gets refreshed
     */
    private void updateUsableClientsFilter() {
        log.warn("updateFilter");
        //
        // setting first to null because there is no other way to update a filtered list :S
        //
        usableClientsPredicateProperty.setValue(null);
        usableClientsPredicateProperty.setValue(client
                -> !ClientStatus.UNSAVED.equals(ClientStatus.from(client.statusProperty().getValue()))
        );
    }

    private ClientManager() {

        //
        //
        //
        FilteredList<ObservableClient> filteredClients = clients.filtered(client
                -> !ClientStatus.UNSAVED.equals(ClientStatus.from(client.statusProperty().getValue()))
        );
        usableClientsPredicateProperty = filteredClients.predicateProperty();
        usableClients = new SortedList<>(filteredClients);
        clients.addListener(usableClientsFilterUpdater);

        //
        // temporary list to prevent concurrentModificationException
        //
        ObservableList<ObservableClient> tempList = FXCollections.observableArrayList();
        
        Connections.onConnection(() -> {
            log.info("initializing clients");
            clientService.getClients().stream().forEach((client) -> {

                if (client.getLocations().isEmpty()) {
                    log.warn("client {} found without location, skipping it", client.getId());

                }
                if (client.getStatus() != null && client.getLocations() != null && !client.getLocations().isEmpty()) {
                    log.info("***** client {} {} {}", client, client.getStatus(), client.getLocations());
                    try {
                        tempList.add(new ObservableClient(client, ClientStatus.from(client.getStatus())));
                    } catch (NoSuchMethodException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
                log.info("adding client {}", client);

            });
            
            //
            // after loop, add all items in templist and set boolean prop to true
            //
            clients.addAll(tempList);
            isDoneLoadingProperty.setValue(Boolean.TRUE);
            
        });
        Connections.onConnection(() -> {
            log.info("initializing locations");
            locationService.getLocations().stream().forEach((location) -> {
                try {
                    locations.add(new ObservableLocation(location));
                    log.info("adding location {}", location);
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                    Exceptions.printStackTrace(ex);
                }
            });
        });

    }

    public SimpleBooleanProperty isDoneLoadingProperty() {
        return isDoneLoadingProperty;
    }
    
    public ObservableList<ObservableLocation> getLocations() {
        return locations;
    }

    public SortedList<ObservableClient> getUsableClients() {
        return usableClients;
    }

    public ObservableList<ObservableClient> getClients() {
        return clients;
    }

    public ObservableClient getClient(String id) {
        log.debug("get client {}",id);
        Objects.requireNonNull(id);
        ObservableClient observableClient = null;

        //
        // look for client in local list
        //
        for (ObservableClient oc : clients) {
            if (oc.getId().equals(id)) {
                observableClient = oc;
            }
        }

        //
        // if still null, check server
        //
        if (observableClient == null) {
            Client client = clientService.getClient(id);
            if (client != null) {
                try {
                    observableClient = new ObservableClient(client, ClientStatus.from(client.getStatus()));
                } catch (NoSuchMethodException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }

        return observableClient;
    }

    public ClientLocationSaveManager getClientLocationSaveManager() {
        return clientLocationSaveManager;
    }

    private void logDebug(String message, Object o, Class c) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JSR310Module());
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            jsonProvider.writeTo(o, c, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
            log.debug(message, baos.toString());

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    void saveClient(ObservableClient client) {
        log.debug("saveClient");
        executor.submit(() -> {
            ClientStatus status = Savable.ClientStatus.from(client.statusProperty().getValue());
            log.debug("saving client {} with status {} which resolves to client status {}", client, client.statusProperty().getValue(), status);
            switch (status) {
                case UNSAVED:
                    logDebug("new client {}", client.getClient(), Client.class);
                    try {
                        clientService.postClient(client.getClient());
                        client.statusProperty().setValue(client.getClient().getStatus().name());
                        client.getLocations().forEach((location) -> {
                            location.statusProperty().setValue(location.getLocation().getStatus().name());
                            //
                            // add to location list directly
                            //
                            locations.add(location);
                        });
                    } catch (Exception e) {
                        log.error("Error saving client{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    log.debug("client saved");
                    break;
                case CHANGED:
                    logDebug("update client {}", client.getClient(), Client.class);
                    clientService.updateClient(client.getClient().getId(), client.getClient());
                    try {
                        client.statusProperty().setValue(client.getClient().getStatus().name());
                        client.statusProperty().setValue(client.getClient().getStatus().name());
                        client.getLocations().forEach((location) -> {
                            location.statusProperty().setValue(location.getLocation().getStatus().name());
                            //
                            // add to location list directly
                            //
                            locations.add(location);
                        });
                    } catch (Exception e) {
                        log.error("Error updating client{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    log.debug("client saved");
                    break;
                default:
                    log.warn("trying to save client with unsavable status {} ", client.statusProperty().getValue());
                    break;
            }
        });
    }

    void saveClient(ObservableLocation location) {
        log.debug("saveLocation");
        executor.submit(() -> {
            ClientStatus status = Savable.ClientStatus.from(location.statusProperty().getValue());
            log.debug("saving location {} with status {} which resolves to location status {}", location, location.statusProperty().getValue(), status);
            switch (status) {
                case UNSAVED:
                    logDebug("new client {}", location.getLocation(), Client.class);
                    try {
                        locationService.postLocation(location.getLocation());
                        location.statusProperty().setValue(location.getLocation().getStatus().name());

                    } catch (Exception e) {
                        log.error("Error saving client{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    log.debug("location saved");
                    break;
                case CHANGED:
                    logDebug("update location {}", location.getLocation(), Location.class);
                    locationService.updateLocation(location.getLocation().getId(), location.getLocation());
                    try {
                        location.statusProperty().setValue(location.getLocation().getStatus().name());
                        location.statusProperty().setValue(location.getLocation().getStatus().name());

                    } catch (Exception e) {
                        log.error("Error updating client{}", e);
                        Exceptions.printStackTrace(e);
                    }
                    log.debug("client saved");
                    break;
                default:
                    log.warn("trying to save location with unsavable status {} ", location.statusProperty().getValue());
                    break;
            }
        });
    }

    public void newClient(ObservableClient client) {
        log.debug("newClient");
        clientLocationSaveManager.notifyChanged(client);
        clients.add(client);
    }

    public void removeClient(ObservableClient observableClient) {
        executor.submit(() -> {
            try {
                Savable.ClientStatus status = Savable.ClientStatus.from(observableClient.statusProperty().getValue());
                if (!Savable.ClientStatus.UNSAVED.equals(status) && !Savable.ClientStatus.ENTRY.equals(status)) {
                    clientService.deleteClient(observableClient.getId());
                }
                clients.remove(observableClient);
                clientLocationSaveManager.notifySaved(observableClient);
                log.debug("client removed");
            } catch (Exception e) {
                log.error("Error removing client{}", e);
                Exceptions.printStackTrace(e);
            }
        });
    }

    public class ClientLocationSaveManager implements SaveManager<ObservableLocation> {

        private final InstanceContent ic = new InstanceContent();
        private final Lookup lookup = new AbstractLookup(ic);

        public void notifyChanged(ObservableLocation location) {
            log.debug("adding client to save maneger");
            ic.add(location);
        }

        public void notifySaved(ObservableLocation location) {
            log.debug("removing client from save maneger");
            ic.remove(location);
        }

        @Override
        public Lookup getLookup() {
            return lookup;
        }
    }

    /**
     * 1 - listens to changes in the shipments list and adds itself as listener
     * to all container properties 2 - listens to changes in thge container
     * property and updates filter when changed
     */
    class UsableClientsFilterUpdater implements ListChangeListener<ObservableClient>, ChangeListener<String> {

        /**
         * fire on every add or remove from the shipments list
         *
         * @param c
         */
        @Override
        public void onChanged(ListChangeListener.Change<? extends ObservableClient> c) {
            while (c.next()) {
                if (c.wasAdded()) {
                    c.getAddedSubList().stream().forEach((observableClient) -> {
                        log.debug("adding listener to new client");
                        observableClient.statusProperty().addListener(this);
                    });
                }
                if (c.wasRemoved()) {
                    c.getRemoved().stream().forEach((observableClient) -> {
                        log.debug("removing listener from removed client");
                        observableClient.statusProperty().removeListener(this);
                    });
                }
            }
        }

        /**
         * fire on every change of container property of a shipment.
         *
         * @param observable
         * @param oldValue
         * @param newValue
         */
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            updateUsableClientsFilter();
        }

    }

}
