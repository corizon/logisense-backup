/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Client;
import eu.logisense.api.Container;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.Origin;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ObservableContainerNGTest {

    public ObservableContainerNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    @Test
    public void testMaxVolumeBinding() throws NoSuchMethodException {
        System.out.println("testMaxVolumeBinding");
        ObservableContainer instance = new ObservableContainer(Container.builder()
                .href("")
                .id("c1")
                .height(2.0)
                .width(2.45)
                .length(16.0)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .build());

        assertEquals(instance.maxVolumeProperty().getValue(), 2.0 * 2.45 * 16.0, 0.0001);
        instance.widthProperty().setValue(1.65);
        assertEquals(instance.maxVolumeProperty().getValue(), 2.0 * 1.65 * 16.0, 0.0001);
        instance.heightProperty().setValue(2.60);
        assertEquals(instance.maxVolumeProperty().getValue(), 2.6 * 1.65 * 16.0, 0.0001);
        instance.lengthProperty().setValue(12.45);
        assertEquals(instance.maxVolumeProperty().getValue(), 2.6 * 1.65 * 12.45, 0.0001);
    }

    @Test
    public void testMaxLoadingMetersBinding() throws NoSuchMethodException {
        System.out.println("testMaxVolumeBinding");
        ObservableContainer instance = new ObservableContainer(Container.builder()
                .href("")
                .id("c1")
                .height(2.0)
                .width(2.45)
                .length(16.0)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .build());

        assertEquals(instance.maxLoadingMetersProperty().getValue(), 16.0 * 2.45 / Container.STANDARD_WIDTH, 0.0001);
        instance.widthProperty().setValue(1.65);
        assertEquals(instance.maxLoadingMetersProperty().getValue(), 16.0 * 1.65 / Container.STANDARD_WIDTH, 0.0001);
        instance.heightProperty().setValue(2.60);
        assertEquals(instance.maxLoadingMetersProperty().getValue(), 16.0 * 1.65 / Container.STANDARD_WIDTH, 0.0001);
        instance.lengthProperty().setValue(12.45);
        assertEquals(instance.maxLoadingMetersProperty().getValue(), 12.45 * 1.65 / Container.STANDARD_WIDTH, 0.0001);
    }

    /**
     * Test of sellPriceProperty method, of class ObservableContainer.
     */
    @Test
    public void testBindings() throws NoSuchMethodException {
        System.out.println("bindings");
        ObservableContainer instance = new ObservableContainer(Container.builder()
                .height(Double.NaN)
                .href("")
                .id("c1")
                .length(Double.NaN)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .build());

        assertEquals(instance.getSellPrice(), 0.0);
        assertEquals(instance.totalShipmentsLoadingMetersProperty().get(), 0.0);
        assertEquals(instance.totalShipmentsVolumeProperty().get(), 0.0);
        assertEquals(instance.totalShipmentsWeightProperty().get(), 0.0);

        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(3)
                .height(3.0)
                .width(4.0)
                .length(2.0)
                .weight(999.9)
                .build());

        ObservableShipment shipment1 = new ObservableShipment(Shipment.builder()
                .container(instance.getContainer())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("1")
                .loadLocation(LoadLocation.builder().id("").build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder().build())
                .build(), Savable.ClientStatus.NEW);

        instance.getShipmentsList().add(shipment1);

        assertEquals(instance.getSellPrice(), 111.11);
        assertEquals(instance.totalShipmentsLoadingMetersProperty().get(), 4 * 2 * 3 / 2.45);
        assertEquals(instance.totalShipmentsVolumeProperty().get(), 3 * 3 * 4 * 2.0);
        assertEquals(instance.totalShipmentsWeightProperty().get(), 999.9);

        instance.getShipmentsList().add(shipment1);

        assertEquals(instance.getSellPrice(), 222.22);
        assertEquals(instance.totalShipmentsLoadingMetersProperty().get(), 4 * 2 * 3 / 2.45 * 2);
        assertEquals(instance.totalShipmentsVolumeProperty().get(), 3 * 3 * 4 * 2.0 * 2);
        assertEquals(instance.totalShipmentsWeightProperty().get(), 999.9 * 2);

        instance.getShipmentsList().remove(shipment1);

        assertEquals(instance.getSellPrice(), 111.11);
        assertEquals(instance.totalShipmentsLoadingMetersProperty().get(), 4 * 2 * 3 / 2.45);
        assertEquals(instance.totalShipmentsVolumeProperty().get(), 3 * 3 * 4 * 2.0);
        assertEquals(instance.totalShipmentsWeightProperty().get(), 999.9);
    }

    @Test
    /**
     * Ride display name should be filled with load + unload location from first
     * shipment in first container
     *
     */
    public void testGetDisplayName() throws NoSuchMethodException {
        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(3)
                .height(3.0)
                .width(4.0)
                .length(2.0)
                .weight(999.9)
                .build());

        ObservableShipment shipment5 = new ObservableShipment(Shipment.builder()
                .container(Resource.buildResource().build())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("5")
                .loadLocation(LoadLocation.builder()
                        .id("")
                        .city("LOAD")
                        .build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder()
                        .id("")
                        .city("UNLOAD")
                        .build())
                .build(), Savable.ClientStatus.NEW);

        ObservableShipment shipment6 = new ObservableShipment(Shipment.builder()
                .container(Resource.buildResource().build())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("5")
                .loadLocation(LoadLocation.builder()
                        .id("")
                        .city("LOAD2")
                        .build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder()
                        .id("")
                        .city("UNLOAD2")
                        .build())
                .build(), Savable.ClientStatus.NEW);

        ObservableContainer instance = new ObservableContainer(Container.builder()
                .height(Double.NaN)
                .href("")
                .id("c1")
                .length(Double.NaN)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .shipments(new ArrayList<>())
                .build());


        String separator = " to ";
        
        StringBuilder shipment5name = new StringBuilder();
        shipment5name.append(shipment5.loadCityProperty().getValue());
        shipment5name.append(separator);
        shipment5name.append(shipment5.unloadCityProperty().getValue());
        
        StringBuilder shipment6name = new StringBuilder();
        shipment6name.append(shipment6.loadCityProperty().getValue());
        shipment6name.append(separator);
        shipment6name.append(shipment6.unloadCityProperty().getValue());

        //
        // test without shipments, should be " to "
        //
        assertEquals(instance.displayNameProperty().getValue(), separator);
        
        //
        // test with 2 shipments, should be name of first, ie shipment5
        //
        instance.getShipmentsList().addAll(shipment5, shipment6);
        assertEquals(instance.displayNameProperty().getValue(), shipment5name.toString());

        
        //
        // test with 1 shipment, should be name of first, ie shipment6
        //
        instance.getShipmentsList().remove(shipment5);
        assertEquals(instance.displayNameProperty().getValue(), shipment6name.toString());
        
        //
        // test without shipments again, should be " to "
        //
        instance.getShipmentsList().remove(shipment6);
        assertEquals(instance.displayNameProperty().getValue(), separator);

        //
        // retest with 1 shipment, should be name of first, ie shipment5
        //
        instance.getShipmentsList().add(shipment5);
        assertEquals(instance.displayNameProperty().getValue(), shipment5name.toString());
        
    }

}
