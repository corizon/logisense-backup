/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Client;
import eu.logisense.api.Container;
import eu.logisense.api.Creditor;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.Origin;
import eu.logisense.api.Resource;
import eu.logisense.api.Ride;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.ObservableList;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ObservableRideNGTest {

    private static final Logger log = LoggerFactory.getLogger(ObservableRideNGTest.class);

    public ObservableRideNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        MockServices.setServices(MockShipmentManager.class);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        MockShipmentManager.clear();
    }

    public static class MockShipmentManager implements ShipmentManager {

        static Map<String, ObservableShipment> shipments = new HashMap<>();

        static void addShipment(ObservableShipment shipment) {
            shipments.put(shipment.getId(), shipment);
        }

        static void clear() {
            shipments.clear();
        }

        @Override
        public void deleteShipment(String id) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ObservableShipment getShipment(String id) {
            return shipments.get(id);
        }

        @Override
        public ObservableShipment getShipmentByHref(String href) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public SaveManager getShipmentSaveManager() {
            return new SaveManager() {
                @Override
                public void notifyChanged(Savable savable) {
                }

                @Override
                public void notifySaved(Savable savable) {
                }

                @Override
                public Lookup getLookup() {
                    return Lookup.getDefault();
                }
            };
        }

        @Override
        public ObservableList<ObservableShipment> getShipments() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ObservableList<ObservableShipment> getShipmentsForStatus(TransportStatus... forStatus) throws NoSuchMethodException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void newShipment(ObservableShipment observableShipment) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ObservableShipment postShipment(ObservableShipment template) throws NoSuchMethodException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void removeShipment(ObservableShipment observableShipment) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void saveShipment(ObservableShipment observableShipment) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ObservableShipment updateShipment(ObservableShipment template) throws NoSuchMethodException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    /**
     * Test of getRide method, of class ObservableRide.
     *
     * @throws java.lang.NoSuchMethodException
     */
    @Test
    public void testGetRide() throws NoSuchMethodException {
        System.out.println("getRide");

        Resource cr = Resource.buildResource().build();

        ObservableShipment shipment1 = new ObservableShipment(Shipment.builder()
                .container(cr)
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("1")
                .loadLocation(LoadLocation.builder().id("").build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(new ArrayList<>())
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(3)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder().build())
                .build(), Savable.ClientStatus.NEW);

        MockShipmentManager.addShipment(shipment1);

        ObservableShipment shipment2 = new ObservableShipment(Shipment.builder()
                .container(cr)
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("2")
                .loadLocation(LoadLocation.builder().id("").build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(123.45)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(new ArrayList<>())
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(3)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder().build())
                .build(), Savable.ClientStatus.NEW);

        MockShipmentManager.addShipment(shipment2);

        ObservableShipment shipment3 = new ObservableShipment(Shipment.builder()
                .container(cr)
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("3")
                .loadLocation(LoadLocation.builder().id("").build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(222.22)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(new ArrayList<>())
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(3)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder().build())
                .build(), Savable.ClientStatus.NEW);

        MockShipmentManager.addShipment(shipment3);

        List<Resource> shipments = new ArrayList<>();
        shipments.add(Resource.buildResource()
                .href("")
                .id("1")
                .index(Integer.SIZE)
                .name("")
                .build());
        shipments.add(Resource.buildResource()
                .href("")
                .id("2")
                .index(Integer.SIZE)
                .name("")
                .build());
//        shipments.add(Resource.buildResource()
//                .href("")
//                .id("3")
//                .index(Integer.SIZE)
//                .name("")
//                .build());        

        List<Container> containers = new ArrayList<>();
        containers.add(Container.builder()
                .height(Double.NaN)
                .href("")
                .id("")
                .length(Double.NaN)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .shipments(shipments)
                .build());

        Ride ride = Ride.builder()
                .containers(containers)
                .creditor(Creditor.builder().build())
                .etd(LocalDateTime.MIN)
                .externalId("")
                .href("")
                .id("1234567890")
                .licensePlate("")
                .margin(Double.NaN)
                .name("")
                .status(TransportStatus.NEW)
                .build();

        log.debug("starting test");

        ObservableRide instance = new ObservableRide(ride, Savable.ClientStatus.NEW);
        assertEquals(instance.getContainers().size(), 1);
        instance.getContainers().stream().forEach((container) -> {
            assertEquals(container.getShipmentsList().size(), 2);
        });

        assertEquals(instance.totalSellPriceProperty().getValue(), 234.56);
        assertEquals(shipment1.getPrice(), 111.11);
        shipment1.sellPriceProperty().setValue(222.22);
        assertEquals(shipment1.getPrice(), 222.22);
        assertEquals(instance.totalSellPriceProperty().getValue(), 345.67);

        // adding shipment 3 to the container
        instance.getContainers().get(0).getShipmentsList().add(ShipmentManagerImpl.getInstance().getShipment("3"));
        instance.getContainers().stream().forEach((container) -> {
            assertEquals(container.getShipmentsList().size(), 3);
        });

        assertEquals(shipment3.getPrice(), 222.22);
        shipment3.sellPriceProperty().setValue(113.11);
        shipment2.sellPriceProperty().setValue(113.11);
        assertEquals(shipment3.getPrice(), 113.11);
        assertEquals(instance.totalSellPriceProperty().getValue(), 448.44);

    }

    @Test
    public void testListChangeListener() throws NoSuchMethodException {
        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(3)
                .height(3.0)
                .width(4.0)
                .length(2.0)
                .weight(999.9)
                .build());

        ObservableShipment shipment1 = new ObservableShipment(Shipment.builder()
                .container(Resource.buildResource().build())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("1")
                .loadLocation(LoadLocation.builder().id("").build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder().build())
                .build(), Savable.ClientStatus.NEW);

        ObservableContainer container = new ObservableContainer(Container.builder()
                .height(Double.NaN)
                .href("")
                .id("c1")
                .length(Double.NaN)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .build());

        ObservableRide instance = new ObservableRide(Ride.builder()
                .id("1")
                .status(TransportStatus.NEW)
                .build(), Savable.ClientStatus.NEW);

        assertEquals(instance.statusProperty().getValue(), Savable.ClientStatus.NEW.toString());
        // adding container should trigger change
        instance.getContainers().add(container);
        assertEquals(instance.statusProperty().getValue(), Savable.ClientStatus.CHANGED.toString());
        // reset status
        instance.statusProperty().setValue(Savable.ClientStatus.NEW.name());
        assertEquals(instance.statusProperty().getValue(), Savable.ClientStatus.NEW.toString());
        // adding shipment to container should trigger change
        container.getShipmentsList().add(shipment1);
        assertEquals(instance.statusProperty().getValue(), Savable.ClientStatus.CHANGED.toString());
        // reset status
        instance.statusProperty().setValue(Savable.ClientStatus.NEW.name());
        assertEquals(instance.statusProperty().getValue(), Savable.ClientStatus.NEW.toString());
        // removing shipment from container should trigger change          
        container.getShipmentsList().remove(shipment1);
        assertEquals(instance.statusProperty().getValue(), Savable.ClientStatus.CHANGED.toString());
    }

    @Test
    public void testPropagateRideStatusUpdate() throws NoSuchMethodException {
        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(3)
                .height(3.0)
                .width(4.0)
                .length(2.0)
                .weight(999.9)
                .build());

        ObservableShipment shipment4 = new ObservableShipment(Shipment.builder()
                .container(Resource.buildResource().build())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("4")
                .loadLocation(LoadLocation.builder().id("").build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder().build())
                .build(), Savable.ClientStatus.NEW);

        MockShipmentManager.addShipment(shipment4);

        ObservableContainer container = new ObservableContainer(Container.builder()
                .height(Double.NaN)
                .href("")
                .id("c1")
                .length(Double.NaN)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .shipments(new ArrayList<>())
                .build());

        ObservableRide instance = new ObservableRide(Ride.builder()
                .id("1")
                .status(TransportStatus.NEW)
                .containers(new ArrayList<>())
                .build(), Savable.ClientStatus.NEW);

        container.getShipmentsList().add(shipment4);
        instance.getContainers().add(container);

        ObservableShipment shipmentFromManager = ShipmentManagerImpl.getInstance().getShipment("4");

        assertEquals(instance.transportStatusProperty().getValue(), TransportStatus.NEW);
        assertEquals(shipmentFromManager.shipmentStatusProperty().getValue(), TransportStatus.NEW);
        //
        // setting ride to LOADED, all shipments underneath should follow
        //
        instance.transportStatusProperty().setValue(TransportStatus.LOADED);

        assertEquals(instance.transportStatusProperty().getValue(), TransportStatus.LOADED);
        assertEquals(shipmentFromManager.shipmentStatusProperty().getValue(), TransportStatus.LOADED);

    }

    @Test
    /**
     * Ride display name should be filled with load + unload location from first
     * shipment in first container
     *
     */
    public void testGetDisplayName() throws NoSuchMethodException {

        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(3)
                .height(3.0)
                .width(4.0)
                .length(2.0)
                .weight(999.9)
                .build());

        ObservableShipment shipment5 = new ObservableShipment(Shipment.builder()
                .container(Resource.buildResource().build())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("5")
                .loadLocation(LoadLocation.builder()
                        .id("")
                        .city("LOAD")
                        .build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder()
                        .id("")
                        .city("UNLOAD")
                        .build())
                .build(), Savable.ClientStatus.NEW);

        ObservableShipment shipment6 = new ObservableShipment(Shipment.builder()
                .container(Resource.buildResource().build())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("5")
                .loadLocation(LoadLocation.builder()
                        .id("")
                        .city("LOAD2")
                        .build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder()
                        .id("")
                        .city("UNLOAD2")
                        .build())
                .build(), Savable.ClientStatus.NEW);

        MockShipmentManager.addShipment(shipment5);

        ObservableContainer container = new ObservableContainer(Container.builder()
                .height(Double.NaN)
                .href("")
                .id("c1")
                .length(Double.NaN)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .shipments(new ArrayList<>())
                .build());

        ObservableContainer container2 = new ObservableContainer(Container.builder()
                .height(Double.NaN)
                .href("")
                .id("c1")
                .length(Double.NaN)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .shipments(new ArrayList<>())
                .build());

        ObservableRide instance = new ObservableRide(Ride.builder()
                .id("1")
                .status(TransportStatus.NEW)
                .containers(new ArrayList<>())
                .build(), Savable.ClientStatus.NEW);

        String separator = " to ";

        StringBuilder shipment5name = new StringBuilder();
        shipment5name.append(shipment5.loadCityProperty().getValue());
        shipment5name.append(separator);
        shipment5name.append(shipment5.unloadCityProperty().getValue());

        StringBuilder shipment6name = new StringBuilder();
        shipment6name.append(shipment6.loadCityProperty().getValue());
        shipment6name.append(separator);
        shipment6name.append(shipment6.unloadCityProperty().getValue());

        //
        // test without container, should be " to "
        //
        assertEquals(instance.displayNameProperty().getValue(), separator);

        //
        // test with 2 containers, should be name of first shipment
        // in first container, ie shipment5
        //
        container.getShipmentsList().add(shipment5);
        container2.getShipmentsList().add(shipment6);
        instance.getContainers().addAll(container, container2);
        assertEquals(instance.displayNameProperty().getValue(), shipment5name.toString());

        //
        // test with 2 containers, but first is empty, should change to name
        // of shipment6
        //
        container.getShipmentsList().remove(shipment5);
        assertEquals(instance.displayNameProperty().getValue(), shipment6name.toString());

        //
        // test with 1 containers, should change name to name of shipment6
        //
        instance.getContainers().remove(container);
        assertEquals(instance.displayNameProperty().getValue(), shipment6name.toString());

        //
        // test with 0 containers again, should be " to "
        //
        instance.getContainers().remove(container2);
        assertEquals(instance.displayNameProperty().getValue(), separator);
    }

    @Test
    public void testPropagateCreditorDetailsToShipments() throws NoSuchMethodException {
        ObservableShipment shipment7 = new ObservableShipment(Shipment.builder()
                .container(Resource.buildResource().build())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("7")
                .loadLocation(LoadLocation.builder()
                        .id("")
                        .build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(new ArrayList<>())
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder()
                        .id("")
                        .build())
                .build(), Savable.ClientStatus.NEW);

        ObservableContainer container = new ObservableContainer(Container.builder()
                .height(Double.NaN)
                .href("")
                .id("c1")
                .length(Double.NaN)
                .location(Location.buildLocation().build())
                .maxWeight(Double.MAX_VALUE)
                .name("")
                .shipments(new ArrayList<>())
                .build());

        ObservableRide instance = new ObservableRide(Ride.builder()
                .id("1")
                .status(TransportStatus.NEW)
                .containers(new ArrayList<>())
                .build(), Savable.ClientStatus.NEW);

        container.getShipmentsList().add(shipment7);
        instance.getContainers().add(container);

        String email = "EMAIL";
        String phone = "PHONE";
        String name = "NAME";

        instance.creditorContactEmailProperty().set(email);
        assertEquals(container.creditorEmailProperty().get(), email);
        assertEquals(shipment7.creditorEmailProperty().get(), email);

        instance.creditorContactPhoneProperty().set(phone);
        assertEquals(container.creditorPhoneProperty().get(), phone);
        assertEquals(shipment7.creditorPhoneProperty().get(), phone);

        instance.creditorNameProperty().set(name);
        assertEquals(container.creditorNameProperty().get(), name);
        assertEquals(shipment7.creditorNameProperty().get(), name);

    }
}
