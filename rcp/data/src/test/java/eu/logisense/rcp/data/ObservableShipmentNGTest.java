/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.data;

import eu.logisense.api.Client;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.Origin;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.adapter.JavaBeanProperty;
import javafx.collections.ObservableList;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ObservableShipmentNGTest {

    public ObservableShipmentNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of getShipment method, of class ObservableShipment.
     */
    @Test
    public void testGetShipment() throws NoSuchMethodException {
        System.out.println("getShipment");

        List<OrderLine> lines = new ArrayList<>();
//        lines.add(OrderLine.builder()
//                .amount(2)
//                .height(2.2)
//                .volume(22.2)
//                .weight(33.3)
//                .build());

        ObservableShipment shipment = new ObservableShipment(Shipment.builder()
                .container(Resource.buildResource().build())
                .deadline(LocalDateTime.MIN)
                .destinationIsUnload(Boolean.TRUE)
                .href("")
                .id("1")
                .loadLocation(LoadLocation.builder().id("").build())
                .name("")
                .note("")
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder()
                                .price(111.11)
                                .build())
                        .destination(Destination.builder().build())
                        .externalId("")
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .senderIsLoad(Boolean.TRUE)
                .shippedUnits(1)
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder().build())
                .build(), Savable.ClientStatus.NEW);
        
        ObservableOrderLine orderLine = new ObservableOrderLine(OrderLine.builder()
                .amount(2)
                .height(2.0)
                .width(3.0)
                .length(1.0)
                .weight(33.3)
                .build());

        assertEquals(orderLine.getWeight(), 33.3);
        assertEquals(orderLine.weightProperty().getValue(), 33.3);
        
        assertEquals(orderLine.getVolume(), 12.0);
        assertEquals(orderLine.volumeProperty().getValue(), 12.0);
        
        assertEquals(orderLine.getLoadingMeters(), (3.0*1.0*2)/2.45);
        
        
        shipment.getOrderLineList().add(orderLine);
        
        assertEquals(shipment.getPrice(), 111.11);
        assertEquals(shipment.getVolume(), 12.0);
        assertEquals(shipment.getWeight(), 33.3);
        assertEquals(shipment.getLoadingMeters(), (3.0*1.0*2)/2.45);
        
        ObservableOrderLine orderLine2 = new ObservableOrderLine(OrderLine.builder()
                .amount(3)
                .height(3.0)
                .width(4.0)
                .length(2.0)
                .weight(999.9)
                .build());        

        shipment.getOrderLineList().add(orderLine2);
        
        assertEquals(shipment.getPrice(), 111.11);
        assertEquals(shipment.getVolume(), 12.0+(3.0*4.0*2.0*3),0.0001);
        assertEquals(shipment.getWeight(), 33.3+999.9,0.0001);
        assertEquals(shipment.getLoadingMeters(), ((3.0*1.0*2)/2.45)+((4.0*2.0*3)/2.45)  ,0.0001 );        
        
        orderLine2.heightProperty().setValue(6.0);
        assertEquals(shipment.getVolume(), 12.0+(6.0*4.0*2.0*3),0.0001);
        
        orderLine.heightProperty().setValue(4.0);
        assertEquals(shipment.getVolume(), 24.0+(6.0*4.0*2.0*3),0.0001);
    }

}
