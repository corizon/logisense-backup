/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.util;

import java.util.Locale;
import static org.testng.Assert.*;

/**
 *
 * @author kenrik
 */
public class IsoNameConverterNGTest {

    public IsoNameConverterNGTest() {
    }

    @org.testng.annotations.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.testng.annotations.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.testng.annotations.BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @org.testng.annotations.AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of toString method, of class IsoNameConverter.
     */
    @org.testng.annotations.Test
    public void testToString() {
        System.out.println("toString");
        String iso = "NL";
        IsoNameConverter instance = new IsoNameConverter();
        String expResult = new Locale("nl", "NL").getDisplayCountry();
        String result = instance.toString(iso);
        assertEquals(result, expResult);
    }

    /**
     * Test of fromString method, of class IsoNameConverter.
     */
    @org.testng.annotations.Test
    public void testFromString() {
        System.out.println("fromString");
        String name = new Locale("es", "ES").getDisplayCountry();
        IsoNameConverter instance = new IsoNameConverter();
        String expResult = "ES";
        String result = instance.fromString(name);
        assertEquals(result, expResult);
    }

}
