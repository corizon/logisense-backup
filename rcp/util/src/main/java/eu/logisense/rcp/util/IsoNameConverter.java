/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.util;

import java.util.Locale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.util.StringConverter;

/**
 *
 * @author kenrik
 */
public class IsoNameConverter extends StringConverter<String> {

    ObservableMap<String, String> nameMap = FXCollections.observableHashMap();
    ObservableMap<String, String> isoMap = FXCollections.observableHashMap();

    public IsoNameConverter() {
        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            nameMap.put(l.getDisplayCountry(), iso);
            isoMap.put(iso, l.getDisplayCountry());
        }
    }

    @Override
    public String toString(String object) {
        //
        // convert from iso to name
        // or return when already is a name
        //
        if (isoMap.get(object) != null) {
            return isoMap.get(object);
        } else if (nameMap.get(object) != null) {
            return object;
        } else {
            return "";
        }
    }

    @Override
    public String fromString(String string) {
        //
        // convert from name to iso 
        // or return when already is iso
        //
        if (nameMap.get(string) != null) {
            return nameMap.get(string);
        } else if (isoMap.get(string) != null) {
            return string;
        } else {
            return "";
        }
    }

}
