/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;
import javafx.util.StringConverter;

/**
 *
 * @author kenrik
 */
public class MoneyStringConverter extends StringConverter<Double> {

    DecimalFormat formatter;

    public MoneyStringConverter() {
        formatter = new DecimalFormat("0.0", new DecimalFormatSymbols(Locale.US));
        formatter.setParseBigDecimal(true);
    }

    @Override
    public String toString(Double value) {

        // default
        if (value == null) {
            return "0";
        }

        return formatter.format(BigDecimal.valueOf(value));

    }

    @Override
    public Double fromString(String text) {

        // default
        if (text == null || text.isEmpty()) {
            return BigDecimal.valueOf(0).doubleValue();
        }

        try {

            return formatter.parse(text).doubleValue();

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }
}
