/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.util;

import java.util.Currency;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author kenrik
 */
@Messages({
    "price label=price",
    "currency label=currency",
    "client name label=client name"
})
public class DebitorCreditorBox extends HBox {

    private static final double LABEL_WIDTH = 100.0;
    private static final double SPACING = 5.0;

    private String title;

    private Label titleLabel;
    private ComboBox clientNameComboBox;
    private TextField priceTF;
    private ComboBox currencyComboBox;

    public DebitorCreditorBox(String title) {
        this.title = title;
        setPadding(new Insets(0.0, 0.0, 10.0, 0.0));
        setSpacing(SPACING);

        initComponents(title);
    }

    private void initComponents(String title) {

        titleLabel = new Label(title);
        clientNameComboBox = new ComboBox();
        priceTF = new TextField();
        currencyComboBox = new ComboBox();

        getChildren().addAll(titleLabel, clientNameComboBox, priceTF, currencyComboBox);

        configComponents();
    }

    private void configComponents() {

        titleLabel.setPrefWidth(LABEL_WIDTH);
        //
        // bind to screen width but subtract width of label and three times the amount of spacing
        //
        clientNameComboBox.prefWidthProperty().bind(widthProperty().subtract(LABEL_WIDTH + SPACING * 2).multiply(0.5));
        clientNameComboBox.setEditable(true);
        clientNameComboBox.setPromptText(Bundle.client_name_label());

        priceTF.prefWidthProperty().bind(clientNameComboBox.prefWidthProperty().subtract(SPACING).multiply(0.50));
        priceTF.setPromptText(Bundle.price_label());
        currencyComboBox.prefWidthProperty().bind(clientNameComboBox.prefWidthProperty().subtract(SPACING).multiply(0.50));
        currencyComboBox.setEditable(true);
        currencyComboBox.setPromptText(Bundle.currency_label());

        ObservableList<Currency> currencies = FXCollections.observableArrayList();
        Currency.getAvailableCurrencies().stream().forEach((c) -> {
            currencies.add(c);
        });

        currencyComboBox.setItems(currencies.sorted());

        //
        // Looks weird but this adds the autocomplete functionality to the comboboxes
        // FIXME: ugly code
        //
        new AutoCompleteComboBoxListener<>(currencyComboBox);

    }

    public ComboBox getClientNameComboBox() {
        return clientNameComboBox;
    }

    public TextField getPriceTF() {
        return priceTF;
    }

    public ComboBox getCurrencyComboBox() {
        return currencyComboBox;
    }

}
