/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.rcp.util;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Skin;
import javafx.util.StringConverter;

public class DateTimePicker extends DatePicker implements Comparable<DateTimePicker> {

    private ObjectProperty<LocalTime> timeValue = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDateTime> dateTimeValue;

    public DateTimePicker() {
        super();
        setValue(LocalDate.now());
        setTimeValue(LocalTime.now());
        setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");

            @Override
            public String toString(LocalDate object) {
                return (getDateTimeValue() == null) ? null : getDateTimeValue().format(formatter);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.parse(string, formatter);
            }
        });
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new DateTimePickerSkin(this);
    }

    public LocalTime getTimeValue() {
        return timeValue.get();
    }

    public void setTimeValue(LocalTime timeValue) {
        this.timeValue.set(timeValue);
    }

    public ObjectProperty<LocalTime> timeValueProperty() {
        return timeValue;
    }

    public LocalDateTime getDateTimeValue() {
        return dateTimeValueProperty().get();
    }

    public void setDateTimeValue(LocalDateTime dateTimeValue) {
        dateTimeValueProperty().set(dateTimeValue);
        setValue(dateTimeValue.toLocalDate());
        setTimeValue(dateTimeValue.toLocalTime());
    }

    public ObjectProperty<LocalDateTime> dateTimeValueProperty() {
        if (dateTimeValue == null) {
            dateTimeValue = new SimpleObjectProperty<>(LocalDateTime.of(this.getValue(), timeValue.get()));
        }
        timeValue.addListener(t -> {
            dateTimeValue.set(LocalDateTime.of(this.getValue(), timeValue.get()));
        });

        valueProperty().addListener(t -> {
            dateTimeValue.set(LocalDateTime.of(this.getValue(), timeValue.get()));
        });
        return dateTimeValue;
    }

    @Override
    public int compareTo(DateTimePicker o) {
        return this.dateTimeValue.get().compareTo(dateTimeValue.get());
    }

}
