/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.rcp.util;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
import com.sun.javafx.scene.control.DatePickerContent;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.skin.DatePickerSkin;
import javafx.scene.layout.HBox;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import org.openide.util.NbBundle.Messages;

@Messages({
    "time.label=Time : ",
    "enter.label=enter"
})
public class DateTimePickerSkin extends DatePickerSkin {

    private DateTimePicker datePicker;
    private DatePickerContent ret;

    public DateTimePickerSkin(DateTimePicker datePicker) {
        super(datePicker);
        this.datePicker = datePicker;
    }

    @Override
    public Node getPopupContent() {
        if (ret == null) {
            ret = (DatePickerContent) super.getPopupContent();

            Label hoursValue = new Label(Bundle.time_label());
            TimeTextField time = new TimeTextField(datePicker.getTimeValue());
            Button close = new Button(Bundle.enter_label());

            // TODO: dateTimePicker's displayed time should update when popup 
            // closes, now only on focus of dateTimePicker lost
            close.setOnAction((ActionEvent e) -> {
                datePicker.hide();
            });

            ret.getChildren().add(new HBox(hoursValue, time, close));

            // adjust times with selected time
            time.textProperty().addListener((observable, oldValue, newValue) -> {
                datePicker.setTimeValue(datePicker.getTimeValue().withHour(time.getHours()));
                datePicker.setTimeValue(datePicker.getTimeValue().withMinute(time.getMinutes()));
                datePicker.setTimeValue(datePicker.getTimeValue().withSecond(time.getSeconds()));
            });

        }
        return ret;
    }

}
