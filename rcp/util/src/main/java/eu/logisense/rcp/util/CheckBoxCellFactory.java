/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.util;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.adapter.JavaBeanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;

/**
 *
 * @author kenrik
 */
public class CheckBoxCellFactory<O>
        implements Callback<TableColumn<O, Boolean>, TableCell<O, Boolean>> {

    @Override
    public TableCell call(TableColumn<O, Boolean> param) {
        CheckBoxCell checkBoxCell = new CheckBoxCell();
        return checkBoxCell;
    }

    public static class CheckBoxCell<O> extends TableCell<O, Boolean> {

        private CheckBox checkBox;
        private JavaBeanProperty<Boolean> boundToCurrently = null;

        public CheckBoxCell() {

            checkBox = new CheckBox();
            //
            // tabbing out checkbox in tablecell doesn't work, fastest solution:
            //
            checkBox.setFocusTraversable(false);

            //
            // add listener to be able to set focus to the row this checkbox
            // is in to remove the correct row when needed
            //
            checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {

                TableView table = this.getTableView();
                Integer row = this.getTableRow().indexProperty().getValue();

                //
                // this check revents eternal focus loop between the row
                // and the checkBox
                //
                if (row != table.getSelectionModel().getSelectedIndex()) {

                    Platform.runLater(() -> {

                        //
                        // request focus on the row this cell is in to be able
                        // to correctly remove items
                        //
                        table.requestFocus();
                        table.getSelectionModel().clearAndSelect(row);
                        table.getFocusModel().focus(row);
                    });
                }
            });

            this.setGraphic(checkBox);
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                // Show the Text Field
                this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

                // Retrieve the actual Boolean Property that should be bound to the CheckBox
                // If the CheckBox is currently bound to a different BooleanProperty
                // Unbind the old property and rebind to the new one
                ObservableValue<Boolean> ov = getTableColumn().getCellObservableValue(getIndex());
                JavaBeanProperty<Boolean> sp = (JavaBeanProperty) ov;

                if (this.boundToCurrently == null) {
                    this.boundToCurrently = sp;
                    Bindings.bindBidirectional(this.checkBox.selectedProperty(), sp);
                } else if (this.boundToCurrently != sp) {
                    this.checkBox.textProperty().unbindBidirectional(this.boundToCurrently);
                    this.boundToCurrently = sp;
                    Bindings.bindBidirectional(this.checkBox.selectedProperty(), this.boundToCurrently);
                }
            } else {
                this.setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }

    }
}
