/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.rcp.util;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.adapter.JavaBeanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.openide.util.NbBundle;

/**
 *
 * @author kenrik
 */
@NbBundle.Messages({
    "validator message for integerTextFieldCellFactory=please enter a value"
})
public class IntegerTextFieldCellFactory<O>
        implements Callback<TableColumn<O, Integer>, TableCell<O, Integer>> {

    private boolean enableTFValidation;
    private ValidationSupport validationSupport;

    /**
     * CellFactory that makes TableCells editable
     *
     * @param enableTFValidation sets Validation on TableCell's underlying
     * TextField
     */
    public IntegerTextFieldCellFactory(boolean enableTFValidation) {
        this.enableTFValidation = enableTFValidation;
    }

    @Override
    public TableCell call(TableColumn<O, Integer> param) {
        TextFieldCell textFieldCell = new TextFieldCell();

        if (enableTFValidation) {
            validationSupport = new ValidationSupport();
            validationSupport.registerValidator(textFieldCell.textField,
                    Validator.createEmptyValidator(Bundle.validator_message_for_integerTextFieldCellFactory()));
        }

        return textFieldCell;
    }

    public static class TextFieldCell<O> extends TableCell<O, Integer> {

        private TextField textField;
        private JavaBeanProperty<Integer> boundToCurrently = null;

        public TextFieldCell() {
            String strCss;
            // Padding in Text field cell is not wanted - we want the Textfield itself to "be"
            // The cell.  Though, this is aesthetic only.  to each his own.  comment out
            // to revert back.  
            strCss = "-fx-padding: 0;";

            this.setStyle(strCss);

            textField = new TextField();

            //
            // add listener to textfield to switch row selection to the row this 
            // textfield is in
            //
            textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue) {

                    TableView table = this.getTableView();
                    Integer row = this.getTableRow().indexProperty().getValue();

                    //
                    // this check revents eternal focus loop between the row
                    // and the textfield
                    //
                    if (row != table.getSelectionModel().getSelectedIndex()) {

                        Platform.runLater(() -> {

                            //
                            // request focus on the row this cell is in to be able
                            // to correctly remove items
                            //
                            table.requestFocus();
                            table.getSelectionModel().clearAndSelect(row);
                            table.getFocusModel().focus(row);

                            //
                            // then, set focus back to textField to enable
                            // editing of values
                            //
                            textField.requestFocus();
                        });
                    }
                }
            });

            // 
            // Default style pulled from caspian.css. Used to play around with the inset background colors
            // ---trying to produce a text box without borders
            strCss = ""
                    + //"-fx-background-color: -fx-shadow-highlight-color, -fx-text-box-border, -fx-control-inner-background;" +
                    "-fx-background-color: -fx-control-inner-background;"
                    + //"-fx-background-insets: 0, 1, 2;" +
                    "-fx-background-insets: 0;"
                    + //"-fx-background-radius: 3, 2, 2;" +
                    "-fx-background-radius: 0;"
                    + "-fx-padding: 3 5 3 5;"
                    + /*Play with this value to center the text depending on cell height??*/ //"-fx-padding: 0 0 0 0;" +
                    "-fx-prompt-text-fill: derive(-fx-control-inner-background,-30%);"
                    + "-fx-cursor: text;"
                    + "";
            // Focused and hover states should be set in the CSS.  This is just a test
            // to see what happens when we set the style in code
            textField.focusedProperty().addListener(new ChangeListener<Boolean>() {

                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    TextField tf = (TextField) getGraphic();
                    String strStyleGotFocus = "-fx-background-color: skyblue, -fx-text-box-border, -fx-control-inner-background;"
                            + "-fx-background-insets: -0.4, 1, 2;"
                            + "-fx-background-radius: 3.4, 2, 2;";
                    String strStyleLostFocus
                            = //"-fx-background-color: -fx-shadow-highlight-color, -fx-text-box-border, -fx-control-inner-background;" +
                            "-fx-background-color: -fx-control-inner-background;"
                            + //"-fx-background-insets: 0, 1, 2;" +
                            "-fx-background-insets: 0;"
                            + //"-fx-background-radius: 3, 2, 2;" +
                            "-fx-background-radius: 0;"
                            + "-fx-padding: 3 5 3 5;"
                            + /**/ //"-fx-padding: 0 0 0 0;" +
                            "-fx-prompt-text-fill: derive(-fx-control-inner-background,-30%);"
                            + "-fx-cursor: text;"
                            + "";
                    if (newValue.booleanValue()) {
                        tf.setStyle(strStyleGotFocus);
                    } else {
                        tf.setStyle(strStyleLostFocus);
                    }
                }
            });
            textField.hoverProperty().addListener(new ChangeListener<Boolean>() {

                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    TextField tf = (TextField) getGraphic();
                    String strStyleGotHover = "-fx-background-color: derive(skyblue,90%), -fx-text-box-border, derive(-fx-control-inner-background, 10%);"
                            + "-fx-background-insets: 1, 2.8, 3.8;"
                            + "-fx-background-radius: 3.4, 2, 2;";
                    String strStyleLostHover
                            = //"-fx-background-color: -fx-shadow-highlight-color, -fx-text-box-border, -fx-control-inner-background;" +
                            "-fx-background-color: -fx-control-inner-background;"
                            + //"-fx-background-insets: 0, 1, 2;" +
                            "-fx-background-insets: 0;"
                            + //"-fx-background-radius: 3, 2, 2;" +
                            "-fx-background-radius: 0;"
                            + "-fx-padding: 3 5 3 5;"
                            + /**/ //"-fx-padding: 0 0 0 0;" +
                            "-fx-prompt-text-fill: derive(-fx-control-inner-background,-30%);"
                            + "-fx-cursor: text;"
                            + "";
                    String strStyleHasFocus = "-fx-background-color: skyblue, -fx-text-box-border, -fx-control-inner-background;"
                            + "-fx-background-insets: -0.4, 1, 2;"
                            + "-fx-background-radius: 3.4, 2, 2;";
                    if (newValue.booleanValue()) {
                        tf.setStyle(strStyleGotHover);
                    } else if (!tf.focusedProperty().get()) {
                        tf.setStyle(strStyleLostHover);
                    } else {
                        tf.setStyle(strStyleHasFocus);
                    }

                }
            });
            textField.setStyle(strCss);
            this.setGraphic(textField);
        }

        @Override
        protected void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                // Show the Text Field
                this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

                // Retrieve the actual Integer Property that should be bound to the TextField
                // If the TextField is currently bound to a different IntegerProperty
                // Unbind the old property and rebind to the new one
                ObservableValue<Integer> ov = getTableColumn().getCellObservableValue(getIndex());
                JavaBeanProperty<Integer> sp = (JavaBeanProperty) ov;

                if (this.boundToCurrently == null) {
                    this.boundToCurrently = sp;
                    Bindings.bindBidirectional(this.textField.textProperty(), sp, new IntegerStringConverter());
                } else if (this.boundToCurrently != sp) {
                    this.textField.textProperty().unbindBidirectional(this.boundToCurrently);
                    this.boundToCurrently = sp;
                    Bindings.bindBidirectional(this.textField.textProperty(), this.boundToCurrently, new IntegerStringConverter());
                }
            } else {
                this.setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }

    }
}
