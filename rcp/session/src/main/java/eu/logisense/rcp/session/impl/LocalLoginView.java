/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.rcp.session.impl;

import eu.logisense.api.session.client.Callback;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javax.swing.JPanel;
import org.openide.util.Exceptions;

/**
 *
 * @author Timon Veenstra <monezz@gmail.com>
 */
public class LocalLoginView extends JPanel {

    private static final int SCENE_WIDTH = 600;
    private static final int SCENE_HEIGHT = 960;

    private JFXPanel fxPanel;

    private final LocalLoginController controller;

    public LocalLoginView(Callback callback) {
        this.controller = new LocalLoginController(callback);
        initComponents();
    }

    public void loadLogin() {
        controller.loadLogin();
    }

    private void initComponents() {
        setLayout(new BorderLayout());

        fxPanel = new JFXPanel();
        //Scene dimensions not used by Dialog as it is created later, set on fxPanel as well:
        fxPanel.setPreferredSize(new Dimension(SCENE_WIDTH, SCENE_HEIGHT));
        Platform.setImplicitExit(false);
        // create JavaFX scene
        Platform.runLater(() -> {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("LocalLoginView.fxml"));
                loader.setController(controller);
                Parent root = (Parent) loader.load();
                Scene scene = new Scene(root,750, 500, Color.web("#666970"));
                fxPanel.setScene(scene);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        });
        add(fxPanel, BorderLayout.CENTER);
    }

}
