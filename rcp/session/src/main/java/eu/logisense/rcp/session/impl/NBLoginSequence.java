/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.rcp.session.impl;

import eu.logisense.api.session.client.LoginSequence;
import eu.logisense.api.session.client.Connections;
import eu.logisense.api.util.ClientUtil;
import java.awt.Component;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.openide.LifecycleManager;
import org.openide.windows.WindowManager;

/**
 *
 * @author johan
 */
public class NBLoginSequence extends LoginSequence {
    private static final Logger LOGGER = Logger.getLogger(NBLoginSequence.class.getName());
    
    private final LocalLoginView loginView = new LocalLoginView(loginCallback);
    
    //
    // store original platform glasspane
    //
    private Component orgGlassPane;

    @Override
    protected void init() {
        LOGGER.finest("init");
        WindowManager.getDefault().invokeWhenUIReady(() -> {
            LOGGER.finest("store original glasspane");
            orgGlassPane = getMainWindow().getGlassPane();
        });
    }

    @Override
    protected void activateLogin() {
        WindowManager.getDefault().invokeWhenUIReady(() -> {
            LOGGER.finest("activateLogin");
            getMainWindow().setGlassPane(loginView);
            loginView.loadLogin();
            loginView.setVisible(true);
        });
    }

    @Override
    protected void resume() {
        WindowManager.getDefault().invokeWhenUIReady(() -> {
            LOGGER.finest("resume");
            loginView.setVisible(false);
            getMainWindow().setGlassPane(orgGlassPane);
        });
        Connections.create(ClientUtil.getBaseURI().toString());
        
    }

    @Override
    protected void exit() {
        LifecycleManager.getDefault().exit();
    }
    
    private static JFrame getMainWindow()  {
        return (JFrame) WindowManager.getDefault().getMainWindow();
    }

}
