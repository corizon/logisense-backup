/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.rcp.session.impl;

import eu.logisense.api.session.Session;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.keycloak.adapters.ServerRequest;
import org.keycloak.common.VerificationException;

/**
 *
 * @author Timon Veenstra <monezz@gmail.com>
 */
public class KeycloakSessionImpl implements Session {
    private static final KeycloakInstalled KEYCLOAK = new KeycloakInstalled();
//    private static final Logger LOGGER = LoggerFactory.getLogger(KeycloakSessionImpl.class);
    
    public KeycloakSessionImpl() {
    }

    static KeycloakInstalled getKeycloak() { return KEYCLOAK; }

    @Override
    public String getUserName() {
        checkState();
        return KEYCLOAK.getToken().getId();
    }

    @Override
    public boolean hasRole(String roleKey) {
        checkState();
        return KEYCLOAK.getToken().getRealmAccess().isUserInRole(roleKey);
    }
    
    private static void checkState(){
        if (KEYCLOAK == null) {
            throw new IllegalStateException("Connections does not contain keycloak");
        }
        if (KEYCLOAK.getToken() == null) {
            throw new IllegalStateException("No token found, user probably not logged in yet!");
        }        
    }
    
    public static String getBearerToken() {
        checkState();
        try {
            return KEYCLOAK.getTokenString(10, TimeUnit.SECONDS);
        } catch (VerificationException | IOException | ServerRequest.HttpFailure ex) {
            throw  new RuntimeException(ex);
        }
    }
}
