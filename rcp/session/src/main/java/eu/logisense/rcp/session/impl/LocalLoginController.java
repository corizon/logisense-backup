/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.rcp.session.impl;

import eu.logisense.api.session.client.Callback;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import org.keycloak.OAuthErrorException;
import org.keycloak.common.VerificationException;
import org.keycloak.adapters.ServerRequest;
import static org.openide.util.NbBundle.Messages;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@Messages({
    "prefix.connection.error=Unable to connect to login server: "
})
public class LocalLoginController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(LocalLoginController.class.getName());

    @FXML
    private ProgressBar progress;

    @FXML
    private WebView webview;

    @FXML
    private Label errorMessage;

    @FXML
    private Label connecting;

    private final Callback callback;

    public LocalLoginController(Callback callback) {
        this.callback = callback;
    }

    void loadLogin() {
        LOGGER.finest("loading login");
        Platform.runLater(() -> {
            WebEngine webEngine = webview.getEngine();

            webEngine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue)
                    -> {
                JSObject window = (JSObject) webEngine.executeScript("window");
                LocalLoginViewLogBridge bridge = new LocalLoginViewLogBridge();
                window.setMember("java", bridge);
                
                webEngine.executeScript("console.log = function(message)\n"
                        + "{\n"
                        + "    java.log(message);\n"
                        + "};");
            });

            webEngine.getLoadWorker().stateProperty().addListener(
                    new ChangeListener<State>() {
                public void changed(ObservableValue ov, State oldState, State newState) {
                    LOGGER.log(Level.FINE, "newState = {0}", newState);
                    if (newState == State.SUCCEEDED) {
                        LOGGER.fine(webEngine.getLocation());
                    }
                }
            });

            webEngine.getLoadWorker().exceptionProperty().addListener((ObservableValue<? extends Throwable> observable, Throwable oldValue, Throwable newValue) -> {
                LOGGER.log(Level.SEVERE, "{0} when trying to load {1}", new Object[]{newValue.getMessage(), webview.getEngine().getLocation()});
                progress.setVisible(false);
                connecting.setVisible(false);
                errorMessage.setText(Bundle.prefix_connection_error() + newValue.getMessage());
                errorMessage.setVisible(true);
            });

            LOGGER.finest("binding progress to web engine load worker");

            //
            // not sure if its usefull to bind, since it seems to be always 0
            //
//            progress.progressProperty().bind(webEngine.getLoadWorker().progressProperty());
            webEngine.getLoadWorker().stateProperty().addListener(
                    new ChangeListener<State>() {
                @Override
                public void changed(ObservableValue ov, State oldState, State newState) {
                    if (newState == State.SUCCEEDED) {
                        LOGGER.finest("Login page ready");
                        webview.setVisible(true);
                    }
                }
            });

            // load the web page
            Task task = new Task<Void>() {
                @Override
                public Void call() {
                    try {
                        LOGGER.finest("load login page task");
                        KeycloakInstalled keycloak = KeycloakSessionImpl.getKeycloak();
                        keycloak.loginDesktop(webEngine);
                        if (keycloak.getToken().isActive()) {
                            LOGGER.finest("Token active!");
                            callback.onSuccess();
                        } else {
                            LOGGER.finest("failed!");
                            callback.onFailure();
                        }
                    } catch (IOException | VerificationException | OAuthErrorException | URISyntaxException | ServerRequest.HttpFailure | InterruptedException ex) {
                        LOGGER.severe(ex.getMessage());
                        callback.onFailure();
                    }
                    return null;
                }
            };
            new Thread(task).start();
        });

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
