/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.core.MediaType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.LoggerFactory;

/**
 *
 * @author perezdf
 */
public class ClientTest {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ClientTest.class);

    public ClientTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testBeanValidation() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();

        Client client = buildClient();

        Set<ConstraintViolation<Client>> violations = validator.validate(client);

        if (!violations.isEmpty()) {
            String message = "";
            for (ConstraintViolation violation : violations) {
                if (violation.getConstraintDescriptor() != null && violation.getConstraintDescriptor().getMessageTemplate() != null) {
                    message += violation.getConstraintDescriptor().getMessageTemplate();
                }
            }

            System.out.println("message {}" + message);

        }
        assertTrue(violations.isEmpty());

    }

    private Client buildClient() {

        List<Location> locations = new ArrayList<>();
        locations.add(Location.buildLocation()
                .address("address ")
                .city("city ")
                .country("country ")
                .latitude(12.34)
                .longitude(56.78)
                .name("name ")
                .postalcode("postalcode ")
                .build());
        Client template = Client.builder()
                .href("anything")
                .name("uhuh")
                .bicCode("12345678901")
                .chamberOfCommerce("")
                .chamberOfCommerceCity("")
                .chargeVat(Boolean.TRUE)
                .creditorNumber("")
                .currency("")
                .debtorNumber("")
                .externalId("")
                .ibanCode("")
                .status(ApprovalStatus.NEW)
                .vatNumber("")
                .locations(locations)
                .build();

        return template;
    }
}
