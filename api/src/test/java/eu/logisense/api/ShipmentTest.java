/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.core.MediaType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.LoggerFactory;

/**
 *
 * @author perezdf
 */
public class ShipmentTest {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ShipmentTest.class);

    public ShipmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of builder method, of class Shipment.
     */
//    @Test
    public void testBuilder() {

        String shipmentString
                = "{"
                + "  \"geodata\": ["
                + "    {"
                + "      \"id\": \"1\","
                + "      \"name\": \"Julie Sherman\","
                + "      \"gender\" : \"female\","
                + "      \"latitude\" : \"37.33774833333334\","
                + "      \"longitude\" : \"-121.88670166666667\""
                + "    },"
                + "    {"
                + "      \"id\": \"2\","
                + "      \"name\": \"Johnny Depp\","
                + "      \"gender\" : \"male\","
                + "      \"latitude\" : \"37.336453\","
                + "      \"longitude\" : \"-121.884985\""
                + "    }"
                + "  ]"
                + "}";

        ObjectMapper mapper = new ObjectMapper();
        Shipment shipment = null;
        try {
            shipment = mapper.readValue(shipmentString, Shipment.class);

        } catch (IOException ex) {
            fail(ex.getMessage());
        }

        assertNotNull(shipment);
    }

//    @Test
    public void testXML() throws JsonProcessingException {
        JacksonXmlModule module = new JacksonXmlModule();
// and then configure, for example:
        module.setDefaultUseWrapper(false);
        XmlMapper xmlMapper = new XmlMapper(module);
        xmlMapper.registerModule(new JavaTimeModule());

        Shipment shipment = buildShipment();

        String xml = xmlMapper.writeValueAsString(shipment);

        System.out.println(xml);
        log.info("look " + xml);
    }

    @Test
    public void testJSON() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

        Shipment shipment = buildShipment();

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            jsonProvider.writeTo(shipment, Shipment.class, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
            log.info(baos.toString());
            System.out.println(baos.toString());
        }
    }

    @Test
    public void testBeanValidation() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();

        Shipment shipment = buildShipment();

        Set<ConstraintViolation<Shipment>> violations = validator.validate(shipment);
        assertTrue(violations.isEmpty());

    }

    private Shipment buildShipment() {

        Origin origin = Origin.builder().name("origin").build();
        Destination destination = Destination.builder().name("destination").build();
        Client client = Client.builder().name("client name").build();

        Order order = Order.builder().name("order name").build();
        order.setOrigin(origin);
        order.setDestination(destination);
        order.setOrderedBy(client);
        order.setDebitor(Debitor.builder().name("debitor").build() );

        LoadLocation from = LoadLocation.builder()
                .timeFrameFrom(LocalDateTime.now())
                .timeFrameTo(LocalDateTime.now())
                .build();
        UnloadLocation to = UnloadLocation.builder()
                .timeFrameFrom(LocalDateTime.now())
                .timeFrameTo(LocalDateTime.now())
                .build();

        Shipment shipment = Shipment.builder()
                .id("test")
               // .name("name")
                .deadline(LocalDateTime.now())
                .loadLocation(from)
                .unloadLocation(to)
                .destinationIsUnload(Boolean.TRUE)
                .note("note")
                .order(order)
                .senderIsLoad(Boolean.FALSE)
                .shippedUnits(0)
                .status(TransportStatus.NEW)
                .build();
        return shipment;
    }
}
