/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@Path("/shipments")
@Api(value = "/shipments")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface ShipmentService {

    /**
     * get all the capability providers
     *
     * @return list of capability providers
     */
    @GET
    @ApiOperation(value = "Get shipments",
            response = Shipment.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no shipments found")})
    public List<Shipment> getShipments();

    /**
     * get a shipment based on id
     *
     * @param id of the shipment to retrieve
     * @return found shipment
     */
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get shipment",
            response = Shipment.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no shipments found")})
    public Shipment getShipment(@ApiParam(value = "id of the shipment", required = true) @PathParam("id") String id);

    
    @GET
    @Path("/href/{href}")
    @ApiOperation(value = "Get shipment by href",
            response = Shipment.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no shipments found")})
    public Shipment getShipmentByHref(String href); 
    /**
     * create a shipment based on provided template
     *
     * @param template for creating the shipment
     * @return created shipment
     */
    @POST
    @ApiOperation(value = "create shipment",
            response = Shipment.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no shipments found")})
    public Shipment postShipment(@ApiParam(value = "shipment template", required = true) Shipment template);

    /**
     * update a shipment based on provided template
     *
     * @param template for creating the shipment
     * @return updated shipment
     */
    @PUT
    @ApiOperation(value = "update shipment",
            response = Shipment.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no shipments found")})
    public Shipment updateShipment(@ApiParam(value = "shipment template", required = true) Shipment template);

    /**
     * delete a shipment based on id
     *
     * @param id of the shipment to be deleted
     */
    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "delete shipment")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no shipments found")})
    public void deleteShipment(@ApiParam(value = "id of the shipment", required = true)@PathParam("id") String id);
    
    @GET
    @Path("/{id}/deliver")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Shipment deliverShipment(@PathParam("id") String id);
    
    @GET
    @Path("/{id}/order")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Shipment orderShipment(@PathParam("id") String id);
    
    @GET
    @Path("/{id}/cancel")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Shipment cancelShipment(@PathParam("id") String id);
    
    /**
     * This method takes one of the available shipment and dispatch it to other provider
     *
     * @param id
     * @param capabilityId
     * @return
     */
    @GET
    @Path("/{id}/dispatchto/{capabilityId}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Shipment dispatchTo(@PathParam("id") String id, @PathParam("capabilityId") String capabilityId);

}
