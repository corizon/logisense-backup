/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author johan
 */
@Path("/locations")
@Api(value = "/locations")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface LocationService {

    @GET
    @ApiOperation(value = "Get locations",
            response = Location.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no location found")})
    public List<Location> getLocations();

    /**
     * get a location based on id
     *
     * @param id of the location to retrieve
     * @return found location
     */
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get location",
            response = Location.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no location found")})
    public Location getLocation(@ApiParam(value = "id of the location", required = true) @PathParam("id") String id);

    /**
     * create a location based on provided template
     *
     * @param template for creating the location
     * @return created location
     */
    @POST
    @ApiOperation(value = "create location",
            response = Location.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no location found")})
    public Location postLocation(@ApiParam(value = "location template", required = true) Location template);

    /**
     * update a location based on provided template
     *
     * @param id
     * @param template for creating the location
     * @return updated location
     */
    @PUT
    @Path("/{id}")
    @ApiOperation(value = "update location",
            response = Location.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no location found")})
    public Location updateLocation(@ApiParam(value = "id of the location", required = true) @PathParam("id") String id,@ApiParam(value = "location template", required = true) Location template);

    /**
     * delete a Location based on id
     *
     * @param id of the Location to be deleted
     */
    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "delete Location")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no Locations found")})
    public void deleteLocation(@ApiParam(value = "id of the Location", required = true) @PathParam("id") String id);
}
