/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import javax.annotation.Nullable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
public class Shipment extends Resource /*implements Timable/*, Trackable*/ {

    @Getter
    private LoadLocation loadLocation;
    public static final String PROP_LOAD_LOCATION = "loadLocation";

    @NotNull(message = "UnloadLocation can not be null")
    @Getter
    private UnloadLocation unloadLocation;
    public static final String PROP_UNLOAD_LOCATION = "unloadLocation";



    @Digits(integer = 5, fraction = 0, message = "The value must be numeric and less than five digits")
    @Nullable
    @Getter
    private Integer shippedUnits;
    public static final String PROP_SHIPPED_UNITS = "shippedUnits";

    @Size(min = 0, max = 1000, message = "Note should be of size {max}")
    @Getter
    private String note;
    public static final String PROP_NOTE = "note";

    @Getter
    private LocalDateTime deadline;
    public static final String PROP_DEADLINE = "deadline";

    @Getter
    private Resource container;
    public static final String PROP_CONTAINER = "container";

    @NotNull(message = "any shipment has to link with one Order")
    @Getter
    private Order order;
    public static final String PROP_ORDER = "order";

    // created timestamp; no attr+getter yet, only used internally on nodes for now
    public static final String PROP_CREATED = "created";

    @Getter
    private Boolean senderIsLoad;
    public static final String PROP_SENDER_IS_LOAD = "senderIsLoad";

    @Getter
    private Boolean destinationIsUnload;
    public static final String PROP_DESTINATION_IS_UNLOAD = "destinationIsUnload";

    @Builder
    public Shipment(LoadLocation loadLocation, UnloadLocation unloadLocation,
            Status status, Order order, String note,
            LocalDateTime deadline, String id, String name, String href, Resource container, Integer shippedUnits, Boolean senderIsLoad, Boolean destinationIsUnload, Integer index) {
        super(id, name, href, status,index);
        this.loadLocation = loadLocation;
        this.unloadLocation = unloadLocation;

        this.order = order;
        this.note = note;
        this.deadline = deadline;
        this.container = container;
        this.shippedUnits = shippedUnits;
        this.senderIsLoad = senderIsLoad;
        this.destinationIsUnload = destinationIsUnload;
    }

    public void setContainer(Resource container) {
        Resource old = this.container;
        this.container = container;
        propertySupport.firePropertyChange(PROP_CONTAINER, old, container);
    }

    public void setDeadline(LocalDateTime deadline) {
        LocalDateTime old = this.deadline;
        this.deadline = deadline;
        propertySupport.firePropertyChange(PROP_DEADLINE, old, deadline);
    }

    public void setLoadLocation(LoadLocation loadLocation) {
        LoadLocation old = this.loadLocation;
        this.loadLocation = loadLocation;
        propertySupport.firePropertyChange(PROP_LOAD_LOCATION, old, loadLocation);
    }

    public void setNote(String note) {
        String old = this.note;
        this.note = note;
        propertySupport.firePropertyChange(PROP_NOTE, old, note);
    }



    public void setUnloadLocation(UnloadLocation unloadLocation) {
        UnloadLocation old = this.unloadLocation;
        this.unloadLocation = unloadLocation;
        propertySupport.firePropertyChange(PROP_UNLOAD_LOCATION, old, unloadLocation);
    }

    public void setOrder(Order order) {
        Order old = this.order;
        this.order = order;
        propertySupport.firePropertyChange(PROP_ORDER, old, order);
    }

    public void setShippedUnits(Integer shippedUnits) {
        Integer old = this.shippedUnits;
        this.shippedUnits = shippedUnits;
        propertySupport.firePropertyChange(PROP_SHIPPED_UNITS, old, shippedUnits);
    }

    public void setSenderIsLoad(Boolean senderIsLoad) {
        Boolean old = this.senderIsLoad;
        this.senderIsLoad = senderIsLoad;
        propertySupport.firePropertyChange(PROP_SENDER_IS_LOAD, old, senderIsLoad);
    }

    public void setDestinationIsUnload(Boolean destinationIsUnload) {
        Boolean old = this.destinationIsUnload;
        this.destinationIsUnload = destinationIsUnload;
        propertySupport.firePropertyChange(PROP_DESTINATION_IS_UNLOAD, old, destinationIsUnload);
    }
}
