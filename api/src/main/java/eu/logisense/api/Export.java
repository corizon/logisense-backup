/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Export extends Resource {

    @Getter
    @Setter
    private List<Link> elements = new ArrayList<>();
    public static final String PROP_ELEMENTS = "elements";

    @Getter
    @Setter
    private ExportStatus status;
    public static final String PROP_STATUS = "status";

    @Getter
    @Setter
    private LocalDateTime statusDate;
    public static final String PROP_STATUS_DATE = "statusDate";

    @Builder
    public Export(String id, String name, String href, ExportStatus status, LocalDateTime statusDate) {
        super(id, name, href, null);
        this.status = status;
        this.statusDate = statusDate;
    }

}
