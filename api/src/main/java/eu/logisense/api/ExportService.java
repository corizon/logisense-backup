/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/exports")
@Api(value = "/exports")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface ExportService {

    @GET
    @ApiOperation(value = "Get exports",
            response = Export.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no export found")})
    public List<Export> getExports();

    /**
     * get a export based on id
     *
     * @param id of the export to retrieve
     * @return found export
     */
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get export",
            response = Export.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no export found")})
    public Export getExport(@ApiParam(value = "id of the export", required = true) @PathParam("id") String id);

    /**
     * create a export based on provided template
     *
     * @param template for creating the export
     * @return created export
     */
    @POST

    @ApiOperation(value = "create export",
            response = Export.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no export found")})
    public Export postExport(@ApiParam(value = "export template", required = true) Export template);

    @GET
    @Path("/{id}/dispatch")
    @ApiOperation(value = "add order to existing export",
            response = Export.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no export found")})
    public Export dispatch(@ApiParam(value = "export template", required = true) String exportId);

    /**
     * update a export based on provided template
     *
     * @param template for creating the export
     * @return updated export
     */
    @PUT
    @ApiOperation(value = "update export",
            response = Export.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no export found")})
    public Export updateExport(@ApiParam(value = "export template", required = true) Export template);

    /**
     * delete a Export based on id
     *
     * @param id of the Export to be deleted
     */
    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "delete Export")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no Exports found")})
    public void deleteExport(@ApiParam(value = "id of the Export", required = true) @PathParam("id") String id);

}
