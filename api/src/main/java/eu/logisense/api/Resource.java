/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Base rest resource with an id, name and href
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@AllArgsConstructor
//@NoArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
@JsonPropertyOrder(alphabetic = true)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class Resource {

    protected final PropertyChangeSupport propertySupport;

    @Getter
    @Setter
    private String href;
    public static final String PROP_HREF = "href";

    @Getter
    private String id;
    public static final String PROP_ID = "id";

    
    @Getter
    private String name;
    public static final String PROP_NAME = "name";

    @Getter
    private Integer index;
    public static final String PROP_INDEX = "index";

    @Getter
    @Setter
    protected Status status;
    public static final String PROP_STATUS = "status";

    public Resource() {
        this.propertySupport = new PropertyChangeSupport(this);
    }

    @Builder(builderMethodName = "buildResource")
    public Resource(String id, String name, String href, Status status, Integer index) {
        this.href = href;
        this.propertySupport = new PropertyChangeSupport(this);
        this.id = id;
        this.name = name;
        this.status = status;
        this.index = index;
    }

    public Resource(String id, String name, String href, Status status) {
        this(id, name, href, status, 0);
    }

    public void setName(String name) {
        String old = this.name;
        this.name = name;
        propertySupport.firePropertyChange(PROP_NAME, old, name);
    }

    public void setId(String id) {
        String old = this.id;
        this.id = id;
        propertySupport.firePropertyChange(PROP_ID, old, id);
    }

    public void setIndex(Integer index) {
        Integer old = this.index;
        this.index = index;
        propertySupport.firePropertyChange(PROP_INDEX, old, index);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(propertyName, listener);
    }

}
