/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api;

import static eu.logisense.api.Shipment.PROP_UNLOAD_LOCATION;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A container is something that can hold goods, for example a sea container, a truck container or a truck trailer.
 *
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Container extends Resource implements Trackable {

    public static final double STANDARD_WIDTH = 2.45;
    
    @Getter
    private List<Resource> shipments;
    public static final String PROP_SHIPMENTS = "shipments";

    @Getter
    private Location location;
    public static final String PROP_LOCATION = "location";

    @Getter
    private Double maxWeight;
    public static final String PROP_MAX_WEIGHT = "maxWeight";

    @Getter
    private Double width;
    public static final String PROP_WIDTH = "width";

    @Getter
    private Double length;
    public static final String PROP_LENGTH = "length";

    @Getter
    private Double height;
    public static final String PROP_HEIGHT = "height";

    @Builder
    public Container(List<Resource> shipments, String id, String name, String href, Location location, Double maxWeight, Double width, Double length, Double height) {

        super(id, name, href, null);
        this.shipments = shipments;
        this.location = location;

        this.maxWeight = maxWeight;

        // always object initializated
        if (shipments == null) {
            this.shipments = new ArrayList<>();
        } else {
            this.shipments = shipments;
        }

        this.width = width;
        this.length = length;
        this.height = height;
    }

    //added for display in pre-planning container selection combo box
    //FIXME use converter:
    /*
// tell the choice how to represent the item
StringConverter<Test> converter = new StringConverter<Test>() {

    @Override
    public String toString(Test album) {
        return album != null ? album.getName() : null;
    }

    @Override
    public Test fromString(String string) {
        return null;
    }

};
choiceBox.setConverter(converter);    
     */
    @Override
    public String toString() {
        return getName();
    }

    public void setHeight(Double height) {
        Double old = height;
        this.height = height;
        propertySupport.firePropertyChange(PROP_HEIGHT, old, height);
    }

    public void setLength(Double length) {
        Double old = length;
        this.length = length;
        propertySupport.firePropertyChange(PROP_LENGTH, old, length);
    }

    public void setLocation(Location location) {
        Location old = location;
        this.location = location;
        propertySupport.firePropertyChange(PROP_LOCATION, old, location);
    }

    public void setMaxWeight(Double maxWeight) {
        Double old = maxWeight;
        this.maxWeight = maxWeight;
        propertySupport.firePropertyChange(PROP_MAX_WEIGHT, old, maxWeight);
    }

    public void setShipments(List<Resource> shipments) {
        List<Resource> old = shipments;
        this.shipments = shipments;
        propertySupport.firePropertyChange(PROP_SHIPMENTS, old, shipments);
    }

    public void setWidth(Double width) {
        Double old = width;
        this.width = width;
        propertySupport.firePropertyChange(PROP_WIDTH, old, width);
    }

}
