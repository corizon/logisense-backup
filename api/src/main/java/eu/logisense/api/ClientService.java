/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@Path("/clients")
@Api(value = "/clients")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface ClientService {
    
    @GET
    @ApiOperation(value = "Get clients",
            response = Client.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no client found")})
    public List<Client> getClients();
    
    
    /**
     * get a client based on id
     *
     * @param id of the client to retrieve
     * @return found client
     */
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get client",
            response = Client.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no client found")})
    public Client getClient(@ApiParam(value = "id of the client", required = true) @PathParam("id") String id);

    /**
     * create a client based on provided template
     *
     * @param template for creating the client
     * @return created client
     */
    @POST
    @ApiOperation(value = "create client",
            response = Client.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no client found")})
    public Client postClient(@ApiParam(value = "client template", required = true) Client template);

    /**
     * update a client based on provided template
     *
     * @param id
     * @param template for creating the client
     * @return updated client
     */
    @PUT
    @Path("/{id}")
    @ApiOperation(value = "update client",
            response = Client.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no client found")})
    public Client updateClient(@ApiParam(value = "id of the client", required = true) @PathParam("id") String id, @ApiParam(value = "client template", required = true) Client template);

    /**
     * delete a Client based on id
     *
     * @param id of the Client to be deleted
     */
    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "delete Client")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no Clients found")})
    public void deleteClient(@ApiParam(value = "id of the Client", required = true)@PathParam("id") String id);
}
