/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Ride extends Resource {

    //TODO LOG-96: Refactor from nested objects to references
    @Getter
    @Setter
    private List<Container> containers = new ArrayList<>();

    @Getter
    @Setter
    private LocalDateTime etd;
    public static final String PROP_ETD = "etd";

    @Getter
    @Setter
    private Creditor creditor;
    public static final String PROP_CREDITOR = "creditor";

    @Getter
    @Setter
    private Double margin;
    public static final String PROP_MARGIN = "margin";

    @Getter
    @Setter
    private String externalId;
    public static final String PROP_EXTERNAL_ID = "externalId";

    @Getter
    @Setter
    private String licensePlate;
    public static final String PROP_LICENSE_PLATE = "licensePlate";

    // created timestamp; no attr+getter yet, only used internally on nodes for now
    public static final String PROP_CREATED = "created";

    @Builder
    public Ride(List<Container> containers, String id, String name, String href,
            LocalDateTime etd, Creditor creditor, Status status, String externalId,
            Double margin, String licensePlate) {
        super(id, name, href, status);
        this.containers = containers;
        this.etd = etd;
        this.creditor = creditor;
        this.externalId = externalId;
        this.margin = margin;
        this.licensePlate = licensePlate;
    }

}
