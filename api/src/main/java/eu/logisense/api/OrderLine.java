/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api;

import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OrderLine extends Resource {

    @Getter
    private Double length;
    public static final String PROP_LENGTH = "length";

    @Getter
    private Double width;
    public static final String PROP_WIDTH = "width";

    @Getter
    private Double height;
    public static final String PROP_HEIGHT = "height";

    /**
     * in kg
     */
    @Getter
    private Double weight;
    public static final String PROP_WEIGHT = "weight";

    @Getter
    private Integer amount;
    public static final String PROP_AMOUNT = "amount";

    @NotNull
    @Getter
    private OrderLineType type;
    public static final String PROP_TYPE = "type";

    @Getter
    private Boolean swapPallets;
    public static final String PROP_SWAP_PALLETS = "swapPallets";

    @Getter
    private String description;
    public static final String PROP_DESCRIPTION = "description";

    @Getter
    private String remarks;
    public static final String PROP_REMARKS = "remarks";

    @Getter
    private Boolean stackable;
    public static final String PROP_STACKABLE = "stackable";

    @Getter
    private Double paymentOnDeliveryAmount;
    public static final String PROP_PAYMENT_ON_DELIVERY_AMOUNT = "paymentOnDeliveryAmount";

    @Getter
    private String paymentOnDeliveryCurrency;
    public static final String PROP_PAYMENT_ON_DELIVERY_CURRENCY = "paymentOnDeliveryCurrency";

    @Builder
    public OrderLine(Double length, Double width, Double height,
            Double weight, Integer amount, OrderLineType type, Boolean swapPallets,
            String description, String remarks, Boolean stackable, Double paymentOnDeliveryAmount,
            String paymentOnDeliveryCurrency, String id, String name, String href) {

        super(id, name, href, null);
        this.length = length;
        this.width = width;
        this.height = height;
        this.weight = weight;
        this.amount = amount;
        this.type = type;
        this.swapPallets = swapPallets;
        this.description = description;
        this.remarks = remarks;
        this.stackable = stackable;
        this.paymentOnDeliveryAmount = paymentOnDeliveryAmount;
        this.paymentOnDeliveryCurrency = paymentOnDeliveryCurrency;
    }

    public void setLength(Double length) {
        Double old = this.length;
        this.length = length;
        propertySupport.firePropertyChange(PROP_LENGTH, old, length);
    }

    public void setWidth(Double width) {
        Double old = this.width;
        this.width = width;
        propertySupport.firePropertyChange(PROP_WIDTH, old, width);
    }

    public void setHeight(Double height) {
        Double old = this.height;
        this.height = height;
        propertySupport.firePropertyChange(PROP_HEIGHT, old, height);
    }

    public void setWeight(Double weight) {
        Double old = this.weight;
        this.weight = weight;
        propertySupport.firePropertyChange(PROP_WEIGHT, old, weight);
    }

    public void setDescription(String description) {
        String old = this.description;
        this.description = description;
        propertySupport.firePropertyChange(PROP_DESCRIPTION, old, description);
    }

    public void setRemarks(String remarks) {
        String old = this.remarks;
        this.remarks = remarks;
        propertySupport.firePropertyChange(PROP_REMARKS, old, remarks);
    }

    public void setStackable(Boolean stackable) {
        Boolean old = this.stackable;
        this.stackable = stackable;
        propertySupport.firePropertyChange(PROP_STACKABLE, old, stackable);
    }

    public void setAmount(Integer amount) {
        Integer old = this.amount;
        this.amount = amount;
        propertySupport.firePropertyChange(PROP_AMOUNT, old, amount);
    }

    public void setPaymentOnDeliveryAmount(Double paymentOnDeliveryAmount) {
        Double old = this.paymentOnDeliveryAmount;
        this.paymentOnDeliveryAmount = paymentOnDeliveryAmount;
        propertySupport.firePropertyChange(PROP_PAYMENT_ON_DELIVERY_AMOUNT, old, paymentOnDeliveryAmount);
    }

    public void setPaymentOnDeliveryCurrency(String paymentOnDeliveryCurrency) {
        String old = this.paymentOnDeliveryCurrency;
        this.paymentOnDeliveryCurrency = paymentOnDeliveryCurrency;
        propertySupport.firePropertyChange(PROP_PAYMENT_ON_DELIVERY_CURRENCY, old, paymentOnDeliveryCurrency);
    }

    public void setType(OrderLineType type) {
        OrderLineType old = this.type;
        this.type = type;
        propertySupport.firePropertyChange(PROP_TYPE, old, type);
    }

    public void setSwapPallets(Boolean swapPallets) {
        Boolean old = this.swapPallets;
        this.swapPallets = swapPallets;
        propertySupport.firePropertyChange(PROP_SWAP_PALLETS, old, swapPallets);
    }

}
