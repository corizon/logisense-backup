/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class Location extends Resource {

    @Getter
    @Setter
    private Double longitude;
    public static final String PROP_LONGITUDE = "longitude";
    @Getter
    @Setter
    private Double latitude;
    public static final String PROP_LATITUDE = "latitude";

    @Getter
    @Setter
    private String address;
    public static final String PROP_ADDRESS = "address";
    @Getter
    @Setter
    private String postalcode;
    public static final String PROP_POSTALCODE = "postalcode";

    @Getter
    @Setter
    private String city;
    public static final String PROP_CITY = "city";

    @Getter
    @Setter
    private String country;
    public static final String PROP_COUNTRY = "country";

    @Getter
    @Setter
    private String phone;
    public static final String PROP_PHONE = "phone";

    @Getter
    @Setter
    private String email;
    public static final String PROP_EMAIL = "email";

    @Getter
    @Setter
    private Status status;
    public static final String PROP_STATUS = "status";

    @Getter
    @Setter
    private String externalId;
    public static final String PROP_EXTERNAL_ID = "externalId";

    @Builder(builderMethodName = "buildLocation")
    public Location(Double longitude, Double latitude, String address, String postalcode, String city, String country, String externalId, Status status, String id, String name, String href, String phone, String email) {
        super(id, name, href, null);
        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
        this.postalcode = postalcode;
        this.city = city;
        this.country = country;
        this.phone = phone;
        this.email = email;
        this.status = status == null ? ApprovalStatus.NEW : status;
        this.externalId = externalId;
    }

}
