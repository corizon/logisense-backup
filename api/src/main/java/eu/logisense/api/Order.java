/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Order extends Resource {

    @Getter
    @Setter
    private List<OrderLine> lines = new ArrayList<>();
    public static final String PROP_ORDER_LINES = "lines";

    @Getter
    private String loadingList;
    public static final String PROP_LOADING_LIST = "loadingList";
    @Getter
    private String batch;
    public static final String PROP_BATCH = "batch";

    @Getter
    private Client orderedBy;
    public static final String PROP_ORDERED_BY = "orderedBy";

    @Getter
    private String invoiceReference;
    public static final String PROP_INVOICE_REFERENCE = "invoiceReference";

    @Getter
    private String unloadReference;
    public static final String PROP_UNLOAD_REFERENCE = "unloadReference";

    @Getter
    private String otherReference;
    public static final String PROP_OTHER_REFERENCE = "otherReference";

    @Getter
    private Origin origin;
    public static final String PROP_ORIGIN = "origin";

    @Getter
    private Destination destination;
    public static final String PROP_DESTINATION = "destination";

    @Getter
    private Debitor debitor;
    public static final String PROP_DEBITOR = "debitor";

    @Getter
    @Setter
    private String externalId;
    public static final String PROP_EXTERNAL_ID = "externalId";

    @Builder
    public Order(List<OrderLine> lines, String loadingList, String batch,
            Client orderedBy, String invoiceReference, String unloadReference,
            String otherReference, Origin origin, Destination destination,
            String id, String name, String href, Debitor debitor, String externalId) {
        super(id, name, href, null);

        if (lines == null) {
            this.lines = new ArrayList<>();
        } else {
            this.lines = lines;
        }

        this.loadingList = loadingList;
        this.batch = batch;
        this.orderedBy = orderedBy;
        this.invoiceReference = invoiceReference;
        this.unloadReference = unloadReference;
        this.otherReference = otherReference;
        this.origin = origin;
        this.destination = destination;
        this.debitor = debitor;
        this.externalId = externalId;
    }

    public void setDebitor(Debitor debitor) {
        Debitor old = this.debitor;
        this.debitor = debitor;
        propertySupport.firePropertyChange(PROP_DEBITOR, old, debitor);
    }

    public void setBatch(String batch) {
        String old = this.batch;
        this.batch = batch;
        propertySupport.firePropertyChange(PROP_BATCH, old, batch);
    }

    public void setInvoiceReference(String invoiceReference) {
        String old = this.invoiceReference;
        this.invoiceReference = invoiceReference;
        propertySupport.firePropertyChange(PROP_INVOICE_REFERENCE, old, invoiceReference);
    }

    public void setLoadingList(String loadingList) {
        String old = this.loadingList;
        this.loadingList = loadingList;
        propertySupport.firePropertyChange(PROP_LOADING_LIST, old, loadingList);
    }

    public void setOrderedBy(Client orderedBy) {
        Client old = this.orderedBy;
        this.orderedBy = orderedBy;
        propertySupport.firePropertyChange(PROP_ORDERED_BY, old, orderedBy);
    }

    public void setOtherReference(String otherReference) {
        String old = this.otherReference;
        this.otherReference = otherReference;
        propertySupport.firePropertyChange(PROP_OTHER_REFERENCE, old, otherReference);
    }

    public void setUnloadReference(String unloadReference) {
        String old = this.unloadReference;
        this.unloadReference = unloadReference;
        propertySupport.firePropertyChange(PROP_UNLOAD_REFERENCE, old, unloadReference);
    }

    public void setDestination(Destination destination) {
        Destination old = this.destination;
        this.destination = destination;
        propertySupport.firePropertyChange(PROP_DESTINATION, old, destination);
    }

    public void setOrigin(Origin origin) {
        Origin old = this.origin;
        this.origin = origin;
        propertySupport.firePropertyChange(PROP_ORIGIN, old, origin);
    }

}
