/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.api;

import io.swagger.annotations.Api;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author johan
 */
@Path("/external")
@Api(value = "/external")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface ExternalSyncService {
    
    //
    // 1) dedicated methods for external ID updates
    //    (so we don't have to fetch and put an object tree just to change a single attribute)
    
//    @PUT
//    @Path("/clients/{id}/{extid}")
//    public void externalClientId(@PathParam("id") String id, @PathParam("extid") String externalId);
//
//    @PUT
//    @Path("/rides/{id}/{extid}")
//    public void externalRideId(@PathParam("id") String id, @PathParam("extid") String externalId);
//
//    //note: extid goed on nested Order
//    @PUT
//    @Path("/shipments/{id}/{extid}")
//    public void externalShipmentId(@PathParam("id") String id, @PathParam("extid") String externalId);
    
    //
    // 2) POST methods for complete items
    //    (implementation similar to POSTs on regular entity resources, but using external IDs to match to existing items)
    
//    @POST
//    @Path("/clients")
//    public Client postClient(Client template);
//    
//    @POST
//    @Path("/locations")
//    public Location postLocation(Location template);
//    
//    @POST
//    @Path("/rides")
//    public Ride postRide(Ride template);

    
    @POST
    @Path("/shipments")
    public Shipment postShipment(Shipment template);
    

}
