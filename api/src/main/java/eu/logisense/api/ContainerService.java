/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/containers")
@Api(value = "/containers")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface ContainerService {

    @GET
    @ApiOperation(value = "Get containers",
            response = Container.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no container found")})
    public List<Container> getContainers();
    
    
    /**
     * get a container based on id
     *
     * @param id of the container to retrieve
     * @return found container
     */
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get container",
            response = Container.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no container found")})
    public Container getContainer(@ApiParam(value = "id of the container", required = true) @PathParam("id") String id);

    /**
     * create a container based on provided template
     *
     * @param template for creating the container
     * @return created container
     */
    @POST
    @ApiOperation(value = "create container",
            response = Container.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no container found")})
    public Container postContainer(@ApiParam(value = "container template", required = true) Container template);

    /**
     * update a container based on provided template
     *
     * @param template for creating the container
     * @return updated container
     */
    @PUT
    @ApiOperation(value = "update container",
            response = Container.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no container found")})
    public Container updateContainer(@ApiParam(value = "container template", required = true) Container template);

    /**
     * delete a Container based on id
     *
     * @param id of the Container to be deleted
     */
    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "delete Container")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no Containers found")})
    public void deleteContainer(@ApiParam(value = "id of the Container", required = true)@PathParam("id") String id);

}
