/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.opower.rest.client.generator.core.Client;
import com.opower.rest.client.generator.core.ClientExecutor;
import com.opower.rest.client.generator.core.ClientRequest;
import com.opower.rest.client.generator.core.ClientRequestFilter;
import com.opower.rest.client.generator.core.ResourceInterface;
import com.opower.rest.client.generator.executors.ApacheHttpClient4Executor;
import eu.logisense.api.session.client.TokenProvider;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import org.openide.util.Lookup;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ClientUtil {

    private static URI baseURI;
    private static final TokenProvider TOKEN_PROVIDER;

    private static HttpClientConnectionManager cm;

    public static URI getBaseURI() {
        return baseURI;
    }

    
    static {
        try {
//            baseURI = new URI("http://madrid.limetri.eu:8011/api");
            baseURI = new URI("http://localhost:8011/api");
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
        
        TOKEN_PROVIDER = Lookup.getDefault().lookup(TokenProvider.class);
        

        ConnectionSocketFactory plainsf = PlainConnectionSocketFactory.getSocketFactory();
//LayeredConnectionSocketFactory sslsf =  = new SSLConnectionSocketFactory(sslContext);
        Registry<ConnectionSocketFactory> r = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", plainsf)
                //        .register("https", sslsf)
                .build();

        cm = new PoolingHttpClientConnectionManager(r);
    }

    public static <T> T proxy(Class<T> claz, final URI base) {
        Client.Builder<T> clientBuilder = new Client.Builder<>(new ResourceInterface<>(claz), () -> {
            return base;
        });
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);
        clientBuilder.errorStatusCriteria((Integer status) -> status >= 400 && status <= 599);
        clientBuilder.registerProviderInstance(jsonProvider);

        List<ClientRequestFilter> filters = new ArrayList<>();
        //add bearer token header:
        if (TOKEN_PROVIDER != null) {
            ClientRequestFilter filter = (ClientRequest cr) -> cr.header("Authorization", "Bearer " + TOKEN_PROVIDER.getBearerToken());
            // opower docs don't match builder impl, add filter via executor ctor instead
//            clientBuilder.addClientRequestFilter(filter);
            filters.add(filter);
        }

        //
        // setting timeout to prevent connection freezes
        //

        int timeout = 5 * 1000;

        CloseableHttpClient client = HttpClients.custom()
                .setConnectionManager(cm)
                .setConnectionManagerShared(true)
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setConnectTimeout(timeout)
                        .setConnectionRequestTimeout(timeout)
                        .setSocketTimeout(timeout)
                        .build())
                .build();

        ClientExecutor executor = new ApacheHttpClient4Executor(client, filters);
        clientBuilder.executor(executor);
        return clientBuilder.build();
    }

    public static <T> T proxy(Class<T> claz) {
        return proxy(claz, baseURI);
    }
}
