/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/rides")
@Api(value = "/rides")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface RideService {

    @GET
    @ApiOperation(value = "Get rides",
            response = Ride.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no ride found")})
    public List<Ride> getRides();
    
    @GET
    @ApiOperation(value = "Get rides by estimated time departure",
            response = Ride.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no ride found")})
    public List<Ride> getRides(LocalDate etd);

    /**
     * get a ride based on id
     *
     * @param id of the ride to retrieve
     * @return found ride
     */
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get ride",
            response = Ride.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no ride found")})
    public Ride getRide(@ApiParam(value = "id of the ride", required = true) @PathParam("id") String id);

    /**
     * create a ride based on provided template
     *
     * @param template for creating the ride
     * @return created ride
     */
    @POST
    @ApiOperation(value = "create ride",
            response = Ride.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no ride found")})
    public Ride postRide(@ApiParam(value = "ride template", required = true) Ride template);

    /**
     * update a ride based on provided template
     *
     * @param template for creating the ride
     * @return updated ride
     */
    @PUT
    @ApiOperation(value = "update ride",
            response = Ride.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no ride found")})
    public Ride updateRide(@ApiParam(value = "ride template", required = true) Ride template);

    /**
     * delete a Ride based on id
     *
     * @param id of the Ride to be deleted
     */
    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "delete Ride")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no Rides found")})
    public void deleteRide(@ApiParam(value = "id of the Ride", required = true)@PathParam("id") String id);

   
}
