/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author kenrik
 */
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Creditor extends Resource {

    @Getter
    private Double price;
    public static final String PROP_PRICE = "price";

    @Getter
    private String currency;
    public static final String PROP_CURRENCY = "currency";

    @Getter
    private String contactName;
    public static final String PROP_CONTACT_NAME = "contactName";

    @Getter
    private String contactPhoneNumber;
    public static final String PROP_CONTACT_PHONE_NUMBER = "contactPhoneNumber";

    @Getter
    private String externalId;
    public static final String PROP_EXTERNAL_ID = "externalId";
    
    @Getter
    private Boolean chargeVat;
    public static final String PROP_CHARGE_VAT = "chargeVat";
    
    

    @Builder
    public Creditor(Double price, String currency, String id, String name, String href, String externalId, String contactName, String contactPhoneNumber,Boolean chargeVat) {
        super(id, name, href, null);
        this.price = price;
        this.currency = currency;
        this.externalId = externalId;
        this.contactName = contactName;
        this.contactPhoneNumber = contactPhoneNumber;
        this.chargeVat = chargeVat;
    }

    public void setChargeVat(Boolean chargeVat) {
        Boolean old = this.chargeVat;
        this.chargeVat = chargeVat;
        propertySupport.firePropertyChange(PROP_CHARGE_VAT, old, chargeVat);
    }

    public void setPrice(Double price) {
        Double old = this.price;
        this.price = price;
        propertySupport.firePropertyChange(PROP_PRICE, old, price);
    }

    public void setCurrency(String currency) {
        String old = this.currency;
        this.currency = currency;
        propertySupport.firePropertyChange(PROP_CURRENCY, old, currency);
    }

    public void setContactName(String contactName) {
        String old = this.contactName;
        this.contactName = contactName;
        propertySupport.firePropertyChange(PROP_CONTACT_NAME, old, contactName);
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        String old = this.contactPhoneNumber;
        this.contactPhoneNumber = contactPhoneNumber;
        propertySupport.firePropertyChange(PROP_CONTACT_PHONE_NUMBER, old, contactPhoneNumber);
    }

    public void setExternalId(String externalId) {
        String old = this.externalId;
        this.externalId = externalId;
        propertySupport.firePropertyChange(PROP_EXTERNAL_ID, old, externalId);
    }

    @Override
    public String toString() {
        return getName();
    }
}
