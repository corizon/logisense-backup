/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import eu.logisense.api.validator.AnonymousMode;
import eu.logisense.api.validator.CheckAnonymous;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@CheckAnonymous(AnonymousMode.ANONYMOUS_DENY)
public class Client extends Resource {

    @NotNull(message = "Charge Vat can not be null")
    @Getter
    @Setter

    private Boolean chargeVat;
    public static final String PROP_CHARGE_VAT = "chargeVat";

    @Size(min = 0, max = 200, message = "Charge of Comerce city can not exceed 200 characters")
    @Getter
    @Setter
    private String chamberOfCommerceCity;
    public static final String PROP_CHAMBER_OF_COMMERCE_CITY = "chamberOfCommerceCity";

    @Size(min = 0, max = 200, message = "Charge of Comerce can not exceed 200 characters")
    @Getter
    @Setter
    private String chamberOfCommerce;
    public static final String PROP_CHAMBER_OF_COMMERCE = "chamberOfCommerce";

    @Size(min = 0, max = 20, message = "VAT number can not exceed 20 characters")
    @Getter
    @Setter
    private String vatNumber;
    public static final String PROP_VAT_NUMBER = "vatNumber";

    @Size(min = 8, max = 11, message = "BIC code must be between 8 and 11 characters")
    @Getter
    @Setter
    private String bicCode;
    public static final String PROP_BIC_CODE = "bicCode";

    @Size(min = 0, max = 30, message = "IBAN code can not exceed 30 characters")
    @Getter
    @Setter
    private String ibanCode;
    public static final String PROP_IBAN_CODE = "ibanCode";

    @Size(min = 0, max = 30, message = "Currency can not exceed 30 characters")
    @Getter
    @Setter
    private String currency;
    public static final String PROP_CURRENCY = "currency";

    @Size(min = 1, message = "Locations must not be empty")
    @NotNull
    @Getter
    @Setter
    private List<Location> locations = new ArrayList<>();
    public static final String PROP_LOCATIONS = "location";

    @Size(min = 0, max = 30)
    @Getter
    @Setter
    private String debtorNumber;
    public static final String PROP_DEBTOR_NUMBER = "debtorNumber";

    @Size(min = 0, max = 30, message = "Creditor can not exceed {max} characters")
    @Getter
    @Setter
    private String creditorNumber;
    public static final String PROP_CREDITOR_NUMBER = "creditorNumber";

    @Size(min = 0, max = 1000000, message = "External Id can not exceed {max} characters")
    @Getter
    @Setter
    private String externalId;
    public static final String PROP_EXTERNAL_ID = "externalId";

    @Getter
    private Status status;
    public static final String PROP_STATUS = "status";

    @Builder
    public Client(Boolean chargeVat, String chamberOfCommerceCity, String chamberOfCommerce, String vatNumber, String bicCode, String ibanCode, String currency, Status status, String debtorNumber, String creditorNumber, String externalId, List<Location> locations, String id, String name, String href) {
        super(id, name, href, null);

        this.chamberOfCommerceCity = chamberOfCommerceCity;
        this.chamberOfCommerce = chamberOfCommerce;

        if (chargeVat == null) {
            this.chargeVat = false;
        } else {
            this.chargeVat = chargeVat;
        }

        this.vatNumber = vatNumber;
        this.bicCode = bicCode;
        this.ibanCode = ibanCode;
        this.currency = currency;
        this.debtorNumber = debtorNumber;
        this.creditorNumber = creditorNumber;
        this.externalId = externalId;
        this.status = status;
        this.locations = locations;
        if (this.status == null) {
            this.status = ApprovalStatus.NEW;
        }
        if (this.locations == null) {
            this.locations = new ArrayList<>();
        }
    }

    public void setStatus(Status status) {
        Status old = this.status;
        this.status = status;
        propertySupport.firePropertyChange(PROP_STATUS, old, status);
    }

}
