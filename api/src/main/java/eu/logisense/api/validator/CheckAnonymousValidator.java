/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api.validator;

import eu.logisense.api.Client;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author perezdf
 */
public class CheckAnonymousValidator implements ConstraintValidator<CheckAnonymous, Client> {

    private static final Logger log = LoggerFactory.getLogger(CheckAnonymousValidator.class);

    private AnonymousMode caseMode;

    @Override
    public void initialize(CheckAnonymous constraintAnnotation) {
        this.caseMode = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(Client object, ConstraintValidatorContext constraintContext) {

        log.info("checking value {}", object);
        if (object == null) {
            return true;
        }

        if (caseMode == AnonymousMode.ANONYMOUS_DENY) {
            return object.getName() != null && !object.getName().trim().isEmpty();
        } else {
            return false;
        }
    }
}
