/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.api.session.client;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 * Helper class to find and manage connections.
 *
 * A client needs to provide a local authorization method like this:
 *
 * @ServiceProvider(service=AuthorizationMethod.class) public class
 * LocalAuthorizationMethod implements AuthorizationMethod{
 *
 * @author Timon Veenstra <monezz@gmail.com>
 */
public class Connections {

    private static Connection connection;
    private static final Logger LOGGER = Logger.getLogger(Connections.class.getName());

    private static final Queue<Runnable> USER_CONNECTION_QUEUE = new ArrayBlockingQueue<>(100);


    /**
     * Create an {@link Connection} from an URL
     *
     * @param baseUri
     * @return
     */
    public static Connection create(String baseUri) {
        if (connection == null) {

            ConnectionFactory factory = Lookup.getDefault().lookup(ConnectionFactory.class);
            if (factory == null) {
                throw new IllegalStateException("No UserConnectionFactory implementation found!");
            }

            LOGGER.log(Level.INFO, "creating connection to {0}", baseUri);
            connection = factory.create(baseUri);

            new ConnectionThread(connection).start();
        } else {
            //TODO implement UserConnection switching?
            LOGGER.warning("attempt to create UserConnection with a possibly different URL, switching not supported yet! returning old UserConnection");
        }
        return connection;
    }
    
    /**
     * <p>
     * Perform a task on the UserConnection.</p>
     *
     * <p>
     * This method can also be called when authorization process is still
     * ongoing. The task will be executed as soon as the connection is
     * validated.</p>
     *
     * @param runnable
     */
    public static void onConnection(Runnable runnable) {
        LOGGER.log(Level.INFO, "adding runnable to USER_CONNECTION_QUEUE");
        USER_CONNECTION_QUEUE.add(runnable);
        LOGGER.log(Level.INFO, "// adding runnable to USER_CONNECTION_QUEUE");
    }

    /**
     * get a user connection. Will return a default connection when no url is
     * specified. see
     * https://bitbucket.org/limetri/agrosense/wiki/DevFaqConfigureServer
     *
     * @return
     */
    public static Connection get() {
        if (connection == null) {
            return create((String)null);
        }
        return connection;
    }

    static class ConnectionThread extends Thread {

        private final Connection connection;

        public ConnectionThread(Connection connection) {
            this.connection = connection;
        }

        @Override
        public void run() {
            LOGGER.info("Starting UserConnectionThread");
            if (!connection.isActive()) {
                connection.addActivationListener(new PropertyChangeListener() {

                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if ((Boolean) evt.getNewValue()) {
                            synchronized (ConnectionThread.this) {
                                LOGGER.info("user session validated");
                                ConnectionThread.this.notify();
                            }
                        }
                    }
                });
                synchronized (this) {
                    try {
                        LOGGER.info("waiting for connection to become active again");
                        wait();
                    } catch (InterruptedException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }

            }
            LOGGER.info("Poll UserConnectionQueue");
            while (connection.isActive()) {
                Runnable job = USER_CONNECTION_QUEUE.poll();
                if (job != null) {
                    job.run();
                } else {
                    try {
                        sleep(5000);
                    } catch (InterruptedException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }
            LOGGER.warning("connection deactivated, stopping ConnectionThread");
        }

    }
}
