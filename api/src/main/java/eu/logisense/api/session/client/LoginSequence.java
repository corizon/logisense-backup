/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.api.session.client;

/**
 *
 * @author johan
 */
public abstract class LoginSequence {
//TODO: 
    // - describes the process from appl startup to a Connection (monitored for active state)
    // - should tie together the interfaces for session, connection and success/failure action callbacks, ...
    
    // authentication on app startup:
    // - hook for login screen ("overlay" in NB, fx could replace primary stage)
    //   - success/failure action callbacks
    // - succesful login creates a (client-) Session
    // - (optional world section via universe)
    // - create Connection (backed by server session impl at other end)
    //     apply activationListener (e.g. send requests for explorer views via rest/dolphin/...)
    
    protected final Callback loginCallback = new Callback() {

        @Override
        public void onSuccess() {
//            activateWorldSelection();
            resume();
        }

        @Override
        public void onFailure() {
            exit();
        }
    };
    
    public void start() {
        init();
        activateLogin();
    }
    
    protected abstract void init();
    
    protected abstract void activateLogin();
    
//    protected abstract void activateWorldSelection();
    
    protected abstract void resume();
    
    protected abstract void exit();
    
    
}
