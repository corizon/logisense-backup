/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/vehicles")
@Api(value = "/vehicles")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface VehicleService {

    @GET
    @ApiOperation(value = "Get vehicles",
            response = Vehicle.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no vehicle found")})
    public List<Vehicle> getVehicles();

    /**
     * get a vehicle based on id
     *
     * @param id of the vehicle to retrieve
     * @return found vehicle
     */
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get vehicle",
            response = Vehicle.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no vehicle found")})
    public Vehicle getVehicle(@ApiParam(value = "id of the vehicle", required = true) @PathParam("id") String id);

    /**
     * create a vehicle based on provided template
     *
     * @param template for creating the vehicle
     * @return created vehicle
     */
    @POST
    @ApiOperation(value = "create vehicle",
            response = Vehicle.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no vehicle found")})
    public Vehicle postVehicle(@ApiParam(value = "vehicle template", required = true) Vehicle template);

    /**
     * update a vehicle based on provided template
     *
     * @param template for creating the vehicle
     * @return updated vehicle
     */
    @PUT
    @ApiOperation(value = "update vehicle",
            response = Vehicle.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no vehicle found")})
    public Vehicle updateVehicle(@ApiParam(value = "vehicle template", required = true) Vehicle template);

    /**
     * delete a Vehicle based on id
     *
     * @param id of the Vehicle to be deleted
     */
    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "delete Vehicle")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "no Vehicles found")})
    public void deleteVehicle(@ApiParam(value = "id of the Vehicle", required = true)@PathParam("id") String id);

}
