/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api.vms;

import eu.logisense.api.Resource;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import eu.logisense.api.Ride;
import eu.logisense.api.Shipment;
import java.util.ArrayList;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ExportObjectCardVM extends ExportCardVM {

    @Getter
    @Setter
    private List<Ride> rides= new ArrayList<>();
    public static final String PROP_RIDE = "rides";

    @Getter
    @Setter
    private List<Shipment> shipments= new ArrayList<>();
    public static final String PROP_SHIPMENTS = "shipments";
    
    @Getter
    @Setter
    private List<ClientLocationVM> clientLocations= new ArrayList<>();
    public static final String PROP_CLIENT_LOCATIONS = "clientLocations";    

    @Builder
    public ExportObjectCardVM(String exportId, List<Ride> rides, List<Shipment> shipments,List<ClientLocationVM> clientLocations) {
        super(exportId);
        if (rides == null) {
            this.rides = new ArrayList<>();
        } else {
            this.rides = rides;
        }

        if (shipments == null) {
            this.shipments = new ArrayList<>();
        } else {
            this.shipments = shipments;
        }
        
        if (clientLocations == null) {
            this.clientLocations = new ArrayList<>();
        } else {
            this.clientLocations = clientLocations;
        }        
    }

}
