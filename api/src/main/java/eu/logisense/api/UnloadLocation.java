/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.api;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UnloadLocation extends Location {

    @Getter
    @Setter
    private LocalDateTime timeFrameFrom;
    public static final String PROP_TIME_FRAME_FROM = "timeFrameFrom";

    @Getter
    @Setter
    private LocalDateTime timeFrameTo;
    public static final String PROP_TIME_FRAME_TO = "timeFrameTo";

    @Builder
    public UnloadLocation(LocalDateTime timeFrameFrom, LocalDateTime timeFrameTo, Double longitude, Double latitude, String address, String postalcode, String city, String country, String externalId,Status status, String id, String name, String href, String phone, String email) {
        super(longitude, latitude, address, postalcode, city, country, externalId, status, id, name, href, phone, email);
        this.timeFrameFrom = timeFrameFrom;
        this.timeFrameTo = timeFrameTo;
    }

}
