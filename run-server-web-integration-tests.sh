echo "****.....          Test 0001      ....****"
echo "****..... Attempt to post a shipment with minimum data ....****"
#OUT OF DATE curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" -d @server/web/integration-tests/test-set-0001.json  http://localhost:8011/api/shipments

echo "****.....          Test 0002      ....****"
echo "****..... Attempt to post a shipment filling the requesting data ....****"
#OUT OF DATE curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" -d @server/web/integration-tests/test-set-0002.json  http://localhost:8011/api/shipments


echo "****.....          Test 0003      ....****"
echo "****..... Attempt to GET shipment list ....****"
# OUT OF DATE curl -X GET -H "Accept: application/json" -H "Content-Type: application/json"  http://localhost:8011/api/shipments


echo "****.....          Test 0004      ....****"
echo "****..... Attempt to post a shipment which is giving problems during development ....****"
curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" -d @server/web/integration-tests/test-set-0004.json  http://localhost:8011/api/shipments
#curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" -d @server/web/integration-tests/test-set-0004.json  http://madrid.limetri.eu:8011/api
