/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public abstract class BaseObjectFactoryTest {
    
    GraphDatabaseService databaseService;
    Transaction transaction;
    Node node;    
    Relationship relationship;    
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        databaseService = mock(GraphDatabaseService.class);
        transaction = mock(Transaction.class);
        node = mock(Node.class);
        relationship = mock(Relationship.class);

        when(node.getGraphDatabase()).thenReturn(databaseService);
        when(relationship.getGraphDatabase()).thenReturn(databaseService);
        when(databaseService.beginTx()).thenReturn(transaction);        

    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
