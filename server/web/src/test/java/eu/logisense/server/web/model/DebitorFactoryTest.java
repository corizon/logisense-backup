/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.Debitor;
import java.math.BigDecimal;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.Times;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
/**
 *
 * @author kenrik
 */
public class DebitorFactoryTest extends BaseObjectFactoryTest{
    
    public DebitorFactoryTest() {
    }
    
    /**
     * Test of fromNode method, of class DebitorFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");

        String id = "1234567";
        String extid = "1245646467";
        String href = "/clients/"+id;
        String name = "some name";
        String currency = "EUR";
        Double price = 123.0d;
        Boolean chargeVat = true;
        
        when(node.getProperty(Debitor.PROP_HREF)).thenReturn(href);
        when(node.getProperty(Debitor.PROP_EXTERNAL_ID)).thenReturn(extid);
        when(node.getProperty(Debitor.PROP_ID)).thenReturn(id);
        when(node.getProperty(Debitor.PROP_NAME)).thenReturn(name);
        when(node.getProperty(Debitor.PROP_CHARGE_VAT)).thenReturn(chargeVat);
        when(relationship.getProperty(Debitor.PROP_CURRENCY)).thenReturn(currency);
        when(relationship.getProperty(Debitor.PROP_PRICE)).thenReturn(price);

        Debitor result = DebitorFactory.fromNode(node,relationship);
        assertEquals(result.getHref(), href);
        assertEquals(result.getId(), id);
        assertEquals(result.getName(), name);
        assertEquals(result.getCurrency(), currency);
        assertEquals(result.getPrice(), price);
        assertEquals(result.getChargeVat(), chargeVat);

        verify(databaseService, new Times(7)).beginTx();
        verify(transaction, new Times(7)).success();
    }

    /**
     * Test of toHref method, of class DebitorFactory.
     */
    @Test
    public void testToHref() {
        System.out.println("toHref");
        assertEquals(ClientFactory.toHref("12345"), "/clients/12345");
        assertEquals(ClientFactory.toHref("67890"), "/clients/67890");
    }
    
}
