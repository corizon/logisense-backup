/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.OrderLine;
import eu.logisense.api.OrderLineType;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.Times;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class OrderLineFactoryNGTest  extends BaseObjectFactoryTest {
    
    public OrderLineFactoryNGTest() {
    }


    /**
     * Test of fromNode method, of class OrderLineFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");

        String orderId = "78768756787";
        String name = "ikuytrewdfghjhgfdsdfghjgfd";
        String id = "KJHGFDS4564DFGDF345654EFG";
        String href = OrderLineFactory.toHref(id,orderId);
        Double length = 4.5678;
        Double width = 4.5678;
        Double height = 4.5678;
        Double volume = 4.5678;
        Double weight = 23.4567;
        String description = "some note";
        String remarks = "some remark";
        Boolean stackable = Boolean.TRUE;
        OrderLineType type = OrderLineType.BLOCK_PALLET;
        Boolean swapPallets = Boolean.FALSE;
        String currency = "EUR";
        Double amount = 34.95;
        Integer packages = 2;

        when(node.getProperty(OrderLine.PROP_NAME)).thenReturn(name);
        when(node.getProperty(OrderLine.PROP_ID)).thenReturn(id);
        when(node.getProperty(OrderLine.PROP_HREF)).thenReturn(href);
        when(node.getProperty(OrderLine.PROP_AMOUNT)).thenReturn(packages);
        when(node.getProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT)).thenReturn(amount);
        when(node.getProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY)).thenReturn(currency);
        when(node.getProperty(OrderLine.PROP_TYPE)).thenReturn(type.name());
        when(node.getProperty(OrderLine.PROP_SWAP_PALLETS)).thenReturn(swapPallets);
        when(node.getProperty(OrderLine.PROP_DESCRIPTION)).thenReturn(description);
        when(node.getProperty(OrderLine.PROP_REMARKS)).thenReturn(remarks);
        when(node.getProperty(OrderLine.PROP_STACKABLE)).thenReturn(stackable);
        when(node.getProperty(OrderLine.PROP_LENGTH)).thenReturn(length);
        when(node.getProperty(OrderLine.PROP_WIDTH)).thenReturn(width);
        when(node.getProperty(OrderLine.PROP_HEIGHT)).thenReturn(height);
        when(node.getProperty(OrderLine.PROP_WEIGHT)).thenReturn(weight);

        OrderLine result = OrderLineFactory.fromNode(node,orderId);
        assertEquals(result.getName(), name);
        assertEquals(result.getId(), id);
        assertEquals(result.getHref(), href);
        assertEquals(result.getDescription(), description);
        assertEquals(result.getRemarks(), remarks);
        assertEquals(result.getStackable(), stackable);
        assertEquals(result.getLength(), length);
        assertEquals(result.getWidth(), width);
        assertEquals(result.getHeight(), height);
        assertEquals(result.getWeight(), weight);
        assertEquals(result.getAmount(), packages);
        assertEquals(result.getType(), type);
        assertEquals(result.getSwapPallets(), swapPallets);
        assertEquals(result.getPaymentOnDeliveryAmount(), amount);
        assertEquals(result.getPaymentOnDeliveryCurrency(), currency);

        verify(databaseService, new Times(15)).beginTx();
        verify(transaction, new Times(15)).success();
    }

    /**
     * Test of toHref method, of class OrderLineFactory.
     */
    @Test
    public void testToHref() {
        System.out.println("toHref");
        assertEquals(OrderLineFactory.toHref("12345","87890"), "/orders/87890/lines/12345");
        assertEquals(OrderLineFactory.toHref("67890","2367"), "/orders/2367/lines/67890");
    }
    
}
