/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web;

import eu.logisense.api.AcceptShipmentsCapability;
import eu.logisense.api.Destination;
import eu.logisense.api.Export;
import eu.logisense.api.Link;
import eu.logisense.api.Order;
import eu.logisense.api.ExportStatus;
import eu.logisense.api.Origin;
import eu.logisense.api.Client;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Debitor;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import eu.logisense.api.UnloadLocation;
import eu.logisense.api.vms.ExportObjectCardVM;
import eu.logisense.server.web.model.ClientFactory;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.mockito.Matchers;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ExportResourceNGTest {

    private static final Logger log = LoggerFactory.getLogger(ExportResourceNGTest.class);

    public ExportResourceNGTest() {
    }

    private GraphDatabaseService graphDb;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
//        graphDb = new RestGraphDatabase("http://localhost:7474");
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
        Util.initTestSet(graphDb);
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }

    /**
     * Test of dispatchTo method, of class ShipmentResource.
     *
     * @throws java.io.IOException
     * @throws java.net.URISyntaxException
     */
    @Test
    public void getExports() throws IOException, URISyntaxException {
        log.info("getExports");
        log.info("Prepare Scenario");
        Export template = Export.builder().name("name1").href("href").build();
        ExportResource instance = new ExportResource(graphDb);

        Export result = instance.postExport(template);
        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());

        log.info("Execute test");
        List<Export> exports = instance.getExports();

        log.info("Check result");
        assertNotNull(exports);
        assertTrue(exports.size() == 1);

    }

    @Test
    public void getExport() throws IOException, URISyntaxException {
        log.info("getExport");
        log.info("Prepare Scenario");
        Export template = Export.builder().name("name1").build();
        ExportResource instance = new ExportResource(graphDb);

        Order order = Order.builder()
                .name("order")
                .origin(Origin.builder()
                        .longitude(0.0).latitude(0.0).build())
                .destination(Destination.builder()
                        .latitude(0.0)
                        .longitude(0.0).build())
                .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                .otherReference("")
                .orderedBy(Client.builder().status(ApprovalStatus.NEW).name("resource").build())
                .build();
        LoadLocation loadLocation = LoadLocation.builder().latitude(0.0).longitude(0.0).build();
        UnloadLocation unloadLocation = UnloadLocation.builder().latitude(0.0).longitude(0.0).build();

        Shipment shipment = Shipment.builder()
                .name("name")
                .order(order)
                .loadLocation(loadLocation)
                .unloadLocation(unloadLocation)
                .build();
        ShipmentResource shipmentResource = new ShipmentResource(graphDb);
        Shipment returnShipment = shipmentResource.postShipment(shipment);
        template.getElements().add(Link.buildLink().href(returnShipment.getHref()).build());

        Export result = instance.postExport(template);

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());

        log.info("Execute test");
        Export exportAfterPost = instance.getExport(result.getId());
        log.info("Check result");
        assertNotNull(exportAfterPost);
        assertEquals("name1", exportAfterPost.getName());
        assertNotNull(exportAfterPost.getId());
        assertEquals(result.getId(), exportAfterPost.getId());
        assertNotNull(exportAfterPost.getHref());
        assertNotNull(exportAfterPost.getElements());
        assertTrue(!exportAfterPost.getElements().isEmpty());
    }
    
  

    @Test
    public void postExport() throws IOException, URISyntaxException {
        log.info("postRide");
        log.info("Prepare Scenario");
        ShipmentResource shipmentResource = new ShipmentResource(graphDb);

        Order order = Order.builder().origin(Origin.builder().longitude(0.0).latitude(0.0).build())
                .destination(Destination.builder().latitude(0.0).longitude(0.0).build())
                .otherReference("")
                .name("order")
                .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                .orderedBy(Client.builder().status(ApprovalStatus.NEW).name("resource").build())
                .build();
        LoadLocation loadLocation = LoadLocation.builder().latitude(0.0).longitude(0.0).build();
        UnloadLocation unloadLocation = UnloadLocation.builder().latitude(0.0).longitude(0.0).build();

        Shipment shipment = Shipment.builder()
                .name("name")
                .order(order)
                .loadLocation(loadLocation)
                .unloadLocation(unloadLocation)
                .build();
        Shipment returnShipment = shipmentResource.postShipment(shipment);

        ExportResource instance = new ExportResource(graphDb);
        Export template = Export.builder().name("name1").href("pepe").build();

        template.getElements().add(Link.buildLink().href(returnShipment.getHref()).build());

        log.info("Execute test");

        Export result = instance.postExport(template);
        log.info("Check result");

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());

        assertNotNull(result.getElements());
        assertEquals(result.getElements().get(0), Link.buildLink().href(returnShipment.getHref()).build());
        assertEquals(ExportStatus.NEW, result.getStatus());
    }

    @Test
    public void dispatchObjects() throws IOException, URISyntaxException {
        log.info("disptachExport");
        log.info("Prepare Scenario");

        AcceptShipmentsCapability usageService = mock(AcceptShipmentsCapability.class);


        Export template = Export.builder().name("name1").href("href").build();

        ShipmentResource shipmentResource = new ShipmentResource(graphDb);

        Order order = Order.builder().origin(Origin.builder().longitude(0.0).latitude(0.0).build())
                .destination(Destination.builder().latitude(0.0).longitude(0.0).build())
                .otherReference("")
                .name("order")
                .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                .orderedBy(Client.builder().status(ApprovalStatus.NEW).name("resource").build())
                .build();
        LoadLocation loadLocation = LoadLocation.builder().latitude(0.0).longitude(0.0).build();
        UnloadLocation unloadLocation = UnloadLocation.builder().latitude(0.0).longitude(0.0).build();

        Shipment shipment = Shipment.builder()
                .name("name")
                .order(order)
                .loadLocation(loadLocation)
                .unloadLocation(unloadLocation)
                .build();
        Shipment returnShipment = shipmentResource.postShipment(shipment);

        ExportResource instance = new ExportResource(graphDb);
        instance.setUsageService(usageService);

        template.getElements().add(Link.buildLink().href(returnShipment.getHref()).build());
        Export result = instance.postExport(template);
        log.info("Execute test");

        result = instance.dispatch(result.getId());
        log.info("Check result");

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());

        assertNotNull(result.getElements());
        assertEquals(ExportStatus.DISPATCHED, result.getStatus());
        assertEquals(result.getElements().get(0), Link.buildLink().href(returnShipment.getHref()).build());

    }

    @Test
    public void dispatchAll() throws IOException, URISyntaxException {
        log.info("dispatchAll");
        log.info("Prepare Scenario");
        Util.initTestSet(graphDb);
        Response response = mock(Response.class);
        when(response.getStatus()).thenReturn(200);

        AcceptShipmentsCapability usageService = mock(AcceptShipmentsCapability.class);


        ExportResource instance = new ExportResource(graphDb);
        instance.setUsageService(usageService);

        log.info("Execute test");
        instance.dispatchAllClients();
        instance.dispatchAllObjectsByResources();
        log.info("Check result");

    }

    @Test
    public void dispatchClients() throws IOException, URISyntaxException {
        log.info("dispatchClients");
        log.info("Prepare Scenario");

        AcceptShipmentsCapability usageService = mock(AcceptShipmentsCapability.class);


        Export template = Export.builder().name("name1").href("href").build();
        List<Location> locations = new ArrayList<>();
        String id = "123457";
        locations.add(Location.buildLocation()
                .address("address " + id)
                .city("city " + id)
                .country("country " + id)
                .id("1")
                .latitude(12.34)
                .longitude(56.78)
                .name("name " + id)
                .postalcode("postalcode " + id)
                .status(ApprovalStatus.APPROVED)
                .externalId("external" + id)
                .build());
        Client client = Client.builder()
                .href(ClientFactory.toHref(id))
                .id(id)
                .name("client " + id)
                .bicCode("12345678901")
                .chamberOfCommerce("")
                .chamberOfCommerceCity("")
                .chargeVat(Boolean.TRUE)
                .creditorNumber("")
                .currency("")
                .debtorNumber("")
                .externalId("")
                .ibanCode("12345678901234567890")
                .vatNumber("")                
                .locations(locations)
                .status(ApprovalStatus.APPROVED)
                .build();
        ClientResource clientResource = new ClientResource(graphDb);
        client = clientResource.postClient(client);
        template.getElements().add(Link.buildLink().href(client.getHref()).build());

        ExportResource instance = new ExportResource(graphDb);
        instance.setUsageService(usageService);

        template.getElements().add(Link.buildLink().href(client.getHref()).build());
        Export result = instance.postExport(template);
        log.info("Execute test");

        result = instance.dispatch(result.getId());
        log.info("Check result");

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());

        assertNotNull(result.getElements());
        assertEquals(ExportStatus.DISPATCHED, result.getStatus());

    }

    @Test
    public void testGetExportCard() throws IOException, URISyntaxException {

        log.info("getExportCard");
        log.info("Prepare Scenario");
        Export template = Export.builder().name("name1").href("href").build();

        ShipmentResource shipmentResource = new ShipmentResource(graphDb);

        Order order = Order.builder().origin(Origin.builder().longitude(0.0).latitude(0.0).build())
                .destination(Destination.builder().latitude(0.0).longitude(0.0).build())
                .otherReference("")
                .name("order")
                .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                .orderedBy(Client.builder().status(ApprovalStatus.NEW).name("resource").build())
                .build();
        LoadLocation loadLocation = LoadLocation.builder().latitude(0.0).longitude(0.0).build();
        UnloadLocation unloadLocation = UnloadLocation.builder().latitude(0.0).longitude(0.0).build();

        Shipment shipment = Shipment.builder()
                .name("name")
                .order(order)
                .loadLocation(loadLocation)
                .unloadLocation(unloadLocation)
                .build();
        Shipment returnShipment = shipmentResource.postShipment(shipment);

        ExportResource instance = new ExportResource(graphDb);

        template.getElements().add(Link.buildLink().href(returnShipment.getHref()).build());
        Export result = instance.postExport(template);

        log.info("Execute test");
        ExportObjectCardVM exportCard = instance.getExportCard(result.getId());

        log.info("Check result");
        assertNotNull(exportCard);
    }

}
