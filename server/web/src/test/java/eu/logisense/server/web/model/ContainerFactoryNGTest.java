/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.Container;
import eu.logisense.api.Location;
import eu.logisense.api.Resource;
import java.time.LocalDateTime;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.Times;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ContainerFactoryNGTest extends BaseObjectFactoryTest{
    
    public ContainerFactoryNGTest() {
    }

    /**
     * Test of fromNode method, of class ContainerFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");

        String id = "1234567";
        String href = "/containers/"+id;
        String name = "some name";
        double height = 2.0;
        double width = 2.45;
        double length = 16.0;
        double maxWeight = 12345.0;
        
        when(node.getProperty(Container.PROP_HREF)).thenReturn(href);
        when(node.getProperty(Container.PROP_ID)).thenReturn(id);
        when(node.getProperty(Container.PROP_NAME)).thenReturn(name);
        when(node.getProperty(Container.PROP_HEIGHT)).thenReturn(height);
        when(node.getProperty(Container.PROP_LENGTH)).thenReturn(length);
        when(node.getProperty(Container.PROP_MAX_WEIGHT)).thenReturn(maxWeight);
        when(node.getProperty(Container.PROP_WIDTH)).thenReturn(width);

        Container result = ContainerFactory.fromNode(node);
        assertEquals(result.getHref(), href);
        assertEquals(result.getId(), id);
        assertEquals(result.getName(), name);
        assertEquals(result.getHeight(), height);
        assertEquals(result.getLength(), length);
        assertEquals(result.getMaxWeight(), maxWeight);
        assertEquals(result.getWidth(), width);


        verify(databaseService, new Times(7)).beginTx();
        verify(transaction, new Times(7)).success();
    }

    /**
     * Test of resourceFromNode method, of class ContainerFactory.
     */
    @Test
    public void testResourceFromNode() {
        System.out.println("resourceFromNode");

        String name = "some name";
        String id = "1234567";
        String href = "/containers/"+id;
        
        when(node.getProperty(Container.PROP_HREF)).thenReturn(href);
        when(node.getProperty(Container.PROP_ID)).thenReturn(id);
        when(node.getProperty(Container.PROP_NAME)).thenReturn(name);

        Resource result = ContainerFactory.resourceFromNode(node);
        assertEquals(result.getHref(), href);
        assertEquals(result.getId(), id);
        assertEquals(result.getName(), name);


        verify(databaseService, new Times(3)).beginTx();
        verify(transaction, new Times(3)).success();
    }

    /**
     * Test of toHref method, of class ContainerFactory.
     */
    @Test
    public void testToHref() {
        System.out.println("toHref");
        assertEquals(ContainerFactory.toHref("12345"), "/containers/12345");
        assertEquals(ContainerFactory.toHref("67890"), "/containers/67890");
    }

}
