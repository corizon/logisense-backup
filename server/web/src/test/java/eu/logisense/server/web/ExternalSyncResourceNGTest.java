/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web;

import eu.logisense.api.Client;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.OrderLineType;
import eu.logisense.api.Origin;
import eu.logisense.api.Shipment;
import eu.logisense.api.UnloadLocation;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.neo4j.graphdb.Label;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 *
 * @author johan
 */
public class ExternalSyncResourceNGTest {

    private GraphDatabaseService graphDb;

    @BeforeMethod
    public void setUpMethod() throws Exception {
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }

    @Test
    public void testPostShipment() throws IOException, URISyntaxException {
        Shipment template = makeShipmentTemplate();
        ExternalSyncResource instance = new ExternalSyncResource(graphDb);
        
        Shipment result = instance.postShipment(template);
        
        assertNotNull(result);
        assertNotNull(result.getLoadLocation());
        assertNotNull(result.getUnloadLocation());
        
        assertNotNull(result.getOrder());
        assertNotNull(result.getOrder().getDebitor());
        assertNotNull(result.getOrder().getDestination());
        assertNotNull(result.getOrder().getLines());
        assertNotNull(result.getOrder().getOrderedBy());
        assertNotNull(result.getOrder().getOrigin());
        
        assertEquals(2, result.getOrder().getLines().size());
    }

    @Test    
    public void testPostShipmentWithExistingOrder() throws IOException, URISyntaxException {
        //db order w/ matching externalId should throw UnsupportedOperationException
        Shipment template = makeShipmentTemplate();
        boolean thrown = false;
        ExternalSyncResource instance = new ExternalSyncResource(graphDb);
        
        Shipment result = instance.postShipment(template);
        assertNotNull(result);
        
        //post existing order: e.g. repost of same template
        try {
            instance.postShipment(template);
            fail("should have thrown UnsupportedOperationException");
        } catch (UnsupportedOperationException e) {
            thrown = true;
        }
        
        assertTrue(thrown);
    }

    @Test
    public void testPostShipmentWithExistingClientAndLocation() throws IOException, URISyntaxException {
        //other db items w/ matching externalId should be reused (clients & locations)
        makeClientAndLocationNodes();
        Shipment template = makeShipmentTemplate();
        ExternalSyncResource instance = new ExternalSyncResource(graphDb);
        
        // compare number of nodes before and after  post:
        int nShipmentsBefore = countNodes(Labels.SHIPMENT);
        int nClientsBefore = countNodes(Labels.CLIENT);
        int nLocationsBefore = countNodes(Labels.LOCATION);
        
        Shipment result = instance.postShipment(template);
        
        int nShipmentsAfter = countNodes(Labels.SHIPMENT);
        int nClientsAfter = countNodes(Labels.CLIENT);
        int nLocationsAfter = countNodes(Labels.LOCATION);
        
        assertNotNull(result);

        assertEquals(nShipmentsBefore, 0);
        assertEquals(nShipmentsAfter, 1);

        assertEquals(nClientsBefore, 1);
        assertEquals(nClientsAfter, 1);

        assertEquals(nLocationsBefore, 1);
        assertEquals(nLocationsAfter, 2);
    }
    
    int countNodes(Label label) {
        AtomicInteger count = new AtomicInteger();
        try (Transaction tx = graphDb.beginTx()) {
            graphDb.findNodes(label).forEachRemaining((node) -> count.incrementAndGet());
        }
        return count.intValue();
    }
    
    Shipment makeShipmentTemplate() {
        LoadLocation loadLocation = LoadLocation.builder()
                .address("Plaza puerta del Sol")
                .externalId("MADR1")
                .city("Madrid")
                .country("ES")
                .latitude(40.4165597)
                .longitude(-3.7037327)
                .name("Plaza puerta del Sol")
                .postalcode("28009")
                .timeFrameFrom(LocalDateTime.of(2016, Month.MARCH, 13, 9, 0))
                .timeFrameTo(LocalDateTime.of(2016, Month.MARCH, 13, 17, 0))
                .build();

        Origin origin = Origin.builder()
                .address("Plaza puerta del Sol")
                .externalId("MADR1")
                .city("Madrid")
                .country("ES")
                .latitude(40.4165597)
                .longitude(-3.7037327)
                .name("Plaza puerta del Sol")
                .postalcode("28009")
                .build();

        UnloadLocation unloadLocation = UnloadLocation.builder()
                .address("Grote markt")
                .externalId("GRON1")
                .city("Groningen")
                .country("NL")
                .latitude(53.218635)
                .longitude(6.566443)
                .name("Grote markt")
                .postalcode("9712")
                .timeFrameFrom(LocalDateTime.of(2016, Month.MARCH, 23, 9, 0))
                .timeFrameTo(LocalDateTime.of(2016, Month.MARCH, 23, 17, 0))
                .build();

        Destination destination = Destination.builder()
                .address("Grote markt")
                .externalId("GRON1")
                .city("Groningen")
                .country("NL")
                .latitude(53.218635)
                .longitude(6.566443)
                .name("Grote markt")
                .postalcode("9712")
                .build();

        Client client = Client.builder()
                .externalId("C123456")
                .name("a client")
                .debtorNumber("D123456")
                .build();
        
        Debitor debitor = Debitor.builder()
                .externalId("C123456")
                .currency("EUR")
                .name("a client")
                .price(2.2)
                .build();

        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline1 desc")
                .remarks("orderline1 remark")
                .paymentOnDeliveryAmount(1.1)
                .type(OrderLineType.CUSTOM)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(3.3)
                .build());

        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline2 desc")
                .remarks(null)
                .paymentOnDeliveryAmount(4.4)
                .type(OrderLineType.CUSTOM)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(6.6)
                .build());

        Shipment template = Shipment.builder()
                .deadline(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0))
                .loadLocation(loadLocation)
                .shippedUnits(2)
                .unloadLocation(unloadLocation)
                .order(Order.builder()
                        .debitor(debitor)
                        .destination(destination)
                        .externalId("ORDER1")
                        .lines(lines)
                        .name("")
                        .orderedBy(client)
                        .origin(origin)
                        .build())
                .build();
        
        return template;
    }
    
    void makeClientAndLocationNodes() {
        //create minimal client and location nodes, with externalIds matching those in the posted template
        try (Transaction tx = graphDb.beginTx()) {
            Node client = graphDb.createNode(Labels.CLIENT);
            client.setProperty(Client.PROP_ID, "1");
            client.setProperty(Client.PROP_EXTERNAL_ID, "C123456");
            
            Node loc1 = graphDb.createNode(Labels.LOCATION);
            loc1.setProperty(Location.PROP_ID, "1001");
            loc1.setProperty(Location.PROP_EXTERNAL_ID, "MADR1");
            
            client.createRelationshipTo(loc1, Relations.RESIDES);
           
            // let postShipment create the other end, so we can see a change in the node count
//            Node loc2 = graphDb.createNode(Labels.LOCATION);
//            loc2.setProperty(Location.PROP_ID, "1002");
//            loc2.setProperty(Location.PROP_EXTERNAL_ID, "GRON1");
            
            tx.success();
        }
    }
    
    
}
