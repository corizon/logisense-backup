/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Location;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.Times;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class LocationFactoryNGTest extends BaseObjectFactoryTest {

    public LocationFactoryNGTest() {
    }

    /**
     * Test of fromNode method, of class LocationFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");

        String city = "Amsterdam";
        String country = "NL";
        String id = "123";
        Double latitude = 4.5678909;
        Double longitude = 5.6423567;
        String name = "Ergens in Amsterdam";
        String postalcode = "1234AA";
        String address = "Ergensweg";
        String phone = "+31123456789";
        String email = "example@example.com";
        ApprovalStatus status = ApprovalStatus.NEW;
        String externalId = "1";

        when(node.getProperty(Location.PROP_CITY)).thenReturn(city);
        when(node.getProperty(Location.PROP_COUNTRY)).thenReturn(country);
        when(node.getProperty(Location.PROP_ID)).thenReturn(id);
        when(node.getProperty(Location.PROP_LATITUDE)).thenReturn(latitude);
        when(node.getProperty(Location.PROP_LONGITUDE)).thenReturn(longitude);
        when(node.getProperty(Location.PROP_NAME)).thenReturn(name);
        when(node.getProperty(Location.PROP_POSTALCODE)).thenReturn(postalcode);
        when(node.getProperty(Location.PROP_ADDRESS)).thenReturn(address);
        when(node.getProperty(Location.PROP_PHONE)).thenReturn(phone);
        when(node.getProperty(Location.PROP_EMAIL)).thenReturn(email);
        when(node.getProperty(Location.PROP_STATUS)).thenReturn(status.name());
        when(node.getProperty(Location.PROP_EXTERNAL_ID)).thenReturn(externalId);

        Location result = LocationFactory.fromNode(node);
        assertEquals(result.getCity(), city);
        assertEquals(result.getCountry(), country);
        assertEquals(result.getId(), id);
        assertEquals(result.getLatitude(), latitude);
        assertEquals(result.getLongitude(), longitude);
        assertEquals(result.getName(), name);
        assertEquals(result.getPhone(), phone);
        assertEquals(result.getEmail(), email);        
        assertEquals(result.getPostalcode(), postalcode);
        assertEquals(result.getAddress(), address);
        assertEquals(result.getStatus(), status);
        assertEquals(result.getExternalId(), externalId);

        verify(databaseService, new Times(14)).beginTx();
        verify(transaction, new Times(14)).success();
    }

}
