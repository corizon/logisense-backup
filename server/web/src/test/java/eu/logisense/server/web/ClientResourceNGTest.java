/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web;

import eu.logisense.api.Client;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Location;
import eu.logisense.api.Status;
import eu.logisense.server.web.model.ClientFactory;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.WebApplicationException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ClientResourceNGTest {

    private static final Logger log = LoggerFactory.getLogger(ClientResourceNGTest.class);

    public ClientResourceNGTest() {
    }

    private GraphDatabaseService graphDb;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    Location location;

    @BeforeMethod
    public void setUpMethod() throws Exception {
//        graphDb = new RestGraphDatabase("http://localhost:7474");
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
        Util.initTestSet(graphDb);

        location = Location.buildLocation()
                .latitude(53.218635)
                .longitude(6.566443)
                .build();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }

    /**
     * Test of getClients method, of class ClientResource.
     */
    @Test
    public void testGetClients() throws IOException, URISyntaxException {
        System.out.println("getClients");
        String id = "1";
        List<Location> locations = new ArrayList<>();
        locations.add(Location.buildLocation()
                .address("address " + id)
                .city("city " + id)
                .country("country " + id)
                .id("1")
                .latitude(12.34)
                .longitude(56.78)
                .name("name " + id)
                .postalcode("postalcode " + id)
                .build());
        Client template = Client.builder()
                .href(ClientFactory.toHref(id))
                .id(id)
                .name("client " + id)
                .bicCode("12345678901")
                .chamberOfCommerce("")
                .chamberOfCommerceCity("")
                .chargeVat(Boolean.TRUE)
                .creditorNumber("")
                .currency("")
                .debtorNumber("")
                .externalId("1")
                .ibanCode("12345678901234567890")
                .status(ApprovalStatus.NEW)
                .vatNumber("")
                .locations(locations)
                .build();

        ClientResource instance = new ClientResource(graphDb);

        Client returnClient = instance.postClient(template);

        List<Client> result = instance.getClients();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(result.size(), 5);
        result.stream().forEach((client) -> {
            assertNotNull(client.getId());
            assertNotNull(client.getName());
            assertNotNull(client.getHref());
            assertEquals(client.getHref(), ClientFactory.toHref(client.getId()));
            assertNotNull(client.getLocations());
            client.getLocations().stream().forEach((loc) -> {
                assertNotNull(loc.getId());
            });
        });
    }

    /**
     * Test of getClient method, of class ClientResource.
     */
    @Test
    public void testGetClient() throws IOException, URISyntaxException {
        System.out.println("getClient");

        String id = "1";
        List<Location> locations = new ArrayList<>();
        locations.add(Location.buildLocation()
                .address("address " + id)
                .city("city " + id)
                .country("country " + id)
                .id("1")
                .latitude(12.34)
                .longitude(56.78)
                .name("name " + id)
                .postalcode("postalcode " + id)
                .build());
        Client template = Client.builder()
                .href(ClientFactory.toHref(id))
                .id(id)
                .name("client " + id)
                .bicCode("12345678901")
                .chamberOfCommerce("")
                .chamberOfCommerceCity("")
                .chargeVat(Boolean.TRUE)
                .creditorNumber("")
                .currency("")
                .debtorNumber("")
                .externalId("1")
                .ibanCode("12345678901")
                .status(ApprovalStatus.NEW)
                .vatNumber("")
                .locations(locations)
                .build();

        ClientResource instance = new ClientResource(graphDb);

        Client returnClient = instance.postClient(template);

        Client client = instance.getClient(returnClient.getId());
        assertNotNull(client.getId());
        assertNotNull(client.getName());
        assertNotNull(client.getHref());
        assertEquals(client.getHref(), ClientFactory.toHref(client.getId()));
        assertEquals(client.getId(), "1");
        assertEquals(client.getName(), "client 1");
        assertEquals(client.getExternalId(), "1");
        assertNotNull(client.getLocations());
        client.getLocations().stream().forEach((loc) -> {
            assertNotNull(loc.getId());
        });
        assertEquals(client.getStatus(), ApprovalStatus.NEW);
    }

    @Test
    public void testGetApprovedClient() throws IOException, URISyntaxException {
        System.out.println("getClient");

        String id = "1";
        List<Location> locations = new ArrayList<>();
        locations.add(Location.buildLocation()
                .address("address " + id)
                .city("city " + id)
                .country("country " + id)
                .id("1")
                .latitude(12.34)
                .longitude(56.78)
                .name("name " + id)
                .postalcode("postalcode " + id)
                .build());
        Client template = Client.builder()
                .href(ClientFactory.toHref(id))
                .id(id)
                .name("client " + id)
                .bicCode("12345678901")
                .chamberOfCommerce("")
                .chamberOfCommerceCity("")
                .chargeVat(Boolean.TRUE)
                .creditorNumber("")
                .currency("")
                .debtorNumber("")
                .externalId("")
                .ibanCode("12345678901234567890")
                .vatNumber("")
                .locations(locations)
                .status(ApprovalStatus.APPROVED)
                .build();

        ClientResource instance = new ClientResource(graphDb);

        Client returnClient = instance.postClient(template);

        Client client = instance.getClient(returnClient.getId());
        assertNotNull(client.getId());
        assertNotNull(client.getName());
        assertNotNull(client.getHref());
        assertEquals(client.getHref(), ClientFactory.toHref(client.getId()));
        assertEquals(client.getId(), "1");
        assertEquals(client.getName(), "client 1");
        assertNotNull(client.getLocations());
        client.getLocations().stream().forEach((loc) -> {
            assertNotNull(loc.getId());
        });
        assertEquals(client.getStatus(), ApprovalStatus.APPROVED);
    }

    /**
     * Test of postClient method, of class ClientResource.
     */
    @Test
    public void testPostClient() throws IOException, URISyntaxException {
        log.info("testPostClient");
        log.info("Prepare Scenario");
        String id = "1234";
        List<Location> locations = new ArrayList<>();
        locations.add(Location.buildLocation()
                .address("address " + id)
                .city("city " + id)
                .country("country " + id)
                .latitude(12.34)
                .longitude(56.78)
                .name("name " + id)
                .postalcode("postalcode " + id)
                .build());
        Client template = Client.builder()
                .href(ClientFactory.toHref(id))
                .name("client " + id)
                .bicCode("12345678901")
                .chamberOfCommerce("")
                .chamberOfCommerceCity("")
                .chargeVat(Boolean.TRUE)
                .creditorNumber("")
                .currency("")
                .debtorNumber("")
                .externalId("")
                .ibanCode("")
                .status(ApprovalStatus.NEW)
                .vatNumber("")
                .locations(locations)
                .build();
        ClientResource instance = new ClientResource(graphDb);
        log.info("Execute test");
        Client client = instance.postClient(template);
        log.info("Check result");
        assertNotNull(client.getId());
        assertNotNull(client.getName());
        assertNotNull(client.getHref());
        assertEquals(client.getStatus(), ApprovalStatus.NEW);
        assertEquals(client.getHref(), ClientFactory.toHref(client.getId()));
        assertEquals(client.getName(), "client " + id);
        assertNotNull(client.getLocations());
        assertEquals(client.getLocations().get(0).getAddress(), "address " + id);
        assertEquals(client.getLocations().get(0).getCity(), "city " + id);
        assertEquals(client.getLocations().get(0).getCountry(), "country " + id);
        assertEquals(client.getLocations().get(0).getLatitude(), 12.34);
        assertEquals(client.getLocations().get(0).getLongitude(), 56.78);
        assertEquals(client.getLocations().get(0).getName(), "name " + id);
        assertEquals(client.getLocations().get(0).getPostalcode(), "postalcode " + id);
    }

    @Test
    public void testPostClientWithoutLocations() throws IOException, URISyntaxException {
        log.info("testPostClientWithoutLocations");
        log.info("Prepare Scenario");
        String id = "1234";

        Client template = Client.builder()
                .href(ClientFactory.toHref(id))
                .name("client " + id)
                .bicCode("12345678901")
                .chamberOfCommerce("")
                .chamberOfCommerceCity("")
                .chargeVat(Boolean.TRUE)
                .creditorNumber("")
                .currency("")
                .debtorNumber("")
                .externalId("")
                .ibanCode("")
                .status(ApprovalStatus.NEW)
                .vatNumber("")
                .locations(null)
                .build();
        ClientResource instance = new ClientResource(graphDb);
        log.info("Execute test");

        boolean exceptionThrown = false;
        try {
            Client client = instance.postClient(template);
        } catch (WebApplicationException e) {
            log.info("Check result");
            exceptionThrown = true;
        }
        if (!exceptionThrown) {
            fail("expected exception not thrown");
        }

    }

    /**
     * Test of postClient method, of class ClientResource.
     */
    @Test
    public void testPostAndGetClient() throws IOException, URISyntaxException {
        System.out.println("postClientAndGet");
        String id = "1234";
        List<Location> locations = new ArrayList<>();
        locations.add(Location.buildLocation()
                .address("address " + id)
                .city("city " + id)
                .country("country " + id)
                .latitude(12.34)
                .longitude(56.78)
                .name("name " + id)
                .postalcode("postalcode " + id)
                .build());
        Client template = Client.builder()
                .href(ClientFactory.toHref(id))
                .name("client " + id)
                .bicCode("12345678901")
                .chamberOfCommerce("")
                .chamberOfCommerceCity("")
                .chargeVat(Boolean.TRUE)
                .creditorNumber("")
                .currency("")
                .debtorNumber("")
                .externalId("")
                .ibanCode("")
                .status(ApprovalStatus.NEW)
                .vatNumber("")
                .locations(locations)
                .build();
        ClientResource instance = new ClientResource(graphDb);
        Client c = instance.postClient(template);
        Client client = instance.getClient(c.getId());

        assertNotNull(client.getId());
        assertNotNull(client.getName());
        assertNotNull(client.getHref());
        assertEquals(client.getHref(), ClientFactory.toHref(client.getId()));
        assertEquals(client.getName(), "client " + id);
        assertNotNull(client.getLocations());
        assertEquals(client.getLocations().get(0).getAddress(), "address " + id);
        assertEquals(client.getLocations().get(0).getCity(), "city " + id);
        assertEquals(client.getLocations().get(0).getCountry(), "country " + id);
        assertEquals(client.getLocations().get(0).getLatitude(), 12.34);
        assertEquals(client.getLocations().get(0).getLongitude(), 56.78);
        assertEquals(client.getLocations().get(0).getName(), "name " + id);
        assertEquals(client.getLocations().get(0).getPostalcode(), "postalcode " + id);
    }

    /**
     * Test of updateClient method, of class ClientResource.
     */
    @Test
    public void testUpdateClient() throws IOException, URISyntaxException {
        System.out.println("updateClient");
        ClientResource instance = new ClientResource(graphDb);
        Client template = instance.getClient("1");

        String bic = "klhflaehgil";
        String chamberOfCommerce = "dlgbjsdgbo";
        String chamberOfCommerceCity = "sdgbjsgbsrgbsgb";
        boolean chargeVat = false;
        String creditorNumber = "sdgbsrtbhsr";
        String currency = "ABC";
        String debitornumber = "4646464";
        String externalId = "asdfs645fsa6df4as6d84f6asd4f";
        String ibanCode = "asdfs5adf4646";
        String vatNumber = "as65df4a6sdf46as4df6sd";
        Status status = ApprovalStatus.APPROVED;

        template.setBicCode(bic);
        template.setChamberOfCommerce(chamberOfCommerce);
        template.setChamberOfCommerceCity(chamberOfCommerceCity);
        template.setChargeVat(chargeVat);
        template.setCreditorNumber(creditorNumber);
        template.setCurrency(currency);
        template.setDebtorNumber(debitornumber);
        template.setExternalId(externalId);
        template.setIbanCode(ibanCode);
        template.setStatus(status);
        template.setVatNumber(vatNumber);

        Client result = instance.updateClient("1", template);

        assertNotNull(result);
        assertEquals(result.getBicCode(), bic);
        assertEquals(result.getChamberOfCommerce(), chamberOfCommerce);
        assertEquals(result.getChamberOfCommerceCity(), chamberOfCommerceCity);
        assertEquals(result.getChargeVat().booleanValue(), chargeVat);
        assertEquals(result.getCreditorNumber(), creditorNumber);
        assertEquals(result.getCurrency(), currency);
        assertEquals(result.getDebtorNumber(), debitornumber);
        assertEquals(result.getExternalId(), externalId);
        assertEquals(result.getIbanCode(), ibanCode);
        assertEquals(result.getStatus(), status);
        assertEquals(result.getVatNumber(), vatNumber);
    }

    /**
     * Test of updateClient method, of class ClientResource checking if the Locations relationship is duplicated
     */
    @Test
    public void testUpdateClientCheckingLocations() throws IOException, URISyntaxException {
        System.out.println("updateClient");
        ClientResource instance = new ClientResource(graphDb);
        Client template = instance.getClient("1");

        String bic = "klhflaehgil";
        String chamberOfCommerce = "dlgbjsdgbo";
        String chamberOfCommerceCity = "sdgbjsgbsrgbsgb";
        boolean chargeVat = false;
        String creditorNumber = "sdgbsrtbhsr";
        String currency = "ABC";
        String debitornumber = "4646464";
        String externalId = "asdfs645fsa6df4as6d84f6asd4f";
        String ibanCode = "asdfs5adf4646";
        String vatNumber = "as65df4a6sdf46as4df6sd";
        Status status = ApprovalStatus.APPROVED;

        template.setBicCode(bic);
        template.setChamberOfCommerce(chamberOfCommerce);
        template.setChamberOfCommerceCity(chamberOfCommerceCity);
        template.setChargeVat(chargeVat);
        template.setCreditorNumber(creditorNumber);
        template.setCurrency(currency);
        template.setDebtorNumber(debitornumber);
        template.setExternalId(externalId);
        template.setIbanCode(ibanCode);
        template.setStatus(status);
        template.setVatNumber(vatNumber);

        Client result = instance.updateClient("1", template);

        assertNotNull(result);
        assertEquals(result.getBicCode(), bic);
        assertEquals(result.getChamberOfCommerce(), chamberOfCommerce);
        assertEquals(result.getChamberOfCommerceCity(), chamberOfCommerceCity);
        assertEquals(result.getChargeVat().booleanValue(), chargeVat);
        assertEquals(result.getCreditorNumber(), creditorNumber);
        assertEquals(result.getCurrency(), currency);
        assertEquals(result.getDebtorNumber(), debitornumber);
        assertEquals(result.getExternalId(), externalId);
        assertEquals(result.getIbanCode(), ibanCode);
        assertEquals(result.getStatus(), status);
        assertEquals(result.getVatNumber(), vatNumber);
        assertTrue(result.getLocations().size() == 1);
    }

    /**
     * Test of updateClient method, of class ClientResource checking if the Locations relationship is duplicated
     */
    @Test
    public void testUpdateClientRemovingLocations() throws IOException, URISyntaxException {
        System.out.println("updateClient");
        ClientResource instance = new ClientResource(graphDb);
        Client template = instance.getClient("1");

        String bic = "klhflaehgil";
        String chamberOfCommerce = "dlgbjsdgbo";
        String chamberOfCommerceCity = "sdgbjsgbsrgbsgb";
        boolean chargeVat = false;
        String creditorNumber = "sdgbsrtbhsr";
        String currency = "ABC";
        String debitornumber = "4646464";
        String externalId = "asdfs645fsa6df4as6d84f6asd4f";
        String ibanCode = "asdfs5adf4646";
        String vatNumber = "as65df4a6sdf46as4df6sd";
        Status status = ApprovalStatus.APPROVED;

        template.setBicCode(bic);
        template.setChamberOfCommerce(chamberOfCommerce);
        template.setChamberOfCommerceCity(chamberOfCommerceCity);
        template.setChargeVat(chargeVat);
        template.setCreditorNumber(creditorNumber);
        template.setCurrency(currency);
        template.setDebtorNumber(debitornumber);
        template.setExternalId(externalId);
        template.setIbanCode(ibanCode);
        template.setStatus(status);
        template.setVatNumber(vatNumber);
        template.setLocations(null);

        Client result = instance.updateClient("1", template);

        assertNotNull(result);
        assertEquals(result.getBicCode(), bic);
        assertEquals(result.getChamberOfCommerce(), chamberOfCommerce);
        assertEquals(result.getChamberOfCommerceCity(), chamberOfCommerceCity);
        assertEquals(result.getChargeVat().booleanValue(), chargeVat);
        assertEquals(result.getCreditorNumber(), creditorNumber);
        assertEquals(result.getCurrency(), currency);
        assertEquals(result.getDebtorNumber(), debitornumber);
        assertEquals(result.getExternalId(), externalId);
        assertEquals(result.getIbanCode(), ibanCode);
        assertEquals(result.getStatus(), status);
        assertEquals(result.getVatNumber(), vatNumber);
        assertTrue(result.getLocations().size() == 0);
    }
}
