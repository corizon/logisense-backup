/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web;

import au.com.bytecode.opencsv.CSVReader;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Order;
import eu.logisense.api.Ride;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author johan
 */
public class CSVExportResourceNGTest {
    private GraphDatabaseService graphDb;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @BeforeMethod
    public void setUpMethod() throws Exception {
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }
    
    @Test
    public void testExportShipments() throws IOException, URISyntaxException {
        Util.initTestSet(graphDb);
        
        CSVExportResource instance = new CSVExportResource(graphDb);
        String csv = instance.exportShipments(null, null);
        
        List<String[]> lines = readCSV(csv);
        assertEquals(lines.size(), 2);
        assertEquals(lines.get(0), CSVExportResource.SHIPMENTS_HEADER);
        assertEquals(lines.get(1).length, 4);
    }
    
    @Test
    public void testExportRides()  throws IOException, URISyntaxException {
        Util.initTestSet(graphDb);
        
        CSVExportResource instance = new CSVExportResource(graphDb);
        String csv = instance.exportRides(null, null);
        
        List<String[]> lines = readCSV(csv);
        assertEquals(lines.size(), 2);
        assertEquals(lines.get(0), CSVExportResource.RIDES_HEADER);
        assertEquals(lines.get(1).length, 3);
    }
    
    @Test
    public void testDumpShipments() throws IOException, URISyntaxException {
        Order o1 = Order.builder().id("O1").externalId("EXT1").build();
        Shipment s1 = Shipment.builder().id("S1").status(TransportStatus.PLANNED).order(o1).build();
        Order o2 = Order.builder().id("O2").externalId("EXT2").build();
        Shipment s2 = Shipment.builder().id("S2").status(TransportStatus.NEW).order(o2).build();
        
        CSVExportResource instance = new CSVExportResource(graphDb);
        String csv = instance.dumpShipments(Arrays.asList(s1, s2));
        
        List<String[]> lines = readCSV(csv);
        assertEquals(lines.size(), 3);
        assertEquals(lines.get(0), CSVExportResource.SHIPMENTS_HEADER);
        assertEquals(lines.get(1), new String[]{s1.getId(), o1.getId(), s1.getStatus().name(), o1.getExternalId()});
        assertEquals(lines.get(2), new String[]{s2.getId(), o2.getId(), s2.getStatus().name(), o2.getExternalId()});
    }
    
    @Test
    public void testDumpRides() throws IOException, URISyntaxException {
        Ride r1 = Ride.builder().id("1").status(ApprovalStatus.APPROVED).externalId("EXT1").build();
        Ride r2 = Ride.builder().id("2").status(ApprovalStatus.APPROVED).externalId("EXT2").build();
        
        CSVExportResource instance = new CSVExportResource(graphDb);
        String csv = instance.dumpRides(Arrays.asList(r1, r2));
        
        List<String[]> lines = readCSV(csv);
        assertEquals(lines.size(), 3);
        assertEquals(lines.get(0), CSVExportResource.RIDES_HEADER);
        assertEquals(lines.get(1), new String[]{r1.getId(), r1.getStatus().name(), r1.getExternalId()});
        assertEquals(lines.get(2), new String[]{r2.getId(), r2.getStatus().name(), r2.getExternalId()});
    }
    
    private List<String[]> readCSV(String csv) throws IOException {
        try (CSVReader r = new CSVReader(new StringReader(csv))) {
            return r.readAll();
        }
    }
}
