/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;


import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.UnloadLocation;
import java.time.LocalDateTime;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.Times;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class UnloadLocationFactoryNGTest  extends BaseObjectFactoryTest{
    
    public UnloadLocationFactoryNGTest() {
    }


    /**
     * Test of fromNode method, of class unloadLocationFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");
        String city = "Amsterdam";
        String country = "NL";
        Double latitude = 4.5678909;
        Double longitude = 5.6423567;
        String name = "Ergens in Amsterdam";
        String postalcode = "1234AA";
        String address = "Ergensweg";
        ApprovalStatus status = ApprovalStatus.NEW;
        String externalId = "1";
        LocalDateTime timeFrameFrom = LocalDateTime.now().minusMinutes(5);
        LocalDateTime timeFrameTo = LocalDateTime.now().plusMinutes(5);

        when(node.getProperty(UnloadLocation.PROP_CITY)).thenReturn(city);
        when(node.getProperty(UnloadLocation.PROP_ADDRESS)).thenReturn(address);
        when(node.getProperty(UnloadLocation.PROP_COUNTRY)).thenReturn(country);
        when(node.getProperty(UnloadLocation.PROP_LATITUDE)).thenReturn(latitude);
        when(node.getProperty(UnloadLocation.PROP_LONGITUDE)).thenReturn(longitude);
        when(node.getProperty(UnloadLocation.PROP_NAME)).thenReturn(name);
        when(node.getProperty(UnloadLocation.PROP_POSTALCODE)).thenReturn(postalcode);
        when(node.getProperty(UnloadLocation.PROP_STATUS)).thenReturn(status.name());
        when(node.getProperty(UnloadLocation.PROP_EXTERNAL_ID)).thenReturn(externalId);
        when(relationship.getProperty(UnloadLocation.PROP_TIME_FRAME_FROM)).thenReturn(ObjectFactory.toString(timeFrameFrom));
        when(relationship.getProperty(UnloadLocation.PROP_TIME_FRAME_TO)).thenReturn(ObjectFactory.toString(timeFrameTo));

        UnloadLocation result = UnloadLocationFactory.fromNode(node, relationship);
        assertEquals(result.getCity(), city);
        assertEquals(result.getAddress(), address);
        assertEquals(result.getCountry(), country);
        assertEquals(result.getLatitude(), latitude);
        assertEquals(result.getLongitude(), longitude);
        assertEquals(result.getName(), name);
        assertEquals(result.getPostalcode(), postalcode);
        assertEquals(result.getStatus(), status);
        assertEquals(result.getExternalId(), externalId);
        assertEquals(result.getTimeFrameFrom(), timeFrameFrom);
        assertEquals(result.getTimeFrameTo(), timeFrameTo);

        verify(databaseService, new Times(15)).beginTx();
        verify(transaction, new Times(15)).success();
    }
    
}
