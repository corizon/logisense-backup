/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web;

import eu.logisense.api.*;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author perezdf
 */
public class RideResourceNGTest {

    private static final Logger logger = LoggerFactory.getLogger(RideResourceNGTest.class);
    private GraphDatabaseService graphDb;

    public RideResourceNGTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @BeforeClass
    public void setUp() {
    }

    @AfterClass
    public void tearDown() {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
        Util.initTestSet(graphDb);
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }

    /**
     * Test of getRides method, of class RideResource.
     */
    @Test
    public void testGetRides() throws IOException, URISyntaxException {
        logger.info("getRides");
        RideResource instance = new RideResource(graphDb);

        logger.info("Execute test");
        List<Ride> rides = instance.getRides();

        assertNotNull(rides);
        assertEquals(rides.size(), 2);
        rides.stream().forEach((ride) -> {
        });
    }

    /**
     * Test of getRide method, of class RideResource.
     */
    @Test
    public void testGetRide() throws IOException, URISyntaxException {
        logger.info("getRide");
        RideResource instance = new RideResource(graphDb);
        String id = "1";

        logger.info("Execute test");
        Ride ride = instance.getRide(id);

        logger.info("Check result");

        assertNotNull(ride);
        assertNotNull(ride.getId());
        assertNotNull(ride.getContainers());
        assertNotNull(ride.getCreditor());
        assertNotNull(ride.getStatus());
        assertNotNull(ride.getName());
        assertNotNull(ride.getHref());
    }

    /**
     * Test of postRide method, of class RideResource.
     */
    @Test
    public void testPostRide() throws IOException, URISyntaxException {
        logger.info("postRide");
        logger.info("Prepare Scenario");
        String licensePlate = UUID.randomUUID().toString();

        Ride template = Ride.builder().name("name1").href("href").licensePlate(licensePlate).build();
        RideResource instance = new RideResource(graphDb);
        logger.info("Execute test");

        Ride result = instance.postRide(template);
        logger.info("Check result");

        assertEquals(licensePlate, result.getLicensePlate());
        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());
    }

    /**
     * Test of postRide method, of class RideResource.
     */
    @Test
    public void testPostRideWithNoExistingContainer() throws IOException, URISyntaxException {
        logger.info("postRide");
        logger.info("Prepare Scenario");
        Ride template = Ride.builder().name("name1").href("href").containers(new ArrayList<>()).build();

        Container container = Container.builder().name("container name").build();

        template.getContainers().add(container);

        RideResource instance = new RideResource(graphDb);
        logger.info("Execute test");

        Ride result = instance.postRide(template);
        logger.info("Check result");

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());
    }

    @Test
    public void testPostRideWithNoExistingContainerNoExistingShipment() throws IOException, URISyntaxException {
        logger.info("postRide");
        logger.info("Prepare Scenario");
        Ride template = Ride.builder().name("name1").href("href").containers(new ArrayList<>()).build();

        Container container = Container.builder().name("container name").build();

        LoadLocation madrid = LoadLocation.builder()
                .id("1000")
                .city("Madrid")
                .country("ES")
                .latitude(40.4165597)
                .longitude(-3.7037327)
                .name("Plaza puerta del Sol")
                .postalcode("28009")
                .timeFrameFrom(LocalDateTime.now())
                .timeFrameTo(LocalDateTime.now())
                .build();

        UnloadLocation groningen = UnloadLocation.builder()
                .id("1001")
                .city("Groningen")
                .country("NL")
                .latitude(53.218635)
                .longitude(6.566443)
                .name("Grote markt")
                .postalcode("9712")
                .timeFrameFrom(LocalDateTime.now())
                .timeFrameTo(LocalDateTime.now())
                .build();

        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline1 desc")
                .remarks("orderline1 remark")
                .stackable(Boolean.TRUE)
                .id("1")
                .name("orderline1")
                .paymentOnDeliveryAmount(1.1)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.CUSTOM)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(3.3)
                .build());

        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline2 desc")
                .remarks("orderline2 remark")
                .stackable(Boolean.FALSE)
                .id("2")
                .name("orderline2")
                .paymentOnDeliveryAmount(4.4)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.EURO_PALLET)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(6.6)
                .build());

        Shipment shipment = Shipment.builder()
                .deadline(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0))
                .loadLocation(madrid)
                .name("madrid to groningen testing complete ride")
                .note("some notes")
                .status(TransportStatus.NEW)
                .shippedUnits(2)
                .unloadLocation(groningen)
                .senderIsLoad(Boolean.TRUE)
                .destinationIsUnload(Boolean.FALSE)
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .externalId("123456")
                        .build())
                .build();

        ShipmentResource shipmentResource = new ShipmentResource(graphDb);
        Shipment returnShipment = shipmentResource.postShipment(shipment);

        List<Resource> shipmentLinkList = new ArrayList<>();
        shipmentLinkList.add(returnShipment);

        container.setShipments(shipmentLinkList);

        template.getContainers().add(container);

        RideResource instance = new RideResource(graphDb);
        logger.info("Execute test");

        Ride result = instance.postRide(template);
        logger.info("Check result");

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());
        assertNotNull(result.getContainers());
        assertNotNull(result.getContainers().get(0).getShipments().get(0));
    }

    /**
     * Test of updateRide method, of class RideResource.
     */
    @Test
    public void testUpdateRide() throws IOException, URISyntaxException {
        logger.info("updateRide");
        RideResource instance = new RideResource(graphDb);

        String id = "1";

        Ride org = instance.getRide(id);
        assertNotNull(org);
        String unique2String = UUID.randomUUID().toString();
        org.setName(unique2String);
        org.setStatus(TransportStatus.NEW);
        org.setExternalId("123456");
        LocalDateTime ldt = LocalDateTime.of(2016, 12, 31, 23, 59);
        org.setEtd(ldt);
        org.setLicensePlate("11-AA-11");
        org.setMargin(0.5);
        logger.info("Execute test");
        Ride updated = instance.updateRide(org);

        logger.info("Check result");
        assertNotNull(updated);
        assertEquals(unique2String, updated.getName());
        assertEquals(TransportStatus.NEW, updated.getStatus());
        assertEquals("123456", updated.getExternalId());
//        assertEquals(ldt, updated.getEtd());
        assertEquals("11-AA-11", updated.getLicensePlate());
//        assertEquals(0.5, updated.getMargin());

    }

}
