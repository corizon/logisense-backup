/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.integration;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.not;
import org.testng.annotations.Test;


/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
//@RunWith(Arquillian.class)
public class VehicleResourceIT /*extends Arquillian*/ {
//
//    @Deployment(testable = false)
//    public static Archive createDeployment() {
//
//        JAXRSArchive deployment = ShrinkWrap.create(JAXRSArchive.class)
//                .addAsLibraries(Maven.resolver().loadPomFromFile("pom.xml").importCompileAndRuntimeDependencies().resolve("eu.logisense:logisense-api").withTransitivity().asFile())
//                .addPackage(RootEndpoint.class.getPackage())
////                .addResource(RootEndpoint.class)
////                .addResource(ContainerResource.class)
////                .addResource(LogisenseApplication.class)
////                .addResource(OrderResource.class)
////                .addResource(RideResource.class)
////                .addResource(ShipmentResource.class)
////                .addResource(VehicleResource.class)
//                ;
//        return deployment;
//    }

//    @BeforeClass
//    public static void setUpClass() throws Exception {
//    }
//
//    @AfterClass
//    public static void tearDownClass() throws Exception {
//    }
//
//    @BeforeMethod
//    public void setUpMethod() throws Exception {
//    }
//
//    @AfterMethod
//    public void tearDownMethod() throws Exception {
//    }
//    @RunAsClient // Same as @Deployment(testable = false), should only be used in mixed mode
    
//    @Drone
//    WebDriver browser;
//
//    @Test
//    public void testIt() {
//        browser.navigate().to("http://localhost:8080/");
//        assertThat(browser.getPageSource()).contains("Howdy at ");
//    }    
//    
    
    
    @Test
    public void testSomething(){
                given()
                .contentType("application/json")
                // FIXME accept type is crucial here,
                .header("Accept", "application/json")
                //                .body("{'name': 'MyApp', 'description' : 'awesome app'}".replaceAll("'", "\""))
                .expect()
                .statusCode(200).body("id", is(not(nullValue())))
                .when()
                .get("http://localhost:8011/").asString();
                
    }
    
    
//    @Test(alwaysRun = true)
//    public void getCustomerById(@ArquillianResource URL baseURL) throws IOException {
//////        // Servlet is listening at <context_path>/User
//        final URL url = new URL(baseURL, "");
////        final User user = new User(1L, "Ike");
//
//        StringBuilder builder = new StringBuilder();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
//        String line;
//
//        while ((line = reader.readLine()) != null) {
//            builder.append(line);
//        }
//        reader.close();
//
//        assertEquals(builder.toString(), "kdkdkd");
//
//        given()
//                .contentType("application/json")
//                // FIXME accept type is crucial here,
//                .header("Accept", "application/json")
//                //                .body("{'name': 'MyApp', 'description' : 'awesome app'}".replaceAll("'", "\""))
//                .expect()
//                .statusCode(200).body("id", is(not(nullValue())))
//                .when()
//                .get(baseURL.toString() + "").asString();
//    }
}
