/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import eu.logisense.api.AcceptShipmentsCapability;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Client;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.OrderLineType;
import eu.logisense.api.Origin;
import eu.logisense.api.Shipment;
import eu.logisense.api.ShipmentService;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import eu.logisense.api.util.ClientUtil;
import eu.logisense.server.web.model.ObjectFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ShipmentResourceNGTest {

    private static final Logger log = LoggerFactory.getLogger(ShipmentResourceNGTest.class);

    public ShipmentResourceNGTest() {
    }

    private GraphDatabaseService graphDb;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    LoadLocation madrid;
    UnloadLocation groningen;
    Location location;

    @BeforeMethod
    public void setUpMethod() throws Exception {
//        graphDb = new RestGraphDatabase("http://localhost:7474");
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
        Util.initTestSet(graphDb);

        madrid = LoadLocation.builder()
                .id("1000")
                .city("Madrid")
                .country("ES")
                .latitude(40.4165597)
                .longitude(-3.7037327)
                .name("Plaza puerta del Sol")
                .postalcode("28009")
                .timeFrameFrom(LocalDateTime.now())
                .timeFrameTo(LocalDateTime.now())
                .build();

        groningen = UnloadLocation.builder()
                .id("1001")
                .city("Groningen")
                .country("NL")
                .latitude(53.218635)
                .longitude(6.566443)
                .name("Grote markt")
                .postalcode("9712")
                .timeFrameFrom(LocalDateTime.now())
                .timeFrameTo(LocalDateTime.now())
                .build();

        location = Location.buildLocation()
                .id("1002")
                .latitude(53.218635)
                .longitude(6.566443)
                .build();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }

    @Test
    public void testInitTestSet() throws IOException, URISyntaxException {
        System.out.println("initTestSet");

        ShipmentResource instance = new ShipmentResource(graphDb);

        instance.initTestSet();

    }

    @Test
    public void testInitOneCompleteRide() throws IOException, URISyntaxException {
        System.out.println("initOneCompleteRide");

        ShipmentResource instance = new ShipmentResource(graphDb);
        instance.initOneCompleteRide();

    }

    /**
     * Test of getShipments method, of class ShipmentResource.
     */
    @Test
    public void testGetShipments() throws IOException, URISyntaxException {
        System.out.println("getShipments");
        ShipmentResource instance = new ShipmentResource(graphDb);

        List<Shipment> result = instance.getShipments();
        assertNotNull(result);
        assertEquals(result.size(), 4);
        for (Shipment shipment : result) {
            assertNotNull(shipment);
            log.trace("checking shipment with id {}", shipment.getId());
            assertNotNull(shipment.getLoadLocation());
            assertNotNull(shipment.getUnloadLocation());

            if (!"4".equals(shipment.getId())) {
                assertEquals(shipment.getLoadLocation().getCity(), "Madrid");
                assertEquals(shipment.getLoadLocation().getCountry(), "ES");
                assertEquals(shipment.getLoadLocation().getLatitude(), 40.4165597);
                assertEquals(shipment.getLoadLocation().getLongitude(), -3.7037327);
                assertEquals(shipment.getLoadLocation().getName(), "Plaza puerta del Sol");
                assertEquals(shipment.getLoadLocation().getPostalcode(), "28009");
                assertEquals(shipment.getUnloadLocation().getCity(), "Groningen");
                assertEquals(shipment.getUnloadLocation().getCountry(), "NL");
                assertEquals(shipment.getUnloadLocation().getLatitude(), 53.218635);
                assertEquals(shipment.getUnloadLocation().getLongitude(), 6.566443);
                assertEquals(shipment.getUnloadLocation().getName(), "Grote markt");
                assertEquals(shipment.getUnloadLocation().getPostalcode(), "9712");
            } else {
                assertEquals(shipment.getLoadLocation().getCity(), "Groningen");
                assertEquals(shipment.getLoadLocation().getCountry(), "NL");
                assertEquals(shipment.getLoadLocation().getLatitude(), 53.218635);
                assertEquals(shipment.getLoadLocation().getLongitude(), 6.566443);
                assertEquals(shipment.getLoadLocation().getName(), "Grote markt");
                assertEquals(shipment.getLoadLocation().getPostalcode(), "9712");
                assertEquals(shipment.getUnloadLocation().getCity(), "Madrid");
                assertEquals(shipment.getUnloadLocation().getCountry(), "ES");
                assertEquals(shipment.getUnloadLocation().getLatitude(), 40.4165597);
                assertEquals(shipment.getUnloadLocation().getLongitude(), -3.7037327);
                assertEquals(shipment.getUnloadLocation().getName(), "Plaza puerta del Sol");
                assertEquals(shipment.getUnloadLocation().getPostalcode(), "28009");

            }

            if ("1".equals(shipment.getId())) {
                assertEquals(shipment.getLoadLocation().getTimeFrameFrom(), LocalDateTime.of(2016, Month.MARCH, 19, 8, 0));
                assertEquals(shipment.getLoadLocation().getTimeFrameTo(), LocalDateTime.of(2016, Month.MARCH, 19, 16, 0));
                assertEquals(shipment.getUnloadLocation().getTimeFrameFrom(), LocalDateTime.of(2016, Month.MARCH, 19, 8, 0));
                assertEquals(shipment.getUnloadLocation().getTimeFrameTo(), LocalDateTime.of(2016, Month.MARCH, 19, 16, 0));
                assertEquals(shipment.getIndex(), new Integer(1));
            } else if ("2".equals(shipment.getId())) {
                assertEquals(shipment.getLoadLocation().getTimeFrameFrom(), LocalDateTime.of(2016, Month.JUNE, 10, 8, 0));
                assertEquals(shipment.getLoadLocation().getTimeFrameTo(), LocalDateTime.of(2016, Month.JUNE, 10, 16, 0));
                assertEquals(shipment.getIndex(), new Integer(2));
                assertEquals(shipment.getUnloadLocation().getTimeFrameFrom(), LocalDateTime.of(2016, Month.JUNE, 10, 8, 0));
                assertEquals(shipment.getUnloadLocation().getTimeFrameTo(), LocalDateTime.of(2016, Month.JUNE, 10, 23, 59));
            }

        }
    }

    /**
     * Test of getShipment method, of class ShipmentResource.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetShipment() throws IOException, URISyntaxException {
        System.out.println("getShipment");
        String id = "1";
        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment unlinkedShipment = instance.getShipment("4");
        assertNotNull(unlinkedShipment);
        assertEquals(unlinkedShipment.getId(), "4");
        assertNull(unlinkedShipment.getIndex());
        assertNull(unlinkedShipment.getContainer());

        Shipment result = instance.getShipment(id);
        assertNotNull(result);
        assertEquals(result.getId(), id);
        assertNotNull(result.getContainer());
        assertEquals(result.getDeadline(), LocalDateTime.of(2016, Month.MARCH, 2, 0, 0));
        assertEquals(result.getHref(), "/shipments/1");
        assertEquals(result.getName(), "shipment 1");
        assertEquals(result.getNote(), "some note");
        assertEquals(result.getStatus(), TransportStatus.NEW);
        assertEquals(result.getShippedUnits(), (Integer) 4);

        assertEquals(result.getIndex(), new Integer(1));

        assertNotNull(result.getLoadLocation());
        assertEquals(result.getLoadLocation().getCity(), "Madrid");
        assertEquals(result.getLoadLocation().getCountry(), "ES");
        assertEquals(result.getLoadLocation().getLatitude(), 40.4165597);
        assertEquals(result.getLoadLocation().getTimeFrameFrom(), LocalDateTime.of(2016, Month.MARCH, 19, 8, 0));
        assertEquals(result.getLoadLocation().getTimeFrameTo(), LocalDateTime.of(2016, Month.MARCH, 19, 16, 0));
        assertEquals(result.getLoadLocation().getLongitude(), -3.7037327);
        assertEquals(result.getLoadLocation().getName(), "Plaza puerta del Sol");
        assertEquals(result.getLoadLocation().getPostalcode(), "28009");

        assertNotNull(result.getUnloadLocation());
        assertEquals(result.getUnloadLocation().getCity(), "Groningen");
        assertEquals(result.getUnloadLocation().getCountry(), "NL");
        assertEquals(result.getUnloadLocation().getLatitude(), 53.218635);
        assertEquals(result.getUnloadLocation().getTimeFrameFrom(), LocalDateTime.of(2016, Month.MARCH, 19, 8, 0));
        assertEquals(result.getUnloadLocation().getTimeFrameTo(), LocalDateTime.of(2016, Month.MARCH, 19, 16, 0));
        assertEquals(result.getUnloadLocation().getLongitude(), 6.566443);
        assertEquals(result.getUnloadLocation().getName(), "Grote markt");
        assertEquals(result.getUnloadLocation().getPostalcode(), "9712");

    }

    /**
     * Test pretends to check the management of timeframe information of the
     * shipment
     */
    @Test
    public void testTimeFrameInformationShipment() throws IOException, URISyntaxException {
        System.out.println("postShipment");

        madrid.setTimeFrameFrom(LocalDateTime.of(2016, Month.JANUARY, 1, 1, 0));
        madrid.setTimeFrameTo(LocalDateTime.of(2016, Month.JANUARY, 1, 2, 0));

        groningen.setTimeFrameFrom(LocalDateTime.of(2016, Month.FEBRUARY, 1, 1, 0));
        groningen.setTimeFrameTo(LocalDateTime.of(2016, Month.FEBRUARY, 1, 2, 0));

        List<OrderLine> lines = new ArrayList<>();

        Shipment template = Shipment.builder()
                .deadline(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0))
                .loadLocation(madrid)
                .name("madrid to groningen")
                .note("some notes")
                .status(TransportStatus.NEW)
                .shippedUnits(2)
                .unloadLocation(groningen)
                .senderIsLoad(Boolean.TRUE)
                .destinationIsUnload(Boolean.FALSE)
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .build();
        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment result = instance.postShipment(template);
        assertNotNull(result);
        assertEquals(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0), result.getDeadline());
        assertEquals(madrid.getId(), result.getLoadLocation().getId());
        assertEquals("madrid to groningen", result.getName());
        assertEquals(LocalDateTime.of(2016, Month.JANUARY, 1, 1, 0), result.getLoadLocation().getTimeFrameFrom());
        assertEquals(LocalDateTime.of(2016, Month.JANUARY, 1, 2, 0), result.getLoadLocation().getTimeFrameTo());
        assertEquals("some notes", result.getNote());
        assertEquals(TransportStatus.NEW, result.getStatus());
        assertEquals((Integer) 2, result.getShippedUnits());
        assertEquals(groningen.getId(), result.getUnloadLocation().getId());
        assertEquals(LocalDateTime.of(2016, Month.FEBRUARY, 1, 1, 0), result.getUnloadLocation().getTimeFrameFrom());
        assertEquals(LocalDateTime.of(2016, Month.FEBRUARY, 1, 2, 0), result.getUnloadLocation().getTimeFrameTo());
        assertEquals(Boolean.TRUE, result.getSenderIsLoad());
        assertEquals(Boolean.FALSE, result.getDestinationIsUnload());

    }

    /**
     * Test of postShipment method, of class ShipmentResource.
     */
    @Test
    public void testPostShipment() throws IOException, URISyntaxException {
        System.out.println("postShipment");

        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline1 desc")
                .remarks("orderline1 remark")
                .stackable(Boolean.TRUE)
                .id("1")
                .name("orderline1")
                .paymentOnDeliveryAmount(1.1)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.CUSTOM)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(3.3)
                .build());

        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline2 desc")
                .remarks("orderline2 remark")
                .stackable(Boolean.FALSE)
                .id("2")
                .name("orderline2")
                .paymentOnDeliveryAmount(4.4)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.EURO_PALLET)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(6.6)
                .build());

        Shipment template = Shipment.builder()
                .deadline(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0))
                .loadLocation(madrid)
                .name("madrid to groningen")
                .note("some notes")
                .status(TransportStatus.NEW)
                .shippedUnits(2)
                .unloadLocation(groningen)
                .senderIsLoad(Boolean.TRUE)
                .destinationIsUnload(Boolean.FALSE)
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .externalId("123456")
                        .build())
                .build();
        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment result = instance.postShipment(template);
        assertNotNull(result);
        assertEquals(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0), result.getDeadline());
        assertEquals(madrid.getId(), result.getLoadLocation().getId());
        assertEquals("madrid to groningen", result.getName());
        assertEquals("some notes", result.getNote());
        assertEquals(TransportStatus.NEW, result.getStatus());
        assertEquals("123456", result.getOrder().getExternalId());
        assertEquals((Integer) 2, result.getShippedUnits());
        assertEquals(groningen.getId(), result.getUnloadLocation().getId());
        assertNotNull(result.getOrder());
        assertNotNull(result.getOrder().getLines());
        assertEquals(Boolean.TRUE, result.getSenderIsLoad());
        assertEquals(Boolean.FALSE, result.getDestinationIsUnload());

    }

    /**
     * Test of postShipment method, of class ShipmentResource.
     */
    @Test
    public void testPostShipmentWithOrderWithoutName() throws IOException, URISyntaxException {
        System.out.println("postShipment");

        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline1 desc")
                .remarks("orderline1 remark")
                .stackable(Boolean.TRUE)
                .id("1")
                .name("orderline1")
                .paymentOnDeliveryAmount(1.1)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.CUSTOM)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(3.3)
                .build());

        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline2 desc")
                .remarks("orderline2 remark")
                .stackable(Boolean.FALSE)
                .id("2")
                .name("orderline2")
                .paymentOnDeliveryAmount(4.4)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.EURO_PALLET)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(6.6)
                .build());

        Shipment template = Shipment.builder()
                .deadline(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0))
                .loadLocation(madrid)
                .name("madrid to groningen")
                .note("some notes")
                .status(TransportStatus.NEW)
                .shippedUnits(2)
                .unloadLocation(groningen)
                .senderIsLoad(Boolean.TRUE)
                .destinationIsUnload(Boolean.FALSE)
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .externalId("123456")
                        .build())
                .build();
        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment result = instance.postShipment(template);
        assertNotNull(result);
        assertEquals(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0), result.getDeadline());
        assertEquals(madrid.getId(), result.getLoadLocation().getId());
        assertEquals("madrid to groningen", result.getName());
        assertEquals("some notes", result.getNote());
        assertEquals(TransportStatus.NEW, result.getStatus());
        assertEquals("123456", result.getOrder().getExternalId());
        assertEquals((Integer) 2, result.getShippedUnits());
        assertEquals(groningen.getId(), result.getUnloadLocation().getId());
        assertNotNull(result.getOrder());
        assertNotNull(result.getOrder().getLines());
        assertEquals(Boolean.TRUE, result.getSenderIsLoad());
        assertEquals(Boolean.FALSE, result.getDestinationIsUnload());

    }

    /**
     * Test of postShipment method, of class ShipmentResource.
     */
    @Test
    public void testPostAndGetShipment() throws IOException, URISyntaxException {
        System.out.println("testPostAndGetShipment");

        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline1 desc")
                .remarks("orderline1 remark")
                .stackable(Boolean.TRUE)
                .id("1")
                .name("orderline1")
                .paymentOnDeliveryAmount(1.1)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.CUSTOM)
                .swapPallets(Boolean.FALSE)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(3.3)
                .build());

        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline2 desc")
                .remarks("orderline2 remark")
                .stackable(Boolean.TRUE)
                .id("2")
                .name("orderline2")
                .paymentOnDeliveryAmount(4.4)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.EURO_PALLET)
                .swapPallets(Boolean.TRUE)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(6.6)
                .build());

        Shipment template = Shipment.builder()
                .deadline(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0))
                .loadLocation(madrid)
                .name("madrid to groningen")
                .note("some notes")
                .status(TransportStatus.NEW)
                .shippedUnits(2)
                .unloadLocation(groningen)
                .senderIsLoad(Boolean.TRUE)
                .destinationIsUnload(Boolean.FALSE)
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .build();
        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment post = instance.postShipment(template);
        assertNotNull(post);
        assertNotNull(post.getId());
        Shipment result = instance.getShipment(post.getId());

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0), result.getDeadline());
        assertEquals(madrid.getId(), result.getLoadLocation().getId());
        assertEquals("madrid to groningen", result.getName());
        assertEquals("some notes", result.getNote());
        assertEquals(TransportStatus.NEW, result.getStatus());
        assertEquals((Integer) 2, result.getShippedUnits());
        assertEquals(groningen.getId(), result.getUnloadLocation().getId());
        assertEquals(Boolean.TRUE, result.getSenderIsLoad());
        assertEquals(Boolean.FALSE, result.getDestinationIsUnload());

        assertNotNull(result.getOrder());
        assertNotNull(result.getOrder().getLines());
        assertEquals(result.getOrder().getLines().size(), 2);

        result.getOrder().getLines().stream().forEach((line) -> {
            assertNotNull(line);
            assertNotNull(line.getAmount());
            assertNotNull(line.getDescription());
            assertNotNull(line.getRemarks());
            assertNotNull(line.getStackable());
            assertNotNull(line.getId());
            assertNotNull(line.getName());
            assertNotNull(line.getPaymentOnDeliveryAmount());
            assertNotNull(line.getPaymentOnDeliveryCurrency());
            assertNotNull(line.getType());
            assertNotNull(line.getSwapPallets());
            assertNotNull(line.getLength());
            assertNotNull(line.getWidth());
            assertNotNull(line.getHeight());
            assertNotNull(line.getWeight());
        });

    }

    /**
     * Test of postShipment method, of class ShipmentResource.
     */
    @Test
    public void testPostMinimumShipment() throws IOException, URISyntaxException {
        System.out.println("postShipment");
        List<OrderLine> lines = new ArrayList<>();

        Shipment template = Shipment.builder()
                .loadLocation(madrid)
                .name("madrid to groningen")
                .status(TransportStatus.NEW)
                .unloadLocation(groningen)
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .build();
        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment result = instance.postShipment(template);
        assertNotNull(result);
        assertEquals(madrid.getId(), result.getLoadLocation().getId());
        assertEquals("madrid to groningen", result.getName());
        assertEquals(TransportStatus.NEW, result.getStatus());
        assertEquals(groningen.getId(), result.getUnloadLocation().getId());
    }

    /**
     * Test of updateShipment method, of class ShipmentResource.
     */
    @Test
    public void testUpdateShipment() throws IOException, URISyntaxException {
        System.out.println("updateShipment");
        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment template = instance.getShipment("1");
        assertNotNull(template);
        assertEquals(template.getName(), "shipment 1");
        assertEquals(ObjectFactory.toString(template.getDeadline()), ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 2, 0, 0)));
        assertEquals(template.getShippedUnits(), (Integer) 4);
        assertEquals(template.getNote(), "some note");

        template.setNote("this not has been changed");

        Shipment result = instance.updateShipment(template);
        assertEquals(result.getNote(), "this not has been changed");
    }

    /**
     * Test of updateShipment method, of class ShipmentResource.
     */
    @Test
    public void testCancelShipment() throws IOException, URISyntaxException {
        System.out.println("cancelShipment");
        System.out.println("Prepare scenario");

        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment template = instance.getShipment("1");
        assertNotNull(template);
        assertEquals(template.getName(), "shipment 1");
        assertEquals(ObjectFactory.toString(template.getDeadline()), ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 2, 0, 0, 0, 0)));
        assertEquals(template.getShippedUnits(), (Integer) 4);
        assertEquals(template.getNote(), "some note");

        System.out.println("Execute test");
        Shipment result = instance.cancelShipment(template.getId());

        System.out.println("Check result");
        assertEquals(result.getStatus(), TransportStatus.CANCELLED);

        result = instance.getShipment(template.getId());
        assertEquals(result.getStatus(), TransportStatus.CANCELLED);

    }
    
    @Test
    public void testCreateUpdateShipment() throws IOException, URISyntaxException{
        System.out.println("createUpdateShipment");
        
        List<OrderLine> lines = new ArrayList<>();

        Shipment template = Shipment.builder()
                .loadLocation(madrid)
                .name("madrid to groningen")
                .status(TransportStatus.NEW)
                .unloadLocation(groningen)
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().address("something").build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .build();
        ShipmentResource instance = new ShipmentResource(graphDb);
        Shipment shipment = instance.postShipment(template);
        shipment.setNote("this is a new note");
        Shipment shipment2 = instance.updateShipment(shipment);
        assertEquals(shipment2.getNote(), "this is a new note");
    }

    @Test
    public void testDeleteShipment() throws IOException, URISyntaxException {
        System.out.println("deleteShipment");
        System.out.println("Prepare scenario");
        ShipmentResource instance = new ShipmentResource(graphDb);

        Shipment template = instance.getShipment("1");
        assertNotNull(template);
        assertEquals(template.getName(), "shipment 1");
        assertEquals(ObjectFactory.toString(template.getDeadline()), ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 2, 0, 0, 0, 0)));
        assertEquals(template.getShippedUnits(), (Integer) 4);
        assertEquals(template.getNote(), "some note");

        System.out.println("Execute test");

        String shipmentIdToDelete = template.getId();
        instance.deleteShipment(shipmentIdToDelete);

        System.out.println("Check result");
        Shipment result = instance.getShipment(shipmentIdToDelete);
        assertNull(result);
    }

    @Test
    public void testBugwithLocations() throws IOException, URISyntaxException {
        System.out.println("Prepare scenario");
        ShipmentService instance = new ShipmentResource(graphDb);
        //ShipmentService instance = ClientUtil.proxy(ShipmentService.class, new URI("http://localhost:8011/api"));
//        String shipmentJson = "{\"container\":null,\"deadline\":null,\"destinationIsUnload\":true,\"href\":null,\"id\":null,\"index\":null,\"loadLocation\":{\"address\":\"e\",\"city\":\"e\",\"country\":\"ES\",\"email\":\"e\",\"etd\":null,\"externalId\":null,\"href\":null,\"id\":\"062125db-ccb4-4646-b4e3-73c71b3483ad\",\"latitude\":\"NaN\",\"longitude\":\"NaN\",\"name\":\"paco\",\"phone\":\"e\",\"postalcode\":\"e\",\"status\":[\"approvalStatus\",\"NEW\"],\"timeFrameFrom\":null,\"timeFrameTo\":null},\"name\":\"\",\"note\":\"\",\"order\":{\"batch\":\"\",\"debitor\":{\"chargeVat\":null,\"currency\":null,\"externalId\":null,\"href\":\"clients/8bb02194-19ba-4904-899d-5876467f461e\",\"id\":\"8bb02194-19ba-4904-899d-5876467f461e\",\"name\":\"paco\",\"price\":null},\"destination\":{\"address\":null,\"city\":null,\"country\":null,\"email\":null,\"externalId\":null,\"href\":null,\"id\":null,\"latitude\":null,\"longitude\":null,\"name\":null,\"phone\":null,\"postalcode\":null,\"status\":[\"approvalStatus\",\"NEW\"]},\"externalId\":null,\"href\":null,\"id\":\"\",\"invoiceReference\":\"\",\"lines\":[],\"loadingList\":null,\"name\":\"\",\"orderedBy\":{\"bicCode\":\"e\",\"chamberOfCommerce\":\"e\",\"chamberOfCommerceCity\":\"e\",\"chargeVat\":null,\"creditorNumber\":null,\"currency\":\"EUR\",\"debtorNumber\":null,\"externalId\":null,\"href\":\"clients/8bb02194-19ba-4904-899d-5876467f461e\",\"ibanCode\":\"e\",\"id\":\"8bb02194-19ba-4904-899d-5876467f461e\",\"locations\":[{\"address\":\"e\",\"city\":\"e\",\"country\":\"ES\",\"email\":\"e\",\"externalId\":null,\"href\":null,\"id\":\"062125db-ccb4-4646-b4e3-73c71b3483ad\",\"latitude\":\"NaN\",\"longitude\":\"NaN\",\"name\":\"paco\",\"phone\":\"e\",\"postalcode\":\"e\",\"status\":[\"approvalStatus\",\"NEW\"]}],\"name\":\"paco\",\"status\":[\"approvalStatus\",\"NEW\"],\"vatNumber\":\"e\"},\"origin\":{\"address\":null,\"city\":null,\"country\":null,\"email\":null,\"externalId\":null,\"href\":null,\"id\":null,\"latitude\":null,\"longitude\":null,\"name\":null,\"phone\":null,\"postalcode\":null,\"status\":[\"approvalStatus\",\"NEW\"]},\"otherReference\":\"\",\"unloadReference\":\"\"},\"senderIsLoad\":true,\"shippedUnits\":0,\"status\":[\"approvalStatus\",\"NEW\"],\"unloadLocation\":{\"address\":\"\",\"city\":\"\",\"country\":\"\",\"email\":null,\"eta\":null,\"externalId\":null,\"href\":null,\"id\":null,\"latitude\":\"NaN\",\"longitude\":\"NaN\",\"name\":\"\",\"phone\":null,\"postalcode\":\"\",\"status\":[\"approvalStatus\",\"NEW\"],\"timeFrameFrom\":null,\"timeFrameTo\":null}}";

        String shipmentJson = "{\n"
                + "    \"@class\":\"eu.logisense.api.Shipment\", \n"
                + "    \"container\": null,\n"
                + "    \"deadline\": [2016, 4, 8, 17, 24, 53, 14000000],\n"
                + "    \"destinationIsUnload\": true,\n"
                + "    \"href\": \"/shipments/d4e5c9ab-d2ce-4be6-b42b-9f5ac34fbe84\",\n"
                + "    \"id\": \"d4e5c9ab-d2ce-4be6-b42b-9f5ac34fbe84\",\n"
                + "    \"index\": null,\n"
                + "    \"loadLocation\": {\n"
                + "        \"@class\": \"eu.logisense.api.LoadLocation\","
                + "        \"address\": \"\",\n"
                + "        \"city\": \"\",\n"
                + "        \"country\": \"\",\n"
                + "        \"email\": null,\n"
                + "        \"externalId\": null,\n"
                + "        \"href\": null,\n"
                + "        \"id\": \"7b7c78c7-1d8b-4f96-94fc-bdcf0936a553\",\n"
                + "        \"index\": 0,\n"
                + "        \"latitude\": \"NaN\",\n"
                + "        \"longitude\": \"NaN\",\n"
                + "        \"name\": \"aaaaaaaaa\",\n"
                + "        \"phone\": null,\n"
                + "        \"postalcode\": \"\",\n"
                + "        \"status\": [\"approvalStatus\", \"NEW\"],\n"
                + "        \"timeFrameFrom\": [2016, 4, 8, 17, 24, 53, 14000000],\n"
                + "        \"timeFrameTo\": [2016, 4, 8, 17, 24, 53, 14000000]\n"
                + "    },\n"
                + "    \"name\": \"name\",\n"
                + "    \"note\": \"\",\n"
                + "    \"order\": {\n"
                + "        \"@class\": \"eu.logisense.api.Order\","
                + "        \"batch\": \"\",\n"
                + "        \"debitor\": {\n"
                + "            \"@class\": \"eu.logisense.api.Debitor\","
                + "            \"chargeVat\": null,\n"
                + "            \"currency\": \"EUR\",\n"
                + "            \"externalId\": null,\n"
                + "            \"href\": \"/clients/79121b83-e942-4d2a-aa36-01600a0f9ada\",\n"
                + "            \"id\": \"79121b83-e942-4d2a-aa36-01600a0f9ada\",\n"
                + "            \"index\": 0,\n"
                + "            \"name\": \"aaaaaaaaa\",\n"
                + "            \"price\": 500.0,\n"
                + "            \"status\": null\n"
                + "        },\n"
                + "        \"destination\": {\n"
                + "            \"@class\": \"eu.logisense.api.Destination\","
                + "            \"address\": \"\",\n"
                + "            \"city\": \"\",\n"
                + "            \"country\": \"\",\n"
                + "            \"email\": null,\n"
                + "            \"externalId\": null,\n"
                + "            \"href\": null,\n"
                + "            \"id\": \"2e411755-bb9a-4c94-aeb5-ebfaa3f857fd\",\n"
                + "            \"index\": 0,\n"
                + "            \"latitude\": \"NaN\",\n"
                + "            \"longitude\": \"NaN\",\n"
                + "            \"name\": \"bbbbbbbbbbbbbbbbbb\",\n"
                + "            \"phone\": null,\n"
                + "            \"postalcode\": \"\",\n"
                + "            \"status\": [\"approvalStatus\", \"NEW\"]\n"
                + "        },\n"
                + "        \"externalId\": \"\",\n"
                + "        \"href\": \"/orders/4d0d1e64-5cd0-4296-8990-d6a489b260ca\",\n"
                + "        \"id\": \"4d0d1e64-5cd0-4296-8990-d6a489b260ca\",\n"
                + "        \"index\": 0,\n"
                + "        \"invoiceReference\": \"\",\n"
                + "        \"lines\": [{\n"
                + "                \"@class\": \"eu.logisense.api.OrderLine\","
                + "                \"amount\": 22,\n"
                + "                \"description\": \"22\",\n"
                + "                \"height\": 2.0,\n"
                + "                \"href\": \"/orders/4d0d1e64-5cd0-4296-8990-d6a489b260ca/lines/b30cb421-7def-4a97-9123-d702fddf5273\",\n"
                + "                \"id\": \"b30cb421-7def-4a97-9123-d702fddf5273\",\n"
                + "                \"index\": 0,\n"
                + "                \"length\": 1.2,\n"
                + "                \"name\": \"\",\n"
                + "                \"paymentOnDeliveryAmount\": 0.0,\n"
                + "                \"paymentOnDeliveryCurrency\": \"EUR\",\n"
                + "                \"remarks\": \"\",\n"
                + "                \"stackable\": false,\n"
                + "                \"status\": null,\n"
                + "                \"swapPallets\": false,\n"
                + "                \"type\": \"EURO_PALLET\",\n"
                + "                \"weight\": 2222.0,\n"
                + "                \"width\": 0.8\n"
                + "            }],\n"
                + "        \"loadingList\": \"\",\n"
                + "        \"name\": \"\",\n"
                + "        \"orderedBy\": {\n"
                + "            \"@class\": \"eu.logisense.api.Client\","
                + "            \"bicCode\": null,\n"
                + "            \"chamberOfCommerce\": null,\n"
                + "            \"chamberOfCommerceCity\": null,\n"
                + "            \"chargeVat\": null,\n"
                + "            \"creditorNumber\": null,\n"
                + "            \"currency\": \"EUR\",\n"
                + "            \"debtorNumber\": null,\n"
                + "            \"externalId\": null,\n"
                + "            \"href\": \"/clients/79121b83-e942-4d2a-aa36-01600a0f9ada\",\n"
                + "            \"ibanCode\": null,\n"
                + "            \"id\": \"79121b83-e942-4d2a-aa36-01600a0f9ada\",\n"
                + "            \"index\": 0,\n"
                + "            \"locations\": [],\n"
                + "            \"name\": \"aaaaaaaaa\",\n"
                + "            \"status\": [\"approvalStatus\", \"NEW\"],\n"
                + "            \"vatNumber\": null\n"
                + "        },\n"
                + "        \"origin\": {\n"
                + "            \"@class\": \"eu.logisense.api.Origin\","
                + "            \"address\": \"\",\n"
                + "            \"city\": \"\",\n"
                + "            \"country\": \"\",\n"
                + "            \"email\": null,\n"
                + "            \"externalId\": null,\n"
                + "            \"href\": null,\n"
                + "            \"id\": \"7b7c78c7-1d8b-4f96-94fc-bdcf0936a553\",\n"
                + "            \"index\": 0,\n"
                + "            \"latitude\": \"NaN\",\n"
                + "            \"longitude\": \"NaN\",\n"
                + "            \"name\": \"aaaaaaaaa\",\n"
                + "            \"phone\": null,\n"
                + "            \"postalcode\": \"\",\n"
                + "            \"status\": [\"approvalStatus\", \"NEW\"]\n"
                + "        },\n"
                + "        \"otherReference\": \"\",\n"
                + "        \"status\": null,\n"
                + "        \"unloadReference\": \"\"\n"
                + "    },\n"
                + "    \"senderIsLoad\": true,\n"
                + "    \"shippedUnits\": 22,\n"
                + "    \"status\": [\"transportStatus\", \"NEW\"],\n"
                + "    \"unloadLocation\": {\n"
                + "        \"@class\": \"eu.logisense.api.UnloadLocation\","
                + "        \"address\": \"\",\n"
                + "        \"city\": \"\",\n"
                + "        \"country\": \"\",\n"
                + "        \"email\": null,\n"
                + "        \"externalId\": null,\n"
                + "        \"href\": null,\n"
                + "        \"id\": \"2e411755-bb9a-4c94-aeb5-ebfaa3f857fd\",\n"
                + "        \"index\": 0,\n"
                + "        \"latitude\": \"NaN\",\n"
                + "        \"longitude\": \"NaN\",\n"
                + "        \"name\": \"bbbbbbbbbbbbbbbbbb\",\n"
                + "        \"phone\": null,\n"
                + "        \"postalcode\": \"\",\n"
                + "        \"status\": [\"approvalStatus\", \"NEW\"],\n"
                + "        \"timeFrameFrom\": [2016, 4, 8, 17, 25, 21, 849000000],\n"
                + "        \"timeFrameTo\": [2016, 4, 8, 17, 25, 21, 849000000]\n"
                + "    }\n"
                + "}";

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JSR310Module());
        Shipment shipment = mapper.readValue(shipmentJson, Shipment.class);

        assertEquals(shipment.getStatus().name(),TransportStatus.NEW.name() );
        
        LocalDateTime now = LocalDateTime.now();
        shipment.setDeadline(now);
        shipment.getLoadLocation().setTimeFrameFrom(now);
        shipment.getLoadLocation().setTimeFrameTo(now);

        shipment.getUnloadLocation().setTimeFrameFrom(now);
        shipment.getUnloadLocation().setTimeFrameTo(now);

        Shipment result = instance.postShipment(shipment);

        assertNotNull(result);

    }
}
