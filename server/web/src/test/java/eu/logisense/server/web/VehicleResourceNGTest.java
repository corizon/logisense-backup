/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web;

import eu.logisense.api.Vehicle;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class VehicleResourceNGTest {

    private static final Logger log = LoggerFactory.getLogger(VehicleResourceNGTest.class);

    public VehicleResourceNGTest() {
    }

    private GraphDatabaseService graphDb;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
//        graphDb = new RestGraphDatabase("http://localhost:7474");
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }

    /**
     * Test of dispatchTo method, of class ShipmentResource.
     */
    @Test
    public void getVehicles() throws IOException {
        log.info("getVehicles");
        log.info("Prepare Scenario");
        Vehicle template = Vehicle.builder().name("name1").href("href").build();
        VehicleResource instance = new VehicleResource(graphDb);

        Vehicle result = instance.postVehicle(template);
        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());

        log.info("Execute test");
        List<Vehicle> vehicles = instance.getVehicles();

        log.info("Check result");
        assertNotNull(vehicles);
        assertTrue(vehicles.size() == 1);

    }

    @Test
    public void getVehicle() throws IOException {
        log.info("getVehicle");
        log.info("Prepare Scenario");
        Vehicle template = Vehicle.builder().name("name1").href("href").build();
        VehicleResource instance = new VehicleResource(graphDb);

        Vehicle result = instance.postVehicle(template);
        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());

        log.info("Execute test");
        Vehicle vehicleAfterPost = instance.getVehicle(result.getId());
        log.info("Check result");
        assertNotNull(vehicleAfterPost);
        assertEquals("name1", vehicleAfterPost.getName());
        assertNotNull(vehicleAfterPost.getId());
        assertEquals(result.getId(), vehicleAfterPost.getId());
        assertNotNull(vehicleAfterPost.getHref());
    }

    @Test
    public void postVehicle() throws IOException {
        log.info("postRide");
        log.info("Prepare Scenario");
        Vehicle template = Vehicle.builder().name("name1").href("href").build();
        VehicleResource instance = new VehicleResource(graphDb);
        log.info("Execute test");

        Vehicle result = instance.postVehicle(template);
        log.info("Check result");

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());
    }

    @Test
    public void updateVehicle() throws IOException {
        log.info("updateVehicle");
        log.info("Prepare Scenario");
        Vehicle template = Vehicle.builder().name("name1").href("href").build();
        VehicleResource instance = new VehicleResource(graphDb);

        Vehicle result = instance.postVehicle(template);
        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());

        log.info("Execute test");
        result.setName("nameUpdate");
        Vehicle vehicleAfterUpdate = instance.updateVehicle(result);

        log.info("Check result");
        assertNotNull(vehicleAfterUpdate);
        assertEquals(vehicleAfterUpdate.getName(), "nameUpdate");
        assertEquals(vehicleAfterUpdate.getId(), result.getId());
    }

    @Test
    public void deleteVehicle() throws IOException {

        log.info("updateVehicle");
        log.info("Prepare Scenario");
        Vehicle template = Vehicle.builder().name("name1").href("href").build();
        VehicleResource instance = new VehicleResource(graphDb);

        Vehicle result = instance.postVehicle(template);
        assertEquals("name1", result.getName());
    }

    
}
