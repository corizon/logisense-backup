/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web.model;

import eu.logisense.api.Ride;
import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import java.util.UUID;
import org.jvnet.fastinfoset.EncodingAlgorithmIndexes;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.Times;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.Test;

/**
 *
 * @author johan
 */
public class RideFactoryNGTest  extends BaseObjectFactoryTest{
    
    public RideFactoryNGTest() {
    }

    /**
     * Test of fromNode method, of class RideFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");
        String name = "ride 1";
        String id = "1";
        String href = RideFactory.toHref(id);
        String externalId = "ext 1";
        Status status = TransportStatus.NEW;
        String licensePlate = UUID.randomUUID().toString();

        when(node.getProperty(Ride.PROP_NAME)).thenReturn(name);
        when(node.getProperty(Ride.PROP_ID)).thenReturn(id);
        when(node.getProperty(Ride.PROP_HREF)).thenReturn(href);
        when(node.getProperty(Ride.PROP_EXTERNAL_ID)).thenReturn(externalId);
        when(node.getProperty(Ride.PROP_STATUS)).thenReturn(status.name());
        when(node.getProperty(Ride.PROP_LICENSE_PLATE)).thenReturn(licensePlate);

        Ride result = RideFactory.fromNode(node);
        assertEquals(result.getName(), name);
        assertEquals(result.getId(), id);
        assertEquals(result.getHref(), href);
        assertEquals(result.getExternalId(), externalId);
        assertEquals(result.getStatus(), status);
        assertEquals(result.getLicensePlate(), licensePlate);

        verify(databaseService, new Times(6)).beginTx();
        verify(transaction, new Times(6)).success();
    }

    /**
     * Test of toHref method, of class RideFactory.
     */
    @Test
    public void testToHref() {
        System.out.println("toHref");
        assertEquals(RideFactory.toHref("12345"), "/rides/12345");
        assertEquals(RideFactory.toHref("67890"), "/rides/67890");
    }

}
