/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.Client;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Resource;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.Times;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ClientFactoryNGTest extends BaseObjectFactoryTest{
    
    public ClientFactoryNGTest() {
    }


    /**
     * Test of fromNode method, of class ContainerFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");

        String id = "1234567";
        String href = "/clients/"+id;
        String name = "some name";
        Boolean chargeVat = true;
        String chamberOfCommerce = "123456789";
        String chamberOfCommerceCity = "Groningen";
        String vatNumber = "NL12345678B01";
        String bicNumber = "INGB2A";
        String ibanNumber = "NL42INGB098765432";
        String currency = "EUR";
        ApprovalStatus status = ApprovalStatus.NEW;
        String externalId = "1";
        
        when(node.getProperty(Client.PROP_HREF)).thenReturn(href);
        when(node.getProperty(Client.PROP_ID)).thenReturn(id);
        when(node.getProperty(Client.PROP_NAME)).thenReturn(name);
        when(node.getProperty(Client.PROP_BIC_CODE)).thenReturn(bicNumber);
        when(node.getProperty(Client.PROP_CHAMBER_OF_COMMERCE)).thenReturn(chamberOfCommerce);
        when(node.getProperty(Client.PROP_CHAMBER_OF_COMMERCE_CITY)).thenReturn(chamberOfCommerceCity);
        when(node.getProperty(Client.PROP_CHARGE_VAT)).thenReturn(chargeVat);
        when(node.getProperty(Client.PROP_CURRENCY)).thenReturn(currency);
        when(node.getProperty(Client.PROP_IBAN_CODE)).thenReturn(ibanNumber);
        when(node.getProperty(Client.PROP_VAT_NUMBER)).thenReturn(vatNumber);
        when(node.getProperty(Client.PROP_STATUS)).thenReturn(status.name());
        when(node.getProperty(Client.PROP_EXTERNAL_ID)).thenReturn(externalId);

        Client result = ClientFactory.fromNode(node);
        assertEquals(result.getHref(), href);
        assertEquals(result.getId(), id);
        assertEquals(result.getName(), name);
        assertEquals(result.getBicCode(), bicNumber);
        assertEquals(result.getChamberOfCommerce(), chamberOfCommerce);
        assertEquals(result.getChamberOfCommerceCity(), chamberOfCommerceCity);
        assertEquals(result.getChargeVat(), chargeVat);
        assertEquals(result.getCurrency(), currency);
        assertEquals(result.getIbanCode(), ibanNumber);
        assertEquals(result.getVatNumber(), vatNumber);
        assertEquals(result.getStatus(), status);
        assertEquals(result.getExternalId(), externalId);

        verify(databaseService, new Times(14)).beginTx();
        verify(transaction, new Times(14)).success();
    }

    /**
     * Test of resourceFromNode method, of class ClientFactory.
     */
    @Test
    public void testResourceFromNode() {
        System.out.println("resourceFromNode");

        String name = "some name";
        String id = "1234567";
        String href = "/clients/"+id;
        
        when(node.getProperty(Client.PROP_HREF)).thenReturn(href);
        when(node.getProperty(Client.PROP_ID)).thenReturn(id);
        when(node.getProperty(Client.PROP_NAME)).thenReturn(name);

        Resource result = ClientFactory.resourceFromNode(node);
        assertEquals(result.getHref(), href);
        assertEquals(result.getId(), id);
        assertEquals(result.getName(), name);


        verify(databaseService, new Times(3)).beginTx();
        verify(transaction, new Times(3)).success();
    }

    /**
     * Test of toHref method, of class ClientFactory.
     */
    @Test
    public void testToHref() {
        System.out.println("toHref");
        assertEquals(ClientFactory.toHref("12345"), "/clients/12345");
        assertEquals(ClientFactory.toHref("67890"), "/clients/67890");
    }
    
}
