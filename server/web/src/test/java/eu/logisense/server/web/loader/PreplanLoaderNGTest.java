/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web.loader;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author johan
 */
public class PreplanLoaderNGTest {

    private GraphDatabaseService db;

    @BeforeMethod
    public void setUp() {
        db = new TestGraphDatabaseFactory().newImpermanentDatabase();
    }

    @AfterMethod
    public void tearDown() {
        db.shutdown();
    }

//    /**
//     * Test of scan method, of class PreplanLoader.
//     */
//    @Test
//    public void testScan() {
//        System.out.println("scan");
//        PreplanLoader instance = new PreplanLoader(db);
//        instance.scan();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of loadFile method, of class PreplanLoader.
     */
    @Test
    public void testLoadFile() {
        System.out.println("loadFile");
        Path path = resourcePath("/data/pronorm/test1.csv");
        String clientId = "ignored";

        PreplanLoader instance = new PreplanLoader(db);
        instance.loadFile(path, clientId);

        
        int nShipments = 0;
        try (Transaction tx = db.beginTx()) {
            Result r = db.execute("MATCH (a:SHIPMENT) RETURN a");
            while (r.hasNext()) {
                Map<String, Object> props = r.next();
                nShipments++;
//                print((Node)props.get("a"));
            }
        }
        assertEquals(10, nShipments);
        
        
        int nOrders = 0;
        try (Transaction tx = db.beginTx()) {
            Result r = db.execute("MATCH (a:ORDER) RETURN a");
            while (r.hasNext()) {
                Map<String, Object> props = r.next();
                nOrders++;
//                print((Node)props.get("a"));
            }
        }
        assertEquals(10, nOrders);
        
        
        int nLocations = 0;
        try (Transaction tx = db.beginTx()) {
            Result r = db.execute("MATCH (a:LOCATION) RETURN a");
            while (r.hasNext()) {
                Map<String, Object> props = r.next();
                nLocations++;
//                print((Node)props.get("a"));
            }
        }
        assertEquals(11, nLocations); // 1+10, see below
        
        
        int nOLocs = 0;
        Set<Long> oLocIds = new HashSet<>();
        try (Transaction tx = db.beginTx()) {
            Result r = db.execute("MATCH (a:LOCATION)<-[r:ORIGIN]-(s:SHIPMENT) RETURN a");
            while (r.hasNext()) {
                Map<String, Object> props = r.next();
                Node n = (Node)props.get("a");
                nOLocs++;
                oLocIds.add(n.getId());
//                print(n);
            }
        }
        assertEquals(10, nOLocs);
        assertEquals(1, oLocIds.size()); // same orig loc reused for all shipments
        

        int nDLocs = 0;
        Set<Long> dLocIds = new HashSet<>();
        try (Transaction tx = db.beginTx()) {
            Result r = db.execute("MATCH (a:LOCATION)<-[r:DESTINATION]-(s:SHIPMENT) RETURN a");
            while (r.hasNext()) {
                Map<String, Object> props = r.next();
                Node n = (Node)props.get("a");
                nDLocs++;
                dLocIds.add(n.getId());
//                print(n);
            }
        }
        assertEquals(10, nDLocs);
        assertEquals(10, dLocIds.size()); // new dest loc created for each shipment
        
        //TODO: verify created nodes are accepted by ObjectFactories
    }
    
    
    @Test
    public void testGetOrCreateDefaultClientLocation() {
        System.out.println("getOrCreateDefaultClientLocation");
        PreplanLoader instance = new PreplanLoader(db);
        
        // check idempotency:
        // after multiple calls only a single pronorm location should exist (default origin for all imported shipments)
        Node n1 = instance.getOrCreateDefaultClientLocation();
        Node n2 = instance.getOrCreateDefaultClientLocation();
        
        assertNotNull(n1);
        assertEquals(n1, n2);
        
        int nLocations = 0;
        try (Transaction tx = db.beginTx()) {
            Result r = db.execute("MATCH (a:LOCATION) RETURN a");
            while (r.hasNext()) {
                Map<String, Object> props = r.next();
                nLocations++;
//                print((Node)props.get("a"));
            }
        }
        assertEquals(1, nLocations);
    }
    
    void print(Node n) {
        System.out.println();
        System.out.println("" + n.getLabels() + n.getId());
        for (String key : n.getPropertyKeys()) {
            System.out.println(key + ": " + n.getProperty(key));
        }
    }

    Path resourcePath(String s) {
        return Paths.get(getClass().getResource(s).getFile());
    }
}