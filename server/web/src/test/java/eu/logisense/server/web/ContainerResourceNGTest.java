/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web;

import eu.logisense.api.Container;
import eu.logisense.api.Debitor;
import eu.logisense.api.Link;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Order;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.WebApplicationException;

import org.testng.annotations.Test;
import static org.testng.Assert.*;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

/**
 *
 * @author perezdf
 */
public class ContainerResourceNGTest {

    private static final Logger logger = LoggerFactory.getLogger(ContainerResourceNGTest.class);
    private GraphDatabaseService graphDb;

    public ContainerResourceNGTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @BeforeClass
    public void setUp() {
    }

    @AfterClass
    public void tearDown() {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
        Util.initTestSet(graphDb);
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }

    /**
     * Test of getContainers method, of class ContainerResource.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetContainers() throws IOException, URISyntaxException {
        logger.info("getContainers");

        ContainerResource instance = new ContainerResource(graphDb);

        logger.info("Execute test");
        List<Container> containers = instance.getContainers();

        logger.info("Check result");
        assertNotNull(containers);
        assertEquals(containers.size(), 3);
    }

    @Test
    public void testGetContainersWithShipments() throws IOException, URISyntaxException {
        logger.info("getContainers");
        logger.info("Prepare Scenario");

        ContainerResource instance = new ContainerResource(graphDb);

        logger.info("Execute test");
        List<Container> containers = instance.getContainers();

        logger.info("Check result");
        assertNotNull(containers);
        assertEquals(containers.size(), 3);

        containers.stream().forEach((c) -> {
            assertNotNull(c);
            assertNotNull(c.getShipments());
            if ("019".equals(c.getId())) {
                assertEquals(c.getShipments().size(), 2);
            }
            if ("121".equals(c.getId())) {
                assertEquals(c.getShipments().size(), 1);
            }

        });
    }

    /**
     * Test of getContainer method, of class ContainerResource.
     */
    @Test
    public void testGetContainerWithShipments() throws IOException, URISyntaxException {
        logger.info("getContainer");
        logger.info("Prepare Scenario");
        ContainerResource instance = new ContainerResource(graphDb);

        logger.info("Execute test");
        Container container = instance.getContainer("019");

        logger.info("Check result");

        assertNotNull(container);
        assertEquals("container 1", container.getName());
        assertEquals(container.getShipments().size(), 2);
    }

    /**
     * Test of postContainer method, of class ContainerResource.
     */
    @Test
    public void testPostContainer() throws IOException, URISyntaxException {
        logger.info("postContainer");
        logger.info("Prepare Scenario");
        Container template = Container.builder().name("name1").href("href").build();
        ContainerResource instance = new ContainerResource(graphDb);
        logger.info("Execute test");

        Container result = instance.postContainer(template);
        logger.info("Check result");

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());
    }

    @Test(expectedExceptions = {WebApplicationException.class})
    public void testPostContainerWithShipmentsNoCreated() throws IOException, URISyntaxException {
        logger.info("postContainer");
        logger.info("Prepare Scenario");

        Container template = Container.builder().name("name1").href("href").build();
        ContainerResource instance = new ContainerResource(graphDb);

        Shipment shipment = Shipment.builder().name("shipment name")
                .deadline(LocalDateTime.MIN)
                .note("")
                .status(TransportStatus.NEW)
                .loadLocation(LoadLocation.builder().build())
                .unloadLocation(UnloadLocation.builder().build())
                .build();

        template.getShipments().add(shipment);
        logger.info("Execute test");

        Container returnContainer = instance.postContainer(template);

        assertNotNull(returnContainer);
        assertEquals(shipment, returnContainer.getShipments().get(0));
    }

    @Test(expectedExceptions = {WebApplicationException.class})
    public void testGetContainerWithShipmentsNoCreated() throws IOException, URISyntaxException {
        logger.info("postContainer");

        Container template = Container.builder().name("name1").href("href").build();
        ContainerResource instance = new ContainerResource(graphDb);

        Shipment shipment = Shipment.builder().name("shipment name")
                .deadline(LocalDateTime.MIN)
                .note("note")
                .status(TransportStatus.NEW)
                .loadLocation(LoadLocation.builder().build())
                .unloadLocation(UnloadLocation.builder().build())
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().build())
                        .build())
                .build();

        template.getShipments().add(shipment);

        Container result = instance.postContainer(template);

    }

    @Test
    public void testPostContainerWithShipmentsAlreadyCreated() throws IOException, URISyntaxException {
        logger.info("testPostContainerWithShipmentsAlreadyCreated");
        logger.info("Prepare Scenario");

        Container template = Container.builder().name("name1").href("href").build();
        ContainerResource instance = new ContainerResource(graphDb);

        Shipment shipment = Shipment.builder().name("shipment name")
                .deadline(LocalDateTime.MIN)
                .note("")
                .status(TransportStatus.NEW)
                .loadLocation(LoadLocation.builder().build())
                .unloadLocation(UnloadLocation.builder().build())
                .build();

        ShipmentResource shipmentInstance = new ShipmentResource(graphDb);

        List<Resource> shipmentLinkList = new ArrayList<>();
        for (Shipment item : shipmentInstance.getShipments()) {
            shipmentLinkList.add(item);
        }
        template.getShipments().addAll(shipmentLinkList);
        logger.info("Execute test");

        Container result = instance.postContainer(template);
        logger.info("Check result");
        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());
        assertNotNull(result.getShipments());
    }

    @Test
    public void testPostContainerWithTotalMaxCapacity() throws IOException, URISyntaxException {
        logger.info("testPostContainerWithTotalMaxCapacity");
        logger.info("Prepare Scenario");

        Container template = Container.builder().name("name1").href("href").build();
        ContainerResource instance = new ContainerResource(graphDb);

        Shipment shipment = Shipment.builder().name("shipment name")
                .deadline(LocalDateTime.MIN)
                .note("")
                .loadLocation(LoadLocation.builder().build())
                .unloadLocation(UnloadLocation.builder().build())
                .build();

        ShipmentResource shipmentInstance = new ShipmentResource(graphDb);

        List<Resource> shipmentLinkList = new ArrayList<>();
        for (Shipment item : shipmentInstance.getShipments()) {
            shipmentLinkList.add(item);
        }
        template.getShipments().addAll(shipmentLinkList);
        template.setMaxWeight(7.0);
        template.setWidth(8.0);
        template.setHeight(9.0);
        template.setLength(10.0);

        logger.info("Execute test");

        Container result = instance.postContainer(template);
        logger.info("Check result");

        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());
        assertNotNull(result.getShipments());

        assertEquals(result.getMaxWeight(), 7.0);
        assertEquals(result.getWidth(), 8.0);
        assertEquals(result.getHeight(), 9.0);
        assertEquals(result.getLength(), 10.0);

    }

    @Test
    public void testPostContainerWithMultipleShipmentsAlreadyCreated() throws IOException, URISyntaxException {
        logger.info("testPostContainerWithMultipleShipmentsAlreadyCreated");
        logger.info("Prepare Scenario");

        Container template = Container.builder().name("name1").href("href").build();
        ContainerResource instance = new ContainerResource(graphDb);

        ShipmentResource shipmentInstance = new ShipmentResource(graphDb);

        List<Resource> shipmentLinkList = new ArrayList<>();
        for (Shipment item : shipmentInstance.getShipments()) {
            shipmentLinkList.add(item);
        }
        template.getShipments().addAll(shipmentLinkList);
        logger.info("Execute test");

        Container result = instance.postContainer(template);
        logger.info("Check result");
        assertEquals("name1", result.getName());
        assertNotNull(result.getId());
        assertNotNull(result.getHref());
        assertNotNull(result.getShipments());
    }

    /**
     * Test of updateContainer method, of class ContainerResource.
     */
    @Test
    public void testUpdateContainer() throws IOException, URISyntaxException {
        System.out.println("testUpdateContainer");
        logger.info("updateContainer");
        logger.info("Prepare Scenario");
        String uniqueString = UUID.randomUUID().toString();
        logger.info("unique name {}", uniqueString);
        Container template = Container.builder().name(uniqueString).build();
        ContainerResource instance = new ContainerResource(graphDb);
        Container result = instance.postContainer(template);
        String id = result.getId();

        String unique2String = UUID.randomUUID().toString();
        Container updatedContainer = Container.builder().id(id).name(unique2String).build();

        logger.info("Execute test");
        Container registeredContainer = instance.updateContainer(updatedContainer);

        logger.info("Check result");
        assertNotNull(registeredContainer);
        assertEquals(unique2String, registeredContainer.getName());
    }

    /**
     * Test of deleteContainer method, of class ContainerResource.
     */
    // @Test
    public void testDeleteContainer() {
    }

}
