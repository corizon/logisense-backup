/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.Order;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.Times;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class OrderFactoryNGTest  extends BaseObjectFactoryTest{
    
    public OrderFactoryNGTest() {
    }

    /**
     * Test of fromNode method, of class OrderFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");
        String name = "ikuytrewdfghjhgfdsdfghjgfd";
        String id = "KJHGFDS4564DFGDF345654EFG";
        String href = OrderFactory.toHref(id);
        String batch = "BATCH1234";
        String invoiceReference = "IR67898";
        String otherReference = "OR67898";
        String unloadReference = "UR67898";
        String loadingList = "1234";

        when(node.getProperty(Order.PROP_NAME)).thenReturn(name);
        when(node.getProperty(Order.PROP_ID)).thenReturn(id);
        when(node.getProperty(Order.PROP_HREF)).thenReturn(href);
        when(node.getProperty(Order.PROP_BATCH)).thenReturn(batch);
        when(node.getProperty(Order.PROP_INVOICE_REFERENCE)).thenReturn(invoiceReference);
        when(node.getProperty(Order.PROP_LOADING_LIST)).thenReturn(loadingList);
        when(node.getProperty(Order.PROP_OTHER_REFERENCE)).thenReturn(otherReference);
        when(node.getProperty(Order.PROP_UNLOAD_REFERENCE)).thenReturn(unloadReference);

        Order result = OrderFactory.fromNode(node);
        assertEquals(result.getName(), name);
        assertEquals(result.getId(), id);
        assertEquals(result.getHref(), href);
        assertEquals(result.getBatch(), batch);
        assertEquals(result.getInvoiceReference(), invoiceReference);
        assertEquals(result.getLoadingList(), loadingList);
        assertEquals(result.getOtherReference(), otherReference);
        assertEquals(result.getUnloadReference(), unloadReference);


        verify(databaseService, new Times(9)).beginTx();
        verify(transaction, new Times(9)).success();
    }

    /**
     * Test of toHref method, of class OrderFactory.
     */
    @Test
    public void testToHref() {
        System.out.println("toHref");
        assertEquals(OrderFactory.toHref("12345"), "/orders/12345");
        assertEquals(OrderFactory.toHref("67890"), "/orders/67890");
    }
    
}
