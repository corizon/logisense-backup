/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools |
 * Templates and open the template in the editor.
 */
package eu.logisense.server.web;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Location;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import javax.ws.rs.WebApplicationException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.test.TestGraphDatabaseFactory;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author johan
 */
public class LocationResourceNGTest {

    private GraphDatabaseService graphDb;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        graphDb.shutdown();
    }

    @Test
    public void testGetLocation() throws IOException, URISyntaxException {
        createNodes(1);
        LocationResource instance = new LocationResource(graphDb);
        Location location = instance.getLocation("0");

        assertNotNull(location);
        assertNotNull(location.getId());
        assertNotNull(location.getName());
    }

    @Test
    public void testGetLocations() throws IOException, URISyntaxException {
        int size = 3;
        createNodes(size);

        LocationResource instance = new LocationResource(graphDb);
        List<Location> locations = instance.getLocations();
        assertNotNull(locations);
        assertEquals(size, locations.size());

        locations.forEach(location -> {
            assertNotNull(location);
            assertNotNull(location.getId());
            assertNotNull(location.getName());
        });
    }

    @Test
    public void testPostLocation() throws IOException, URISyntaxException {
        Location template = Location.buildLocation()
                .name("name")
                .build();
        assertNull(template.getId());
        LocationResource instance = new LocationResource(graphDb);

        // test response:
        Location created = instance.postLocation(template);
        assertNotNull(created);
        assertNotNull(created.getId());
        assertEquals(template.getName(), created.getName());

        // test stored:
        Location queried = instance.getLocation(created.getId());
        assertNotNull(queried);
        assertEquals(template.getName(), queried.getName());
         assertNotNull(queried.getHref());
    }

    @Test
    public void testUpdateLocation() throws IOException, URISyntaxException {
        createNodes(1);
        Location template = Location.buildLocation()
                .id("0")
                .name("new name")
                .build();
        LocationResource instance = new LocationResource(graphDb);

        // test response:
        Location updated = instance.updateLocation("0", template);
        assertNotNull(updated);
        assertEquals(template.getId(), updated.getId());
        assertEquals(template.getName(), updated.getName());

        // test stored:
        Location queried = instance.getLocation(template.getId());
        assertNotNull(queried);
        assertEquals(template.getName(), queried.getName());
    }

    @Test
    public void testUpdateLocationFromScratch() throws IOException, URISyntaxException {
        Location template = Location.buildLocation()
                .id("0")
                .name("new name")
                .build();
        LocationResource instance = new LocationResource(graphDb);

        template = instance.postLocation(template);

        template.setName("changed");
        // test response:
        Location updated = instance.updateLocation("0", template);
        assertNotNull(updated);
        assertEquals(template.getId(), updated.getId());
        assertEquals("changed", updated.getName());

        // test stored:
        Location queried = instance.getLocation(template.getId());
        assertNotNull(queried);
        assertEquals("changed", queried.getName());
    }

    @Test
    public void testDeleteLocation() throws IOException, URISyntaxException {
        createNodes(1);
        String id = "0";
        LocationResource instance = new LocationResource(graphDb);

        assertNotNull(instance.getLocation(id));
        instance.deleteLocation(id);
        try {
            instance.getLocation(id);
            fail("should have thrown 404");
        } catch (WebApplicationException ex) {
            assertEquals(404, ex.getResponse().getStatus());
        }
    }

    private void createNodes(int n) {
        try (Transaction tx = graphDb.beginTx();) {
            for (int i = 0; i < n; i++) {
                Node node = graphDb.createNode(Labels.LOCATION);
                String id = Integer.toString(i);
                node.setProperty(Location.PROP_ID, id);
                node.setProperty(Location.PROP_NAME, Labels.LOCATION + id);
                node.setProperty(Location.PROP_STATUS, ApprovalStatus.NEW.name());
            }
            tx.success();
        }
    }

}
