/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import java.time.LocalDateTime;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import org.mockito.internal.verification.Times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ShipmentFactoryNGTest extends BaseObjectFactoryTest {

    public ShipmentFactoryNGTest() {
    }

    /**
     * Test of fromNode method, of class ShipmentFactory.
     */
    @Test
    public void testFromNode() {
        System.out.println("fromNode");

        String name = "ikuytrewdfghjhgfdsdfghjgfd";
        String id = "KJHGFDS4564DFGDF345654EFG";
        String href = "/shipments/KJHGFDS4564DFGDF345654EFG";
        Double volume = 4.5678;
        Double weight = 23.4567;
        Integer shippedUnits = 2;
        String note = "some note";
        LocalDateTime deadline = LocalDateTime.now();
        TransportStatus status = TransportStatus.NEW;
        Boolean senderIsLoad = Boolean.TRUE;
        Boolean destinationIsUnload = Boolean.FALSE;
        Integer index = 991;

        when(node.getProperty(Shipment.PROP_NAME)).thenReturn(name);
        when(node.getProperty(Shipment.PROP_ID)).thenReturn(id);
        when(node.getProperty(Shipment.PROP_HREF)).thenReturn(href);
        when(node.getProperty(Shipment.PROP_DEADLINE)).thenReturn(ObjectFactory.toString(deadline));
        when(node.getProperty(Shipment.PROP_NOTE)).thenReturn(note);
        when(node.getProperty(Shipment.PROP_SHIPPED_UNITS)).thenReturn(shippedUnits);
        when(node.getProperty(Shipment.PROP_STATUS)).thenReturn(TransportStatus.NEW.name());
        when(node.getProperty(Shipment.PROP_SENDER_IS_LOAD)).thenReturn(senderIsLoad);
        when(node.getProperty(Shipment.PROP_DESTINATION_IS_UNLOAD)).thenReturn(destinationIsUnload);
        when(node.getProperty(Shipment.PROP_INDEX)).thenReturn(index);

        Shipment result = ShipmentFactory.fromNode(node);
        assertEquals(result.getName(), name);
        assertEquals(result.getId(), id);
        assertEquals(result.getHref(), href);
        assertEquals(result.getDeadline(), deadline);
        assertEquals(result.getShippedUnits(), shippedUnits);
        assertEquals(result.getNote(), note);
        assertEquals(result.getStatus(), status);
        assertEquals(result.getSenderIsLoad(), senderIsLoad);
        assertEquals(result.getDestinationIsUnload(), destinationIsUnload);
        assertEquals(result.getIndex(), index);

        verify(databaseService, new Times(10)).beginTx();
        verify(transaction, new Times(10)).success();
    }

    /**
     * Test of toHref method, of class ShipmentFactory.
     */
    @Test
    public void testToHref() {
        System.out.println("toHref");
        assertEquals(ShipmentFactory.toHref("12345"), "/shipments/12345");
        assertEquals(ShipmentFactory.toHref("67890"), "/shipments/67890");
    }

}
