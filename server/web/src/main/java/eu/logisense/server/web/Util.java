/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

import eu.logisense.api.Client;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Container;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.OrderLineType;
import eu.logisense.api.Origin;
import eu.logisense.api.Ride;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import static eu.logisense.server.web.Labels.*;
import static eu.logisense.server.web.Relations.*;
import eu.logisense.server.web.model.ObjectFactory;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.UUID;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class Util {

    private static final Logger log = LoggerFactory.getLogger(Util.class);

    public static void initTestSet(GraphDatabaseService graphDb) {
        try (Transaction tx = graphDb.beginTx()) {
            log.trace("initializing test storage");
            Node client1 = graphDb.createNode(CLIENT);
            client1.setProperty(Client.PROP_ID, "1");
            client1.setProperty(Client.PROP_NAME, "client 1");
            client1.setProperty(Client.PROP_HREF, "clients/1");
            client1.setProperty(Client.PROP_BIC_CODE, "INGBNL2A");
            client1.setProperty(Client.PROP_CHAMBER_OF_COMMERCE, "87654321");
            client1.setProperty(Client.PROP_CHAMBER_OF_COMMERCE_CITY, "Groningen");
            client1.setProperty(Client.PROP_CHARGE_VAT, Boolean.FALSE);
            client1.setProperty(Client.PROP_CURRENCY, "GBP");
            client1.setProperty(Client.PROP_IBAN_CODE, "UK52ABCD0123456789");
            client1.setProperty(Client.PROP_VAT_NUMBER, "UK123456788B01");
            client1.setProperty(Client.PROP_STATUS, ApprovalStatus.APPROVED.name());
            client1.setProperty(Client.PROP_EXTERNAL_ID, UUID.randomUUID().toString());

            Node client2 = graphDb.createNode(CLIENT);
            client2.setProperty(Client.PROP_ID, "2");
            client2.setProperty(Client.PROP_NAME, "client 2");
            client2.setProperty(Client.PROP_HREF, "clients/2");

            client2.setProperty(Client.PROP_BIC_CODE, "INGBNL2A");
            client2.setProperty(Client.PROP_CHAMBER_OF_COMMERCE, "87654321");
            client2.setProperty(Client.PROP_CHAMBER_OF_COMMERCE_CITY, "Groningen");
            client2.setProperty(Client.PROP_CHARGE_VAT, Boolean.FALSE);
            client2.setProperty(Client.PROP_CURRENCY, "GBP");
            client2.setProperty(Client.PROP_IBAN_CODE, "UK52ABCD0123456789");
            client2.setProperty(Client.PROP_VAT_NUMBER, "UK123456788B01");
            client2.setProperty(Client.PROP_STATUS, ApprovalStatus.APPROVED.name());
            client2.setProperty(Client.PROP_EXTERNAL_ID, UUID.randomUUID().toString());

            Node client3 = graphDb.createNode(CLIENT);
            client3.setProperty(Client.PROP_ID, "3");
            client3.setProperty(Client.PROP_NAME, "client 3");
            client3.setProperty(Client.PROP_HREF, "clients/3");

            client3.setProperty(Client.PROP_BIC_CODE, "INGBNL2A");
            client3.setProperty(Client.PROP_CHAMBER_OF_COMMERCE, "87654321");
            client3.setProperty(Client.PROP_CHAMBER_OF_COMMERCE_CITY, "Groningen");
            client3.setProperty(Client.PROP_CHARGE_VAT, Boolean.FALSE);
            client3.setProperty(Client.PROP_CURRENCY, "GBP");
            client3.setProperty(Client.PROP_IBAN_CODE, "UK52ABCD0123456789");
            client3.setProperty(Client.PROP_VAT_NUMBER, "UK123456788B01");
            client3.setProperty(Client.PROP_STATUS, ApprovalStatus.APPROVED.name());
            client3.setProperty(Client.PROP_EXTERNAL_ID, UUID.randomUUID().toString());

            Node client4 = graphDb.createNode(CLIENT);
            client4.setProperty(Client.PROP_ID, "4");
            client4.setProperty(Client.PROP_NAME, "client 4");
            client4.setProperty(Client.PROP_HREF, "clients/4");

            client4.setProperty(Client.PROP_BIC_CODE, "INGBNL2A");
            client4.setProperty(Client.PROP_CHAMBER_OF_COMMERCE, "87654321");
            client4.setProperty(Client.PROP_CHAMBER_OF_COMMERCE_CITY, "Groningen");
            client4.setProperty(Client.PROP_CHARGE_VAT, Boolean.FALSE);
            client4.setProperty(Client.PROP_CURRENCY, "GBP");
            client4.setProperty(Client.PROP_IBAN_CODE, "UK52ABCD0123456789");
            client4.setProperty(Client.PROP_VAT_NUMBER, "UK123456788B01");
            client4.setProperty(Client.PROP_STATUS, ApprovalStatus.APPROVED.name());

            Node orderLine1 = graphDb.createNode(ORDER_LINE);
            orderLine1.setProperty(OrderLine.PROP_DESCRIPTION, "description order line 1");
            orderLine1.setProperty(OrderLine.PROP_HREF, "/orders/1/lines/1");
            orderLine1.setProperty(OrderLine.PROP_ID, "1");
            orderLine1.setProperty(OrderLine.PROP_NAME, "line 1");
            orderLine1.setProperty(OrderLine.PROP_AMOUNT, 2);
            orderLine1.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, 12.34);
            orderLine1.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, "EUR");
            orderLine1.setProperty(OrderLine.PROP_LENGTH, 1.45);
            orderLine1.setProperty(OrderLine.PROP_WIDTH, 1.45);
            orderLine1.setProperty(OrderLine.PROP_HEIGHT, 1.45);
            orderLine1.setProperty(OrderLine.PROP_TYPE, OrderLineType.CUSTOM.name());
            orderLine1.setProperty(OrderLine.PROP_SWAP_PALLETS, Boolean.FALSE);
            orderLine1.setProperty(OrderLine.PROP_STACKABLE, Boolean.TRUE);
            orderLine1.setProperty(OrderLine.PROP_WEIGHT, 0.45);

            Node orderLine2 = graphDb.createNode(ORDER_LINE);
            orderLine2.setProperty(OrderLine.PROP_DESCRIPTION, "description order line 2");
            orderLine2.setProperty(OrderLine.PROP_HREF, "/orders/1/lines/2");
            orderLine2.setProperty(OrderLine.PROP_ID, "2");
            orderLine2.setProperty(OrderLine.PROP_NAME, "line 2");
            orderLine2.setProperty(OrderLine.PROP_AMOUNT, 2);
            orderLine2.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, 12.34);
            orderLine2.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, "EUR");
            orderLine2.setProperty(OrderLine.PROP_LENGTH, 1.45);
            orderLine2.setProperty(OrderLine.PROP_WIDTH, 1.45);
            orderLine2.setProperty(OrderLine.PROP_HEIGHT, 1.45);
            orderLine2.setProperty(OrderLine.PROP_TYPE, OrderLineType.EURO_PALLET.name());
            orderLine2.setProperty(OrderLine.PROP_SWAP_PALLETS, Boolean.FALSE);
            orderLine2.setProperty(OrderLine.PROP_STACKABLE, Boolean.TRUE);
            orderLine2.setProperty(OrderLine.PROP_WEIGHT, 0.45);

            Node orderLine3 = graphDb.createNode(ORDER_LINE);
            orderLine3.setProperty(OrderLine.PROP_DESCRIPTION, "description order line 3");
            orderLine3.setProperty(OrderLine.PROP_HREF, "/orders/2/lines/3");
            orderLine3.setProperty(OrderLine.PROP_ID, "3");
            orderLine3.setProperty(OrderLine.PROP_NAME, "line 3");
            orderLine3.setProperty(OrderLine.PROP_AMOUNT, 2);
            orderLine3.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, 12.34);
            orderLine3.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, "EUR");
            orderLine3.setProperty(OrderLine.PROP_LENGTH, 1.45);
            orderLine3.setProperty(OrderLine.PROP_WIDTH, 1.45);
            orderLine3.setProperty(OrderLine.PROP_HEIGHT, 1.45);
            orderLine3.setProperty(OrderLine.PROP_TYPE, OrderLineType.EURO_PALLET.name());
            orderLine3.setProperty(OrderLine.PROP_SWAP_PALLETS, Boolean.FALSE);
            orderLine3.setProperty(OrderLine.PROP_STACKABLE, Boolean.TRUE);
            orderLine3.setProperty(OrderLine.PROP_WEIGHT, 0.45);

            Node orderLine4 = graphDb.createNode(ORDER_LINE);
            orderLine4.setProperty(OrderLine.PROP_DESCRIPTION, "description order line 4");
            orderLine4.setProperty(OrderLine.PROP_HREF, "/orders/2/lines/4");
            orderLine4.setProperty(OrderLine.PROP_ID, "4");
            orderLine4.setProperty(OrderLine.PROP_NAME, "line 4");
            orderLine4.setProperty(OrderLine.PROP_AMOUNT, 2);
            orderLine4.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, 12.34);
            orderLine4.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, "EUR");
            orderLine4.setProperty(OrderLine.PROP_LENGTH, 1.45);
            orderLine4.setProperty(OrderLine.PROP_WIDTH, 1.45);
            orderLine4.setProperty(OrderLine.PROP_HEIGHT, 1.45);
            orderLine4.setProperty(OrderLine.PROP_TYPE, OrderLineType.BLOCK_PALLET.name());
            orderLine4.setProperty(OrderLine.PROP_SWAP_PALLETS, Boolean.FALSE);
            orderLine4.setProperty(OrderLine.PROP_STACKABLE, Boolean.TRUE);
            orderLine4.setProperty(OrderLine.PROP_WEIGHT, 0.45);

            Node orderLine5 = graphDb.createNode(ORDER_LINE);
            orderLine5.setProperty(OrderLine.PROP_DESCRIPTION, "description order line 5");
            orderLine5.setProperty(OrderLine.PROP_HREF, "/orders/3/lines/5");
            orderLine5.setProperty(OrderLine.PROP_ID, "5");
            orderLine5.setProperty(OrderLine.PROP_NAME, "line 5");
            orderLine5.setProperty(OrderLine.PROP_AMOUNT, 2);
            orderLine5.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, 12.34);
            orderLine5.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, "EUR");
            orderLine5.setProperty(OrderLine.PROP_LENGTH, 1.45);
            orderLine5.setProperty(OrderLine.PROP_WIDTH, 1.45);
            orderLine5.setProperty(OrderLine.PROP_HEIGHT, 1.45);
            orderLine5.setProperty(OrderLine.PROP_TYPE, OrderLineType.CUSTOM.name());
            orderLine5.setProperty(OrderLine.PROP_SWAP_PALLETS, Boolean.FALSE);
            orderLine5.setProperty(OrderLine.PROP_STACKABLE, Boolean.TRUE);
            orderLine5.setProperty(OrderLine.PROP_WEIGHT, 0.45);

            Node orderLine6 = graphDb.createNode(ORDER_LINE);
            orderLine6.setProperty(OrderLine.PROP_DESCRIPTION, "description order line 6");
            orderLine6.setProperty(OrderLine.PROP_HREF, "/orders/3/lines/6");
            orderLine6.setProperty(OrderLine.PROP_ID, "6");
            orderLine6.setProperty(OrderLine.PROP_NAME, "line 6");
            orderLine6.setProperty(OrderLine.PROP_AMOUNT, 2);
            orderLine6.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, 12.34);
            orderLine6.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, "EUR");
            orderLine6.setProperty(OrderLine.PROP_LENGTH, 1.45);
            orderLine6.setProperty(OrderLine.PROP_WIDTH, 1.45);
            orderLine6.setProperty(OrderLine.PROP_HEIGHT, 1.45);
            orderLine6.setProperty(OrderLine.PROP_TYPE, OrderLineType.EURO_PALLET.name());
            orderLine6.setProperty(OrderLine.PROP_SWAP_PALLETS, Boolean.FALSE);
            orderLine6.setProperty(OrderLine.PROP_STACKABLE, Boolean.TRUE);
            orderLine6.setProperty(OrderLine.PROP_WEIGHT, 0.45);

            Node orderLine7 = graphDb.createNode(ORDER_LINE);
            orderLine7.setProperty(OrderLine.PROP_DESCRIPTION, "description order line 7");
            orderLine7.setProperty(OrderLine.PROP_HREF, "/orders/4/lines/7");
            orderLine7.setProperty(OrderLine.PROP_ID, "7");
            orderLine7.setProperty(OrderLine.PROP_NAME, "line 7");
            orderLine7.setProperty(OrderLine.PROP_AMOUNT, 2);
            orderLine7.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, 12.34);
            orderLine7.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, "EUR");
            orderLine7.setProperty(OrderLine.PROP_LENGTH, 1.45);
            orderLine7.setProperty(OrderLine.PROP_WIDTH, 1.45);
            orderLine7.setProperty(OrderLine.PROP_HEIGHT, 1.45);
            orderLine7.setProperty(OrderLine.PROP_TYPE, OrderLineType.BLOCK_PALLET.name());
            orderLine7.setProperty(OrderLine.PROP_SWAP_PALLETS, Boolean.FALSE);
            orderLine7.setProperty(OrderLine.PROP_STACKABLE, Boolean.TRUE);
            orderLine7.setProperty(OrderLine.PROP_WEIGHT, 0.45);

            Node orderLine8 = graphDb.createNode(ORDER_LINE);
            orderLine8.setProperty(OrderLine.PROP_DESCRIPTION, "description order line 8");
            orderLine8.setProperty(OrderLine.PROP_HREF, "/orders/4/lines/8");
            orderLine8.setProperty(OrderLine.PROP_ID, "8");
            orderLine8.setProperty(OrderLine.PROP_NAME, "line 8");
            orderLine8.setProperty(OrderLine.PROP_AMOUNT, 2);
            orderLine8.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, 12.34);
            orderLine8.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, "EUR");
            orderLine8.setProperty(OrderLine.PROP_LENGTH, 1.45);
            orderLine8.setProperty(OrderLine.PROP_WIDTH, 1.45);
            orderLine8.setProperty(OrderLine.PROP_HEIGHT, 1.45);
            orderLine8.setProperty(OrderLine.PROP_TYPE, OrderLineType.CUSTOM.name());
            orderLine8.setProperty(OrderLine.PROP_SWAP_PALLETS, Boolean.FALSE);
            orderLine8.setProperty(OrderLine.PROP_STACKABLE, Boolean.TRUE);
            orderLine8.setProperty(OrderLine.PROP_WEIGHT, 0.45);

            Node order1 = graphDb.createNode(ORDER);
            order1.setProperty(Order.PROP_ID, "1");
            order1.setProperty(Order.PROP_NAME, "order1");
            order1.setProperty(Order.PROP_HREF, "/orders/1");
            order1.setProperty(Order.PROP_BATCH, "2134567");
            order1.setProperty(Order.PROP_LOADING_LIST, "loading list 1");
            order1.setProperty(Order.PROP_INVOICE_REFERENCE, "IR1234");
            order1.setProperty(Order.PROP_OTHER_REFERENCE, "OR1234");
            order1.setProperty(Order.PROP_UNLOAD_REFERENCE, "UR1234");
            order1.setProperty(Order.PROP_EXTERNAL_ID, "ext order 1");
            client1.createRelationshipTo(order1, ORDERER);
            client1.createRelationshipTo(order1, DEBITOR);
            order1.createRelationshipTo(orderLine1, CONTAINS);
            order1.createRelationshipTo(orderLine2, CONTAINS);

            Node order2 = graphDb.createNode(Labels.ORDER);
            order2.setProperty(Order.PROP_ID, "2");
            order2.setProperty(Order.PROP_NAME, "order2");
            order2.setProperty(Order.PROP_HREF, "/orders/2");
            order2.setProperty(Order.PROP_BATCH, "09876544");
            order2.setProperty(Order.PROP_LOADING_LIST, "loading list 2");
            order2.setProperty(Order.PROP_INVOICE_REFERENCE, "IR1234");
            order2.setProperty(Order.PROP_OTHER_REFERENCE, "OR1234");
            order2.setProperty(Order.PROP_UNLOAD_REFERENCE, "UR1234");
            client2.createRelationshipTo(order2, ORDERER);
            client2.createRelationshipTo(order2, DEBITOR);
            order2.createRelationshipTo(orderLine3, CONTAINS);
            order2.createRelationshipTo(orderLine4, CONTAINS);

            Node order3 = graphDb.createNode(Labels.ORDER);
            order3.setProperty(Order.PROP_ID, "3");
            order3.setProperty(Order.PROP_NAME, "order3");
            order3.setProperty(Order.PROP_HREF, "/orders/3");
            order3.setProperty(Order.PROP_BATCH, "9345793475");
            order3.setProperty(Order.PROP_LOADING_LIST, "loading list 3");
            order3.setProperty(Order.PROP_INVOICE_REFERENCE, "IR1234");
            order3.setProperty(Order.PROP_OTHER_REFERENCE, "OR1234");
            order3.setProperty(Order.PROP_UNLOAD_REFERENCE, "UR1234");
            client3.createRelationshipTo(order3, ORDERER);
            client3.createRelationshipTo(order3, DEBITOR);
            order3.createRelationshipTo(orderLine5, CONTAINS);
            order3.createRelationshipTo(orderLine6, CONTAINS);

            Node order4 = graphDb.createNode(Labels.ORDER);
            order4.setProperty(Order.PROP_ID, "4");
            order4.setProperty(Order.PROP_NAME, "order4");
            order4.setProperty(Order.PROP_HREF, "/orders/4");
            order4.setProperty(Order.PROP_BATCH, "9345sfsdfsdf475");
            order4.setProperty(Order.PROP_LOADING_LIST, "loading list 4");
            order4.setProperty(Order.PROP_INVOICE_REFERENCE, "IR1234");
            order4.setProperty(Order.PROP_OTHER_REFERENCE, "OR1234");
            order4.setProperty(Order.PROP_UNLOAD_REFERENCE, "UR1234");
            client4.createRelationshipTo(order4, ORDERER);
            client4.createRelationshipTo(order4, DEBITOR);
            order4.createRelationshipTo(orderLine7, CONTAINS);
            order4.createRelationshipTo(orderLine8, CONTAINS);

            Origin originMadrid = Origin.builder()
                    .address("Plaza puerta del Sol")
                    .city("Madrid")
                    .country("ES")
                    .id("1")
                    .latitude(40.4165597)
                    .longitude(-3.7037327)
                    .name("Plaza puerta del Sol")
                    .postalcode("28009")
                    .build();

            LoadLocation loadMadrid = LoadLocation.builder()
                    .address("Plaza puerta del Sol")
                    .city("Madrid")
                    .country("ES")
                    .id("2")
                    .latitude(40.4165597)
                    .longitude(-3.7037327)
                    .name("Plaza puerta del Sol")
                    .postalcode("28009")
                    .timeFrameFrom(LocalDateTime.of(2015, Month.MARCH, 5, 8, 0))
                    .timeFrameTo(LocalDateTime.of(2015, Month.MARCH, 5, 16, 0))
                    .build();

            Node madridNode = graphDb.createNode(Labels.LOCATION);
            madridNode.setProperty(Location.PROP_LATITUDE, originMadrid.getLatitude());
            madridNode.setProperty(Location.PROP_ID, originMadrid.getId());
            madridNode.setProperty(Location.PROP_LONGITUDE, originMadrid.getLongitude());
            madridNode.setProperty(Location.PROP_CITY, originMadrid.getCity());
            madridNode.setProperty(Location.PROP_COUNTRY, originMadrid.getCountry());
            madridNode.setProperty(Location.PROP_NAME, originMadrid.getName());
            madridNode.setProperty(Location.PROP_POSTALCODE, originMadrid.getPostalcode());
            madridNode.setProperty(Location.PROP_ADDRESS, originMadrid.getAddress());
            madridNode.setProperty(Location.PROP_EMAIL, "example@example.com");
            madridNode.setProperty(Location.PROP_PHONE, "876543456789");
            madridNode.setProperty(Location.PROP_STATUS, ApprovalStatus.NEW.name());
            order1.createRelationshipTo(madridNode, DESTINATION);
            order2.createRelationshipTo(madridNode, DESTINATION);
            order3.createRelationshipTo(madridNode, DESTINATION);
            order4.createRelationshipTo(madridNode, ORIGIN);

            client1.createRelationshipTo(madridNode, RESIDES);
            client3.createRelationshipTo(madridNode, RESIDES);

            Destination destGroningen = Destination.builder()
                    .address("Grote Markt, 1")
                    .city("Groningen")
                    .country("NL")
                    .id("3")
                    .latitude(53.218635)
                    .longitude(6.566443)
                    .name("Grote markt")
                    .postalcode("9712")
                    .build();

            UnloadLocation unloadGroningen = UnloadLocation.builder()
                    .address("Grote Markt, 1")
                    .city("Groningen")
                    .country("NL")
                    .id("4")
                    .latitude(53.218635)
                    .longitude(6.566443)
                    .name("Grote markt")
                    .postalcode("9712")
                    .timeFrameFrom(LocalDateTime.of(2016, Month.AUGUST, 12, 8, 0))
                    .timeFrameTo(LocalDateTime.of(2016, Month.AUGUST, 12, 16, 0))
                    .build();

            Node groningenNode = graphDb.createNode(Labels.LOCATION);
            groningenNode.setProperty(Location.PROP_LATITUDE, destGroningen.getLatitude());
            groningenNode.setProperty(Location.PROP_ID, destGroningen.getId());
            groningenNode.setProperty(Location.PROP_LONGITUDE, destGroningen.getLongitude());
            groningenNode.setProperty(Location.PROP_CITY, destGroningen.getCity());
            groningenNode.setProperty(Location.PROP_COUNTRY, destGroningen.getCountry());
            groningenNode.setProperty(Location.PROP_NAME, destGroningen.getName());
            groningenNode.setProperty(Location.PROP_POSTALCODE, destGroningen.getPostalcode());
            groningenNode.setProperty(Location.PROP_ADDRESS, destGroningen.getAddress());
            groningenNode.setProperty(Location.PROP_EMAIL, "example2@example.com");
            groningenNode.setProperty(Location.PROP_PHONE, "654345678");
            groningenNode.setProperty(Location.PROP_STATUS, ApprovalStatus.NEW.name());

            order1.createRelationshipTo(groningenNode, ORIGIN);
            order2.createRelationshipTo(groningenNode, ORIGIN);
            order3.createRelationshipTo(groningenNode, ORIGIN);
            order4.createRelationshipTo(groningenNode, DESTINATION);

            client2.createRelationshipTo(groningenNode, RESIDES);
            client4.createRelationshipTo(groningenNode, RESIDES);

            Node shipment1 = graphDb.createNode(Labels.SHIPMENT);
            shipment1.setProperty(Shipment.PROP_ID, "1");
            shipment1.setProperty(Shipment.PROP_NAME, "shipment 1");
            shipment1.setProperty(Shipment.PROP_HREF, "/shipments/1");
            shipment1.setProperty(Shipment.PROP_CREATED, LocalDateTime.now().minusDays(1).toEpochSecond(ZoneOffset.UTC));
            shipment1.setProperty(Shipment.PROP_DEADLINE, ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 2, 0, 0)));
            shipment1.setProperty(Shipment.PROP_NOTE, "some note");
            shipment1.setProperty(Shipment.PROP_SHIPPED_UNITS, 4);
            shipment1.setProperty(Shipment.PROP_STATUS, TransportStatus.NEW.toString());
            shipment1.setProperty(Shipment.PROP_SENDER_IS_LOAD, Boolean.TRUE);

            Node shipment2 = graphDb.createNode(Labels.SHIPMENT);
            shipment2.setProperty(Shipment.PROP_ID, "2");
            shipment2.setProperty(Shipment.PROP_NAME, "shipment 2");
            shipment2.setProperty(Shipment.PROP_HREF, "/shipments/2");
            shipment2.setProperty(Shipment.PROP_CREATED, LocalDateTime.now().minusDays(2).toEpochSecond(ZoneOffset.UTC));
            shipment2.setProperty(Shipment.PROP_DEADLINE, ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 2, 0, 0)));
            shipment2.setProperty(Shipment.PROP_NOTE, "some note");
            shipment2.setProperty(Shipment.PROP_SHIPPED_UNITS, 4);
            shipment2.setProperty(Shipment.PROP_STATUS, TransportStatus.NEW.toString());
            shipment2.setProperty(Shipment.PROP_SENDER_IS_LOAD, Boolean.TRUE);

            Node shipment3 = graphDb.createNode(Labels.SHIPMENT);
            shipment3.setProperty(Shipment.PROP_ID, "3");
            shipment3.setProperty(Shipment.PROP_NAME, "shipment 3");
            shipment3.setProperty(Shipment.PROP_HREF, "/shipments/3");
            shipment3.setProperty(Shipment.PROP_CREATED, LocalDateTime.now().minusDays(3).toEpochSecond(ZoneOffset.UTC));
            shipment3.setProperty(Shipment.PROP_DEADLINE, ObjectFactory.toString(LocalDateTime.of(2016, Month.JULY, 21, 0, 0)));
            shipment3.setProperty(Shipment.PROP_NOTE, "some note for shipment 3");
            shipment3.setProperty(Shipment.PROP_SHIPPED_UNITS, 4);
            shipment3.setProperty(Shipment.PROP_STATUS, TransportStatus.NEW.toString());
            shipment3.setProperty(Shipment.PROP_SENDER_IS_LOAD, Boolean.TRUE);

            Node shipment4 = graphDb.createNode(Labels.SHIPMENT);
            shipment4.setProperty(Shipment.PROP_ID, "4");
            shipment4.setProperty(Shipment.PROP_NAME, "shipment 4");
            shipment4.setProperty(Shipment.PROP_HREF, "/shipments/4");
            shipment4.setProperty(Shipment.PROP_CREATED, LocalDateTime.now().minusDays(4).toEpochSecond(ZoneOffset.UTC));
            shipment4.setProperty(Shipment.PROP_DEADLINE, ObjectFactory.toString(LocalDateTime.of(2016, Month.JULY, 21, 0, 0)));
            shipment4.setProperty(Shipment.PROP_NOTE, "some note for shipment 4");
            shipment4.setProperty(Shipment.PROP_SHIPPED_UNITS, 4);
            shipment4.setProperty(Shipment.PROP_STATUS, TransportStatus.NEW.toString());
            shipment4.setProperty(Shipment.PROP_SENDER_IS_LOAD, Boolean.TRUE);

            order1.createRelationshipTo(shipment1, Relations.SHIPPED_AS);
            order2.createRelationshipTo(shipment2, Relations.SHIPPED_AS);
            order3.createRelationshipTo(shipment3, Relations.SHIPPED_AS);
            order4.createRelationshipTo(shipment4, Relations.SHIPPED_AS);

            Node container1 = graphDb.createNode(Labels.CONTAINER);
            container1.setProperty(Container.PROP_ID, "019");
            container1.setProperty(Container.PROP_NAME, "container 1");

            Node container2 = graphDb.createNode(Labels.CONTAINER);
            container2.setProperty(Container.PROP_ID, "121");
            container2.setProperty(Container.PROP_NAME, "Container 2");

            Node container3 = graphDb.createNode(Labels.CONTAINER);
            container3.setProperty(Container.PROP_ID, "999");
            container3.setProperty(Container.PROP_NAME, "Container 3");

            Relationship s1c1 = shipment1.createRelationshipTo(container1, Relations.SHIPPED_IN);
            shipment1.setProperty(Shipment.PROP_INDEX, 1);

            Relationship s2c1 = shipment2.createRelationshipTo(container1, Relations.SHIPPED_IN);
            shipment2.setProperty(Shipment.PROP_INDEX, 2);

            Relationship s3c2 = shipment3.createRelationshipTo(container2, Relations.SHIPPED_IN);
            shipment3.setProperty(Shipment.PROP_INDEX, 1);

            Node ride = graphDb.createNode(Labels.RIDE);
            ride.setProperty(Ride.PROP_ID, "1");
            ride.setProperty(Ride.PROP_NAME, "my ride");
            ride.setProperty(Ride.PROP_HREF, "/rides/1");
            ride.setProperty(Ride.PROP_CREATED, LocalDateTime.now().minusDays(1).toEpochSecond(ZoneOffset.UTC));
            ride.setProperty(Ride.PROP_STATUS, TransportStatus.NEW.name());
            ride.setProperty(Ride.PROP_ETD, ObjectFactory.toString(LocalDateTime.of(2016, Month.JULY, 12, 0, 0)));
            ride.setProperty(Ride.PROP_EXTERNAL_ID, "ext ride 1");
            container1.createRelationshipTo(ride, Relations.PART_OF);
            container2.createRelationshipTo(ride, Relations.PART_OF);

            client1.createRelationshipTo(ride, CREDITOR);

            Node ride2 = graphDb.createNode(Labels.RIDE);
            ride2.setProperty(Ride.PROP_ID, "2");
            ride2.setProperty(Ride.PROP_NAME, "my ride2");
            ride2.setProperty(Ride.PROP_HREF, "/rides/2");
            ride2.setProperty(Ride.PROP_CREATED, LocalDateTime.now().minusDays(2).toEpochSecond(ZoneOffset.UTC));
            ride2.setProperty(Ride.PROP_STATUS, TransportStatus.NEW.name());
            ride2.setProperty(Ride.PROP_ETD, ObjectFactory.toString(LocalDateTime.of(2016, Month.JULY, 12, 0, 0)));
            container3.createRelationshipTo(ride2, Relations.PART_OF);

            client3.createRelationshipTo(ride2, CREDITOR);

            Relationship load1 = shipment1.createRelationshipTo(madridNode, LOAD);
            Relationship load2 = shipment2.createRelationshipTo(madridNode, LOAD);
            Relationship load3 = shipment3.createRelationshipTo(madridNode, LOAD);
            Relationship load4 = shipment4.createRelationshipTo(groningenNode, LOAD);

            load1.setProperty(LoadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 19, 8, 0)));
            load1.setProperty(LoadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 19, 16, 0)));
            load2.setProperty(LoadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.of(2016, Month.JUNE, 10, 8, 0)));
            load2.setProperty(LoadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.of(2016, Month.JUNE, 10, 16, 0)));
            load3.setProperty(LoadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.of(2016, Month.JUNE, 11, 8, 0)));
            load3.setProperty(LoadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.of(2016, Month.JUNE, 11, 16, 0)));
            load4.setProperty(LoadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.of(2017, Month.JUNE, 11, 8, 0)));
            load4.setProperty(LoadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.of(2017, Month.JUNE, 11, 16, 0)));

            Relationship unload1 = shipment1.createRelationshipTo(groningenNode, UNLOAD);
            Relationship unload2 = shipment2.createRelationshipTo(groningenNode, UNLOAD);
            Relationship unload3 = shipment3.createRelationshipTo(groningenNode, UNLOAD);
            Relationship unload4 = shipment4.createRelationshipTo(madridNode, UNLOAD);

            unload1.setProperty(UnloadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 19, 8, 0)));
            unload1.setProperty(UnloadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.of(2016, Month.MARCH, 19, 16, 0)));
            unload2.setProperty(UnloadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.of(2016, Month.JUNE, 10, 8, 0)));
            unload2.setProperty(UnloadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.of(2016, Month.JUNE, 10, 23, 59)));
            unload3.setProperty(UnloadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.of(2016, Month.JUNE, 10, 8, 0)));
            unload3.setProperty(UnloadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.of(2016, Month.JUNE, 10, 22, 59)));
            unload4.setProperty(UnloadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.of(2017, Month.JUNE, 10, 8, 0)));
            unload4.setProperty(UnloadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.of(2017, Month.JUNE, 10, 22, 59)));

            tx.success();
        }
    }

}
