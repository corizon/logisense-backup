/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web;

import au.com.bytecode.opencsv.CSVWriter;
import eu.logisense.api.Order;
import eu.logisense.api.Ride;
import eu.logisense.api.Shipment;
import static eu.logisense.server.web.BaseNeo4jService.graphDb;
import static eu.logisense.server.web.Labels.ORDER;
import static eu.logisense.server.web.Labels.RIDE;
import static eu.logisense.server.web.Labels.SHIPMENT;
import static eu.logisense.server.web.Relations.SHIPPED_AS;
import eu.logisense.server.web.model.OrderFactory;
import eu.logisense.server.web.model.RideFactory;
import eu.logisense.server.web.model.ShipmentFactory;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author johan
 */
@javax.ws.rs.Path("/csvexport")
public class CSVExportResource extends BaseNeo4jService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CSVExportResource.class);
    private static final String CSV_MIME = "text/csv";
    
    public CSVExportResource(GraphDatabaseService graphDb) throws IOException, URISyntaxException {
        super(graphDb);
    }

    public CSVExportResource() throws IOException, URISyntaxException {
    }
    
    @GET
    @Produces({CSV_MIME})
    @javax.ws.rs.Path("/shipments")
    public String exportShipments(@QueryParam("from") String from, @QueryParam("to") String to) {
        long esFrom = epochSeconds(from, defaultFrom());
        long esTo = epochSeconds(to, defaultTo());
        List<Shipment> shipments = getShipments(esFrom, esTo);
        return dumpShipments(shipments);
    }

    @GET
    @Produces({CSV_MIME})
    @javax.ws.rs.Path("/rides")
    public String exportRides(@QueryParam("from") String from, @QueryParam("to") String to) {
        long esFrom = epochSeconds(from, defaultFrom());
        long esTo = epochSeconds(to, defaultTo());
        List<Ride> rides = getRides(esFrom, esTo);
        return dumpRides(rides);
    }
    
    private LocalDateTime defaultFrom() {
        return LocalDateTime.now().minusDays(1).truncatedTo(ChronoUnit.DAYS);
    }

    private LocalDateTime defaultTo() {
        return LocalDateTime.now();
    }

    private long epochSeconds(String iso, LocalDateTime fallback) {
        LocalDateTime ldt = isEmpty(iso) ? fallback : LocalDateTime.parse(iso, DateTimeFormatter.ISO_DATE_TIME);
        return ldt.toEpochSecond(ZoneOffset.UTC);
    }
    
    private boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }
    
    private List<Shipment> getShipments(long esFrom, long esTo) {
        LOGGER.debug("getShipments");

        List<Shipment> shipments = new ArrayList<>();
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("esFrom", esFrom);
        parameters.put("esTo", esTo);

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (s:" + SHIPMENT + ")<-[:" + SHIPPED_AS + "]-(o:" + ORDER + ")"
                        + " WHERE s.created is not null and s.created > {esFrom} and s.created < {esTo}"
                        + " RETURN s, o", parameters)) {
            
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                Shipment shipment = ShipmentFactory.fromNode((Node) row.get("s"));
                Order order = OrderFactory.fromNode((Node) row.get("o"));
                shipment.setOrder(order);
                shipments.add(shipment);
            }
            tx.success();
        }
        return shipments;
    }
    
    private List<Ride> getRides(long from, long to) {
        LOGGER.debug("getRides");

        List<Ride> rides = new ArrayList<>();
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("esFrom", from);
        parameters.put("esTo", to);

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (s:" + RIDE + ")"
                        + " WHERE s.created is not null and s.created > {esFrom} and s.created < {esTo}"
                        + " RETURN s", parameters)) {
            
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                Ride ride = RideFactory.fromNode((Node) row.get("s"));
                rides.add(ride);
            }
            tx.success();
        }
        return rides;
    }
    
    protected static final String[] SHIPMENTS_HEADER = new String[] {"shipmentId", "orderId", "status", "externalId"};
    
    protected String dumpShipments(List<Shipment> shipments) {
        try (StringWriter sw = new StringWriter();
                CSVWriter csv = new CSVWriter(sw)) {
            csv.writeNext(SHIPMENTS_HEADER);
            shipments.forEach(s -> {
                Order o = s.getOrder();
                csv.writeNext(new String[]{
                    s.getId(),
                    o.getId(),
                    s.getStatus() == null ? "" : s.getStatus().name(),
                    o.getExternalId(),
                });
            });
            return sw.toString();
        } catch (IOException ex) {
            LOGGER.error("error creating shipments csv", ex);
            return "error creating shipments csv: " + ex;
        }
    }
    
    protected static final String[] RIDES_HEADER = new String[] {"id", "status", "externalId"};
    
    protected String dumpRides(List<Ride> rides) {
        try (StringWriter sw = new StringWriter();
                CSVWriter csv = new CSVWriter(sw)) {
            csv.writeNext(RIDES_HEADER);
            rides.forEach(r -> {
                csv.writeNext(new String[]{
                    r.getId(),
                    r.getStatus() == null ? "" : r.getStatus().name(),
                    r.getExternalId(),
                });
            });
            return sw.toString();
        } catch (IOException ex) {
            LOGGER.error("error creating rides csv", ex);
            return "error creating rides csv: " + ex;
        }
    }
}
