/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.UnloadLocation;
import static eu.logisense.server.web.model.ObjectFactory.getApprovalStatusValue;
import static eu.logisense.server.web.model.ObjectFactory.getDateTimeValue;
import static eu.logisense.server.web.model.ObjectFactory.getDoubleValue;
import static eu.logisense.server.web.model.ObjectFactory.getStringValue;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class UnloadLocationFactory {
        public static UnloadLocation fromNode(Node node,Relationship relationship){
        return UnloadLocation.builder()
                .id(getStringValue(node, UnloadLocation.PROP_ID))
                .address(getStringValue(node, UnloadLocation.PROP_ADDRESS))
                .name(getStringValue(node, UnloadLocation.PROP_NAME))
                .href(getStringValue(node, UnloadLocation.PROP_HREF))                    
                .city(getStringValue(node, UnloadLocation.PROP_CITY))
                .country(getStringValue(node, UnloadLocation.PROP_COUNTRY))
                .latitude(getDoubleValue(node, UnloadLocation.PROP_LATITUDE))
                .longitude(getDoubleValue(node, UnloadLocation.PROP_LONGITUDE))
                .postalcode(getStringValue(node, UnloadLocation.PROP_POSTALCODE))
                .timeFrameFrom(getDateTimeValue(relationship, UnloadLocation.PROP_TIME_FRAME_FROM))
                .timeFrameTo(getDateTimeValue(relationship, UnloadLocation.PROP_TIME_FRAME_TO))
                .phone(getStringValue(node, UnloadLocation.PROP_PHONE))
                .email(getStringValue(node, UnloadLocation.PROP_EMAIL))                     
                .status(getApprovalStatusValue(node, UnloadLocation.PROP_STATUS))
                .externalId(getStringValue(node, UnloadLocation.PROP_EXTERNAL_ID))
                .build();
    }
}
