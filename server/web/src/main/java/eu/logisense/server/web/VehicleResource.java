/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

import eu.logisense.api.Vehicle;
import eu.logisense.api.VehicleService;
import static eu.logisense.server.web.BaseNeo4jService.graphDb;
import static eu.logisense.server.web.Labels.VEHICLE;
import eu.logisense.server.web.model.VehicleFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VehicleResource extends BaseNeo4jService implements VehicleService {

    private static final Logger logger = LoggerFactory.getLogger(VehicleResource.class);

    public VehicleResource(GraphDatabaseService graphDb) throws IOException {
        super(graphDb);
    }
    
    public VehicleResource() throws IOException {
    }
    
    

    @Override
    public List<Vehicle> getVehicles() {
        List<Vehicle> vehicles = new ArrayList<>();

        try (Transaction ignored = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (p:" + VEHICLE + ")"
                        + " RETURN p.id, p")) {
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                vehicles.add(VehicleFactory.fromNode((Node) row.get("p")));
            }
        }
        return vehicles;
    }

    @Override
    public Vehicle getVehicle(String id) {
        logger.debug("getVehicle id:{}", id);
        Vehicle vehicle = null;
        
        Map<String, Object> params = new HashMap<>();
        params.put("vehicleId", id);

        // Maybe is not the best, also, taking into account possible SQL Injection equivalencies
        try (Transaction ignored = graphDb.beginTx();
                Result result = graphDb.execute("MATCH  (s:" + VEHICLE + "{id:{vehicleId}}) RETURN s",params)) {
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                vehicle = VehicleFactory.fromNode((Node) row.get("s"));
            }
        }
        return vehicle;
    }

    @Override
    public Vehicle postVehicle(Vehicle template) {
        logger.debug("post vehicle:{}", template);
        Vehicle created = null;
        if (template != null) {

            try (Transaction tx = graphDb.beginTx()) {
                Node node = graphDb.createNode(Labels.VEHICLE);
                String id = UUID.randomUUID().toString();
                node.setProperty(Vehicle.PROP_NAME, template.getName());
                node.setProperty(Vehicle.PROP_ID, id);
                node.setProperty(Vehicle.PROP_HREF, (template.getHref() == null) ? "" : template.getHref());


                //
                // create the vehicle object from the nodes and relations
                //
                created = VehicleFactory.fromNode(node);

                tx.success();
            }
        }
        return created;
    }

    @Override
    public Vehicle updateVehicle(Vehicle template) {
        Vehicle created = null;
        Map<String, Object> params = new HashMap<>();
        params.put("vehicleId", template.getId());
        params.put("name", template.getName());

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (vehicle:" + VEHICLE + " {id: {vehicleId}}) "
                        + " SET vehicle.name = {name}"
                        + " RETURN vehicle", params);) {
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                created = VehicleFactory.fromNode((Node) row.get("vehicle"));
            }
            tx.success();
        }
        return created;
    }

    @Override
    public void deleteVehicle(String id) {
         Map<String, Object> params = new HashMap<>();
        params.put("vehicleId", id);

        graphDb.beginTx();
        graphDb.execute("MATCH (p:" + VEHICLE + " {id: {vehicleId}})" + " DETACH DELETE p", params);
    }

}
