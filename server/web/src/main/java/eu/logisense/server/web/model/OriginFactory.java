/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Origin;
import static eu.logisense.server.web.model.ObjectFactory.getApprovalStatusValue;
import static eu.logisense.server.web.model.ObjectFactory.getDoubleValue;
import static eu.logisense.server.web.model.ObjectFactory.getStringValue;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class OriginFactory {

    public static Origin fromNode(Node node, Relationship relationship) {
        return Origin.builder()
                .id(getStringValue(node, Origin.PROP_ID))
                .address(getStringValue(node, Origin.PROP_ADDRESS))
                .name(getStringValue(node, Origin.PROP_NAME))
                .href(getStringValue(node, Origin.PROP_HREF))
                .city(getStringValue(node, Origin.PROP_CITY))
                .country(getStringValue(node, Origin.PROP_COUNTRY))
                .latitude(getDoubleValue(node, Origin.PROP_LATITUDE))
                .longitude(getDoubleValue(node, Origin.PROP_LONGITUDE))
                .postalcode(getStringValue(node, Origin.PROP_POSTALCODE))
                .phone(getStringValue(node, Origin.PROP_PHONE))
                .email(getStringValue(node, Origin.PROP_EMAIL))                       
                .status(getApprovalStatusValue(node, Origin.PROP_STATUS))
                .externalId(getStringValue(node, Origin.PROP_EXTERNAL_ID))
                .build();
    }
}
