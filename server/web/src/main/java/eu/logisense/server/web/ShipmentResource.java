/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import static eu.logisense.server.web.Labels.*;
import static eu.logisense.server.web.Relations.*;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Client;
import eu.logisense.api.Container;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.OrderLineType;
import eu.logisense.api.Origin;
import eu.logisense.api.Resource;
import eu.logisense.api.Ride;
import eu.logisense.api.Shipment;
import eu.logisense.api.ShipmentService;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import eu.logisense.api.util.ClientUtil;
import static eu.logisense.server.web.BaseNeo4jService.graphDb;
import eu.logisense.server.web.model.ClientFactory;
import eu.logisense.server.web.model.ContainerFactory;
import eu.logisense.server.web.model.DebitorFactory;
import eu.logisense.server.web.model.DestinationFactory;
import eu.logisense.server.web.model.LoadLocationFactory;
import eu.logisense.server.web.model.ObjectFactory;
import eu.logisense.server.web.model.OrderFactory;
import eu.logisense.server.web.model.OrderLineFactory;
import eu.logisense.server.web.model.OriginFactory;
import eu.logisense.server.web.model.ShipmentFactory;
import eu.logisense.server.web.model.UnloadLocationFactory;
import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/shipments")
public class ShipmentResource extends BaseNeo4jService implements ShipmentService {

    private static final Logger logger = LoggerFactory.getLogger(ShipmentResource.class);


    public ShipmentResource(GraphDatabaseService graphDb) throws IOException, URISyntaxException {
        super(graphDb);
    }

    public ShipmentResource() throws IOException, URISyntaxException {
    }

    @GET
    @Path("/init_complete_ride")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public String initOneCompleteRide()
            throws IOException, URISyntaxException {

        LoadLocation madrid = LoadLocation.builder()
                .id("1000")
                .city("Madrid")
                .country("ES")
                .latitude(40.4165597)
                .longitude(-3.7037327)
                .name("Plaza puerta del Sol")
                .postalcode("28009")
                .timeFrameFrom(LocalDateTime.now())
                .timeFrameTo(LocalDateTime.now())
                .build();

        UnloadLocation groningen = UnloadLocation.builder()
                .id("1001")
                .city("Groningen")
                .country("NL")
                .latitude(53.218635)
                .longitude(6.566443)
                .name("Grote markt")
                .postalcode("9712")
                .timeFrameFrom(LocalDateTime.now())
                .timeFrameTo(LocalDateTime.now())
                .build();

        List<OrderLine> lines = new ArrayList<>();
        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline1 desc")
                .remarks("orderline1 remark")
                .stackable(Boolean.TRUE)
                .id("1")
                .name("orderline1")
                .paymentOnDeliveryAmount(1.1)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.CUSTOM)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(3.3)
                .build());

        lines.add(OrderLine.builder()
                .amount(0)
                .description("orderline2 desc")
                .remarks("orderline2 remark")
                .stackable(Boolean.FALSE)
                .id("2")
                .name("orderline2")
                .paymentOnDeliveryAmount(4.4)
                .paymentOnDeliveryCurrency("EUR")
                .type(OrderLineType.EURO_PALLET)
                .length(5.5)
                .width(5.5)
                .height(5.5)
                .weight(6.6)
                .build());

        Shipment template = Shipment.builder()
                .deadline(LocalDateTime.of(2016, Month.MARCH, 23, 0, 0))
                .loadLocation(madrid)
                .name("madrid to groningen testing complete ride")
                .note("some notes")
                .status(TransportStatus.NEW)
                .shippedUnits(2)
                .unloadLocation(groningen)
                .senderIsLoad(Boolean.TRUE)
                .destinationIsUnload(Boolean.FALSE)
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .id("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().build())
                        .otherReference("")
                        .unloadReference("")
                        .externalId("123456")
                        .build())
                .build();

        Shipment shipment = postShipment(template);

        // Container
        Container containerTemplate = Container.builder().name("name1").href("href").build();
        List<Resource> shipmentList = new ArrayList<>();
        shipmentList.add(shipment);
        containerTemplate.setShipments(shipmentList);
        ContainerResource containerResource = new ContainerResource(graphDb);
        Container container = containerResource.postContainer(containerTemplate);

        // Ride
        String licensePlate = UUID.randomUUID().toString();
        Ride rideTemplate = Ride.builder().name("name1").href("href").licensePlate(licensePlate).build();
        List<Container> containerList = new ArrayList<>();
        containerList.add(container);
        rideTemplate.setContainers(containerList);
        RideResource rideResource = new RideResource(graphDb);
        Ride ride = rideResource.postRide(rideTemplate);

        return "test set loaded";
    }

    @GET
    @Path("/init_test_set")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public String initTestSet()
            throws IOException, URISyntaxException {
        // @perezdf: I am wondering is the Util.initTestSet is getting too complex and not generating representative data test set
        Util.initTestSet(graphDb);

        return "test set loaded";
    }

    /**
     * This method deliver a shipment. A delivered shipment cant be proceeded again, it has finished the transportation
     *
     * @param id
     * @return
     */
    @Override
    public Shipment deliverShipment(@PathParam("id") String id) {
        throw new UnsupportedOperationException();
//
//        Shipment shipment = getShipment(id);
//        shipment.setStatus(ShipmentStatus.DELIVERED);
//        updateShipment(shipment);
//        return getShipment(id);
    }

    /**
     * This method start the transportation of and already existing shipment
     *
     * @param id
     * @return
     */
    @Override
    public Shipment orderShipment(@PathParam("id") String id) {
        throw new UnsupportedOperationException();
//        Shipment shipment = getShipment(id);
//        shipment.setStatus(ShipmentStatus.PROGRESS);
//        updateShipment(shipment);
//        return getShipment(id);
    }

    @Override
    public Shipment cancelShipment(@PathParam("id") String id) {

        Shipment shipment = getShipment(id);
        shipment.setStatus(TransportStatus.CANCELLED);
        updateShipment(shipment);
        return getShipment(id);
    }

    @Override
    public Shipment dispatchTo(@PathParam("id") String id, @PathParam("capabilityId") String capabilityId) {

        Shipment shipment = getShipment(id);
        shipment.setStatus(TransportStatus.DISPATCHED);
        updateShipment(shipment);

        return getShipment(id);
    }

    /**
     * List method returns the list of shipments
     *
     * @return
     */
    @Override
    public List<Shipment> getShipments() {
        logger.debug("getShipments");
        Map<String, Shipment> shipments = new HashMap<>();

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (s:" + SHIPMENT + ") "
                        + " , (s)<-[:" + SHIPPED_AS + "]-(o:" + ORDER + ")"
                        + " , (s)-[loadlocation:" + LOAD + "]->(load:" + LOCATION + ")"
                        + " , (s)-[unloadlocation:" + UNLOAD + "]->(unload:" + LOCATION + ")"
                        + " OPTIONAL MATCH (s)-[r]->(t)"
                        + " RETURN s.id, s,r, t,o.id,loadlocation,load,unloadlocation,unload")) {
//            logger.trace("\n" + result.resultAsString());
            //
            // one row for every relation to shipment
            //
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                //
                // only parse shipment for first row
                //
                if (!shipments.containsKey((String) row.get("s.id"))) {
                    Shipment shipment = ShipmentFactory.fromNode((Node) row.get("s"));
                    shipments.put((String) row.get("s.id"), shipment);
                    shipment.setOrder(getOrder((String) row.get("o.id")));
                    shipment.setUnloadLocation(UnloadLocationFactory.fromNode((Node) row.get("unload"), (Relationship) row.get("unloadlocation")));
                    shipment.setLoadLocation(LoadLocationFactory.fromNode((Node) row.get("load"), (Relationship) row.get("loadlocation")));
                }
                Shipment shipment = shipments.get((String) row.get("s.id"));

                Relationship r = (Relationship) row.get("r");
                //
                // check type of relationship and parse object
                //
                if (r != null) {
                    switch (Relations.valueOf(r.getType().name())) {
                        case SHIPPED_IN:
                            shipment.setContainer(ContainerFactory.resourceFromNode((Node) row.get("t")));
                            break;
                        default:
                            break;
                    }
                }

            }
            tx.success();
        }
        return new ArrayList<>(shipments.values());

    }

    @Override
    public Shipment getShipment(String id) {
        logger.debug("getShipment id:{}", id);
        Shipment shipment = null;
        Map<String, Object> params = new HashMap<>();
        params.put("shipmentId", id);

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (s:" + SHIPMENT + " {id: {shipmentId}})"
                        + " , (s)<-[:" + SHIPPED_AS + "]-(o:" + ORDER + ")"
                        + " , (s)-[loadlocation:" + LOAD + "]->(load:" + LOCATION + ")"
                        + " , (s)-[unloadlocation:" + UNLOAD + "]->(unload:" + LOCATION + ")"
                        + " OPTIONAL MATCH (s)-[r]->(t)"
                        + " RETURN s.id, s,r,t,o.id,loadlocation,load,unloadlocation,unload", params)) {
//            logger.trace("\n" + result.resultAsString());
            //
            // one row for every relation to shipment
            //
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                //
                // only parse shipment for first row
                //
                if (shipment == null) {
                    shipment = ShipmentFactory.fromNode((Node) row.get("s"));
                    shipment.setOrder(getOrder((String) row.get("o.id")));
                    shipment.setUnloadLocation(UnloadLocationFactory.fromNode((Node) row.get("unload"), (Relationship) row.get("unloadlocation")));
                    shipment.setLoadLocation(LoadLocationFactory.fromNode((Node) row.get("load"), (Relationship) row.get("loadlocation")));
                }
                Relationship r = (Relationship) row.get("r");
                //
                // check type of relationship and parse object
                //
                if (r != null) {
                    switch (Relations.valueOf(r.getType().name())) {
                        case SHIPPED_IN:
                            shipment.setContainer(ContainerFactory.resourceFromNode((Node) row.get("t")));
                            break;
                        default:
                            break;
                    }
                }

            }
            tx.success();

        }
        return shipment;
    }

    @Override
    public Shipment getShipmentByHref(String href) {
        logger.debug("getShipment id:{}", href);
        Shipment shipment = null;
        Map<String, Object> params = new HashMap<>();
        params.put("shipmentHref", href);

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (s:" + SHIPMENT + " {href: {shipmentHref}})"
                        + " , (s)<-[:" + SHIPPED_AS + "]-(o:" + ORDER + ")"
                        + " , (s)-[loadlocation:" + LOAD + "]->(load:" + LOCATION + ")"
                        + " , (s)-[unloadlocation:" + UNLOAD + "]->(unload:" + LOCATION + ")"
                        + " OPTIONAL MATCH (s)-[r]->(t)"
                        + " RETURN s.id, s,r,t,o.id,loadlocation,load,unloadlocation,unload", params)) {
//            logger.trace("\n" + result.resultAsString());
            //
            // one row for every relation to shipment
            //
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                //
                // only parse shipment for first row
                //
                if (shipment == null) {
                    shipment = ShipmentFactory.fromNode((Node) row.get("s"));
                    shipment.setOrder(getOrder((String) row.get("o.id")));
                    shipment.setUnloadLocation(UnloadLocationFactory.fromNode((Node) row.get("unload"), (Relationship) row.get("unloadlocation")));
                    shipment.setLoadLocation(LoadLocationFactory.fromNode((Node) row.get("load"), (Relationship) row.get("loadlocation")));
                }
                Relationship r = (Relationship) row.get("r");
                //
                // check type of relationship and parse object
                //
                if (r != null) {
                    switch (Relations.valueOf(r.getType().name())) {
                        case SHIPPED_IN:
                            shipment.setContainer(ContainerFactory.resourceFromNode((Node) row.get("t")));
                            break;
                        default:
                            break;
                    }
                }

            }
            tx.success();

        }
        return shipment;
    }

    /**
     * This method creates a shipment to be processed
     *
     */
    @Override
    public Shipment postShipment(Shipment template) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JSR310Module());
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            jsonProvider.writeTo(template, Shipment.class, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
            logger.debug(baos.toString());
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }

        logger.debug("postShipment:{}", template.getName());

        validate(template);

        template.setStatus(TransportStatus.NEW);
        Shipment created;

        try (Transaction tx = graphDb.beginTx()) {
            Node shipmentNode = graphDb.createNode(Labels.SHIPMENT);
            String id = template.getId() == null ? UUID.randomUUID().toString() : template.getId();
            shipmentNode.setProperty(Shipment.PROP_ID, id);
            shipmentNode.setProperty(Shipment.PROP_HREF, ShipmentFactory.toHref(id));
            shipmentNode.setProperty(Shipment.PROP_CREATED, LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            setProperty(shipmentNode, Shipment.PROP_NAME, template.getName());
            setProperty(shipmentNode, Shipment.PROP_DEADLINE, ObjectFactory.toString(template.getDeadline()));
            setProperty(shipmentNode, Shipment.PROP_NOTE, template.getNote());
            setProperty(shipmentNode, Shipment.PROP_STATUS, template.getStatus().name());
            setProperty(shipmentNode, Shipment.PROP_SHIPPED_UNITS, template.getShippedUnits());
            setProperty(shipmentNode, Shipment.PROP_SENDER_IS_LOAD, template.getSenderIsLoad());
            setProperty(shipmentNode, Shipment.PROP_DESTINATION_IS_UNLOAD, template.getDestinationIsUnload());
            //
            // construct LOAD
            //
            logger.debug("from {}", template.getLoadLocation());
            Node loadNode = getOrCreateLocationNode(template.getLoadLocation());
            Relationship loadRelation = shipmentNode.createRelationshipTo(loadNode, LOAD);

            if (template.getLoadLocation() != null) {
                if (template.getLoadLocation().getTimeFrameFrom() != null) {
                    loadRelation.setProperty(LoadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(template.getLoadLocation().getTimeFrameFrom()));
                }
                if (template.getLoadLocation().getTimeFrameTo() != null) {
                    loadRelation.setProperty(LoadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(template.getLoadLocation().getTimeFrameTo()));
                }
            }
            //
            // construct UNLOAD
            //
            Node unloadNode = getOrCreateLocationNode(template.getUnloadLocation());
            Relationship unloadRelation = shipmentNode.createRelationshipTo(unloadNode, UNLOAD);

            if (template.getUnloadLocation() != null) {
                if (template.getUnloadLocation().getTimeFrameFrom() != null) {
                    unloadRelation.setProperty(UnloadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(template.getUnloadLocation().getTimeFrameFrom()));
                }
                if (template.getUnloadLocation().getTimeFrameTo() != null) {
                    unloadRelation.setProperty(UnloadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(template.getUnloadLocation().getTimeFrameTo()));
                }
            }
            //
            // Create order
            //
            Order order = template.getOrder();
            logger.debug("post order:{}", order);
            Objects.requireNonNull(order);
            Objects.requireNonNull(order.getLines());
            Objects.requireNonNull(order.getOrigin());
//            Objects.requireNonNull(order.getDestination()); //FIXME: destination is optional in plan&go; posted value (if any) is not used;
            Objects.requireNonNull(order.getOrderedBy());
            Order createdOrder = null;

            Node orderNode = graphDb.createNode(Labels.ORDER);
            String orderId = UUID.randomUUID().toString();
            orderNode.setProperty(Order.PROP_NAME, order.getName());
            orderNode.setProperty(Order.PROP_ID, orderId);
            orderNode.setProperty(Order.PROP_HREF, OrderFactory.toHref(orderId));
            orderNode.setProperty(Order.PROP_LOADING_LIST, (order.getLoadingList() == null) ? "" : order.getLoadingList());
            orderNode.setProperty(Order.PROP_BATCH, (order.getBatch() == null) ? "" : order.getBatch());
            orderNode.setProperty(Order.PROP_INVOICE_REFERENCE, (order.getInvoiceReference() == null) ? "" : order.getInvoiceReference());
            orderNode.setProperty(Order.PROP_OTHER_REFERENCE, (order.getOtherReference() == null) ? "" : order.getOtherReference());
            orderNode.setProperty(Order.PROP_UNLOAD_REFERENCE, (order.getUnloadReference() == null) ? "" : order.getUnloadReference());
            orderNode.setProperty(Order.PROP_EXTERNAL_ID, (order.getExternalId() == null) ? "" : order.getExternalId());

            orderNode.createRelationshipTo(shipmentNode, SHIPPED_AS);

            order.getLines().forEach((line) -> {
                Node orderLineNode = graphDb.createNode(Labels.ORDER_LINE);
                orderLineNode.setProperty(OrderLine.PROP_AMOUNT, notNull(line.getAmount()));
                orderLineNode.setProperty(OrderLine.PROP_DESCRIPTION, notNull(line.getDescription()));
                orderLineNode.setProperty(OrderLine.PROP_REMARKS, notNull(line.getRemarks()));
//                orderLineNode.setProperty(OrderLine.PROP_HREF, line.getHref());
                orderLineNode.setProperty(OrderLine.PROP_ID, UUID.randomUUID().toString());
                orderLineNode.setProperty(OrderLine.PROP_NAME, notNull(line.getName()));
                orderLineNode.setProperty(OrderLine.PROP_STACKABLE, notNull(line.getStackable()));
                orderLineNode.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, notNull(line.getPaymentOnDeliveryAmount()));
                orderLineNode.setProperty(OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, notNull(line.getPaymentOnDeliveryCurrency()));

                //FIXME we really need some input validation
                if (line.getType() == null) {
                    line.setType(OrderLineType.CUSTOM);
                }

                orderLineNode.setProperty(OrderLine.PROP_TYPE, notNull(line.getType().name()));
                orderLineNode.setProperty(OrderLine.PROP_SWAP_PALLETS, notNull(line.getSwapPallets()));
                orderLineNode.setProperty(OrderLine.PROP_LENGTH, notNull(line.getLength()));
                orderLineNode.setProperty(OrderLine.PROP_WIDTH, notNull(line.getWidth()));
                orderLineNode.setProperty(OrderLine.PROP_HEIGHT, notNull(line.getHeight()));
                orderLineNode.setProperty(OrderLine.PROP_WEIGHT, notNull(line.getWeight()));
                orderNode.createRelationshipTo(orderLineNode, CONTAINS);

            });
            //
            // FIXME set origin to load location for now and ignore origin
            //
//            Node origin = getOrCreateLocationNode(order.getOrigin());
            orderNode.createRelationshipTo(loadNode, ORIGIN);

//            Node destination = getOrCreateLocationNode(order.getDestination());
            orderNode.createRelationshipTo(unloadNode, DESTINATION);

            Node ordererClient = graphDb.findNode(CLIENT, Client.PROP_ID, order.getOrderedBy().getId());
            if (ordererClient == null) {
                //FIXME remove creating of new clients, client should always be present
                logger.warn("client ordered by not found, creating one, but clients should always exist!");
                ordererClient = graphDb.createNode(CLIENT);
                String clientId = (order.getOrderedBy().getId() == null) ? UUID.randomUUID().toString() : order.getOrderedBy().getId();
                ordererClient.setProperty(Client.PROP_ID, clientId);
                ordererClient.setProperty(Client.PROP_NAME, (order.getOrderedBy().getName() == null) ? "" : order.getOrderedBy().getName());
                ordererClient.setProperty(Client.PROP_HREF, ClientFactory.toHref(clientId));
                
                setProperty(ordererClient, Client.PROP_DEBTOR_NUMBER, order.getOrderedBy().getDebtorNumber());
                setProperty(ordererClient, Client.PROP_EXTERNAL_ID, order.getOrderedBy().getExternalId());
                setProperty(ordererClient, Client.PROP_STATUS, ApprovalStatus.NEW.name());
            }

            ordererClient.createRelationshipTo(orderNode, ORDERER);

            Node debitorNode = graphDb.findNode(CLIENT, Client.PROP_ID, order.getDebitor().getId());
            if (debitorNode == null) {
                debitorNode = ordererClient;
            }

            Relationship debitorRelation = debitorNode.createRelationshipTo(orderNode, DEBITOR);
            logger.debug("setting order price to {}", order.getDebitor().getPrice());
            setProperty(debitorRelation, Debitor.PROP_PRICE, order.getDebitor().getPrice());
            setProperty(debitorRelation, Debitor.PROP_CURRENCY, order.getDebitor().getCurrency());

            //
            // create the order object from the nodes and relations
            //
//            createdOrder = OrderFactory.fromNode(orderNode);
//            createdOrder = getOrder(order.getId());
//
//            //
//            // create the shipment object from the nodes and relations
//            //
//            created = ShipmentFactory.fromNode(shipmentNode);
//            created.setOrder(createdOrder);
//            created.setLoadLocation(LoadLocationFactory.fromNode(loadNode, loadRelation));
//            created.setUnloadLocation(UnloadLocationFactory.fromNode(unloadNode, unloadRelation));
            created = getShipment(id);
            tx.success();
        }
        return created;

    }

    private Boolean notNull(Boolean value) {
        return (value == null) ? false : value;
    }

    private String notNull(String value) {
        return (value == null) ? "" : value;
    }

    private Double notNull(Double value) {
        return (value == null) ? 0.0 : value;
    }

    private Integer notNull(Integer value) {
        return (value == null) ? 0 : value;
    }

    protected Order getOrder(String id) {
        logger.debug("getOrder id:{}", id);
        Order order = null;
        Map<String, Object> params = new HashMap<>();
        params.put("orderId", id);
        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (order:" + ORDER + " {id: {orderId}})"
                        + ", (order)<-[:" + ORDERER + "]-(orderer:" + CLIENT + ") "
                        + ", (order)<-[rde:" + DEBITOR + "]-(debitor:" + CLIENT + ") "
                        + ", (order)-[rd:" + DESTINATION + "]->(destination:" + LOCATION + ") "
                        + ", (order)-[ro:" + ORIGIN + "]->(origin:" + LOCATION + ") "
                        + "OPTIONAL MATCH (order)-[:" + CONTAINS + "]->(line:" + ORDER_LINE + ")"
                        + " RETURN order,line,orderer,debitor,rde,rd,destination,ro,origin", params)) {
//            logger.debug(result.resultAsString());

            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                if (order == null) {
                    order = OrderFactory.fromNode((Node) row.get("order"));
                    order.setOrderedBy(ClientFactory.fromNode((Node) row.get("orderer")));
                    order.setDebitor(DebitorFactory.fromNode((Node) row.get("debitor"), (Relationship) row.get("rde")));
                    order.setDestination(DestinationFactory.fromNode((Node) row.get("destination"), (Relationship) row.get("rd")));
                    order.setOrigin(OriginFactory.fromNode((Node) row.get("origin"), (Relationship) row.get("ro")));
                    order.setLines(new ArrayList<>());
                }
                if (row.get("line") != null) {
                    order.getLines().add(OrderLineFactory.fromNode((Node) row.get("line"), order.getId()));
                } else {
                    logger.warn("order with id {} has no order lines!", id);
                }
            }
            tx.success();
        }
        return order;
    }

    /**
     * This method creates a shipment to be processed
     *
     */
    @Override
    public Shipment updateShipment(Shipment template) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JSR310Module());
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            jsonProvider.writeTo(template, Shipment.class, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
            logger.debug(baos.toString());
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }

        logger.debug("updateShipment with id:{} and status {} : {}", template.getId(), template.getStatus());

        validate(template);

        if (template.getId() == null) {
            throw new WebApplicationException("Cannot update Shipment without ID", Response.Status.BAD_REQUEST);
        }

        Shipment created = null;

        try (Transaction tx = graphDb.beginTx()) {
            Node shipmentNode = graphDb.findNode(Labels.SHIPMENT, Shipment.PROP_ID, template.getId());
            if (shipmentNode == null) {
                throw new WebApplicationException("Shipment not found", Response.Status.NOT_FOUND);
            }

            setProperty(shipmentNode, Shipment.PROP_STATUS, (template.getStatus() == null) ? "NEW" : template.getStatus().name());
            setProperty(shipmentNode, Shipment.PROP_NAME, template.getName());
            setProperty(shipmentNode, Shipment.PROP_DEADLINE, ObjectFactory.toString(template.getDeadline()));
            setProperty(shipmentNode, Shipment.PROP_NOTE, template.getNote());

            setProperty(shipmentNode, Shipment.PROP_SHIPPED_UNITS, template.getShippedUnits());
            setProperty(shipmentNode, Shipment.PROP_SENDER_IS_LOAD, template.getSenderIsLoad());
            setProperty(shipmentNode, Shipment.PROP_DESTINATION_IS_UNLOAD, template.getDestinationIsUnload());

            //
            // update LOAD
            //
            logger.debug("from {}", template.getLoadLocation());
            Node loadNode = getOrCreateLocationNode(template.getLoadLocation());
            if (loadNode == null) {
                throw new WebApplicationException("LoadLocation not found", Response.Status.NOT_FOUND);
            }
            //
            // should be only one relationship between load and shipment
            //
            Relationship loadRelation = shipmentNode.getSingleRelationship(LOAD, Direction.BOTH);

            if (template.getLoadLocation() != null) {
                if (template.getLoadLocation().getTimeFrameFrom() != null) {
                    setProperty(loadRelation, LoadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(template.getLoadLocation().getTimeFrameFrom()));
                }
                if (template.getLoadLocation().getTimeFrameTo() != null) {
                    setProperty(loadRelation, LoadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(template.getLoadLocation().getTimeFrameTo()));
                }
            }

            //
            // update UNLOAD
            //
            Location unloadLocation = template.getUnloadLocation();
            Node unloadNode = graphDb.findNode(Labels.LOCATION, Location.PROP_ID, unloadLocation.getId());
            if (unloadNode == null) {
                throw new WebApplicationException("UnloadLocation not found", Response.Status.NOT_FOUND);
            } else {
                setProperty(unloadNode, Location.PROP_CITY, unloadLocation.getCity());
                setProperty(unloadNode, Location.PROP_ADDRESS, unloadLocation.getAddress());
                setProperty(unloadNode, Location.PROP_EMAIL, unloadLocation.getEmail());
                setProperty(unloadNode, Location.PROP_PHONE, unloadLocation.getPhone());
                setProperty(unloadNode, Location.PROP_COUNTRY, unloadLocation.getCountry());
                setProperty(unloadNode, Location.PROP_LATITUDE, unloadLocation.getLatitude());
                setProperty(unloadNode, Location.PROP_LONGITUDE, unloadLocation.getLongitude());
                setProperty(unloadNode, Location.PROP_NAME, unloadLocation.getName());
                setProperty(unloadNode, Location.PROP_POSTALCODE, unloadLocation.getPostalcode());
                setProperty(unloadNode, Location.PROP_STATUS, unloadLocation.getStatus().name());
                setProperty(unloadNode, Location.PROP_EXTERNAL_ID, unloadLocation.getExternalId());
            }
            //
            // should be only one relationship between unload and shipment
            //
            Relationship unloadRelation = shipmentNode.getSingleRelationship(UNLOAD, Direction.BOTH);

//            Node unloadNode = getOrCreateLocationNode(template.getUnloadLocation());
//            if (unloadNode == null) {
//                throw new WebApplicationException("UnloadLocation not found", Response.Status.NOT_FOUND);
//            }
//
//            if (template.getUnloadLocation() != null) {
//                if (template.getUnloadLocation().getTimeFrameFrom() != null) {
//                    setProperty(unloadRelation, UnloadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(template.getUnloadLocation().getTimeFrameFrom()));
//                }
//                if (template.getUnloadLocation().getTimeFrameTo() != null) {
//                    setProperty(unloadRelation, UnloadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(template.getUnloadLocation().getTimeFrameTo()));
//                }
//            }
            //
            // Create order
            //
            Order order = template.getOrder();
            logger.debug("post order:{}", order);
            //TODO replace Objects.requireNonNull with validation:
            Objects.requireNonNull(order);
            Objects.requireNonNull(order.getId());
            Objects.requireNonNull(order.getLines());
            Objects.requireNonNull(order.getOrigin());
            Objects.requireNonNull(order.getDestination());
            Objects.requireNonNull(order.getOrderedBy());
            Order createdOrder = null;

            Node orderNode = graphDb.findNode(Labels.ORDER, Order.PROP_ID, order.getId());
            if (orderNode == null) {
                logger.debug("Was looking for order with ID {}", order.getId());
                throw new WebApplicationException("Order not found", Response.Status.NOT_FOUND);
            }

            setProperty(orderNode, Order.PROP_NAME, order.getName());
            setProperty(orderNode, Order.PROP_LOADING_LIST, (order.getLoadingList() == null) ? "" : order.getLoadingList());
            setProperty(orderNode, Order.PROP_BATCH, (order.getBatch() == null) ? "" : order.getBatch());
            setProperty(orderNode, Order.PROP_INVOICE_REFERENCE, (order.getInvoiceReference() == null) ? "" : order.getInvoiceReference());
            setProperty(orderNode, Order.PROP_OTHER_REFERENCE, (order.getOtherReference() == null) ? "" : order.getOtherReference());
            setProperty(orderNode, Order.PROP_UNLOAD_REFERENCE, (order.getUnloadReference() == null) ? "" : order.getUnloadReference());
            setProperty(orderNode, Order.PROP_EXTERNAL_ID, (order.getExternalId() == null) ? "" : order.getExternalId());

            order.getLines().forEach((line) -> {

                String orderLineId = line.getId() == null ? UUID.randomUUID().toString() : line.getId();

                //
                // check if orderline already existing in db, else creating new one
                //
                Node orderLineNode = graphDb.findNode(Labels.ORDER_LINE, OrderLine.PROP_ID, orderLineId);
                if (orderLineNode == null) {
                    logger.debug("Creating orderline with id {}", orderLineId);
                    orderLineNode = graphDb.createNode(Labels.ORDER_LINE);
                    orderLineNode.setProperty(OrderLine.PROP_ID, orderLineId);

                }

                setProperty(orderLineNode, OrderLine.PROP_AMOUNT, notNull(line.getAmount()));
                setProperty(orderLineNode, OrderLine.PROP_DESCRIPTION, notNull(line.getDescription()));
                setProperty(orderLineNode, OrderLine.PROP_REMARKS, notNull(line.getRemarks()));
//                orderLineNode.setProperty(OrderLine.PROP_HREF, line.getHref());
                setProperty(orderLineNode, OrderLine.PROP_NAME, notNull(line.getName()));
                setProperty(orderLineNode, OrderLine.PROP_STACKABLE, notNull(line.getStackable()));
                setProperty(orderLineNode, OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT, notNull(line.getPaymentOnDeliveryAmount()));
                setProperty(orderLineNode, OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY, notNull(line.getPaymentOnDeliveryCurrency()));

                //FIXME we really need some input validation
                if (line.getType() == null) {
                    line.setType(OrderLineType.CUSTOM);
                }

                setProperty(orderLineNode, OrderLine.PROP_TYPE, notNull(line.getType().name()));
                setProperty(orderLineNode, OrderLine.PROP_SWAP_PALLETS, notNull(line.getSwapPallets()));
                setProperty(orderLineNode, OrderLine.PROP_LENGTH, notNull(line.getLength()));
                setProperty(orderLineNode, OrderLine.PROP_WIDTH, notNull(line.getWidth()));
                setProperty(orderLineNode, OrderLine.PROP_HEIGHT, notNull(line.getHeight()));
                setProperty(orderLineNode, OrderLine.PROP_WEIGHT, notNull(line.getWeight()));

                //
                // if line didn't have relation, create it
                //
                if (!orderLineNode.hasRelationship(CONTAINS)) {
                    orderNode.createRelationshipTo(orderLineNode, CONTAINS);
                }

            });
            //
            // FIXME set origin to load location for now and ignore origin
            //
//            Node origin = getOrCreateLocationNode(order.getOrigin());
//            orderNode.createRelationshipTo(loadNode, ORIGIN);

//            Node destination = getOrCreateLocationNode(order.getDestination());
//            orderNode.createRelationshipTo(unloadNode, DESTINATION);
            //
            // Update Orderer
            //
            Node ordererClientNode = graphDb.findNode(Labels.CLIENT, Client.PROP_ID, order.getOrderedBy().getId());
            if (order.getOrderedBy() == null || order.getOrderedBy().getId() == null || ordererClientNode == null) {
                throw new WebApplicationException("Orderer client is a manditory property of order and should exist before creating the order", Response.Status.BAD_REQUEST);
            } else {
                if (orderNode.hasRelationship(ORDERER)) {
                    orderNode.getRelationships(ORDERER).forEach((relationship) -> {
                        if (!ordererClientNode.getProperty(Client.PROP_ID).equals(relationship.getStartNode().getProperty(Client.PROP_ID))) {
                            relationship.delete();
                        }
                    });

                }
                if (!orderNode.hasRelationship(ORDERER)) {
                    ordererClientNode.createRelationshipTo(orderNode, ORDERER);
                }
            }

            //
            // Update Debitor
            //
            Node debitorNode = graphDb.findNode(Labels.CLIENT, Client.PROP_ID, order.getDebitor().getId());
            if (order.getDebitor() == null || order.getDebitor().getId() == null || debitorNode == null) {
                throw new WebApplicationException("Debitor is a manditory property of order and should exist before creating the order", Response.Status.BAD_REQUEST);
            } else {
                if (orderNode.hasRelationship(DEBITOR)) {
                    orderNode.getRelationships(DEBITOR).forEach((relationship) -> {
                        if (debitorNode.getProperty(Client.PROP_ID).equals(relationship.getStartNode().getProperty(Client.PROP_ID))) {
                            setProperty(relationship, Debitor.PROP_CURRENCY, order.getDebitor().getCurrency());
                            setProperty(relationship, Debitor.PROP_PRICE, order.getDebitor().getPrice());
                        } else {
                            relationship.delete();
                        }
                    });

                }
                if (!orderNode.hasRelationship(DEBITOR)) {
                    Relationship debitorRelation = debitorNode.createRelationshipTo(orderNode, DEBITOR);
                    setProperty(debitorRelation, Debitor.PROP_CURRENCY, order.getDebitor().getCurrency());
                    setProperty(debitorRelation, Debitor.PROP_PRICE, order.getDebitor().getPrice());
                }
            }

            //
            // create the order object from the nodes and relations
            //
//            createdOrder = OrderFactory.fromNode(orderNode);
//            createdOrder = getOrder(order.getId());
//
//            //
//            // create the shipment object from the nodes and relations
//            //
//            created = ShipmentFactory.fromNode(shipmentNode);
//            created.setOrder(createdOrder);
//            created.setLoadLocation(LoadLocationFactory.fromNode(loadNode, loadRelation));
//            created.setUnloadLocation(UnloadLocationFactory.fromNode(unloadNode, unloadRelation));
            created = getShipment(template.getId());
            tx.success();
        }
        return created;
    }

    @Override
    public void deleteShipment(String id) {
        logger.debug("deleteShipment:{}", id);

        Map<String, Object> params = new HashMap<>();
        params.put("shipmentId", id);

        try (Transaction tx = graphDb.beginTx()) {
            graphDb.execute("MATCH (p:" + SHIPMENT + " {id: {shipmentId}})" + " DETACH DELETE p", params);
            tx.success();
        }

    }
// ***************************** PRIVATE METHODS  ***************************** 

    @Deprecated
    /**
     * @perezdf: I am doubting seruiously about if this method is appropiated
     *
     * @param template
     * @return
     */
    private Node getLocationNode(Location template) {
        Objects.requireNonNull(template.getLatitude());
        Objects.requireNonNull(template.getLongitude());

        Node location = null;
        Map<String, Object> params = new HashMap<>();
        params.put("latitude", template.getLatitude());
        params.put("longitude", template.getLongitude());

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (l:" + LOCATION + " {latitude: {latitude},longitude:{longitude}})"
                        + " RETURN l", params)) {
            if (!result.hasNext()) {
                logger.info("No location found for latitude {} and longitude {}, creating new one", template.getLatitude(), template.getLongitude());
                location = graphDb.createNode(LOCATION);
                if (template.getCity() != null) {
                    location.setProperty(Location.PROP_CITY, template.getCity());
                }
                if (template.getCountry() != null) {
                    location.setProperty(Location.PROP_COUNTRY, template.getCountry());
                }
                if (template.getLatitude() != null) {
                    location.setProperty(Location.PROP_LATITUDE, template.getLatitude());
                }
                if (template.getLongitude() != null) {
                    location.setProperty(Location.PROP_LONGITUDE, template.getLongitude());
                }

                if (template.getName() != null) {
                    location.setProperty(Location.PROP_NAME, template.getName());
                }

                if (template.getPostalcode() != null) {
                    location.setProperty(Location.PROP_POSTALCODE, template.getPostalcode());
                }

            } else {
                location = (Node) result.next().get("l");
            }
            tx.success();
        }
        return location;
    }

}
