/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.Resource;
import org.neo4j.graphdb.Node;
import eu.logisense.api.Shipment;
import static eu.logisense.server.web.model.ObjectFactory.getStringValue;
import static eu.logisense.server.web.model.ObjectFactory.getIntegerValue;
import static eu.logisense.server.web.model.ObjectFactory.getTransportStatusValue;

public class ShipmentFactory {

    public static Resource resourceFromNode(Node node){
        return Shipment.buildResource()
                .id(getStringValue(node, Shipment.PROP_ID))
                .name(getStringValue(node, Shipment.PROP_NAME))
                .href(toHref(getStringValue(node, Shipment.PROP_ID)))
                .status(getTransportStatusValue(node, Shipment.PROP_STATUS))
                .index(getIntegerValue(node, Shipment.PROP_INDEX))                
                .build();
    }
    
    public static Shipment fromNode(Node node) {
        return Shipment.builder()
                .id(getStringValue(node, Shipment.PROP_ID))
                .name(getStringValue(node, Shipment.PROP_NAME))
                .href(toHref(getStringValue(node, Shipment.PROP_ID)))
                .status(getTransportStatusValue(node, Shipment.PROP_STATUS))
                .index(getIntegerValue(node, Shipment.PROP_INDEX))
                .note(getStringValue(node, Shipment.PROP_NOTE))
                .shippedUnits(getIntegerValue(node, Shipment.PROP_SHIPPED_UNITS))
                .deadline(ObjectFactory.getDateTimeValue(node, Shipment.PROP_DEADLINE))
                .senderIsLoad(ObjectFactory.getBooleanValue(node, Shipment.PROP_SENDER_IS_LOAD))
                .destinationIsUnload(ObjectFactory.getBooleanValue(node, Shipment.PROP_DESTINATION_IS_UNLOAD))
                .build();
    }

    public static String toHref(String id) {
        return "/shipments/" + id;
    }
}
