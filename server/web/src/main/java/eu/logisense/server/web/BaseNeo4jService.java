/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.logisense.api.Location;
import static eu.logisense.server.web.Labels.LOCATION;
import eu.logisense.server.web.model.LocationFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.neo4j.cluster.ClusterSettings;
import org.neo4j.graphdb.ConstraintViolationException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseBuilder;
import org.neo4j.graphdb.factory.HighlyAvailableGraphDatabaseFactory;
import org.neo4j.kernel.ha.HaSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public abstract class BaseNeo4jService {

    private static final Logger log = LoggerFactory.getLogger(BaseNeo4jService.class);
    protected static GraphDatabaseService graphDb;
    protected static Validator validator;

//    public static final String PROP_DB_DIR = "agrosense.server.storage.neo4j.db";
//    public static final String DEFAULT_DB_DIR = "/var/lib/neo4j/data/graph.db";
//    protected final static String DB_LOCATION = "target/graph-master";
    public BaseNeo4jService(GraphDatabaseService graphDb) {
        Objects.requireNonNull(graphDb);
        BaseNeo4jService.graphDb = graphDb;
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        this.validator = vf.getValidator();
    }

    static void requireNonNull(Object object) {
        if (object == null) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
    }

    /**
     * @throws java.io.IOException
     * @see https://wiki.limetri.eu/display/LIM/Install+local+development+neo4j+cluster
     */
    public BaseNeo4jService() throws IOException {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        this.validator = vf.getValidator();

        if (graphDb == null) {

            URL properties = Config.getConfigURL();
            String dbDir = Config.getValue(Config.PROP_GRAPH_LOCATION);
            log.info("Initializing storage at {}", dbDir);
            GraphDatabaseBuilder builder = new HighlyAvailableGraphDatabaseFactory().newHighlyAvailableDatabaseBuilder(dbDir);

            builder.setConfig(ClusterSettings.server_id, Config.getValue("ha.server_id"));
            builder.setConfig(HaSettings.ha_server, Config.getValue("ha.server"));
            builder.setConfig(ClusterSettings.cluster_server, Config.getValue("ha.cluster_server"));
            builder.setConfig(HaSettings.slave_only, Config.getValue("ha.slave_only"));
            builder.setConfig(ClusterSettings.initial_hosts, Config.getValue("ha.initial_hosts"));

//            final File dbFile = new File(dbDir);
//            GraphDatabaseBuilder builder = new EnterpriseGraphDatabaseFactory()
//                    .setUserLogProvider(new Slf4jLogProvider())
//                    .newEmbeddedDatabaseBuilder(dbFile);            
//            GraphDatabaseBuilder builder = new GraphDatabaseFactory().setUserLogProvider(new Slf4jLogProvider()).newEmbeddedDatabaseBuilder(dbFile);
//            builder.loadPropertiesFromURL(properties);
//            builder.setConfig(ClusterSettings.server_id, SERVER_ID);
//            builder.setConfig(HaSettings.ha_server, "localhost:6363");
//            builder.setConfig(HaSettings.slave_only, Settings.FALSE);
//            builder.setConfig(ClusterSettings.cluster_server, "localhost:5001");
//            builder.setConfig(ClusterSettings.initial_hosts, "localhost:5001");
            graphDb = builder.newGraphDatabase();

            registerShutdownHook(BaseNeo4jService.graphDb);
            log.info("Neo4jStorageService initialized");
        }
        Objects.requireNonNull(graphDb);
    }

    protected void setProperty(Relationship relation, String key, Object value) {
        if (value != null) {
            relation.setProperty(key, value);
        }
    }

    protected void setProperty(Node node, String key, Object value) {
        if (value != null) {
            node.setProperty(key, value);
        }
    }

    protected void logDebug(String message, Object o, Class c) {
        logDebug(message, o, c, null);
    }

    protected void logDebug(String message, Object o, Class c, Type genericType) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            jsonProvider.writeTo(o, c, genericType, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
            log.debug(message, baos);

        } catch (IOException ex) {
            log.error("serialization exception {}", ex);
        }
    }

    //TODO: only used by ShipmentResource, let it delegate to a LocationResource instead of using super
    protected Node getOrCreateLocationNode(Location template) {

        Node location;

        try (Transaction tx = graphDb.beginTx()) {
            log.debug("getOrCreateLocationNode for location: {}", template.getId());

            location = graphDb.findNode(LOCATION, Location.PROP_ID, template.getId());

            if (location == null) {
                log.debug("no node found for location id {}, creating new one as {}", template.getId(), template);
                location = graphDb.createNode(LOCATION);
                String locationId = (template.getId() == null) ? UUID.randomUUID().toString() : template.getId();
                location.setProperty(Location.PROP_ID, locationId);
                location.setProperty(Location.PROP_HREF, LocationFactory.toHref(locationId));
                setProperty(location, Location.PROP_CITY, template.getCity());
                setProperty(location, Location.PROP_ADDRESS, template.getAddress());
                setProperty(location, Location.PROP_EMAIL, template.getEmail());
                setProperty(location, Location.PROP_PHONE, template.getPhone());
                setProperty(location, Location.PROP_COUNTRY, template.getCountry());
                setProperty(location, Location.PROP_LATITUDE, template.getLatitude());
                setProperty(location, Location.PROP_LONGITUDE, template.getLongitude());
                setProperty(location, Location.PROP_NAME, template.getName());
                setProperty(location, Location.PROP_POSTALCODE, template.getPostalcode());
                setProperty(location, Location.PROP_STATUS, template.getStatus().name());
                setProperty(location, Location.PROP_EXTERNAL_ID, template.getExternalId());
            }

            tx.success();
        }
        return location;
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    protected void validate(Object object) {

        // Create a bean validator and check for issues.
        Set<ConstraintViolation<Object>> violations = validator.validate(object);

        if (!violations.isEmpty()) {
            String message = "";
            for (ConstraintViolation violation : violations) {
                if (violation.getConstraintDescriptor() != null && violation.getConstraintDescriptor().getMessageTemplate() != null) {
                    message += violation.getConstraintDescriptor().getMessageTemplate();
                }
            }
            throw new WebApplicationException(message, Response.Status.BAD_REQUEST);
        }
    }
}
