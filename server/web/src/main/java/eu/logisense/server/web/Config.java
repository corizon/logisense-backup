/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * static configuration class
 * <p>
 * properties can be defined in this class with a default value, provided as
 * commandline option, as environment variable or in a properties file. Value
 * will be determined based on priority:
 * </p>
 * <p>
 * <ul>
 * <li> injected values using Config.setProperty
 * <li>commandline arguments (-Dsomeproperty=somevalue)
 * <li>environment variable (SOME_PROPERTY=somevalue java -jar some.jar)
 * <li>values loaded from property file 5 - fallback values specified in this
 * class
 * </ul><p>
 * </p><p>
 * First value found will be returned by getValue.
 * </p><p>
 * To load a property file use load(URL), please not that this will not unload
 * any previously loaded property file.
 * </p><p>
 * Loading new property files can change the behaviour for any application using
 * the object, so use with care.
 * </p>
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class Config {

    private static File store;
    private static final Properties CONFIG = new Properties();

    private static final Logger log = LoggerFactory.getLogger(Config.class);

    //
    // predefined properties
    //
    public static final String PROP_GRAPH_LOCATION = "logisense.neo4j.location";
    public static final String PROP_BIND_ADDRESS = "logisense.bind.address";
    public static final String PROP_PORT = "logisense.port";
    //
    // pointers 
    //
    private static final String PROP_ENV_VARIABLE = "PROP_ENV_VARIABLE";
    private static final String PROP_VARIABLE = "PROP_VARIABLE";
    private static final String VALUE_FALLBACK = "PROP_FALLBACK";
    //
    // value types
    //
    private static final String VALUE_ENV_VARIABLE = "VALUE_ENV_VARIABLE";
    private static final String VALUE_COMMANDLINE = "VALUE_COMMANDLINE";
    private static final String VALUE_PROPERTYFILE = "VALUE_PROPERTYFILE";
    private static final String VALUE_INJECTED = "VALUE_INJECTED";

    private static final Map<String, Map<String, String>> properties = new HashMap<>();

    private static final List<String> priority = new LinkedList<>(Arrays.asList(VALUE_INJECTED, VALUE_COMMANDLINE, VALUE_ENV_VARIABLE, VALUE_PROPERTYFILE, VALUE_FALLBACK));

    public static void setProperty(String property, String value) {
        if (!properties.containsKey(property)) {
            properties.put(property, new HashMap<>());
        }
        properties.get(property).put(VALUE_INJECTED, value);
        updateStore();
    }

    static {
        reset();
        load();
    }

    /**
     * load properties from properties from classspath
     */
    public static void load() {
        //
        // first try to load configured external file
        //
        URL defaultConfig = null;
        String extConfig = System.getProperty("CONFIG");
        if (extConfig != null) {
            File extConfigFile = new File(extConfig);
            try {
                defaultConfig = extConfigFile.toURI().toURL();
            } catch (MalformedURLException ex) {
                java.util.logging.Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //
        // fallback load logisense.properties or empty.properties from classpath
        //
        if (defaultConfig == null) {
            defaultConfig = Config.class.getResource("/logisense.properties");
            if (defaultConfig == null) {
                defaultConfig = Config.class.getResource("/empty.properties");
            }
        }
        load(defaultConfig);
    }

    /**
     * Rest the Configuration to defaults, does not load new configuration! If
     * needed use in combination with load()
     */
    protected static void reset() {
        properties.clear();
        store = null;
        CONFIG.clear();
        properties.put(PROP_GRAPH_LOCATION, new HashMap<>());
        properties.get(PROP_GRAPH_LOCATION).put(PROP_ENV_VARIABLE, "LOGISENSE_GRAPH_LOCATION");
        properties.get(PROP_GRAPH_LOCATION).put(PROP_VARIABLE, PROP_VARIABLE);
        try {
            properties.get(PROP_GRAPH_LOCATION).put(VALUE_FALLBACK, Files.createTempDirectory("logisense.db").toString());
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }

        properties.put(PROP_BIND_ADDRESS, new HashMap<>());
        properties.get(PROP_BIND_ADDRESS).put(PROP_ENV_VARIABLE, "LOGISENSE_BIND_ADDRESS");
        properties.get(PROP_BIND_ADDRESS).put(PROP_VARIABLE, PROP_VARIABLE);
        properties.get(PROP_BIND_ADDRESS).put(VALUE_FALLBACK, "0.0.0.0");

        properties.put(PROP_PORT, new HashMap<>());
        properties.get(PROP_PORT).put(PROP_ENV_VARIABLE, "LOGISENSE_PORT");
        properties.get(PROP_PORT).put(PROP_VARIABLE, PROP_VARIABLE);
        properties.get(PROP_PORT).put(VALUE_FALLBACK, "8002");
    }

    /**
     * load properties from custom config location
     *
     * @param configURL
     */
    public static void load(URL configURL) {
        Objects.requireNonNull(configURL);
        log.trace("loading properties from url {}", configURL.getPath());
        // TODO: add initial data when db files are created, e.g. admin user, default roles/groups
        Properties config = new Properties();
        try (InputStream in = configURL.openStream()) {
            if (in != null) {
                config.load(in);
            } else {
                log.info("no property file found on location:", configURL.getPath());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        //
        // load all properties from file
        //
        config.keySet().stream().forEach((_item) -> {
            if (!properties.containsKey((String) _item)) {
                properties.put((String) _item, new HashMap<>());
                properties.get((String) _item).put(PROP_VARIABLE, (String) _item);
                properties.get((String) _item).put(PROP_ENV_VARIABLE, (String) _item);
            }
            log.trace("setting value for property {} to {}", _item, config.getProperty((String) _item));
            properties.get((String) _item).put(VALUE_PROPERTYFILE, config.getProperty((String) _item));
        });
        //
        // load command line and environment variables
        //
//        properties.values().stream().map((prop) -> {
//            prop.put(VALUE_COMMANDLINE, System.getProperty(prop.get(PROP_VARIABLE)));
//            return prop;
//        }).forEach((prop) -> {
//            prop.put(VALUE_ENV_VARIABLE, System.getenv(prop.get(PROP_ENV_VARIABLE)));
//        });        

        properties.values().stream().forEach((prop) -> {
            prop.put(VALUE_COMMANDLINE, System.getProperty(prop.get(PROP_VARIABLE)));
            prop.put(VALUE_ENV_VARIABLE, System.getenv(prop.get(PROP_ENV_VARIABLE)));
        });
        updateStore();
    }

    private static void updateStore() {
        properties.keySet().stream().forEach((prop) -> {
            CONFIG.setProperty(prop, getValue(prop));
        });
        try {
            CONFIG.store(new FileWriter(getStore()), "");
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private static File getStore() throws IOException {
        if (store == null) {
            store = Files.createTempFile("logisense.config", ".properties").toFile();
        }
        return store;
    }

    /**
     * Get the url to a file containing the entire configuration
     *
     * @return
     * @throws IOException
     */
    public static URL getConfigURL() throws IOException {
        return getStore().toURI().toURL();
    }

    /**
     * get value for property WARNING, this method fails hard when property
     * doesn't exist (RuntimeException)
     *
     * @param property
     * @return
     */
    public static String getValue(String property) {
        Map<String, String> prop = properties.get(property);
        //
        // fail hard when property doesn't exist
        //
        if (prop != null) {
//            log.trace("dumping property {}", property);
//            dumpProperty(prop);
            //
            // look for first non null value in order of priority
            //
            for (String valueKey : priority) {
                if (prop.containsKey(valueKey) && prop.get(valueKey) != null) {
                    //
                    // value found, returning
                    //
                    return prop.get(valueKey);
                }
            }
        } else {
            log.warn("property {} not found in Config", property);
        }
        throw new RuntimeException("No value found for property " + property);
    }

    /**
     * debug method
     *
     * @param values
     */
    private static void dumpProperty(Map<String, String> values) {
        values.keySet().stream().forEach((key) -> {
            log.trace("type: {}, value: {}", key, values.get(key));
        });
    }
}
