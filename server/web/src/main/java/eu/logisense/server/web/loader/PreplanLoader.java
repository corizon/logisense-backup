/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web.loader;

import au.com.bytecode.opencsv.CSVReader;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.Origin;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import eu.logisense.server.web.Labels;
import eu.logisense.server.web.Relations;
import eu.logisense.server.web.model.ObjectFactory;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;

/**
 *
 * @author johan
 */
public class PreplanLoader {
    private static final Logger LOGGER = Logger.getLogger(PreplanLoader.class.getName());
    
    private static final Path DEFAULT_SCAN_FOLDER = Paths.get("/tmp/logisense/pronorm/");//TODO: ConFig.get(...)
    private static final Path DEFAULT_DONE_FOLDER = DEFAULT_SCAN_FOLDER.resolve("done");
    private static final Path DEFAULT_FAILED_FOLDER = DEFAULT_SCAN_FOLDER.resolve("failed");
    
    private final GraphDatabaseService db;

    public PreplanLoader(GraphDatabaseService db) {
        this.db = db;
    }
    
    public void scan() {
        loadFolder(DEFAULT_SCAN_FOLDER);
    }
    
    protected void loadFolder(Path p) {
        if (p == null || !Files.exists(p) || !Files.isDirectory(p)) {
            LOGGER.log(Level.WARNING, "invalid root folder: {0}", p);
            return;
        }
        
        try {
            Files.find(p, 1, this::isCsvFile).forEach((f)-> processFile(f, null));
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
    protected boolean isCsvFile(Path p, BasicFileAttributes a) {
        return a.isRegularFile() && p.getFileName().toString().toUpperCase().endsWith("CSV");
    }
    
    public void processFile(Path file, String clientId) {
        LOGGER.log(Level.INFO, "processing file {0}", file);
        try {
            loadFile(file, clientId);
            moveInto(DEFAULT_DONE_FOLDER, file);
        } catch(Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            moveInto(DEFAULT_FAILED_FOLDER, file);
        }
    }
    
    private void moveInto(Path dirPath, Path filePath) {
        try {
            Files.move(filePath, dirPath.resolve(filePath.getFileName()));
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
    protected void loadFile(Path path, String clientId) {
        //TODO:
        // csv: per row via opencsv, or cypher "LOAD CSV" + some post-processing (uuid, dates, )
        // xls/xlsx: per row via POI SS, or convert to csv
        
        Node clientLocation = getClientLocation(clientId);
        if (clientLocation == null) {
            LOGGER.warning("failed to find or create default client location");
            return;
        }
        
        try (CSVReader reader = new CSVReader(new FileReader(path.toFile()), ';', '"', 1)) {
            String[] line;
            while ((line = reader.readNext()) != null) {
                doLine(line, clientLocation);
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
    protected void doLine(String[] line, Node oLocation) {
        // commit per line for now
        try (Transaction tx = db.beginTx()) {

            //Nodes:
            
            //order
            Node order = db.createNode(Labels.ORDER);
            String orderId = nextId();
            order.setProperty(Order.PROP_ID, orderId);
            order.setProperty(Order.PROP_NAME, Labels.ORDER + orderId);
            order.setProperty("loadingList", line[2]);
            order.setProperty("batch", line[14]);
            
            //shipment
            Node shipment = db.createNode(Labels.SHIPMENT);
            String shipmentId = nextId();
            shipment.setProperty(Shipment.PROP_ID, shipmentId);
            shipment.setProperty(Shipment.PROP_NAME, Labels.SHIPMENT + shipmentId);
            //FIXME volume is calculated property of order line
//            shipment.setProperty(Shipment.PROP_VOLUME, Double.valueOf(line[21]));
//FIXME weight is property of order line
//            shipment.setProperty(Shipment.PROP_WEIGHT, Double.valueOf(line[22]));
            shipment.setProperty(Shipment.PROP_NOTE, line[23]);
//            shipment.setProperty(Shipment.PROP_DEADLINE, line[24]);
            //FIXME: model requires date,
            //  but sample input file has inconsistent and incomplete strings (if a value is present at all),
            //  like "01.02.", "Fixtermin: 05.02.2016", "Bis 02.02.", ...
            try {
                LocalDate deadline = LocalDate.parse(line[24], DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                shipment.setProperty(Shipment.PROP_DEADLINE, ObjectFactory.toString(deadline));
            } catch (DateTimeParseException ex) {
                LOGGER.log(Level.WARNING, "could not convert value for `deadline` to date: {0}", line[24]);
                shipment.setProperty(Shipment.PROP_DEADLINE, ObjectFactory.toString(LocalDate.MAX));
            }
            shipment.setProperty(Shipment.PROP_STATUS, TransportStatus.NEW.name());
            
            //destination location
            Node dLocation  = db.createNode(Labels.LOCATION);
            dLocation.setProperty(Resource.PROP_ID, nextId());
            dLocation.setProperty(Location.PROP_NAME, line[16]);
            dLocation.setProperty(Location.PROP_POSTALCODE, line[19]);
            dLocation.setProperty(Location.PROP_CITY, line[20]);
            dLocation.setProperty(Location.PROP_COUNTRY, line[18]);
            //TODO: geocoding
            dLocation.setProperty(Location.PROP_LATITUDE, 0.0);
            dLocation.setProperty(Location.PROP_LONGITUDE, 0.0);
            
            //Relationships:
            
            //order-[SHIPPED_AS]->shipment
            order.createRelationshipTo(shipment, Relations.SHIPPED_AS);
            
            //shipment-[ORIGIN]->oLocation
            Relationship origin = shipment.createRelationshipTo(oLocation, Relations.ORIGIN);
            try {
                LocalDate loadingDate = LocalDate.parse(line[4], DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                origin.setProperty(LoadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(loadingDate));
                origin.setProperty(LoadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(loadingDate));
            } catch (DateTimeParseException ex) {
                LOGGER.log(Level.WARNING, "could not convert value for `loadingDate` to date: {0}", line[4]);
                origin.setProperty(LoadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDate.MAX));
                origin.setProperty(LoadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDate.MAX));
            }
           
            //shipment-[DESTINATION]->dLocation
            Relationship destination = shipment.createRelationshipTo(dLocation, Relations.DESTINATION);
            destination.setProperty(UnloadLocation.PROP_TIME_FRAME_FROM, ObjectFactory.toString(LocalDateTime.MAX));
            destination.setProperty(UnloadLocation.PROP_TIME_FRAME_TO, ObjectFactory.toString(LocalDateTime.MAX));
            
            tx.success();
        }
    }

    protected Node getClientLocation(String clientId) {
        return getOrCreateDefaultClientLocation();
    }
    
    protected Node getOrCreateDefaultClientLocation() {
        String pronorm = "pronorm Einbauküchen GmbH";
        Node location;
        try (Transaction tx = db.beginTx()) {
            Result r = db.execute("MATCH ( location:LOCATION { name: '" + pronorm + "' }) RETURN location");
            if (r.hasNext()) {
                Map<String, Object> map = r.next();
                location = (Node) map.get("location");
            } else {
                location = db.createNode(Labels.LOCATION);
                location.setProperty(Resource.PROP_ID, nextId());
                location.setProperty(Location.PROP_NAME, pronorm);
                location.setProperty(Location.PROP_POSTALCODE, "32602");
                location.setProperty(Location.PROP_CITY, "Vlotho");
                location.setProperty(Location.PROP_COUNTRY, "DE");
                location.setProperty(Location.PROP_LATITUDE, 52.181);
                location.setProperty(Location.PROP_LONGITUDE, 8.868);
            }
            tx.success();
        }
        return location;
    }

    private String nextId() {
        return UUID.randomUUID().toString();
    }
    
}
