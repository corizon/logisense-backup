/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web.loader;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.neo4j.graphdb.GraphDatabaseService;

/**
 *
 * @author johan
 */
public enum PreplanLoaderManager {
    INSTANCE;
    
    private static final Logger LOGGER = Logger.getLogger(PreplanLoaderManager.class.getName());
    
    private final AtomicBoolean scanRunning = new AtomicBoolean(false);

    public static PreplanLoaderManager getInstance() {
        return INSTANCE;
    }
    
    public boolean isScanRunning() {
        return scanRunning.get();
    }
    
    public synchronized boolean submitScanJob(final GraphDatabaseService db) {
        // single server-side folder to scan, so one job at a time please
        if (scanRunning.get()) return false;
        
        scanRunning.set(true);
        new Thread(()-> {
            try {
                new PreplanLoader(db).scan();
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
            scanRunning.set(false);
        }, "PreplanFolderScanner").start();
        
        return true;
    }
    
    //TODO: submit job for uploaded file; unlike scan, this should be able to handle multiple in parallel
}
