/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools |
 * Templates and open the template in the editor.
 */
package eu.logisense.server.web;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Location;
import eu.logisense.api.LocationService;
import static eu.logisense.server.web.Labels.LOCATION;
import eu.logisense.server.web.model.LocationFactory;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.MultipleFoundException;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author johan
 */
public class LocationResource extends BaseNeo4jService implements LocationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationResource.class);

    public LocationResource(GraphDatabaseService graphDb) throws IOException, URISyntaxException {
        super(graphDb);
    }

    public LocationResource() throws IOException, URISyntaxException {
    }

    @Override
    public List<Location> getLocations() {
        LOGGER.debug("getLocations");
        List<Location> locations = new ArrayList<>();

        try (Transaction tx = graphDb.beginTx();
                ResourceIterator<Node> list = graphDb.findNodes(LOCATION)) {
            while (list.hasNext()) {
                Node node = list.next();
                Location location = LocationFactory.fromNode(node);
                locations.add(location);
            }
            tx.success();
        }
        return locations;
    }

    @Override
    public Location getLocation(String id) {
        LOGGER.debug("getLocation id: {}", id);

        try (Transaction tx = graphDb.beginTx()) {
            Node node = graphDb.findNode(LOCATION, Location.PROP_ID, id);
            if (node == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            Location location = LocationFactory.fromNode(node);
            tx.success();
            return location;
        } catch (MultipleFoundException ex) {
            throw new WebApplicationException(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public Location postLocation(Location template) {
        LOGGER.debug("postLocation: {}", template);
        return LocationFactory.fromNode(getOrCreateLocationNode(template));

    }

    // set all non-id properties, shared between create and update
    protected void setNodeProperties(Node node, Location template) {
        node.setProperty(Location.PROP_NAME, template.getName());

        // several attrs might be empty in the plan&go import; 
        // TODO: determine which are required, so we can validate before posting to the rest api;
        setProperty(node, Location.PROP_ADDRESS, template.getAddress());
        setProperty(node, Location.PROP_CITY, template.getCity());
        setProperty(node, Location.PROP_COUNTRY, template.getCountry());
        setProperty(node, Location.PROP_POSTALCODE, template.getPostalcode());
        setProperty(node, Location.PROP_LATITUDE, template.getLatitude());
        setProperty(node, Location.PROP_LONGITUDE, template.getLongitude());
        setProperty(node, Location.PROP_PHONE, template.getPhone());
        setProperty(node, Location.PROP_EMAIL, template.getEmail());
        setProperty(node, Location.PROP_EXTERNAL_ID, template.getExternalId());
        if (template.getStatus() == null) {
            setProperty(node, Location.PROP_STATUS, ApprovalStatus.NEW.name());
        } else {
            setProperty(node, Location.PROP_STATUS, template.getStatus().name());
        }
    }

    @Override
    public Location updateLocation(String id, Location template) {
        LOGGER.debug("updateLocation id {} name {}", template.getId(), template.getName());

        try (Transaction tx = graphDb.beginTx()) {
            Node node = graphDb.findNode(LOCATION, Location.PROP_ID, id);
            if (node == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            setNodeProperties(node, template);
            Location updated = LocationFactory.fromNode(node);
            tx.success();
            return updated;
        } catch (MultipleFoundException ex) {
            throw new WebApplicationException(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void deleteLocation(String id) {
        LOGGER.debug("deleteLocation id: {}", id);

        try (Transaction tx = graphDb.beginTx()) {
            Node node = graphDb.findNode(LOCATION, Location.PROP_ID, id);
            if (node == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            node.getRelationships().forEach(r -> r.delete());
            node.delete();
            tx.success();
        } catch (MultipleFoundException ex) {
            throw new WebApplicationException(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

}
