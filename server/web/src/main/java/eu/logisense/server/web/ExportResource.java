/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.opower.rest.client.generator.core.ClientResponseFailure;
import eu.logisense.api.Export;
import eu.logisense.api.ExportService;
import eu.logisense.api.ExportStatus;
import eu.logisense.api.Link;
import eu.logisense.api.Ride;
import eu.logisense.api.AcceptShipmentsCapability;
import eu.logisense.api.Shipment;
import eu.logisense.api.Client;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Container;
import eu.logisense.api.Location;
import eu.logisense.api.Resource;
import eu.logisense.api.util.ClientUtil;
import eu.logisense.api.vms.ClientLocationVM;

import eu.logisense.api.vms.ExportCardVM;
import eu.logisense.api.vms.ExportClientVM;
import eu.logisense.api.vms.ExportLocationVM;
import eu.logisense.api.vms.ExportObjectCardVM;
import static eu.logisense.server.web.BaseNeo4jService.graphDb;
import static eu.logisense.server.web.Labels.*;
import static eu.logisense.server.web.Relations.*;
import eu.logisense.server.web.model.ExportFactory;
import eu.logisense.server.web.model.LinkFactory;
import eu.logisense.server.web.model.LocationFactory;
import eu.logisense.server.web.model.ObjectFactory;
import eu.logisense.server.web.model.RideFactory;
import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/exports")
public class ExportResource extends BaseNeo4jService implements ExportService {

    private static final Logger logger = LoggerFactory.getLogger(ExportResource.class);
    private AcceptShipmentsCapability usageService;

    private final ShipmentResource shipmentResource;
    private final RideResource rideResource;
    private final ClientResource clientResource;


    public ExportResource() throws IOException, URISyntaxException {

        shipmentResource = new ShipmentResource(graphDb);
        rideResource = new RideResource(graphDb);
        clientResource = new ClientResource(graphDb);
    }

    public ExportResource(GraphDatabaseService graphDb) throws IOException, URISyntaxException {
        super(graphDb);

        shipmentResource = new ShipmentResource(graphDb);
        rideResource = new RideResource(graphDb);
        clientResource = new ClientResource(graphDb);

    }

    public void setUsageService(AcceptShipmentsCapability usageService) {
        this.usageService = usageService;
    }

    @Override
    public List<Export> getExports() {
        List<Export> exports = new ArrayList<>();

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (p:" + EXPORT + ")"
                        + " RETURN p.id, p")) {
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                exports.add(ExportFactory.fromNode((Node) row.get("p")));
            }
            tx.success();
        }
        return exports;
    }

    @Override
    public Export getExport(String id) {
        logger.debug("getExport id:{}", id);
        Export export = null;

        Map<String, Object> params = new HashMap<>();
        params.put("exportId", id);

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute("MATCH  (s:" + EXPORT + "{id:{exportId}})-[r]->(t) RETURN s,r,t", params)) {
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                export = ExportFactory.fromNode((Node) row.get("s"));

                Relationship r = (Relationship) row.get("r");
                //
                // check type of relationship and parse object
                //
                if (r != null && r.getType().name().equals(EXPORTED_IN.name())) {

                    Link link = LinkFactory.fromNode((Node) row.get("t"));
                    export.getElements().add(link);

                }
            }

            tx.success();
        }
        return export;

    }

    @Override
    public Export postExport(Export template) {
        logger.debug("post export:{}", template);
        Export created = null;
        if (template != null) {

            try (Transaction tx = graphDb.beginTx()) {
                Node node = graphDb.createNode(Labels.EXPORT);
                String id = UUID.randomUUID().toString();
                node.setProperty(Export.PROP_NAME, template.getName());
                node.setProperty(Export.PROP_ID, id);
                node.setProperty(Export.PROP_HREF, (template.getHref() == null) ? "" : template.getHref());
                node.setProperty(Export.PROP_STATUS, ExportStatus.NEW.name());
                node.setProperty(Export.PROP_STATUS_DATE, ObjectFactory.toString(LocalDateTime.now()));

                if (template.getElements() != null) {
                    template.getElements().stream().map((link) -> getExportedNode(link)).forEach((exportedNode) -> {
                        if (exportedNode == null) {
                            logger.warn("object in export which cannot be found");
                        } else {
                            node.createRelationshipTo(exportedNode, EXPORTED_IN);
                        }
                    });
                }

                created = ExportFactory.fromNode(node);
                if (template.getElements() != null) {
                    for (Link link : template.getElements()) {
                        Node exportedNode = getExportedNode(link);
                        if (exportedNode == null) {
                            logger.warn("object in export which cannot be found");
                        } else {
                            created.getElements().add(LinkFactory.fromNode(exportedNode));
                        }
                    }
                }

                tx.success();
            }
        }
        return created;
    }

    @Override
    public Export updateExport(Export template) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteExport(String id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Export dispatch(String exportId) {

        Export exportDump = null;
        try {

            exportDump = getExport(exportId);
            ExportObjectCardVM exportCardVMDump = getExportCard(exportId);

            exportCardVMDump.setShipments(null);
            exportCardVMDump.setRides(null);
            logger.debug("exportCardVMDump id {}", exportCardVMDump.getExportId());

            usageService.exportObjects(exportCardVMDump);

            Map<String, Object> params = new HashMap<>();
            params.put("exportId", exportId);
            params.put("status", ExportStatus.DISPATCHED.name());
            params.put("statusDate", ObjectFactory.toString(LocalDateTime.now()));

            try (Transaction tx = graphDb.beginTx();
                    Result result = graphDb.execute(
                            "MATCH (export:EXPORT {id: {exportId}})-[r]->(t) "
                            + " SET export.status = {status},"
                            + " export.statusDate = {statusDate}"
                            + " RETURN export, r", params)) {
                if (result.hasNext()) {

                    // obtain the export recently updated
                    exportDump = getExport(exportId);
                }
                tx.success();
            }

        } catch (IOException ex) {
            logger.error("Error when sending location: {}", ex);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return exportDump;
    }

    private Node createExportNode() {
        Node exportNode = graphDb.createNode(EXPORT);
        String id = UUID.randomUUID().toString();
        exportNode.setProperty(Export.PROP_ID, id);
        exportNode.setProperty(Export.PROP_NAME, "automated export");
        exportNode.setProperty(Export.PROP_HREF, "/exports/" + id);
        exportNode.setProperty(Export.PROP_STATUS, ExportStatus.DISPATCHED.name());
        exportNode.setProperty(Export.PROP_STATUS_DATE, ObjectFactory.toString(LocalDateTime.now()));
        return exportNode;
    }

    public void dispatchAll() throws IOException, URISyntaxException {
        dispatchAllLocations();
        dispatchAllClients();
        dispatchAllObjects();

    }

    public void dispatchAllObjectsByResources() throws IOException {
        ExportObjectCardVM exportObject = new ExportObjectCardVM();
        List<Ride> rides = rideResource.getRides();
        if (rides != null) {
            rides.stream().forEach((ride) -> {
                exportObject.getRides().add(ride);
            });
        }
        List<Shipment> shipments = shipmentResource.getShipments();
        shipments.stream().forEach((shipment) -> {
            exportObject.getShipments().add(shipment);
        });

        try {
            usageService.exportObjects(exportObject);
        } catch (ClientResponseFailure e) {
            logger.error("Error when sending location: {}", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

    }

    public void dispatchAllObjects() throws IOException {
        logger.debug("dispatchAllObjects");
        try (Transaction tx = graphDb.beginTx()) {
            Node exportNode = null;

            List<Shipment> shipments = new ArrayList<>();
            List<Ride> rides = new ArrayList<>();

            Result rOrders = graphDb.execute(" MATCH (shipment:SHIPMENT)<-[:SHIPPED_AS]-(order:ORDER)<-[:DEBITOR]-(:CLIENT {status:'" + ApprovalStatus.APPROVED + "'}) "
                    + " WHERE NOT (shipment)<-[:EXPORTED_IN]-(:EXPORT) "
                    + " RETURN shipment,shipment.id,order,order.id");
            while (rOrders.hasNext()) {
                exportNode = (exportNode == null) ? createExportNode() : exportNode;
                Map<String, Object> row = rOrders.next();
                Node shipmentNode = (Node) row.get("shipment");
                exportNode.createRelationshipTo(shipmentNode, EXPORTED_IN);
                Shipment shipment = shipmentResource.getShipment((String) row.get("shipment.id"));
                shipments.add(shipment);
            }
            logger.info("gathered {} shipments", shipments.size());

            Result rRides = graphDb.execute(//                    " MATCH (ride:RIDE)<-[:CREDITOR]-(:CLIENT {status:'"+ClientStatus.APPROVED+"'}) "
                    " MATCH (ride:RIDE)<-[:CREDITOR]-(:CLIENT {status:'" + ApprovalStatus.APPROVED + "'}) "
                    + " WHERE NOT (ride)<-[:EXPORTED_IN]-(:EXPORT) "
                    + " RETURN ride,ride.id");
            while (rRides.hasNext()) {
                exportNode = (exportNode == null) ? createExportNode() : exportNode;
                Map<String, Object> row = rRides.next();
                Node rideNode = (Node) row.get("ride");
                exportNode.createRelationshipTo(rideNode, EXPORTED_IN);
                rides.add(rideResource.getRide((String) row.get("ride.id")));
                //
                // replace shipment resources with full shipments
                //
                rides.stream().forEach((ride)->{
                    ride.getContainers().stream().forEach((container)->{
                        List<Shipment> fullShipments = new ArrayList<>();
                        container.getShipments().stream().forEach((shipment)->{
                            fullShipments.add(shipmentResource.getShipment(shipment.getId()));
                        });
                        container.getShipments().clear();
                        container.getShipments().addAll(fullShipments);
                    });
                });
            }
            // replace shipment resources with full shipments

            for (Ride ride : rides) {
                if (ride.getContainers() != null) {
                    for (Container container : ride.getContainers()) {
                        if (container.getShipments() != null) {
                            List<Resource> fullShipmentList = new ArrayList<>();
                            for (Resource resource : container.getShipments()) {
                                Shipment shipment = shipmentResource.getShipment(resource.getId());
                                fullShipmentList.add(resource);
                            }
                            container.getShipments().clear();
                            container.setShipments(fullShipmentList);
                        }

                    }
                }
            }

            logger.info("gathered {} rides", rides.size());

            if (exportNode != null) {

                ExportObjectCardVM exportCardVM = ExportObjectCardVM.builder()
                        .exportId((String) exportNode.getProperty(Export.PROP_ID))
                        .shipments(shipments)
                        .rides(rides)
                        .build();

                try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

                    ObjectMapper mapper = new ObjectMapper();
                    mapper.registerModule(new JSR310Module());
                    JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

                    jsonProvider.writeTo(exportCardVM, ExportCardVM.class, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
                    logger.debug("sending ExportCardVM {}", baos.toString());
                    usageService.exportObjects(exportCardVM);
                } catch (ClientResponseFailure e) {
                    logger.error("Error when sending location: {}", e);
                    // throwing the exception here would let rest of the export fail
                    //throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
                }
            } else {
                logger.debug("nothing to export");
            }
            tx.success();
            logger.info("export dispathed");
        }
    }

    public void dispatchAllLocations() throws IOException {
        logger.info("dispatchAll");
        try (Transaction tx = graphDb.beginTx()) {
            Node exportNode = null;

            List<Location> locations = new ArrayList<>();

            Result locationResult = graphDb.execute(
                    " MATCH (location:LOCATION)"
                    + " WHERE NOT (location)<-[:EXPORTED_IN]-(:EXPORT) "
                    + " RETURN location");
//            logger.debug(locationResult.resultAsString());
            while (locationResult.hasNext()) {
                exportNode = (exportNode == null) ? createExportNode() : exportNode;
                Map<String, Object> row = locationResult.next();
                Node locationNode = (Node) row.get("location");
                exportNode.createRelationshipTo(locationNode, EXPORTED_IN);
                Location location = LocationFactory.fromNode(locationNode);

                locations.add(location);
            }
            logger.info("gathered {} locations", locations.size());
            if (exportNode != null) {
                final Node export = exportNode;

                locations.stream().forEach((location) -> {
                    ExportLocationVM exportLocationVM = ExportLocationVM.builder()
                            .exportId((String) export.getProperty(Export.PROP_ID))
                            .location(location)
                            .build();

                    ObjectMapper mapper = new ObjectMapper();
                    mapper.registerModule(new JSR310Module());
                    JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

                    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                        jsonProvider.writeTo(exportLocationVM, ExportLocationVM.class, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
                        logger.debug("sending location: {}", baos.toString());
                        usageService.exportLocation(exportLocationVM);
                    } catch (ClientResponseFailure | IOException e) {
                        logger.error("Error when sending location: {}", e);
                        // throwing the exception here would let rest of the export fail
//                        throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
                    }
                });
            } else {
                logger.debug("nothing to export");
            }
            tx.success();
        }
    }

    public void dispatchAllClients() throws IOException {
        logger.info("dispatchAllClients");
        try (Transaction tx = graphDb.beginTx()) {
            Node exportNode = null;

            List<ClientLocationVM> clients = new ArrayList<>();

            Result clientResult = graphDb.execute(" MATCH (client:CLIENT {status:'" + ApprovalStatus.NEW.name() + "'})"
                    + " WHERE NOT (client)<-[:EXPORTED_IN]-(:EXPORT) "
                    + " RETURN client,client.id");
            while (clientResult.hasNext()) {
                exportNode = (exportNode == null) ? createExportNode() : exportNode;
                Map<String, Object> row = clientResult.next();
                Node clientNode = (Node) row.get("client");
                exportNode.createRelationshipTo(clientNode, EXPORTED_IN);
                Client client = clientResource.getClient((String) row.get("client.id"));
                clients.add(ClientLocationVM.builder()
                        .client(client)
                        .isCreditor(clientNode.hasRelationship(CREDITOR) ? "J" : "N")
                        .isDebitor(clientNode.hasRelationship(DEBITOR) ? "J" : "N")
                        .chargeVat(client.getChargeVat() != null && client.getChargeVat() ? "J" : "N")
                        .location(client.getLocations().get(0))
                        .build());
            }
            logger.info("gathered {} client", clients.size());

            if (exportNode != null) {
                final Node export = exportNode;
                clients.stream().forEach((clientLocation) -> {
                    ExportClientVM exportClientVM = ExportClientVM.builder()
                            .exportId((String) export.getProperty(Export.PROP_ID))
                            .clientLocation(clientLocation)
                            .build();

                    ObjectMapper mapper = new ObjectMapper();
                    mapper.registerModule(new JSR310Module());
                    JacksonJsonProvider jsonProvider = new JacksonJsonProvider(mapper);

                    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                        jsonProvider.writeTo(exportClientVM, ExportClientVM.class, null, null, MediaType.APPLICATION_JSON_TYPE, null, baos);
                        logger.debug(baos.toString());
                        logger.debug("sending client export");
                        usageService.exportClient(exportClientVM);
                        logger.debug("client export send");
                    } catch (ClientResponseFailure | IOException e) {
                        logger.error("Error when sending location: {}", e);
                        // throwing the exception here would let rest of the export fail
                        //throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
                    }
                });
            } else {
                logger.debug("nothing to export");
            }
            tx.success();
            logger.info("export dispathed");
        }
    }

    // ***************************** PRIVATE METHODS  ***************************** 
    /**
     *
     * @param link
     * @return
     */
    private Node getExportedNode(Link link) {
        logger.debug("getExportedNode:{}", link.getHref());
        Node node = null;

        Map<String, Object> params = new HashMap<>();
        params.put("href", link.getHref());

        boolean found = false;

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute("MATCH (s:" + ORDER + ") WHERE s.href ='" + link.getHref() + "' RETURN s")) {
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                node = (Node) row.get("s");
                found = true;
            }
            tx.success();
        }
        // TODO maybe is possible to combine in one query
        if (!found) {
            try (Transaction tx = graphDb.beginTx();
                    Result result = graphDb.execute("MATCH (s:" + RIDE + ") WHERE s.href ='" + link.getHref() + "' RETURN s")) {
                while (result.hasNext()) {
                    Map<String, Object> row = result.next();
                    node = (Node) row.get("s");
                    found = true;
                }
                tx.success();
            }
        }
        // TODO maybe is possible to combine in one query
        if (!found) {
            try (Transaction tx = graphDb.beginTx();
                    Result result = graphDb.execute("MATCH (s:" + CLIENT + ") WHERE s.href ='" + link.getHref() + "' RETURN s")) {
                while (result.hasNext()) {
                    Map<String, Object> row = result.next();
                    node = (Node) row.get("s");
                    found = true;
                }
                tx.success();
            }
        }
        // TODO maybe is possible to combine in one query
        if (!found) {
            try (Transaction tx = graphDb.beginTx();
                    Result result = graphDb.execute("MATCH (s:" + SHIPMENT + ") WHERE s.href ='" + link.getHref() + "' RETURN s")) {
                while (result.hasNext()) {
                    Map<String, Object> row = result.next();
                    node = (Node) row.get("s");
                }
                tx.success();
            }
        }
        return node;
    }

    public ExportObjectCardVM getExportCard(String id) {
        Export export = getExport(id);
        if (export == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        List<Shipment> shipments = new ArrayList<>();
        List<Ride> riders = new ArrayList<>();

        try (Transaction tx = graphDb.beginTx()) {
            export.getElements().stream().map((link) -> getExportedNode(link)).forEach((node) -> {
                if (node.hasLabel(SHIPMENT)) {
                    shipments.add(this.shipmentResource.getShipment((String) node.getProperty(Shipment.PROP_ID)));
                } else if (node.hasLabel(RIDE)) {
                    riders.add(RideFactory.fromNode(node));
                }
            });
            tx.success();
        }
        return ExportObjectCardVM.builder().exportId(export.getId()).shipments(shipments).rides(riders).build();
    }

}
