/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

import static eu.logisense.server.web.Labels.CONTAINER;
import static eu.logisense.server.web.Relations.PART_OF;
import eu.logisense.api.Container;
import eu.logisense.api.Link;
import eu.logisense.api.ContainerService;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import static eu.logisense.server.web.BaseNeo4jService.graphDb;
import static eu.logisense.server.web.Labels.SHIPMENT;
import eu.logisense.server.web.model.ContainerFactory;
import eu.logisense.server.web.model.ShipmentFactory;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Result;

public class ContainerResource extends BaseNeo4jService implements ContainerService {

    private static final Logger logger = LoggerFactory.getLogger(ContainerResource.class);
    private final ShipmentResource shipmentResource;

    public ContainerResource(GraphDatabaseService graphDb) throws IOException, URISyntaxException {
        super(graphDb);
        shipmentResource = new ShipmentResource(graphDb);
    }

    public ContainerResource() throws IOException, URISyntaxException {
        shipmentResource = new ShipmentResource(graphDb);
    }

    @Override
    public List<Container> getContainers() {
        List<Container> containers = new ArrayList<>();

        try (Transaction tx = graphDb.beginTx();
                ResourceIterator<Node> list = graphDb.findNodes(CONTAINER)) {
            while (list.hasNext()) {
                Node node = list.next();
                Container container = ContainerFactory.fromNode(node);

                Iterable<Relationship> shipmentRelationship = node.getRelationships(Relations.SHIPPED_IN, Direction.INCOMING);
                for (Relationship item : shipmentRelationship) {
                    container.getShipments().add(ShipmentFactory.fromNode(item.getStartNode()));
                }
                containers.add(container);
            }
            tx.success();
        }
        return containers;
    }

    @Override
    public Container getContainer(String id) {
        logger.debug("getContainer id:{}", id);
        Container container = null;

        // Maybe is not the best, also, taking into account possible SQL Injection equivalencies
        try (Transaction tx = graphDb.beginTx()) {
            ResourceIterator<Node> list = graphDb.findNodes(CONTAINER, "id", id);
            while (list.hasNext()) {
                Node containerNode = list.next();
                container = ContainerFactory.fromNode(containerNode);

                Iterable<Relationship> shipmentRelationship = containerNode.getRelationships(Relations.SHIPPED_IN, Direction.INCOMING);
                for (Relationship item : shipmentRelationship) {
                    container.getShipments().add(ShipmentFactory.fromNode(item.getStartNode()));
                }
            }
            tx.success();
            return container;
        }
    }

    private Node getShipmentNodeById(String id) {
        Objects.requireNonNull(id);

        Node shipmentNode = null;

        Map<String, Object> params = new HashMap<>();
        params.put("shipmentId", id);

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (s:" + SHIPMENT + " {id: {shipmentId}})-[r]->(t)"
                        + " RETURN s.id, s,r,t", params)) {
            if (!result.hasNext()) {
                logger.warn("No shipment found for id {}", id);
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                shipmentNode = (Node) row.get("s");
            }
            tx.success();
        }

        return shipmentNode;
    }

    @Override
    public Container postContainer(Container template) {
        logger.debug("post container:{}", template);
        Container created = null;

        try (Transaction tx = graphDb.beginTx()) {
            Node node = graphDb.createNode(Labels.CONTAINER);
            String id = UUID.randomUUID().toString();
            node.setProperty(Container.PROP_NAME, template.getName());
            node.setProperty(Container.PROP_ID, id);
            node.setProperty(Container.PROP_HREF, (template.getHref() == null) ? "" : template.getHref());

            node.setProperty(Container.PROP_MAX_WEIGHT, (template.getMaxWeight() == null) ? 0.0 : template.getMaxWeight());

            node.setProperty(Container.PROP_WIDTH, (template.getWidth() == null) ? 0.0 : template.getWidth());
            node.setProperty(Container.PROP_HEIGHT, (template.getHeight() == null) ? 0.0 : template.getHeight());
            node.setProperty(Container.PROP_LENGTH, (template.getLength() == null) ? 0.0 : template.getLength());

            if (template.getShipments() != null && !template.getShipments().isEmpty()) {
                // construct shipments

                for (Resource shipment : template.getShipments()) {
                    // Check is the node already exist in Graph
                    Shipment shipmentToUse = shipmentResource.getShipment(shipment.getId());

                    if (shipmentToUse == null) {
                        //create node
                        throw new WebApplicationException("Linked shipment with id " + shipment.getHref() + " does not exist", Response.Status.CREATED);
                    }
                    Node shipmentNode = getShipmentNodeById(shipmentToUse.getId());

                    Relationship relationship = node.createRelationshipTo(shipmentNode, PART_OF);
                    relationship.setProperty("shipmentId", shipmentToUse.getId());
                }

            }

            //
            // create the container object from the nodes and relations
            //
            created = ContainerFactory.fromNode(node);

            // In case to have nested object
            if (template.getShipments() != null && !template.getShipments().isEmpty()) {
                List<Resource> retrievedShipment = new ArrayList<>();

                for (Resource shipment : template.getShipments()) {
                    retrievedShipment.add(shipment);

                }
                created.setShipments(retrievedShipment);
            }
            tx.success();
        }
        return created;
    }

    @Override
    public Container updateContainer(Container template) {
        Container created = null;
        Map<String, Object> params = new HashMap<>();
        params.put("containerId", template.getId());
        params.put("name", template.getName());

        try (Transaction tx = graphDb.beginTx();
                Result result = graphDb.execute(
                        "MATCH (container:" + CONTAINER + " {id: {containerId}}) "
                        + " SET container.name = {name}"
                        + " RETURN container", params)) {
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                created = ContainerFactory.fromNode((Node) row.get("container"));
            }
            tx.success();
        }

        return created;
    }

    @Override
    public void deleteContainer(String id
    ) {
        throw new UnsupportedOperationException();
    }
}
