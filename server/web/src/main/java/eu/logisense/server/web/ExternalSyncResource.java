/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web;

import eu.logisense.api.Client;
import eu.logisense.api.ExternalSyncService;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.Resource;
import eu.logisense.api.Shipment;
import static eu.logisense.server.web.BaseNeo4jService.graphDb;
import static eu.logisense.server.web.Labels.CLIENT;
import static eu.logisense.server.web.Labels.LOCATION;
import static eu.logisense.server.web.Labels.ORDER;
import eu.logisense.server.web.model.ObjectFactory;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Objects;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author johan
 */
public class ExternalSyncResource  extends BaseNeo4jService implements ExternalSyncService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalSyncResource.class);
    
    private final ShipmentResource shipmentResource;
    
    public ExternalSyncResource(GraphDatabaseService graphDb) throws IOException, URISyntaxException {
        super(graphDb);
        shipmentResource = new ShipmentResource(graphDb);
    }

    public ExternalSyncResource() throws IOException, URISyntaxException {
        shipmentResource = new ShipmentResource(graphDb);
    }

    @Override
    public Shipment postShipment(Shipment template) {
        LOGGER.debug("postShipment:{}", template);
        
        Objects.requireNonNull(template);
        Objects.requireNonNull(template.getOrder());
        Objects.requireNonNull(template.getLoadLocation());
        Objects.requireNonNull(template.getUnloadLocation());
        
        Order order = template.getOrder();

        Objects.requireNonNull(order.getDebitor());
        Objects.requireNonNull(order.getOrderedBy());
//        Objects.requireNonNull(order.getDestination());
        Objects.requireNonNull(order.getOrigin());

        template = enrichIds(template);

        if (template.getId() != null || template.getOrder().getId() != null) {
            //template not ready for forward to update method of shipmentResource: 
            // template from plan&go misses several attributes (e.g. status), which will cause unwanted clearing of db values.
            // so we should fetch a full shipment object via the shipmentResource, and then apply changes for only the attributes that are present in the template,
            
//            shipmentResource.updateShipment(template);
            throw new UnsupportedOperationException("Update of existing shipment/order not supported yet");
        } else {
            return  shipmentResource.postShipment(template);
        }
        
    }
    
    private Shipment enrichIds(Shipment template) {
        try (Transaction tx = graphDb.beginTx()) {
            Order order = template.getOrder();
            
            Node orderNode = getOrderNode(order.getExternalId());
            
            if (orderNode != null) {
                order.setId(getOrderId(orderNode));
                template.setId(getShipmentId(orderNode));
            }
                
            order.getDebitor().setId(getClientId(order.getDebitor().getExternalId()));
            order.getOrderedBy().setId(getClientId(order.getOrderedBy().getExternalId()));

            if (order.getDestination() != null) {
                order.getDestination().setId(getLocationId(order.getDestination().getExternalId()));
            }
            if (order.getOrigin() != null) {
                order.getOrigin().setId(getLocationId(order.getOrigin().getExternalId()));
            }

            template.getLoadLocation().setId(getLocationId(template.getLoadLocation().getExternalId()));
            template.getUnloadLocation().setId(getLocationId(template.getUnloadLocation().getExternalId()));
            
            tx.success();
            return template;
        }
    }

    private Node getOrderNode(String externalId) {
        if (externalId == null || externalId.isEmpty()) return null;
        return graphDb.findNode(ORDER, Order.PROP_EXTERNAL_ID, externalId);
    }
    
    private String getOrderId(Node orderNode) {
        String id = ObjectFactory.getStringValue(orderNode, Resource.PROP_ID);
        LOGGER.debug("got id {}", id);
        return id;
    }
    
    private String getShipmentId(Node orderNode) {
        if (orderNode == null) return null;
        Relationship shippedAs = orderNode.getSingleRelationship(Relations.SHIPPED_AS, Direction.BOTH);
        if (shippedAs == null) return null;
        Node node = shippedAs.getOtherNode(orderNode);
        if (node == null) return null;
        String id = ObjectFactory.getStringValue(node, Resource.PROP_ID);
        LOGGER.debug("got id {}", id);
        return id;
    }

    private String getClientId(String externalId) {
        if (externalId == null || externalId.isEmpty()) return null;
        Node node = graphDb.findNode(CLIENT, Client.PROP_EXTERNAL_ID, externalId);
        if (node == null) return null;
        String id = ObjectFactory.getStringValue(node, Resource.PROP_ID);
        LOGGER.debug("got id {}", id);
        return id;
    }

    private String getLocationId(String externalId) {
        if (externalId == null || externalId.isEmpty()) return null;
        Node node = graphDb.findNode(LOCATION, Location.PROP_EXTERNAL_ID, externalId);
        if (node == null) return null;
        String id = ObjectFactory.getStringValue(node, Resource.PROP_ID);
        LOGGER.debug("got id {}", id);
        return id;
    }

}
