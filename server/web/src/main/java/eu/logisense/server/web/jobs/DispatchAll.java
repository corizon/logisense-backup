/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.jobs;

import eu.logisense.server.web.BaseNeo4jService;
import eu.logisense.server.web.ExportResource;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ws.rs.WebApplicationException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@Singleton
@Startup
public class DispatchAll extends BaseNeo4jService {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(DispatchAll.class);
    
    public DispatchAll(GraphDatabaseService graphDb) {
        super(graphDb);
    }

    public DispatchAll() throws IOException {
    }

    @  Schedule(second = "0", minute = "*/1", hour = "*", persistent = false)
    public void dispatchAll() {
        log.info("dispatchAllObjectsByResources");
        try {
            ExportResource instance = new ExportResource(graphDb);
            instance.dispatchAll();
           
        } catch (IOException | URISyntaxException | WebApplicationException ex) {
            log.error("Failed to dispatch export: {}",ex);
        }

    }
}
