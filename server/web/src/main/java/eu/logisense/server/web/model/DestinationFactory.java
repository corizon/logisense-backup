/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Destination;
import static eu.logisense.server.web.model.ObjectFactory.getApprovalStatusValue;
import static eu.logisense.server.web.model.ObjectFactory.getDoubleValue;
import static eu.logisense.server.web.model.ObjectFactory.getStringValue;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class DestinationFactory {
    public static Destination fromNode(Node node,Relationship relationship){
        return Destination.builder()
                .id(getStringValue(node, Destination.PROP_ID))
                .address(getStringValue(node, Destination.PROP_ADDRESS))
                .name(getStringValue(node, Destination.PROP_NAME))
                .href(getStringValue(node, Destination.PROP_HREF))                    
                .city(getStringValue(node, Destination.PROP_CITY))
                .country(getStringValue(node, Destination.PROP_COUNTRY))
                .latitude(getDoubleValue(node, Destination.PROP_LATITUDE))
                .longitude(getDoubleValue(node, Destination.PROP_LONGITUDE))
                .postalcode(getStringValue(node, Destination.PROP_POSTALCODE))
                .phone(getStringValue(node, Destination.PROP_PHONE))
                .email(getStringValue(node, Destination.PROP_EMAIL))                   
                .status(getApprovalStatusValue(node, Destination.PROP_STATUS))
                .externalId(getStringValue(node, Destination.PROP_EXTERNAL_ID))
                .build();
    }
}
