/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

import org.neo4j.graphdb.RelationshipType;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public enum Relations implements RelationshipType{
    /**
     * orders are shipped as one or more shipments
     */
    SHIPPED_AS,
    /**
     * shipment is shipped in a container
     */
    SHIPPED_IN,
    /**
     * containers are part of a ride
     */
    PART_OF,
    /**
     * The final location for an order
     */
    DESTINATION,
    /**
     * origin of the order
     */    
    ORIGIN,
    /**
     * location to load the shipment
     */
    LOAD,
    /**
     * location to unload the shipment
     */
    UNLOAD,
    /**
     * a client resides at a location
     */
    RESIDES,
    /**
     * a element exported in an export
     */
    EXPORTED_IN,
    /**
     * client is orderer for an order
     */
    ORDERER,
    /**
     * client that is paying for the ride
     */
    DEBITOR,
    /**
     * client that receives payment for the ride
     */
    CREDITOR,
    /**
     * order contains order lines
     */
    CONTAINS
    
    
}
