/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.OrderLineType;
import eu.logisense.api.TransportStatus;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.PropertyContainer;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ObjectFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectFactory.class);

    public static String toString(LocalDate date) {
        return (date == null) ? null : date.format(DateTimeFormatter.ISO_DATE);
    }

    public static String toString(LocalDateTime dateTime) {
        return (dateTime == null) ? null : dateTime.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public static Integer getIntegerValue(PropertyContainer pc, String property) {
        return (Integer) getProperty(pc, property);
    }

    public static Boolean getBooleanValue(PropertyContainer pc, String property) {
        return (Boolean) getProperty(pc, property);
    }

    public static String getStringValue(PropertyContainer pc, String property) {
        return (String) getProperty(pc, property);
    }

    public static ApprovalStatus getApprovalStatusValue(PropertyContainer pc, String property) {
        String value = (String) getProperty(pc, property);
        return value == null ? ApprovalStatus.NEW : ApprovalStatus.valueOf(value);
    }

    public static TransportStatus getTransportStatusValue(PropertyContainer pc, String property) {
        String value = (String) getProperty(pc, property);
        return value == null ? TransportStatus.NEW : TransportStatus.valueOf(value);
    }
    public static OrderLineType getOrderLineTypeValue(PropertyContainer pc, String property) {
        String value = (String) getProperty(pc, property);
        return value == null ? OrderLineType.EURO_PALLET : OrderLineType.valueOf(value);
    }

    public static Double getDoubleValue(PropertyContainer pc, String property) {
        return (Double) getProperty(pc, property);
    }

    public static Long getLongValue(PropertyContainer pc, String property) {
        return (Long) getProperty(pc, property);
    }

    public static BigDecimal getBigDecimalValue(PropertyContainer pc, String property) {
        return (BigDecimal) getProperty(pc, property);
    }

    public static LocalDate getDateValue(PropertyContainer pc, String property) {
        String s = getStringValue(pc, property);
        return s == null ? null : LocalDate.parse(s, DateTimeFormatter.ISO_DATE);
    }

    public static LocalDateTime getDateTimeValue(PropertyContainer pc, String property) {
        String s = getStringValue(pc, property);
        return s == null ? null : LocalDateTime.parse(s, DateTimeFormatter.ISO_DATE_TIME);
    }

    private static Object getProperty(PropertyContainer pc, String property) {
        Object value = null;
        //TODO: new transaction per property not needed when the calling method has already started a transaction
        // (requires different way of verifying Factory tests)
        Transaction tx = pc.getGraphDatabase().beginTx();
        try {
            //TODO: prefer hasProperty check over creating exception objects,
            // but keep catching the exception because of concurrent transactions
//            if (pc.hasProperty(property))//requires mocking of hasProperty in all Factory tests
            value = pc.getProperty(property);
//            else LOGGER.debug(" property {} not found ", property);
        } catch (NotFoundException e) {
            LOGGER.debug(" property {} not found ", property);
        } finally {
            tx.success();
        }
        return value;
    }
}
