/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.server.web;

import eu.logisense.server.web.loader.PreplanLoaderManager;
import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.neo4j.graphdb.GraphDatabaseService;

/**
 *
 * @author johan
 */
@Path("/pronorm")
public class PreplanLoaderResource extends BaseNeo4jService {

    public PreplanLoaderResource(GraphDatabaseService graphDb) {
        super(graphDb);
    }

    public PreplanLoaderResource() throws IOException {
    }
        
    /**
     * Scan server folder for csv/xls files to load into preplanning db.
     * 
     * @return 
     */
    @GET
    @Path("/scan")
    @Produces(MediaType.TEXT_PLAIN)
    public Response scan() {
        //TODO: async response; for now: only submit scanjob, 
        
        if (PreplanLoaderManager.getInstance().submitScanJob(graphDb)) {
            return Response.accepted("job submitted").build();
        } else {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("busy, another job is still running").build();
        }
    }

    //TODO: return status of currently running or recent scan jobs; log to db?
//    @GET
//    @Path("/status")
//    @Produces(MediaType.TEXT_PLAIN)
//    public Response status() {
//    }

    
    //TODO: upload file, e.g. via rcp GUI
    // public Response upload(String clientId, Object postedFile) {}
    
}
