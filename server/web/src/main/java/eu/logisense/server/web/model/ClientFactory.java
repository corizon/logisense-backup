/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.Client;
import eu.logisense.api.Resource;
import static eu.logisense.server.web.model.ObjectFactory.*;
import org.neo4j.graphdb.Node;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ClientFactory {
    
    public static Client fromNode(Node node) {
        return Client.builder()
                .id(getStringValue(node, Client.PROP_ID))
                .name(getStringValue(node, Client.PROP_NAME))
                .href(toHref(getStringValue(node, Client.PROP_ID)))
                .status(getApprovalStatusValue(node, Client.PROP_STATUS))
                .debtorNumber(getStringValue(node, Client.PROP_DEBTOR_NUMBER))
                .creditorNumber(getStringValue(node, Client.PROP_CREDITOR_NUMBER))
                .externalId(getStringValue(node, Client.PROP_EXTERNAL_ID))
                .bicCode(getStringValue(node, Client.PROP_BIC_CODE))
                .chamberOfCommerce(getStringValue(node, Client.PROP_CHAMBER_OF_COMMERCE))
                .chamberOfCommerceCity(getStringValue(node, Client.PROP_CHAMBER_OF_COMMERCE_CITY))
                .currency(getStringValue(node, Client.PROP_CURRENCY))
                .ibanCode(getStringValue(node, Client.PROP_IBAN_CODE))
                .vatNumber(getStringValue(node, Client.PROP_VAT_NUMBER))
                .chargeVat(getBooleanValue(node, Client.PROP_CHARGE_VAT))
                .build();
    }

    public static Resource resourceFromNode(Node node){
        return Resource.buildResource()
                .id(getStringValue(node, Client.PROP_ID))
                .name(getStringValue(node, Client.PROP_NAME))
                .href(toHref(getStringValue(node, Client.PROP_ID)))
                .build();
    }
   
    
    public static String toHref(String id) {
        return "/clients/" + id;
    }
}
