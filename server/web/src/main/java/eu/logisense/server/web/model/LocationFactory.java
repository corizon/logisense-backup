/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Location;
import static eu.logisense.server.web.model.ObjectFactory.getApprovalStatusValue;
import org.neo4j.graphdb.Node;
import static eu.logisense.server.web.model.ObjectFactory.getStringValue;
import static eu.logisense.server.web.model.ObjectFactory.getDoubleValue;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class LocationFactory {
    
    public static Location fromNode(Node node){
        return Location.buildLocation()
                .id(getStringValue(node, Location.PROP_ID))
                .name(getStringValue(node, Location.PROP_NAME))
                .href(getStringValue(node, Location.PROP_HREF))
                .city(getStringValue(node, Location.PROP_CITY))
                .country(getStringValue(node, Location.PROP_COUNTRY))
                .latitude(getDoubleValue(node, Location.PROP_LATITUDE))
                .longitude(getDoubleValue(node, Location.PROP_LONGITUDE))
                .name(getStringValue(node, Location.PROP_NAME))
                .postalcode(getStringValue(node, Location.PROP_POSTALCODE))
                .address(getStringValue(node, Location.PROP_ADDRESS))
                .phone(getStringValue(node, Location.PROP_PHONE))
                .email(getStringValue(node, Location.PROP_EMAIL))
                .status(getApprovalStatusValue(node, Location.PROP_STATUS))
                .externalId(getStringValue(node, Location.PROP_EXTERNAL_ID))
                .build();
    }
    public static String toHref(String id) {
        return "/locations/" + id;
    }
}
