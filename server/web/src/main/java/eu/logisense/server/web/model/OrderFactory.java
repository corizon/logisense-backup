/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import static eu.logisense.server.web.model.ObjectFactory.getStringValue;
import org.neo4j.graphdb.Node;
import eu.logisense.api.Order;

public class OrderFactory {

    public static Order fromNode(Node node) {
        return Order.builder()
                .id(getStringValue(node, Order.PROP_ID))
                .name(getStringValue(node, Order.PROP_NAME))
                .href(toHref(getStringValue(node, Order.PROP_ID)))
                .loadingList(getStringValue(node, Order.PROP_LOADING_LIST))
                .batch(getStringValue(node, Order.PROP_BATCH))
                .invoiceReference(getStringValue(node, Order.PROP_INVOICE_REFERENCE))
                .otherReference(getStringValue(node, Order.PROP_OTHER_REFERENCE))
                .unloadReference(getStringValue(node, Order.PROP_UNLOAD_REFERENCE))
                .externalId(getStringValue(node, Order.PROP_EXTERNAL_ID))
                .build();
    }

    public static String toHref(String id) {
        return "/orders/" + id;
    }
}
