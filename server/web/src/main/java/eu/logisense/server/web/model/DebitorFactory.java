/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.Debitor;
import static eu.logisense.server.web.model.ObjectFactory.*;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

/**
 *
 * @author kenrik
 */
public class DebitorFactory {

    public static Debitor fromNode(Node node, Relationship debitorRelation) {
        return Debitor.builder()
                .id(getStringValue(node, Debitor.PROP_ID))
                .name(getStringValue(node, Debitor.PROP_NAME))
                .href(toHref(getStringValue(node, Debitor.PROP_ID)))
                .price(getDoubleValue(debitorRelation, Debitor.PROP_PRICE))
                .currency(getStringValue(debitorRelation, Debitor.PROP_CURRENCY))
                .externalId(getStringValue(node, Debitor.PROP_EXTERNAL_ID))
                .chargeVat(getBooleanValue(node, Debitor.PROP_CHARGE_VAT))
                .build();
    }

    public static String toHref(String id) {
        return "/clients/" + id;
    }
}
