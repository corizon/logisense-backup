/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web;

import eu.logisense.api.Client;
import eu.logisense.api.ClientService;
import eu.logisense.api.Location;
import eu.logisense.server.web.model.ClientFactory;
import eu.logisense.server.web.model.LocationFactory;
import org.neo4j.graphdb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import static eu.logisense.server.web.Labels.CLIENT;
import static eu.logisense.server.web.Labels.LOCATION;
import static eu.logisense.server.web.Relations.RESIDES;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ClientResource extends BaseNeo4jService implements ClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientResource.class);
    private final ShipmentResource shipmentResource;
    private final LocationResource locationResource;

    public ClientResource(GraphDatabaseService graphDb) throws IOException, URISyntaxException {
        super(graphDb);
        shipmentResource = new ShipmentResource(graphDb);
        locationResource = new LocationResource(graphDb);
    }

    public ClientResource() throws IOException, URISyntaxException {
        shipmentResource = new ShipmentResource(graphDb);
        locationResource = new LocationResource(graphDb);
    }

    @Override
    public List<Client> getClients() {
        List<Client> clients = new ArrayList<>();

        try (Transaction tx = graphDb.beginTx();
                ResourceIterator<Node> list = graphDb.findNodes(CLIENT)) {
            while (list.hasNext()) {
                Node node = list.next();
                Client client = ClientFactory.fromNode(node);
                if (client.getLocations() == null) {
                    client.setLocations(new ArrayList<>());
                }

                Iterable<Relationship> residences = node.getRelationships(RESIDES, Direction.OUTGOING);
                for (Relationship item : residences) {
                    client.getLocations().add(LocationFactory.fromNode(item.getEndNode()));
                }
                clients.add(client);
            }
            tx.success();
        }
        return clients;
    }

    @Override
    public Client getClient(String id) {
        LOGGER.debug("getClient id:{}", id);
        Client client = null;

        // Maybe is not the best, also, taking into account possible SQL Injection equivalencies
        try (Transaction tx = graphDb.beginTx()) {
            ResourceIterator<Node> list = graphDb.findNodes(CLIENT, Client.PROP_ID, id);
            while (list.hasNext()) {
                Node clientNode = list.next();
                client = ClientFactory.fromNode(clientNode);
                if (client.getLocations() == null) {
                    client.setLocations(new ArrayList<>());
                }

                Iterable<Relationship> residences = clientNode.getRelationships(RESIDES, Direction.OUTGOING);
                for (Relationship item : residences) {
                    client.getLocations().add(LocationFactory.fromNode(item.getEndNode()));
                }
            }
            tx.success();
            return client;
        }
    }

    @Override
    public Client postClient(Client template) {
        LOGGER.debug("post Client:{}", template.getName());

        validate(template);

        Client created = null;

        try (Transaction tx = graphDb.beginTx()) {
            Node node = graphDb.createNode(CLIENT);
            String id = (template.getId() == null) ? UUID.randomUUID().toString() : template.getId();
            node.setProperty(Client.PROP_NAME, template.getName());
            node.setProperty(Client.PROP_ID, id);
            node.setProperty(Client.PROP_HREF, ClientFactory.toHref(id));
            node.setProperty(Client.PROP_STATUS, template.getStatus().name());

            setProperty(node, Client.PROP_DEBTOR_NUMBER, template.getDebtorNumber());
            setProperty(node, Client.PROP_CREDITOR_NUMBER, template.getCreditorNumber());
            setProperty(node, Client.PROP_EXTERNAL_ID, template.getExternalId());

            setProperty(node, Client.PROP_BIC_CODE, template.getBicCode());
            setProperty(node, Client.PROP_CHAMBER_OF_COMMERCE, template.getChamberOfCommerce());
            setProperty(node, Client.PROP_CHAMBER_OF_COMMERCE_CITY, template.getChamberOfCommerceCity());
            setProperty(node, Client.PROP_CHARGE_VAT, template.getChargeVat());
            setProperty(node, Client.PROP_CURRENCY, template.getCurrency());
            setProperty(node, Client.PROP_IBAN_CODE, template.getIbanCode());
            setProperty(node, Client.PROP_VAT_NUMBER, template.getVatNumber());

            List<Location> newLocations = new ArrayList<>();

            if (template.getLocations() != null) {
                template.getLocations().stream().forEach((location) -> {

                    Node locationNode = locationResource.getOrCreateLocationNode(location);
                    node.createRelationshipTo(locationNode, RESIDES);
                    newLocations.add(LocationFactory.fromNode(locationNode));

                });

            }

            //
            // create the client object from the nodes and relations
            //
            created = ClientFactory.fromNode(node);
            LOGGER.debug("adding {} locations to the new client", newLocations.size());
            created.setLocations(newLocations);
            tx.success();
        }
        return created;
    }

    @Override
    public Client updateClient(String id, Client template) {
        LOGGER.debug("post Client:{}", template);
        if (id == null) {
            throw new WebApplicationException("Cannot update client without ID", Response.Status.BAD_REQUEST);
        }

        Client created = null;

        try (Transaction tx = graphDb.beginTx()) {
            Node node = graphDb.findNode(CLIENT, Client.PROP_ID, id);

            Client existingClient = getClient(id);

            if (node == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            setProperty(node, Client.PROP_EXTERNAL_ID, template.getExternalId());
            if (template.getStatus() != null) {
                node.setProperty(Client.PROP_STATUS, template.getStatus().name());
            }
            node.setProperty(Client.PROP_NAME, template.getName());

            setProperty(node, Client.PROP_DEBTOR_NUMBER, template.getDebtorNumber());
            setProperty(node, Client.PROP_CREDITOR_NUMBER, template.getCreditorNumber());

            setProperty(node, Client.PROP_BIC_CODE, template.getBicCode());
            setProperty(node, Client.PROP_CHAMBER_OF_COMMERCE, template.getChamberOfCommerce());
            setProperty(node, Client.PROP_CHAMBER_OF_COMMERCE_CITY, template.getChamberOfCommerceCity());
            setProperty(node, Client.PROP_CHARGE_VAT, template.getChargeVat() == null ? false : template.getChargeVat());
            setProperty(node, Client.PROP_CURRENCY, template.getCurrency());
            setProperty(node, Client.PROP_IBAN_CODE, template.getIbanCode());
            setProperty(node, Client.PROP_VAT_NUMBER, template.getVatNumber());

            List<Location> newLocations = new ArrayList<>();

            if (template.getLocations() != null) {
                template.getLocations().stream().forEach((location) -> {

                    //
                    //    Check if I should skip Location because already LINKED
                    //
                    // Check if the location linked with the existing user has same id than template
                    // In that case, nothing to do with location, it is not a new location.
                    // In case of updated in any of the attributes of the locations,
                    // LocationResource should be used
                    boolean found = false;
                    Iterator<Location> it = existingClient.getLocations().iterator();

                    while (!found && it.hasNext()) {
                        Location existingLocationOfClient = it.next();
                        if (existingLocationOfClient.getId().equals(location.getId())) {
                            found = true;
                        }
                    }

                    if (!found) {

                        //
                        //    Location exists BUT it is not LINK, so, time to LINK
                        //
                        Node locationNode = graphDb.findNode(LOCATION, Location.PROP_ID, location.getId());

                        if (locationNode == null) {
                            locationNode = graphDb.createNode(LOCATION);
                        }
                        //
                        // Addiotionally, update properties (TEMPORALLY)
                        //
                        // TODO @perezdf I would suggest to remove this update of 
                        // locations attributes and move to Location Resource
                        String locationId = (location.getId() == null) ? UUID.randomUUID().toString() : location.getId();
                        locationNode.setProperty(Location.PROP_ID, locationId);
                        locationNode.setProperty(Location.PROP_NAME, location.getName());

                        // TODO: determine which are required, so we can validate before posting to the rest api;
                        setProperty(locationNode, Location.PROP_ADDRESS, location.getAddress());
                        setProperty(locationNode, Location.PROP_CITY, location.getCity());
                        setProperty(locationNode, Location.PROP_COUNTRY, location.getCountry());
                        setProperty(locationNode, Location.PROP_POSTALCODE, location.getPostalcode());
                        setProperty(locationNode, Location.PROP_LATITUDE, location.getLatitude());
                        setProperty(locationNode, Location.PROP_LONGITUDE, location.getLongitude());
                        setProperty(locationNode, Location.PROP_PHONE, location.getPhone());
                        setProperty(locationNode, Location.PROP_EMAIL, location.getEmail());
                        setProperty(locationNode, Location.PROP_EXTERNAL_ID, location.getExternalId());
                        setProperty(locationNode, Location.PROP_STATUS, location.getStatus().name());

                        // @perezdf NO CLUE
                        // check if id already exists in list to prevent duplicates
                        //
                        for (Location loc : newLocations) {
                            if (!loc.getId().equals(locationId)) {
                                newLocations.add(LocationFactory.fromNode(locationNode));
                            }
                        }

                        node.createRelationshipTo(locationNode, RESIDES);
                    }
                });

            }

            //
            // Check if in the template there are less Locations than in the Existing Client
            // Therefore, those Location must be removed
            //
            Client temporalClient = getClient(id);

            List<Location> locationsFromClient = new ArrayList(temporalClient.getLocations().size());

            List<Location> locationsToRemove = new ArrayList<>();
            temporalClient.getLocations().stream().forEach((item) -> {
                locationsFromClient.add(item);
                locationsToRemove.add(item);
            });

            List<Location> locationsFromTemplate = null;
            if (template.getLocations() != null && !template.getLocations().isEmpty()) {
                locationsFromTemplate = new ArrayList(template.getLocations().size());

                for (Location item : template.getLocations()) {
                    locationsFromTemplate.add(item);

                }

            }

            if (locationsFromClient != null && !locationsFromClient.isEmpty()) {

                // Remove the common nodes, which are the ones to remain
                if (locationsFromTemplate != null) {
                    for (Location lClient : locationsFromClient) {
                        for (Location lTemplate : locationsFromTemplate) {
                            if (lTemplate.getId().equals(lClient.getId())) {
                                locationsToRemove.remove(lClient);
                            }
                        }
                    }

                }

                Node savedNode = graphDb.findNode(CLIENT, Client.PROP_ID, id);

                // Retrieve all the nodes
                Iterable<Relationship> relationships = savedNode.getRelationships(Direction.BOTH, RESIDES);

                // Search if fom retrieve nodes, any of them must be removed
                Iterator<Relationship> it = relationships.iterator();
                while (it.hasNext()) {
                    Relationship relation = it.next();
                    Node locationNode = relation.getOtherNode(savedNode);
                    Location location = LocationFactory.fromNode(locationNode);
                    for (Location item : locationsToRemove) {
                        if (item.getId().equals(location.getId())) {
                            relation.delete();
                        }
                    }

                }
            }
            created = getClient(id);

            tx.success();
        }

        return created;
    }

    @Override
    public void deleteClient(String id
    ) {
        throw new UnsupportedOperationException();
    }
}
