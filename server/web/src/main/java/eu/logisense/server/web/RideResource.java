/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.server.web;

/*
 * Copyright (C) 2015 LimeTri. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the FLOSS License Exception
 * <http://www.limetri.eu/foss-exception.html>.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this software. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *    Timon Veenstra <timon@limetri.eu> - initial API and implementation and/or initial documentation
 */
import com.fasterxml.jackson.databind.type.TypeFactory;
import eu.logisense.api.Client;
import eu.logisense.api.Resource;
import static eu.logisense.server.web.Labels.*;
import static eu.logisense.server.web.Relations.*;
import eu.logisense.api.Ride;
import eu.logisense.api.RideService;
import eu.logisense.api.Container;
import eu.logisense.api.Status;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.Creditor;
import eu.logisense.api.Order;
import eu.logisense.api.Shipment;
import static eu.logisense.server.web.BaseNeo4jService.graphDb;
import static eu.logisense.server.web.Relations.PART_OF;
import eu.logisense.server.web.model.ContainerFactory;
import eu.logisense.server.web.model.CreditorFactory;
import eu.logisense.server.web.model.ObjectFactory;
import eu.logisense.server.web.model.RideFactory;
import eu.logisense.server.web.model.ShipmentFactory;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Result;

@Path("/rides")
public class RideResource extends BaseNeo4jService implements RideService {

    private static final Logger log = LoggerFactory.getLogger(RideResource.class);
    private final ContainerResource containerResource;

    public RideResource(GraphDatabaseService graphDb) throws IOException, URISyntaxException {
        super(graphDb);

        containerResource = new ContainerResource(graphDb);
    }

    public RideResource() throws IOException, URISyntaxException {
        containerResource = new ContainerResource(graphDb);
    }

    @Override
    public List<Ride> getRides() {
        log.debug("get rides");

        List<Ride> rides = new ArrayList<>();

        try (Transaction tx = graphDb.beginTx();
                ResourceIterator<Node> list = graphDb.findNodes(RIDE)) {
            while (list.hasNext()) {
                Node node = list.next();
                rides.add(buildRide(node));
            }
            tx.success();
        }
        log.debug("returning {} rides", rides.size());
        logDebug("rides:{}", rides, List.class, TypeFactory.rawClass(Ride.class));
        return rides;

    }

    private Ride buildRide(Node rideNode) {
        Ride ride = RideFactory.fromNode(rideNode);

        Iterable<Relationship> containerRelationship = rideNode.getRelationships(PART_OF, Direction.INCOMING);
        for (Relationship item : containerRelationship) {
            if (ride.getContainers() == null) {
                ride.setContainers(new ArrayList<>());
            }
            Container container = ContainerFactory.fromNode(item.getStartNode());
            Iterable<Relationship> shipmentRelationships = item.getStartNode().getRelationships(SHIPPED_IN, Direction.INCOMING);

            for (Relationship shipmentRelationship : shipmentRelationships) {
                if (container.getShipments() == null) {
                    container.setShipments(new ArrayList<>());
                }
                Resource shipment = ShipmentFactory.resourceFromNode(shipmentRelationship.getStartNode());
                container.getShipments().add(shipment);
            }

            ride.getContainers().add(container);
        }
        Iterable<Relationship> creditorRelations = rideNode.getRelationships(CREDITOR, Direction.INCOMING);
        for (Relationship creditor : creditorRelations) {
            ride.setCreditor(CreditorFactory.fromNode(creditor.getStartNode(), creditor));
        }
        return ride;
    }

    @Override
    public Ride getRide(String id) {
        log.debug("getRide id:{}", id);
        Ride ride = null;

        try (Transaction tx = graphDb.beginTx()) {
            Node node = graphDb.findNode(RIDE, "id", id);

            ride = buildRide(node);
            tx.success();
        }
        return ride;

    }

    @Override
    public List<Ride> getRides(LocalDate etd) {

        throw new UnsupportedOperationException();
    }

    @Override
    public Ride postRide(Ride template) {
        log.debug("post ride:{}", template);
        return createRide(template);
    }

    @Override
    public Ride updateRide(Ride template) {
        return createRide(template);
    }

    @Override
    public void deleteRide(String id) {
        throw new UnsupportedOperationException();
    }

    private Node getContainerNodeById(String id) {
        Objects.requireNonNull(id);

        return graphDb.findNode(CONTAINER, Container.PROP_ID, id);

    }

    private Ride createRide(Ride template) {
        Ride created = null;
        if (template != null) {

            try (Transaction tx = graphDb.beginTx()) {
                String id = template.getId() == null ? UUID.randomUUID().toString() : template.getId();
                //
                // try to find existing node and create one if not found
                //
                Node rideNode = graphDb.findNode(RIDE, Order.PROP_ID, id);
                if (rideNode == null) {
                    log.debug("Creating new ride with id {}", id);
                    rideNode = graphDb.createNode(RIDE);
                    rideNode.setProperty(Ride.PROP_ID, id);
                    rideNode.setProperty(Ride.PROP_CREATED, LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                    rideNode.setProperty(Ride.PROP_HREF, RideFactory.toHref(id));
                } else {
                    log.debug("ride already exists, upating");
                }
                //
                // set ride properties
                //
                setProperty(rideNode, Ride.PROP_ETD, ObjectFactory.toString(template.getEtd()));
                setProperty(rideNode, Ride.PROP_EXTERNAL_ID, template.getExternalId());
                setProperty(rideNode, Ride.PROP_NAME, template.getName());
                setProperty(rideNode, Ride.PROP_MARGIN, template.getMargin());
                Status status = template.getStatus() == null ? TransportStatus.NEW : template.getStatus();
                setProperty(rideNode, Ride.PROP_STATUS, status.name());
                setProperty(rideNode, Ride.PROP_LICENSE_PLATE, template.getLicensePlate());
                //
                // Set CREDITOR
                //
                Creditor creditor = template.getCreditor();
                if (creditor != null) {
                    Node creditorNode = graphDb.findNode(CLIENT, Client.PROP_ID, creditor.getId());
                    if (creditorNode == null) {
                        log.warn("no client found with id {}", creditor.getId());
                    } else {
                        log.debug("creating relation to creditor");

                        if (rideNode.hasRelationship(CREDITOR)) {
                            rideNode.getRelationships(CREDITOR).forEach((relationship) -> {
                                if (creditorNode.getProperty(Client.PROP_ID).equals(relationship.getStartNode().getProperty(Client.PROP_ID))) {
                                    setProperty(relationship, Creditor.PROP_CURRENCY, creditor.getCurrency());
                                    setProperty(relationship, Creditor.PROP_PRICE, creditor.getPrice());
                                } else {
                                    relationship.delete();
                                }
                            });

                        }
                        if (!rideNode.hasRelationship(CREDITOR)) {
                            Relationship creditorRelation = creditorNode.createRelationshipTo(rideNode, CREDITOR);
                            setProperty(creditorRelation, Creditor.PROP_CURRENCY, creditor.getCurrency());
                            setProperty(creditorRelation, Creditor.PROP_PRICE, creditor.getPrice());
                        }
                    }
                }

                //
                // loop through containers in ride card
                //
                //FIXME what if container gets removed from a ride? not yet possible in client, but might be in future
                if (template.getContainers() == null || template.getContainers().isEmpty()) {
                    log.warn("ridecard submitted without containers");
                } else {
                    log.debug("adding {} containers", template.getContainers().size());

                    for (Container c : template.getContainers()) {
                        //
                        // find container node, create one if not exists
                        //
                        String containerId = (c.getId() == null) ? UUID.randomUUID().toString() : c.getId();
                        Node containerNode = graphDb.findNode(CONTAINER, Container.PROP_ID, containerId);

                        if (containerNode == null) {
                            log.debug("Creating new container with id {}", containerId);
                            containerNode = graphDb.createNode(CONTAINER);
                            containerNode.setProperty(Container.PROP_ID, containerId);
                            containerNode.setProperty(Container.PROP_HREF, "/containers/" + containerId);
                        } else {
                            log.debug("container already exists, using it");
                        }
                        //
                        // create relation between container and ride if not already exists
                        //
                        boolean relationWithRideExists = false;
                        if (containerNode.hasRelationship(PART_OF, Direction.OUTGOING)) {
                            for (Relationship r : containerNode.getRelationships(Direction.OUTGOING, PART_OF)) {
                                String relRideId = ObjectFactory.getStringValue(r.getEndNode(), Ride.PROP_ID);
                                if (id.equals(relRideId)) {
                                    log.debug("found relation between container and ride");
                                    relationWithRideExists = true;
                                } else {
                                    log.debug("found another relation between container and ride, deleting it");
                                    r.delete();
                                }
                            }
                        }
                        if (!relationWithRideExists) {
                            log.debug("creating relation between container and ride");
                            containerNode.createRelationshipTo(rideNode, PART_OF);
                        }

                        //
                        // remove all relations between container and shipments, recreating them in next block
                        //
                        for (Relationship shipmentContainerRelation: containerNode.getRelationships(Direction.INCOMING, SHIPPED_IN)){
                            shipmentContainerRelation.delete();
                        }
                        //
                        // iterate through shipments in a container
                        //
                        log.debug("adding {} shipments to container", c.getShipments().size());
                        for (Resource s : c.getShipments()) {
                            String shipmentId = s.getId();
                            if (shipmentId == null) {
                                log.warn("something wrong, container contains shipment with empty id");
                                throw new WebApplicationException(Response.Status.BAD_REQUEST);
                            }
                            Node shipmentNode = graphDb.findNode(SHIPMENT, Shipment.PROP_ID, shipmentId);
                            if (shipmentNode == null) {
                                log.warn("something wrong, no shipment node found with shipment id {}", shipmentId);
                                throw new WebApplicationException(Response.Status.BAD_REQUEST);
                            }
                            //
                            // Update shipment status
                            //
                            setProperty(shipmentNode, Shipment.PROP_STATUS, TransportStatus.PLANNED.name());

                            boolean relationWithShipmentExists = false;
                            if (shipmentNode.hasRelationship(SHIPPED_IN, Direction.OUTGOING)) {
                                for (Relationship r : shipmentNode.getRelationships(Direction.OUTGOING, SHIPPED_IN)) {
                                    String relShipmentId = ObjectFactory.getStringValue(r.getEndNode(), Shipment.PROP_ID);
                                    if (shipmentId.equals(relShipmentId)) {
                                        log.debug("shipment relation found");
                                        relationWithShipmentExists = true;
                                        //update index in case changed
                                        setProperty(shipmentNode, Shipment.PROP_INDEX, s.getIndex());
                                    } else {
                                        log.warn("shipment was still linked to another container, removing link");
                                        r.delete();
                                    }
                                }
                            }
                            if (!relationWithShipmentExists) {
                                log.debug("shipment relation not found, creating it");
                                Relationship r = shipmentNode.createRelationshipTo(containerNode, SHIPPED_IN);
                                setProperty(shipmentNode, Shipment.PROP_INDEX, s.getIndex());
                            }

                        }

                    }
                }
                created = buildRide(rideNode);

                tx.success();
            }
        }
        return created;
    }
}
