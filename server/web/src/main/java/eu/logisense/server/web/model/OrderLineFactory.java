/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import eu.logisense.api.OrderLine;
import static eu.logisense.server.web.model.ObjectFactory.*;
import org.neo4j.graphdb.Node;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class OrderLineFactory {

    public static OrderLine fromNode(Node node, String orderId) {
        return OrderLine.builder()
                .id(getStringValue(node, OrderLine.PROP_ID))
                .name(getStringValue(node, OrderLine.PROP_NAME))
                .href(toHref(getStringValue(node, OrderLine.PROP_ID), orderId))
                .description(getStringValue(node, OrderLine.PROP_DESCRIPTION))
                .remarks(getStringValue(node, OrderLine.PROP_REMARKS))
                .amount(getIntegerValue(node, OrderLine.PROP_AMOUNT))
                .stackable(getBooleanValue(node, OrderLine.PROP_STACKABLE))
                .paymentOnDeliveryAmount(getDoubleValue(node, OrderLine.PROP_PAYMENT_ON_DELIVERY_AMOUNT))
                .paymentOnDeliveryCurrency(getStringValue(node, OrderLine.PROP_PAYMENT_ON_DELIVERY_CURRENCY))
                .type(getOrderLineTypeValue(node, OrderLine.PROP_TYPE))
                .swapPallets(getBooleanValue(node, OrderLine.PROP_SWAP_PALLETS))
                .length(getDoubleValue(node, OrderLine.PROP_LENGTH))
                .width(getDoubleValue(node, OrderLine.PROP_WIDTH))
                .height(getDoubleValue(node, OrderLine.PROP_HEIGHT))
                .weight(getDoubleValue(node, OrderLine.PROP_WEIGHT))
                .build();
    }

    public static String toHref(String id, String orderId) {
        return "/orders/" + orderId + "/lines/" + id;
    }
}
