/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.server.web.model;

import static eu.logisense.server.web.model.ObjectFactory.getStringValue;
import org.neo4j.graphdb.Node;
import eu.logisense.api.Ride;
import eu.logisense.api.TransportStatus;
import static eu.logisense.server.web.model.ObjectFactory.getTransportStatusValue;

public class RideFactory {
    
    public static Ride fromNode(Node node) {
        // TODO what happens with Shipment List??
        return Ride.builder()
                .id(getStringValue(node, Ride.PROP_ID))
                .name(getStringValue(node, Ride.PROP_NAME))
                .href(toHref(getStringValue(node, Ride.PROP_ID)))
                .status(getTransportStatusValue(node, Ride.PROP_STATUS))
                .externalId(getStringValue(node, Ride.PROP_EXTERNAL_ID))
                .licensePlate(getStringValue(node, Ride.PROP_LICENSE_PLATE))
                .build();
    }
    
    public static String toHref(String id) {
        return "/rides/" + id;
    }
}
