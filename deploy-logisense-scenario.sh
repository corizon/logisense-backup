#cd adapters/jkAdapter
#xterm -hold -e mvn wildfly-swarm:run&
#cd ../..

cd adapters/xml-adapter
xterm -hold -e mvn wildfly-swarm:run  -Plogisense-target-localhost&
cd ../..

cd server/web
xterm -hold -e mvn wildfly-swarm:run -Plogisense-target-localhost&
cd ../..

#cd frontend
#xterm -hold -e ionic serve&

#cd rcp/application
#xterm -hold -e mvn nbm:cluster-app nbm:run-platform&

