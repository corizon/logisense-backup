angular.module('app.routes', [])

        .config(function ($stateProvider, $urlRouterProvider) {

            $stateProvider

                    .state('logisense.profile', {
                        url: '/profile',
                        views: {
                            'profile': {
                                templateUrl: 'templates/profile.html',
                                controller: 'profileCtrl'
                            }
                        }
                    })

                    .state('logisense.planning', {
                        url: '/planning',
                        views: {
                            'settings': {
                                templateUrl: 'templates/planning.html',
                                controller: 'planningCtrl'
                            }
                        }
                    })

                    .state('logisense.settings', {
                        url: '/settings',
                        views: {
                            'settings': {
                                templateUrl: 'templates/settings.html',
                                controller: 'settingsCtrl'
                            }
                        }
                    })

                    .state('logisense', {
                        url: '/logisense',
                        abstract: true,
                        templateUrl: 'templates/tabsController.html'
                    })
                    // *********************** 
                    // *** FLEET ROUTES ***
                    // ***********************
                    .state('logisense.fleet', {
                        url: '/fleet',
                        views: {
                            'fleet': {
                                templateUrl: 'templates/fleet.html',
                                controller: 'fleetCtrl'
                            }
                        }
                    })
                    
                    .state('logisense.fleetform', {
                        url: '/fleetForm',
                        views: {
                            'settings': {
                                templateUrl: 'templates/fleet_form.html',
                                controller: 'fleet_formCtrl'
                            }
                        }
                    })
                    // *********************** 
                    // *** eXPORT ROUTES ***
                    // ***********************
                    .state('logisense.export', {
                        url: '/export',
                        views: {
                            'export': {
                                templateUrl: 'templates/export.html',
                                controller: 'exportCtrl'
                            }
                        }
                    })
                    
                    // *********************** 
                    // *** SHIPMENT ROUTES ***
                    // ***********************
                    .state('logisense.shipments', {
                        url: '/shipments',
                        views: {
                            'shipments': {
                                templateUrl: 'templates/shipments.html',
                                controller: 'shipmentsCtrl'
                            }
                        }
                    })
                    .state('shipment_details', {
                        url: '/logisense/:shipment/shipment_details',
                        templateUrl: 'templates/shipment_details.html',
                        controller: 'shipment_detailsCtrl',
                        resolve: {
                            shipment: function ($stateParams, Shipments) {
                                return Shipments.find($stateParams.shipment);
                            }
                        }
                    })

                    .state('shipment_dispatch', {
                        url: '/logisense/:shipment/shipment_dispatch',
                        templateUrl: 'templates/shipment_dispatch.html',
                        controller: 'shipment_dispatchCtrl',
                        resolve: {
                            shipment: function ($stateParams, Shipments) {
                                return Shipments.find($stateParams.shipment);
                            }
                        }
                    })

                    .state('logisense.shipmentform', {
                        url: '/shipmentForm',
                        views: {
                            'settings': {
                                templateUrl: 'templates/shipment_form.html',
                                controller: 'shipment_formCtrl'
                            }
                        }
                    })



                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/login.html'
                    })
                    ;

            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/login');

        });