// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
app = angular.module('app', ['ionic', 'js-data', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'app.controllers', 'app.shimentCcontrollers', 'app.fleetControllers', 'app.planningControllers','app.exportControllers']);

app.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
});


app.config(function (DSProvider, DSHttpAdapterProvider) {
    angular.extend(DSProvider.defaults, {});

    var baseApiUrlVar = AppSettings.baseApiUrl;

    angular.extend(DSHttpAdapterProvider.defaults, {
        basePath: baseApiUrlVar
    });
});

app.run(function (DS) {
    // DS is the result of `new JSData.DS()`

    // We don't register the "User" resource
    // as a service, so it can only be used
    // via DS.<method>('user', ...)
    // The advantage here is that this code
    // is guaranteed to be executed, and you
    // only ever have to inject "DS"
    DS.defineResource('user');
});

app.factory('Shipments', function (DS) {
    return DS.defineResource({
        name: 'shipments', idAttribute: 'id'
    });
});


app.factory('Orders', function (DS) {
    return DS.defineResource({
        name: 'orders', idAttribute: 'id'
    });
});

app.factory('Rides', function (DS) {
    return DS.defineResource({
        name: 'rides', idAttribute: 'id'
    });
});

app.factory('Exports', function (DS) {
    return DS.defineResource({
        name: 'exports', idAttribute: 'id'
    });
});

app.factory('Vehicles', function (DS) {
    return DS.defineResource({
        name: 'vehicles', idAttribute: 'id'
    });
});