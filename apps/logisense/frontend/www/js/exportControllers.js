angular.module('app.exportControllers', [])
        .controller('exportCtrl', function ($scope, $ionicPopup, $window, Shipments, Orders, Rides, Exports, $http, $state) {

            $scope.exportElements = [];
            $scope.filters_shipments = false;



            $scope.export = function () {
                $scope.newExport = Exports.createInstance();
                $scope.newExport.id = null;
                $scope.newExport.name = 'export from agent';

                var elements = [];
                for (var index = 0; index < $scope.exportElements.length; index++)
                {
                    var item = new Object();
                    item.href = $scope.exportElements[index].href;
                    elements.push(item);
                }
                $scope.newExport.elements = elements;
                console.log($scope.newExport);
                Exports.create($scope.newExport).then(function () {
                    console.log('created');
                });

            }

            $scope.refresh = function () {
                $scope.exportElements = [];
                Shipments.findAll().then(function (shipments) {
                    if ($scope.filters_shipments)
                    {
                        $scope.shipments = shipments;
                        if (shipments.length !== 0)
                        {
                            for (var index = 0; index < shipments.length; index++)
                            {
                                var item = shipments[index];
                                var normalizate = {id: item.id, type: 'shipment', name: item.name, href: item.href};
                                $scope.exportElements.push(normalizate);

                            }
                        }
                    }

                    Orders.findAll().then(function (orders) {
                        if ($scope.filters_orders)
                        {
                            $scope.orders = orders;
                            if (orders.length !== 0)
                            {
                                for (var index = 0; index < orders.length; index++)
                                {
                                    var item = orders[index];
                                    var normalizate = {id: item.id, type: 'order', name: item.name, href: item.href};
                                    $scope.exportElements.push(normalizate);

                                }
                            }
                        }
                        Rides.findAll().then(function (rides) {
                            if ($scope.filters_rides)
                            {
                                $scope.rides = rides;
                                if (rides.length !== 0)
                                {
                                    for (var index = 0; index < rides.length; index++)
                                    {
                                        var item = rides[index];
                                        var normalizate = {id: item.id, type: 'ride', name: item.name, href: item.href};
                                        $scope.exportElements.push(normalizate);

                                    }
                                }
                            }
                        });
                    });

                    var url = 'http://localhost:8011/api/exports/' + $scope.newExport.id + '/dispatch';

                    $http({
                        method: 'GET',
                        url: url
                    }).success(function () {

                        console.log('success !!');
                    });
                });


            };

        });

        