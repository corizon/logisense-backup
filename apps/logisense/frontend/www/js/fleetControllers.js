angular.module('app.fleetControllers',[])
        .controller('fleetCtrl', function ($scope, $ionicPopup, $window, Vehicles, $http, $state) {
            console.log('fleetCtrl init');

    // *********************
    // *** CREATION FORM ***
    // *********************
    $scope.showForm = function () {
console.log('go to logisense.fleetform')
        $state.go('logisense.fleetform');
    };

    // ***********************
    // *** DELETE FUNCTION ***
    // ***********************
    $scope.onItemDelete = function (item) {

        Vehicles.destroy(item);
    };
    // *********************
    // *** MOVE FUNCTION ***
    // *********************
    $scope.moveItem = function (item, fromIndex, toIndex) {

        $scope.shipments.splice(fromIndex, 1);
        $scope.shipments.splice(toIndex, 0, item);
    };

    // *****************************
    // *** INITIALIZATE FUNCTION ***
    // *****************************
    Vehicles.findAll().then(function (vehicles) {
        console.log('retrieve vehicles');
        $scope.vehicles = vehicles;
    });
})
        .controller('vehicle_detailsCtrl', function ($scope, vehicle) {
            $scope.vehicle = vehicle;
        })

        .controller('fleet_formCtrl', function ($scope, Vehicles, $state, $window) {
 console.log('fleet_formCtrl started');
            $scope.newVehicle = Vehicles.createInstance();
            $scope.newVehicle.id = null;
            console.log($scope.newVehicle);
            // *** CREATE SHIPMENT FUNCTION ***
            $scope.createVehicle= function (form) {

                if (form.$valid) {
                    console.log($scope.newShipment);
                    Vehicles.create($scope.newShipment).then(function () {
                        Vehicles.findAll().then(function (vehicles) {
                            $scope.vehicles = vehicles;
                        }).finally(function () {
                            // TODO it is just a dirty solution to the reload issue
                            $window.location.reload(true);
                            $state.go('logisense.vehicles', null, {reload: true, notify: true});
                        });
                    });

                }
            };
        });