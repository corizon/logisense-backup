angular.module('app.planningControllers', ['angular-country-picker'])
        .controller('planningCtrl', function ($scope, $ionicPopup, $window, Shipments, $http, $state) {

            console.log('planningControllers init');
            $scope.eventArray = new Array();
            // *** INITIALIZATE FUNCTION ***
            Shipments.findAll().then(function (shipments) {

                for (var i = 0; i < shipments.length; i++) {
                    console.log(shipments[i].name);
                    console.log(shipments[i].from.timeFrameFrom[0]);
                    console.log(shipments[i].from.timeFrameFrom[1]);
                    console.log(shipments[i].from.timeFrameFrom[2]);

                    $scope.eventArray.push({
                        title: shipments[i].name,
                        start: shipments[i].from.timeFrameFrom[0] + '-' + (shipments[i].from.timeFrameFrom[1]+1) + '-' + (shipments[i].from.timeFrameFrom[2]+1)
                    });
                }
                console.log(JSON.stringify($scope.eventArray));

                $(document).ready(function () {

                    var date = new Date();
                    var defaultDateString = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                    console.log(defaultDateString);
                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,basicWeek,basicDay'
                        },
                        defaultDate: defaultDateString,
                        editable: true,
                        eventLimit: true, // allow "more" link when too many events
                        events: $scope.eventArray
   
                    });

                });
            });
        });