angular.module('app.shimentCcontrollers', ['angular-country-picker', 'ngMessages', 'ngMaterial'])
        .controller('shipmentsCtrl', function ($scope, $ionicPopup, $window, Shipments, $http, $state) {



            $scope.showShipmentForm = function () {

                $state.go('logisense.shipmentform');
            };
            // **********************
            // *** ORDER FUNCTION ***
            // **********************
            $scope.order = function ($item) {
                var url = AppSettings.baseApiUrl + 'shipments/' + $item.id + '/order';
                $http({
                    method: "GET",
                    url: url
                }).success(function () {
                    // TODO BUG LOG-123 it is just a dirty solution to the reload issue
                    $window.location.reload(true);
                    $ionicPopup.alert({
                        title: 'Info',
                        template: 'Shipment ordered'
                    });
                }).error(function (data) {

                    $ionicPopup.alert({
                        title: 'Error',
                        template: data
                    });
                });
            };
            // ************************
            // *** CANCEL FUNCTION ***
            // ************************
            $scope.cancel = function ($item) {
                var url = AppSettings.baseApiUrl + 'shipments/' + $item.id + '/cancel';
                $http({
                    method: "GET",
                    url: url
                }).success(function () {

                    // TODO it is just a dirty solution to the reload issue
                    $window.location.reload(true);

                    $ionicPopup.alert({
                        title: 'Info',
                        template: 'Shipment canceled'
                    });
                }).error(function (data) {

                    $ionicPopup.alert({
                        title: 'Error',
                        template: data
                    });
                });
            };
            // ************************
            // *** DELIVER FUNCTION ***
            // ************************
            $scope.deliver = function ($item) {
                var url = AppSettings.baseApiUrl + 'shipments/' + $item.id + '/deliver';
                $http({
                    method: "GET",
                    url: url
                }).success(function () {

                    // TODO it is just a dirty solution to the reload issue
                    $window.location.reload(true);

                    $ionicPopup.alert({
                        title: 'Info',
                        template: 'Shipment delivered'
                    });
                }).error(function (data) {

                    $ionicPopup.alert({
                        title: 'Error',
                        template: data
                    });
                });
            };
            // ***********************
            // *** DELETE FUNCTION ***
            // ***********************
            $scope.onItemDelete = function (item) {

                Shipments.destroy(item);
            };

            // ***********************
            // *** exportXML FUNCTION ***
            // ***********************
            $scope.exportXML = function (item) {

                
            };

            // *** MOVE FUNCTION ***
            $scope.moveItem = function (item, fromIndex, toIndex) {

                $scope.shipments.splice(fromIndex, 1);
                $scope.shipments.splice(toIndex, 0, item);
            };

            // *** INITIALIZATE FUNCTION ***
            Shipments.findAll().then(function (shipments) {
                $scope.shipments = shipments;
            });
        })
        // **********************************
        // *** SHIPMENT DETAIL CONTROLLER ***
        // **********************************
        .controller('shipment_detailsCtrl', function ($scope, shipment) {
            $scope.shipment = shipment;
            $scope.shipmentLines = shipment.shipmentLines;

        })
        // **********************************
        // *** SHIPMENT FORM CONTROLLER ***
        // **********************************
        .controller('shipment_formCtrl', function ($scope, Shipments, $state, $window) {


            $scope.hourPickupTimeframeWindowFrom = "12";
            $scope.minutePickupTimeframeWindowFrom = "00";
            $scope.hourPickupTimeframeWindowTo = "12";
            $scope.minutePickupTimeframeWindowTo = "00";
            $scope.hourDeliveryTimeframeWindowFrom = "12";
            $scope.minuteDeliveryTimeframeWindowFrom = "00";
            $scope.hourDeliveryTimeframeWindowTo = "12";
            $scope.minuteDeliveryTimeframeWindowTo = "00";
            $scope.deliveryDate = null;
            $scope.pickupDate = null;
            $scope.rangeValue = 0;
            $scope.updatePickupDate = function (date)
            {
                $scope.pickupDate = date;
            }
            $scope.updateDeliveryDate = function (date)
            {
                $scope.deliveryDate = date;
            }
            $scope.dragPickupTo = function (value) {
                var timeInMinutes = 24 * 60;
                var timeSelectedInMinutes = timeInMinutes * value / 100;

                $scope.hourPickupTimeframeWindowTo = Math.floor(timeSelectedInMinutes / 60);

                var remainingMinute = Math.floor(Math.round(timeSelectedInMinutes % 60));
                var remainingMinute5 = remainingMinute - (remainingMinute % 5);
                if (remainingMinute5 < 10)
                {
                    $scope.minutePickupTimeframeWindowTo = "0" + remainingMinute5;

                } else
                {
                    $scope.minutePickupTimeframeWindowTo = remainingMinute5;

                }
            };
            $scope.dragPickupFrom = function (value) {
                var timeInMinutes = 24 * 60;
                var timeSelectedInMinutes = timeInMinutes * value / 100;

                $scope.hourPickupTimeframeWindowFrom = Math.floor(timeSelectedInMinutes / 60);

                var remainingMinute = Math.floor(Math.round(timeSelectedInMinutes % 60));
                var remainingMinute5 = remainingMinute - (remainingMinute % 5);
                if (remainingMinute5 < 10)
                {
                    $scope.minutePickupTimeframeWindowFrom = "0" + remainingMinute5;

                } else
                {
                    $scope.minutePickupTimeframeWindowFrom = remainingMinute5;

                }

            };
            $scope.dragDeliveryTo = function (value) {
                var timeInMinutes = 24 * 60;
                var timeSelectedInMinutes = timeInMinutes * value / 100;

                $scope.hourDeliveryTimeframeWindowTo = Math.floor(timeSelectedInMinutes / 60);

                var remainingMinute = Math.floor(Math.round(timeSelectedInMinutes % 60));
                var remainingMinute5 = remainingMinute - (remainingMinute % 5);
                if (remainingMinute5 < 10)
                {
                    $scope.minuteDeliveryTimeframeWindowTo = "0" + remainingMinute5;

                } else
                {
                    $scope.minuteDeliveryTimeframeWindowTo = remainingMinute5;

                }

            };
            $scope.dragDeliveryFrom = function (value) {
                var timeInMinutes = 24 * 60;
                var timeSelectedInMinutes = timeInMinutes * value / 100;

                $scope.hourDeliveryTimeframeWindowFrom = Math.floor(timeSelectedInMinutes / 60);

                var remainingMinute = Math.floor(Math.round(timeSelectedInMinutes % 60));
                var remainingMinute5 = remainingMinute - (remainingMinute % 5);
                if (remainingMinute5 < 10)
                {
                    $scope.minuteDeliveryTimeframeWindowFrom = "0" + remainingMinute5;

                } else
                {
                    $scope.minuteDeliveryTimeframeWindowFrom = remainingMinute5;

                }

            };

            $scope.newShipment = Shipments.createInstance();
            $scope.newShipment.id = null;
            var from = new Object();
            from.latitude = 0;
            from.longitude = 0;

            var to = new Object();
            to.latitude = 0;
            to.longitude = 0;


            // *** CREATE SHIPMENT FUNCTION ***
            $scope.createShipment = function (form) {

                if (form.$valid) {
                    $scope.newShipment.from = new Object();
                    $scope.newShipment.to = new Object();


                    from.address = $scope.newShipment.from.address;
                    from.postalcode = $scope.newShipment.from.postalcode;
                    from.city = $scope.newShipment.from.city;
                    from.country = $scope.newShipment.from.country;

                    to.address = $scope.newShipment.to.address;
                    to.postalcode = $scope.newShipment.to.postalcode;
                    to.city = $scope.newShipment.to.city;
                    to.country = $scope.newShipment.to.country;



                    $scope.hourPickupTimeframeWindowFrom = "12";

                    var year = $scope.pickupDate.getFullYear();
                    var month = $scope.pickupDate.getMonth();
                    var date = $scope.pickupDate.getDate();
                    var hour = $scope.hourPickupTimeframeWindowFrom;
                    var minute = $scope.minutePickupTimeframeWindowFrom;
                    var pickupTimeframeFrom = [year, month, date, parseInt(hour), parseInt(minute), 0];

                    from.timeFrameFrom = pickupTimeframeFrom;
                    from.timeFrameTo = pickupTimeframeFrom;

                    to.timeFrameFrom = pickupTimeframeFrom;
                    to.timeFrameTo = pickupTimeframeFrom;


                    $scope.newShipment.from = from;
                    $scope.newShipment.to = to;

                    console.log(from);

                    Shipments.create($scope.newShipment).then(function () {
                        Shipments.findAll().then(function (shipments) {
                            $scope.shipments = shipments;
                        }).finally(function () {
                            // TODO it is just a dirty solution to the reload issue
                            $window.location.reload(true);
                            $state.go('logisense.shipments', null, {reload: true, notify: true});
                        });
                    });

                }
            }
            ;
        })

        .controller('shipment_dispatchCtrl', function ($scope, $state, $http, $ionicPopup, shipment) {
            $scope.shipment = shipment;
            $scope.shipmentLines = shipment.shipmentLines;
            var url = AppSettings.baseApiUrl + 'management/capabilities'

            $http({
                method: "GET",
                url: url
            }).success(function (data) {
                $scope.capabilities = data;
            }).error(function (data) {

                $ionicPopup.alert({
                    title: 'Error',
                    template: data
                });
            });

            $scope.onDispatch = function (capability, shipment) {

                var url = AppSettings.baseApiUrl + 'shipments/' + shipment.id + '/dispatchto/' + capability.id;
                $http({
                    method: "GET",
                    url: url
                }).success(function () {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Info',
                        template: 'Shipment dispatched'
                    });
                    alertPopup.then(function () {
                        $state.go('logisense.shipments');
                    });
                }).error(function (data) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'error:' + data
                    });
                });
            };
        });