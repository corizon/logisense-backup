
/* global cordova, StatusBar */

var app = angular.module('driver', ['ionic', 'js-data']);

app.config(function (DSProvider, DSHttpAdapterProvider) {
    angular.extend(DSProvider.defaults, {});
    angular.extend(DSHttpAdapterProvider.defaults, {
        // when change, also change Content-Security-Policy in index.html
        basePath: 'http://192.168.71.27:3000/'
    });
});

app.run(function (DS) {
    DS.defineResource('user');
});

app.factory('Shipments', function (DS) {
    return DS.defineResource({
        name: 'shipments',
        idAttribute: 'id'
    });
});

app.factory('Shipment_Details', function (DS) {
    return DS.defineResource({
        name: 'shipment_details',
        idAttribute: 'id',
        relations: {
            belongsTo: {
                shipments: {
                    localKey: "shipmentId",
                    parent: true
                }
            }
        }
    });
});

app.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            StatusBar.styleLightContent();
        }
    });
});

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('shipments', {
        url: '/shipments',
        templateUrl: 'templates/shipments.html',
        controller: 'ShipmentsCtrl'
    });

    $stateProvider.state('shipment_details', {
        url: '/shipments/:shipment/details',
        templateUrl: 'templates/shipment_details.html',
        controller: 'ShipmentDetailsCtrl',
        resolve: {
            shipment: function ($stateParams, Shipments) {
                return Shipments.find($stateParams.shipment);
            }
        }
    });

    $stateProvider.state('gps', {
        url: '/gps',
        templateUrl: 'templates/gps.html'
    });

    $urlRouterProvider.otherwise("/shipments");
});

app.controller('ShipmentsCtrl', function ($scope, Shipments) {
    console.log('ShipmentsCtrl');
    Shipments.findAll().then(function (shipments) {
        $scope.shipments = shipments;
    });
});

app.controller('ShipmentDetailsCtrl', function ($scope, shipment) {
    console.log('ShipmentDetailsCtrl');
    $scope.shipment = shipment;
});
