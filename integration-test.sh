#!/usr/bin/perl -w

use strict;
use Data::Dumper;
use Gtk2 '-init';
use constant TRUE  => 1;
use constant FALSE => 0;

my $window = Gtk2::Window->new;
$window->set_title ('Integration Tests');

$window->signal_connect (destroy => sub { Gtk2->main_quit; });

my $backendButton = Gtk2::Button->new ('Launch backend');
my $preplanningButton = Gtk2::Button->new ('Launch pre-planning');
my $xmlAdapterIntegrationTestButton = Gtk2::Button->new ('Launch xml-adapter integration tests');
my $serverWebIntegrationTestButton = Gtk2::Button->new ('Launch server-web integration tests');
my $keycloakButton = Gtk2::Button->new ('Launch keycloak');
#
$backendButton->signal_connect (clicked => \&backend_callback);
$preplanningButton->signal_connect (clicked => \&preplanning_callback);
$xmlAdapterIntegrationTestButton->signal_connect (clicked => \&xmlAdapterIntegrationTestButton_callback);
$serverWebIntegrationTestButton->signal_connect (clicked => \&launch_server_web_integration_test_callback);
$keycloakButton->signal_connect (clicked => \&keycloakButton_callback);

my $vbox = Gtk2::VBox->new(FALSE, 6);
$vbox->set_border_width(3);

$vbox->pack_start($backendButton, FALSE, FALSE, 0);
$vbox->pack_start($preplanningButton, FALSE, FALSE, 0);
$vbox->pack_start($xmlAdapterIntegrationTestButton, FALSE, FALSE, 0);
$vbox->pack_start($serverWebIntegrationTestButton, FALSE, FALSE, 0);
$vbox->pack_start($keycloakButton, FALSE, FALSE, 0);

my $frame = Gtk2::Frame->new('Integration Test Actions');
$frame->add($vbox);
$window->add ($frame);

$window->show_all;
Gtk2->main;

sub keycloakButton_callback 
{
        system("./start-keycloak.sh");
        1;
}


sub preplanning_callback 
{
        system("mvn nbm:cluster-app nbm:run-platform -f rcp/application/pom.xml -Plogisense-target-localhost");
        1;
}

sub xmlAdapterIntegrationTestButton_callback 
{
        # print out the parameters we were passed: (button, user_data)
        system("./run-xml-adapter-integration-tests.sh");
        # then quit
        #Gtk2->main_quit;
        1;
}

sub launch_server_web_integration_test_callback 
{
        # print out the parameters we were passed: (button, user_data)
        system("./run-server-web-integration-tests.sh");
        # then quit
        #Gtk2->main_quit;
        1;
}

sub backend_callback 
{
        # print out the parameters we were passed: (button, user_data)
        system("./deploy-logisense-scenario.sh");
        # then quit
        #Gtk2->main_quit;
        1;
}

