/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api.adapters.jkadapter;

import au.com.bytecode.opencsv.CSVWriter;
import eu.logisense.api.Shipment;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
public class JavaToCSVWriter {

    static final char SEPARATOR = ';';

    /**
     * Takes a list<Shipment> and writes it's values into a csv file, one line per Shipment
     *
     * @param list
     * @param fileUrl
     * @throws java.io.IOException
     */
    public static void writeListToFile(List<Shipment> list, String fileUrl) throws IOException {

        //header
        try ( // write csv seperated by ';', without quotations around each element
                CSVWriter writer = new CSVWriter(new FileWriter(fileUrl), SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER)) {
            //header
            writer.writeNext(new String[]{"id", "name", "href"});

            list.forEach((Shipment shipment) -> writer.writeNext(buildShipmentString(shipment)));

            writer.flush();
        }

    }

    /**
     * Takes a Shipment and writes it's values into a csv file
     *
     * @param shipment
     * @param fileUrl
     * @throws java.io.IOException
     */
    public static void writeSingleToFile(Shipment shipment, String fileUrl) throws IOException {

        //add header
        try ( // write csv seperated by ';', without quotations around each element
                CSVWriter writer = new CSVWriter(new FileWriter(fileUrl), SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER)) {
            //add header
            writer.writeNext(new String[]{"id", "name", "href"});
            writer.writeNext(buildShipmentString(shipment));

            writer.flush();
        }

    }

    static String[] buildShipmentString(Shipment shipment) {
        // TODO check with the final result of shipment
        String address = "";
        if (shipment.getLoadLocation() != null) {
            address = shipment.getLoadLocation().getAddress();
        }
        String[] shipmentBuilder = {
            address,
            shipment.getId(),
            shipment.getName(),
            shipment.getHref()};
        return shipmentBuilder;
    }
}
