/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api.adapters.jkadapter;

import eu.logisense.api.Shipment;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
@Path("/shipments")
public class ShipmentAdapter  {

    private static final String FILE_NAME = "temp";
    private static final String FORMAT = ".csv";



    @POST
    public Shipment postShipment(Shipment template) {
        try {
            JavaToCSVWriter.writeSingleToFile(template, FILE_NAME + FORMAT);
        } catch (IOException ex) {
            //todo, auto-generated error message
            Logger.getLogger(ShipmentAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return template;
    }

    /**
     * Expose the availability to transport a shipment. The idea of this method
     * is to be called by external systems
     *
     * @param shipment
     * @return
     */
    @GET
    @Path("/transportShipment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransportShipment() {

        System.out.println("***** getTransportShipment");

        //postShipment(shipment);
        return Response.ok().entity(true).build();
    }

    @POST
    @Path("/transportShipment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Response postTransportShipment(Shipment shipment) {
        System.out.println("***** postTransportShipment");

        postShipment(shipment);

        return Response.ok().entity(shipment).build();
    }
}
