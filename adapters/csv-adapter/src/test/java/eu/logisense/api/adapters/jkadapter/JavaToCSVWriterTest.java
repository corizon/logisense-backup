/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.api.adapters.jkadapter;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import eu.logisense.api.Location;
import eu.logisense.api.Shipment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Kenrik Veenstra <kenrik@limetri.eu>
 */
public class JavaToCSVWriterTest {

    public JavaToCSVWriterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private final String id = "id",
            id2 = "id2",
            name = "name",
            name2 = "name2",
            href = "href",
            href2 = "href2";

    private final Location location = Location.buildLocation().build();
    private final String listFileName = "multiTtest.csv";
    private final String fileName = "singleTest.csv";

    /**
     * Test of writeListToFile method, of class JavaToCSVWriter.
     *
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    //@Test
    public void testWriteListToFile() throws FileNotFoundException, IOException {
        System.out.println("writeListToFile");

        Shipment shipment1 = Shipment.builder()
                .id(id)
                .name(name)
                .href(href)
                .build();
        Shipment shipment2 = Shipment.builder()
                .id(id2)
                .name(name2)
                .href(href2)
                .build();
        List<Shipment> shipmentList = new ArrayList<>();

        shipmentList.add(shipment1);
        shipmentList.add(shipment2);

        //writing test.csv with given List<Shipment>
        JavaToCSVWriter.writeListToFile(shipmentList, listFileName);

        //Reading
        CSVReader reader = new CSVReader(new FileReader(listFileName), JavaToCSVWriter.SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, 1);

        //Store in list to accumulate lines so they can be passed through assertions
        List<String> shipments = new ArrayList<>();
        String[] shipmentsStorage;
        while ((shipmentsStorage = reader.readNext()) != null) {
            shipments.addAll(Arrays.asList(shipmentsStorage));
        }

        int index = 0;
        //actual testing
        assertEquals(shipment1.getLoadLocation(), shipments.get(index));
//        assertEquals(shipment1.getTo(), shipments.get(id++));
        assertEquals(shipment1.getId(), shipments.get(++index));
        assertEquals(shipment1.getName(), shipments.get(++index));
        assertEquals(shipment1.getHref(), shipments.get(++index));

        assertEquals(shipment2.getLoadLocation(), shipments.get(++index));
//        assertEquals(shipment2.getTo(), shipments.get(id++));
        assertEquals(shipment2.getId(), shipments.get(++index));
        assertEquals(shipment2.getName(), shipments.get(++index));
        assertEquals(shipment2.getHref(), shipments.get(++index));

        //delete test file from system
        File file = new File(listFileName);

        if (file.delete()) {
            System.out.println(file.getName() + " is deleted!");
        } else {
            System.out.println("Delete operation is failed.");
        }

    }

    /**
     * Test of writeSingleToFile method, of class JavaToCSVWriter.
     *
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    //@Test
    public void testWriteSingleToFile() throws FileNotFoundException, IOException {
        System.out.println("writeSingleToFile");

//        shipmentLines.add(shipmentLine);
        Shipment shipment = Shipment.builder()
                .id(id)
                .name(name)
                .href(href)
                .build();

        //Writing
        JavaToCSVWriter.writeSingleToFile(shipment, fileName);

        //Reading
        CSVReader reader = new CSVReader(new FileReader(fileName), JavaToCSVWriter.SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, 1);

        List<String> shipments = new ArrayList<>();
        String[] shipmentsStorage;
        while ((shipmentsStorage = reader.readNext()) != null) {
            shipments.addAll(Arrays.asList(shipmentsStorage));

        }
        int index = 0;
        //actual testing
        assertEquals(shipment.getLoadLocation(), shipments.get(index));
//        assertEquals(shipment.getTo(), shipments.get(id++));
        assertEquals(shipment.getId(), shipments.get(++index));
        assertEquals(shipment.getName(), shipments.get(++index));
        assertEquals(shipment.getHref(), shipments.get(++index));

        //delete test file from system
        File file = new File(fileName);

        if (file.delete()) {
            System.out.println(file.getName() + " is deleted!");
        } else {
            System.out.println("Delete operation is failed.");
        }

    }

}
