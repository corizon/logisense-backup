/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.adapters.xml.mapper;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import eu.logisense.adapters.xml.BooleanSerializer;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Client;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.Origin;
import eu.logisense.api.Ride;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import eu.logisense.api.vms.ExportObjectCardVM;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.core.MediaType;

import org.testng.annotations.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author perezdf
 */
public class LogisenseXmlMapperTest {

    public LogisenseXmlMapperTest() {
    }

    /**
     * Test of setDumpFolder method, of class LogisenseXmlMapper.
     */
    @Test
    public void testSetDumpFolder() throws IOException {
        System.out.println("setDumpFolder");
        LogisenseXmlMapper instance = new LogisenseXmlMapper();
        instance.setDumpFolder(Files.createTempDirectory("logisense-test").toString());

    }

    /**
     * Test of write method, of class LogisenseXmlMapper.
     */
    @Test
    public void testWrite_Shipment() throws Exception {
        System.out.println("write");
        Order order = Order.builder().debitor(Debitor.builder().price(100.10).build()).build();

        Shipment shipment = Shipment.builder().id(UUID.randomUUID().toString()).name("name").build();

        shipment.setOrder(order);
        LogisenseXmlMapper instance = new LogisenseXmlMapper();
        instance.setDumpFolder(Files.createTempDirectory("logisense-test").toString());

        File result = instance.write(shipment);
        assertNotNull(result);
        result.delete();
    }

    /**
     * Test of write method, of class LogisenseXmlMapper.
     */
    @Test
    public void testWrite_Ride() throws Exception {
        System.out.println("write");
        Ride ride = Ride.builder().id(UUID.randomUUID().toString()).name("name").build();
        LogisenseXmlMapper instance = new LogisenseXmlMapper();
        instance.setDumpFolder(Files.createTempDirectory("logisense-test").toString());

        File result = instance.write(ride);
        assertNotNull(result);
        result.delete();
    }

    @Test
    public void testWrite_ExportCard() throws Exception {
        System.out.println("testWrite_ExportCard");
        Ride ride = Ride.builder().id(UUID.randomUUID().toString()).name("rideName").build();
        List<Ride> rides = new ArrayList<>();
        Shipment shipment = Shipment.builder().id(UUID.randomUUID().toString()).name("orderName").build();
        List<Shipment> shipments = new ArrayList<>();
        shipments.add(shipment);

        ExportObjectCardVM exportDump = ExportObjectCardVM.builder().exportId(UUID.randomUUID().toString()).shipments(shipments).rides(rides).build();

        LogisenseXmlMapper instance = new LogisenseXmlMapper();
        instance.setDumpFolder(Files.createTempDirectory("logisense-test").toString());

        File result = instance.write(exportDump);
        assertNotNull(result);
        result.delete();
    }

    private static final Logger log = LoggerFactory.getLogger(LogisenseXmlMapperTest.class);

    @Test
    public void testWriteXml() throws IOException {
        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        module.addSerializer(new BooleanSerializer());
        XmlMapper xmlMapper = new XmlMapper(module);

        xmlMapper.registerModule(new JSR310Module());
        xmlMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        List<OrderLine> lines = new ArrayList<>();

        Shipment template = Shipment.builder()
                .loadLocation(LoadLocation.builder().build())
                .name("madrid to groningen")
                .status(TransportStatus.NEW)
                .unloadLocation(UnloadLocation.builder().build())
                .order(Order.builder()
                        .batch("")
                        .debitor(Debitor.builder().id("1").currency("EUR").price(2.2).build())
                        .destination(Destination.builder().build())
                        .href("")
                        .invoiceReference("")
                        .lines(lines)
                        .loadingList("")
                        .name("")
                        .orderedBy(Client.builder().name("any").status(ApprovalStatus.NEW).build())
                        .origin(Origin.builder().address("something").build())
                        .otherReference("")
                        .unloadReference("")
                        .build())
                .build();

        StringWriter writer = new StringWriter();
        xmlMapper.writeValue(writer, template);
        System.out.println(writer.toString());
    }

    @Test
    public void testWrite_LargeExportCard() throws Exception {
        System.out.println("testWrite_ExportCard");
        List<Ride> rides = new ArrayList<>();
        List<Shipment> shipments = new ArrayList<>();

        for (int i = 0; i < 1500; i++) {
            // Ride
            Ride ride = Ride.builder().id(UUID.randomUUID().toString()).name("rideName" + i).build();
            rides.add(ride);

            // Order
            Shipment shipment = Shipment.builder().id(UUID.randomUUID().toString()).name("orderName").build();

            shipments.add(shipment);

        }

        ExportObjectCardVM exportDump = ExportObjectCardVM.builder().exportId(UUID.randomUUID().toString()).shipments(shipments).rides(rides).build();

        LogisenseXmlMapper instance = new LogisenseXmlMapper();
        instance.setDumpFolder(Files.createTempDirectory("logisense-test").toString());

        File result = instance.write(exportDump);
        assertNotNull(result);
        result.delete();
    }

}
