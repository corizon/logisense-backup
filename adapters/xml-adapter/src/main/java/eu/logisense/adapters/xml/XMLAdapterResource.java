/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.adapters.xml;

import eu.logisense.api.AcceptShipmentsCapability;
import eu.logisense.adapters.xml.mapper.LogisenseXmlMapper;
import eu.logisense.api.vms.ExportObjectCardVM;
import eu.logisense.api.vms.ExportClientCardVM;
import eu.logisense.api.vms.ExportClientVM;
import eu.logisense.api.vms.ExportLocationVM;
import java.io.IOException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class XMLAdapterResource implements AcceptShipmentsCapability {

    private static final Logger logger = LoggerFactory.getLogger(XMLAdapterResource.class);
    private final LogisenseXmlMapper xmlMapper;

    public XMLAdapterResource() throws IOException {
        xmlMapper = new LogisenseXmlMapper();
    }

    @Override
    public void exportObjects(ExportObjectCardVM export) throws IOException {
        logger.info("exportObjects {}", export.getExportId());

        xmlMapper.write(export);

//        return Response.ok().build();
    }

    @Override
    public void exportClients(ExportClientCardVM export) throws IOException {
        logger.info("exportClients {}", export.getExportId());

        xmlMapper.write(export);

//        return Response.ok().build();
    }

    @Override
    public void exportLocation(ExportLocationVM location) throws IOException {
        logger.info("exportLocation {}", location.getExportId());
        xmlMapper.write(location);
//        return Response.ok().build();
    }

    @Override
    public void exportClient(ExportClientVM client) throws IOException {
        logger.info("exportClient {}", client.getExportId());
        xmlMapper.write(client);
//        return Response.ok().build();
    }
}
