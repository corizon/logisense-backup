/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.adapters.xml.mapper;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import eu.logisense.adapters.xml.BooleanSerializer;
import eu.logisense.adapters.xml.Config;
import eu.logisense.api.Ride;
import eu.logisense.api.Shipment;
import eu.logisense.api.vms.ExportCardVM;
import eu.logisense.api.vms.ExportObjectCardVM;
import eu.logisense.api.vms.ExportClientCardVM;
import eu.logisense.api.vms.ExportClientVM;
import eu.logisense.api.vms.ExportLocationVM;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

/**
 *
 * @author perezdf
 */
public class LogisenseXmlMapper {

    private static final Logger log = Logger.getLogger(LogisenseXmlMapper.class.getName());

    private String dumpFolder = Config.getValue(Config.PROP_DUMP_FOLDER);

    private static final String DEFAULT_POSIX_PERMISSIONS = "rw-rw----";
    private final XmlMapper xmlMapper;

    public LogisenseXmlMapper() throws IOException {
        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        module.addSerializer(new BooleanSerializer());
        xmlMapper = new XmlMapper(module);
        
        xmlMapper.registerModule(new JSR310Module());
        xmlMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    public void setDumpFolder(String dumpFolder) {
        this.dumpFolder = dumpFolder;
    }

        private File buildFile(String prefix) throws IOException {

        return buildFile(prefix,UUID.randomUUID().toString());
    }

    
    private File buildFile() throws IOException {

        return buildFile("",UUID.randomUUID().toString());
    }

    private File buildFile(String prefix,String id) throws IOException {
        Set<PosixFilePermission> permissions = PosixFilePermissions.fromString(DEFAULT_POSIX_PERMISSIONS);
        FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(permissions);
        String pathString = dumpFolder + File.separator + prefix + id + ".xml";
        if (!Files.exists(Paths.get(dumpFolder))) {
            Files.createDirectories(Paths.get(dumpFolder));
        }
        Path dumpPath = Paths.get(pathString);
        Path dumpFile = Files.createFile(dumpPath, fileAttributes);
        return dumpFile.toFile();
    }

    public File write(Shipment shipment) throws IOException {
        File file = buildFile();
        xmlMapper.writeValue(file, shipment);
        return file;
    }

    public File write(Ride ride) throws IOException {
        File file = buildFile();
        xmlMapper.writeValue(file, ride);
        return file;
    }

    public File write(ExportClientVM client) throws IOException {
        File file = buildFile("c-",client.getClientLocation().getClient().getId());
        xmlMapper.writeValue(file, client);
        return file;
    }

    
    public File write(ExportLocationVM location) throws IOException {
        File file = buildFile("l-",location.getLocation().getId());
        xmlMapper.writeValue(file, location);
        return file;
    }
    
    public File write(ExportClientCardVM export) throws IOException {
        File file = buildFile("cc-",export.getExportId());
        xmlMapper.writeValue(file, export);
        return file;
    }

    public File write(ExportObjectCardVM export) throws IOException {
        File file = buildFile("or-",export.getExportId());
        xmlMapper.writeValue(file, export);
        return file;
    }

    public Shipment readShipment(String shipment) throws IOException {
        return xmlMapper.readValue(shipment, Shipment.class);
    }
}
