# Installer

## install4j maven plugin

activate via profile: `-Pbuild-installer`

### setup

add install4j location to `settings.xml`, e.g.:

```
        <profile>
            <id>devenv</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <install4j.home>/home/johan/apps/install4j6/</install4j.home>
            </properties>
        </profile>
``` 


NB1: sonatype plugin uses install4j folder (compared to install4jc executable location for older googlecode plugin);

NB2: `install4j.home` needs an absolute path (path starting w/ `~/apps/...` will be interpreted as relative to maven project);

TODO: license key


## run scripts

`installer/run_*.sh`

not included in installer file set, but used to emulate startup via launchers
