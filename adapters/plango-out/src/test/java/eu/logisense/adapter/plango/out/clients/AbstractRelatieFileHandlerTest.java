/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.clients;

import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.adapter.plango.out.TestUtil;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.mockito.Mockito.mock;

/**
 *
 * @author johan
 */
public class AbstractRelatieFileHandlerTest {
    private static final String FILENAME1 = "deb-cred1.xml";

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    
    
    @Test
    public void testHandle() throws Exception {
        TestUtil.createDefaultSubDirs(testFolder);
        DirConfig dirs = new DirConfig(testFolder.getRoot().toString());
        Path inFile = dirs.getIncomingPath().resolve(FILENAME1);
        Path doneFile = dirs.getDonePath().resolve(FILENAME1);
        
        Files.copy(resourcePath("/data/" + FILENAME1), inFile);
        assertTrue(Files.exists(inFile));
        
        List<RelatieRow> rows= new ArrayList<>();
        
        AbstractRelatieFileHandler instance = new AbstractRelatieFileHandler(mock(ApiClient.class), dirs) {
            @Override
            public boolean handleRow(RelatieRow row, Writer logWriter) {
                rows.add(row);
                return true;
            }
        };
        
        instance.handle(inFile);
        
        assertEquals(2, rows.size());
        //initial deb-cred.xml: 3575 rows in ~0.6 seconds
        //initial Relaties.xml: 65948 rows in ~1.3 seconds
        
        //check some contents:
        assertEquals("123", rows.get(0).getDebtorNumber());
        assertEquals("457", rows.get(1).getCreditorNumber());
        
        assertTrue(Files.exists(doneFile));
        assertFalse(Files.exists(inFile));
    }
    
    private Path resourcePath(String s) {
        return Paths.get(getClass().getResource(s).getFile());
    }
}
