/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.adapter.plango.out;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author johan
 */
public class DirWatcherTest {
    
    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    /**
     * Test of start method, of class DirWatcher.
     */
    @Test
    public void testWatch() {
        System.out.println("watch");
        
        final CountDownLatch latch = new CountDownLatch(1);
        final String testFilename = "test.csv";
        final AtomicReference<String> filename = new AtomicReference<>();
        
        DirWatcher instance = new DirWatcher(testFolder.getRoot().toPath(), (Path fp) -> {
            filename.set(fp.getFileName().toString());
            latch.countDown();
        });
        
        //callback workaround: watcher thread not yet in watch-loop when file is created immediately after starting
        instance.onStarted(() -> {
            try {
                testFolder.newFile(testFilename);
            } catch (IOException ex) {}
        });
        
        instance.start(false);
                
        boolean handlerCalled = false;
        try {
            handlerCalled = latch.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {}
        
        assertTrue(handlerCalled);
        assertEquals(testFilename, filename.get());
        
        //TODO: cleanly stop watcher
        // exit via invalid watchKey or NoSuchFileException is logged when junit rule removes testfolder after test completion
    }
    
    @Test
    public void testScan() {
        System.out.println("scan");
        
        final CountDownLatch latch = new CountDownLatch(1);
        final String testFilename = "test.csv";
        final AtomicReference<String> filename = new AtomicReference<>();
        
        try {
            testFolder.newFile(testFilename);
        } catch (IOException ex) {}

        DirWatcher instance = new DirWatcher(testFolder.getRoot().toPath(), (Path fp) -> {
            if (!fp.equals(testFolder.getRoot().toPath())) {
                filename.set(fp.getFileName().toString());
                latch.countDown();
            }
        });
        
        instance.start(true);
        
        boolean handlerCalled = false;
        try {
            handlerCalled = latch.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {}
        
        assertTrue(handlerCalled);
        assertEquals(testFilename, filename.get());
    }

}
