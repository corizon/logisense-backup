/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author johan
 */
public class WriteDetectorTest {
    private static final Logger LOGGER = Logger.getLogger(WriteDetectorTest.class.getName());
    
    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Test
    public void testHandle() {
        System.out.println("handle");
        final CountDownLatch latch = new CountDownLatch(1);
        final String testFilename = "test.csv";
        final AtomicReference<String> filename = new AtomicReference<>();
        final AtomicLong nLinesRead = new AtomicLong(-1);
        int nLinesToWrite = 5;

        FileHandler original = new FileHandler() {
            @Override
            public boolean supports(Path fp, BasicFileAttributes a) {
                return a.isRegularFile();
            }

            @Override
            public boolean supports(Path fp) {
                return Files.isRegularFile(fp);
            }
            
            @Override
            public void handle(Path fp) {
                filename.set(fp.getFileName().toString());
                try {
                    nLinesRead.set(Files.lines(fp).collect(Collectors.counting()));
                } catch (IOException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
                latch.countDown();
            }
        };
        FileHandler instance = new WriteDetector(original);
                
        Thread writerThread = new Thread(() -> {
            File f = new File(testFolder.getRoot(), testFilename);
            
            try (Writer writer = new FileWriter(f)) {
                for (int i = 1; i <= nLinesToWrite; i++) {
                    Thread.sleep(500);
                    writer.append("line " + i + "\n");
                    writer.flush();
                }
            } catch (IOException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        });
        
        //TODO: try to test the detector without the dirwatcher
        DirWatcher watcher = new DirWatcher(testFolder.getRoot().toPath(), instance);
        watcher.onStarted(writerThread::start);
        watcher.start(false);
        
        boolean handlerCalled = false;
        try {
            handlerCalled = latch.await(10, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {}
        
        assertTrue(handlerCalled);
        assertEquals(testFilename, filename.get());
        
        // lockStrategy:
        // reads 0 lines (or 1 if writer's `sleep()` is moved after `flush()`);
        // looks like it can get an exclusive lock immediately with the writer still active,
        // so we can't assume arbitrary writing processes to have an exclusive lock until done;
        //
        // sizeStrategy should do for now.
        assertEquals(nLinesToWrite, nLinesRead.intValue());
    }
    
    @Test
    public void testSupports() {
        System.out.println("supports");
        
        Path path = mock(Path.class);
        BasicFileAttributes attrs = mock(BasicFileAttributes.class);
        FileHandler original = mock(FileHandler.class);
        when(original.supports(any(Path.class))).thenReturn(true);
        // let the overloaded version return a different value to check delegation
        when(original.supports(any(Path.class), any(BasicFileAttributes.class))).thenReturn(false);
        
        FileHandler instance = new WriteDetector(original);
        
        assertEquals(original.supports(path), instance.supports(path));
        assertEquals(original.supports(path, attrs), instance.supports(path, attrs));
        
        // direct + delegated:
        verify(original, times(2)).supports(any(Path.class));
        verify(original, times(2)).supports(any(Path.class), any(BasicFileAttributes.class));
    }

}
