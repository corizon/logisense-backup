/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.clients;

import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.adapter.plango.out.TestUtil;
import eu.logisense.api.Client;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author johan
 */
public class ClientImportAdapterTest {
    private static final String FILENAME1 = "deb-cred1.xml";

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    
    
    @Test
    public void testHandle() throws Exception {
        TestUtil.createDefaultSubDirs(testFolder);
        DirConfig dirs = new DirConfig(testFolder.getRoot().toString());
        Path inFile = dirs.getIncomingPath().resolve(FILENAME1);
        Path doneFile = dirs.getDonePath().resolve(FILENAME1);
        
        Files.copy(resourcePath("/data/" + FILENAME1), inFile);
        assertTrue(Files.exists(inFile));
        
        ApiClient apiClient = mock(ApiClient.class);
        when(apiClient.postClient(any(Client.class))).thenReturn(mock(Client.class));
        
        ClientImportAdapter instance = new ClientImportAdapter(apiClient, dirs);
        instance.handle(inFile);
        
        assertTrue(Files.exists(doneFile));
        assertFalse(Files.exists(inFile));
        
        Optional<Path> firstLog = Files.list(dirs.getLogPath()).findFirst();
        assertTrue(firstLog.isPresent());
        firstLog.map(p -> p.getFileName().toString())
                .ifPresent(name -> {
            assertTrue(name.startsWith(FILENAME1));
            assertTrue(name.endsWith(".log"));
        });

        verify(apiClient, times(2)).postClient(any(Client.class));
    }
    
    @Test
    public void testHandleRow() throws Exception {
        DirConfig dirs = mock(DirConfig.class);
        
        ApiClient apiClient = mock(ApiClient.class);
        when(apiClient.postClient(any(Client.class))).thenReturn(mock(Client.class));

        RelatieRow row = mock(RelatieRow.class);
        when(row.getRelationNumber()).thenReturn("1");

        ClientImportAdapter instance = new ClientImportAdapter(apiClient, dirs);
        instance.handleRow(row, mock(Writer.class));
        
        verify(apiClient, times(1)).postClient(any(Client.class));
    }
    
    @Test
    public void testSupports() throws Exception {
        Path filePath = resourcePath("/data/" + FILENAME1);
        BasicFileAttributes attrs = Files.readAttributes(filePath, BasicFileAttributes.class);
        
        ClientImportAdapter instance = new ClientImportAdapter(mock(ApiClient.class), mock(DirConfig.class));
        
//        assertTrue(instance.hasXmlExtension(filePath));
//        assertTrue(instance.commonSupports(filePath));
        assertTrue(instance.supports(filePath));
        assertTrue(instance.supports(filePath, attrs));
    }
    
    private Path resourcePath(String s) {
        return Paths.get(getClass().getResource(s).getFile());
    }
}
