/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import eu.logisense.api.Client;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import eu.logisense.api.session.client.LoginSequence;
import java.time.LocalDateTime;
import java.util.Arrays;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 *
 * @author johan
 */
public class ApiClientTest {
    
    private static final String CAPABILITY_ID = "123";
    private static final String CAPABILITY_ID_404 = "124";

    private static UndertowJaxrsServer server;

    @BeforeClass
    public static void init() throws Exception {
        server = StubServer.defaultInstance();
    }

    @AfterClass
    public static void stop() throws Exception {
        server.stop();
    }
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testExample() throws Exception {
        ResteasyClient client = new ResteasyClientBuilder().build();
        String val = client
//                .target(TestPortProvider.generateURL("/app/example"))
                .target(StubServer.DEFAULT_CONFIG.getResourceUri("/example"))
                .request().get(String.class);
        assertEquals("hello world", val);
        client.close();
    }
    
//    @Test
    public void testDispatchShipment() throws Exception {
        Shipment template = shipmentTemplate();
        assertNull("template shipment should not have ID set", template.getId());
        
        ApiClient instance = new ApiClient(StubServer.DEFAULT_CONFIG, mock(LoginSequence.class));
        Shipment shipment = instance.dispatchShipment(CAPABILITY_ID, template);
        
        assertNotNull("should return non-null shipment", shipment);
        assertNotNull("returned shipment should have ID set", shipment.getId());
    }
    
//    @Test
    public void testDispatchShipment404() throws Exception {
        thrown.expect(ApiException.class);
        thrown.expect(new ApiExceptionCodeMatcher(404));
        ApiClient instance = new ApiClient(StubServer.DEFAULT_CONFIG, mock(LoginSequence.class));
        instance.dispatchShipment(CAPABILITY_ID_404, shipmentTemplate());
    }
    
    private static Shipment shipmentTemplate() {
        LoadLocation load = LoadLocation.builder()
                .name("from A")
                .address("street 1")
                .city("city 1")
                .postalcode("1234AB")
                .country("NL")
                .status(ApprovalStatus.NEW)
                .timeFrameFrom(LocalDateTime.of(2015, 12, 31,0,0))
                .timeFrameTo(LocalDateTime.of(2015, 12, 31,0,0))
                .build();
        UnloadLocation unload = UnloadLocation.builder()
                .name("to B")
                .address("street 2")
                .city("city 2")
                .postalcode("9999ZZ")
                .country("NL")
                .status(ApprovalStatus.NEW)
                .timeFrameFrom(LocalDateTime.of(2016, 1, 31, 23, 59))
                .timeFrameTo(LocalDateTime.of(2016, 1, 31, 23, 59))
                .build();
        Shipment template = Shipment.builder()
                .loadLocation(load)
                .unloadLocation(unload)
//                .weight(99.9)
//                .volume(1.1)
                .status(TransportStatus.NEW)
                .deadline(LocalDateTime.of(2016, 2, 1,0,0))
                .build();
        return template;
    }
    
//    @Test
    public void testPostClient() throws Exception {
        Client template = clientTemplate();
        assertNull("template client should not have ID set", template.getId());
        
        ApiClient instance = new ApiClient(StubServer.DEFAULT_CONFIG, mock(LoginSequence.class));
        Client client = instance.postClient(template);
        
        assertNotNull("should return non-null client", client);
        assertNotNull("returned client should have ID set", client.getId());
        //TODO: repeat id checks for nested locations
    }
    
    private static Client clientTemplate() {
        Location loc1 = Location.buildLocation()
                .name("client 1 location")
                .address("street 1")
                .postalcode("1234AB")
                .city("city 1")
                .country("NL")
                .build();
        Client template = Client.builder()
                .name("Client 1")
                .externalId("123")
                .debtorNumber("234")
                .creditorNumber("345")
                .locations(Arrays.asList(loc1))
                .status(ApprovalStatus.APPROVED)
                .build();
        return template;
    }

    static class ApiExceptionCodeMatcher extends TypeSafeMatcher<ApiException> {
        private final int code;

        public ApiExceptionCodeMatcher(int code) {
            this.code = code;
        }

        @Override
        protected boolean matchesSafely(ApiException item) {
            return item.getCode() == code;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("expects code ").appendValue(code);
        }

        @Override
        protected void describeMismatchSafely(ApiException item, Description mismatchDescription) {
            mismatchDescription.appendText("was ").appendValue(item.getCode());
        }
    }
}
