/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import eu.logisense.api.Client;
import eu.logisense.api.ClientService;
import eu.logisense.api.Location;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author johan
 */
public class StubClientResource implements ClientService {
    private static int NEXT_CLIENT_ID = 0;
    private static int NEXT_LOCATION_ID = 0;
    
    @Override
    public List<Client> getClients() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Client getClient(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Client postClient(Client template) {
        List<Location> locations = new ArrayList<>();
        if (template.getLocations() != null) {
            template.getLocations().forEach(loc -> {
                Location.LocationBuilder lb = Location.buildLocation();
                lb.id(nextLocationId(null))
                        .name(loc.getName())
                        .address(loc.getAddress())
                        .postalcode(loc.getPostalcode())
                        .city(loc.getCity())
                        .country(loc.getCountry())
                        .latitude(loc.getLatitude())
                        .longitude(loc.getLongitude())
                        .email(loc.getEmail())
                        .phone(loc.getPhone())
                        ;
                locations.add(lb.build());
            });
        }
        
        Client.ClientBuilder cb = Client.builder();
        cb.id(nextClientId(null))
                .name(template.getName())
                .locations(locations)
                .status(template.getStatus())
                .debtorNumber(template.getDebtorNumber())
                .creditorNumber(template.getCreditorNumber())
                .externalId(template.getExternalId())
                ;
        
        return cb.build();
    }

    @Override
    public Client updateClient(String id, Client template) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteClient(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String nextClientId(String capabilityId) {
        return String.valueOf(++NEXT_CLIENT_ID);
    }
    
    private String nextLocationId(String capabilityId) {
        return String.valueOf(++NEXT_LOCATION_ID);
    }
}
