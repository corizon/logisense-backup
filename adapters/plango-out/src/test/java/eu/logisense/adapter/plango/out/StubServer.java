/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import io.undertow.Undertow;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.test.TestPortProvider;

/**
 *
 * @author johan
 */
public class StubServer {
    private static final String APP_PATH = "/app";
    
    @ApplicationPath(APP_PATH)
    public static class App extends Application {

        @Override
        public Set<Class<?>> getClasses() {
            Set<Class<?>> classes = new HashSet<>();
            classes.add(ExampleResource.class);
            classes.add(StubUsageResource.class);
            //maybe later: stub logisense shipment service to emulate direct communication 
            classes.add(StubClientResource.class);
            classes.add(JacksonContextResolver.class);
            return classes;
        }
    }
    
    @Path("/example")
    public static class ExampleResource {
        
        @GET
        @Produces("text/plain")
        public String hello() {
            return "hello world";
        }
    }
    
    public static final ApiConfig DEFAULT_CONFIG = new ApiConfig("http", "localhost", 8081, APP_PATH);
    public static final ApiConfig RESTEASY_CONFIG = new ApiConfig("http", TestPortProvider.getHost(), TestPortProvider.getPort(), APP_PATH);
    
    public static UndertowJaxrsServer defaultInstance() {
        return newInstance(DEFAULT_CONFIG);
    }

    public static UndertowJaxrsServer newInstance(ApiConfig config) {
        //TODO: https support
        if(!"http".equals(config.getProtocol())) throw new IllegalArgumentException();
        Undertow.Builder builder = Undertow.builder().addHttpListener(config.getPort(), config.getHost());
        UndertowJaxrsServer server = new UndertowJaxrsServer().start(builder);
        server.deploy(App.class);
        return server;
    }
    
    public static void main(String[] args) {
        defaultInstance();        
    }
}
