/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.full;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import eu.logisense.adapter.plango.out.shipments.full.model.ShipmentImport;
import eu.logisense.api.Shipment;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author johan
 */
public class TmpTest {
    private static final String FILENAME1 = "NP-20160422.xml";
    
    //deserialize & object mapping tests combined; smoke test for NP file format changes
    @Test
    public void testDeserializeAndMap() throws IOException {
        Path filePath = resourcePath("/data/" + FILENAME1);
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.registerModule(new JSR310Module());
        
        ShipmentImport imported = xmlMapper.readValue(Files.newInputStream(filePath), ShipmentImport.class);
        
        assertNotNull(imported);
        assertNotNull(imported.getShipments());
        assertEquals(1, imported.getShipments().length);
        
        ShipmentMapper instance = new ShipmentMapper();
        Shipment shipment = instance.map(imported.getShipments()[0]);
        
        assertNotNull(shipment);
        
        assertNotNull(shipment.getOrder());
        assertNotNull(shipment.getLoadLocation());
        assertNotNull(shipment.getUnloadLocation());
        
        assertNotNull(shipment.getOrder().getDebitor());
        assertNotNull(shipment.getOrder().getLines());
        assertNotNull(shipment.getOrder().getOrderedBy());
        assertNotNull(shipment.getOrder().getOrigin());
        
        System.out.println(shipment.getDeadline());
        
    }
    
    private Path resourcePath(String s) {
        return Paths.get(getClass().getResource(s).getFile());
    }
    
}
