/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule; // 2.7.x
import com.fasterxml.jackson.datatype.jsr310.JSR310Module; // 2.4.x
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static eu.logisense.adapter.plango.out.ApiConfig.ERROR_HEADER;

/**
 * 
 * @author johan
 */
public class StubUsageResource {
    private static final Logger LOGGER = Logger.getLogger(StubUsageResource.class.getName());
    
    //TODO: map w/ counters per provider / capability
    private static int NEXT_SHIPMENT_ID = 0;
    
    // "magic numbers" for supported capability ids; return 404 for everything else;
    private static final List<String> SUPPORTED_CAPABILITIES = Arrays.asList("123", "456");

    public Response useGet(String string) {
        // not needed for shipment dispatch api
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Response usePost(String capabilityId, Object o) {
        if (o == null) {
            return Response.serverError()
                    .header(ERROR_HEADER, "empty post body")
                    .build();
        }
        
        if (!SUPPORTED_CAPABILITIES.contains(capabilityId)) {
            return Response.status(Response.Status.NOT_FOUND)
                    .header(ERROR_HEADER, "capability id " + capabilityId + " not supported")
                    .build();
        }
        
        //FIXME: prefer to always receive body of type Shipment,
        // though might not be possible if it's not in the method signature (Jackson/Jettison providers won't know the type)
        if (o instanceof Map) {
            Shipment template;
            // jackson workaround: more compact than manual conversion, some overhead because of intermediate json
            try {
                ObjectMapper mapper = new ObjectMapper();
                mapper.registerModule(new JSR310Module());
                template = mapper.convertValue(o, Shipment.class);
            } catch (IllegalArgumentException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
                return Response.serverError()
                        .header(ERROR_HEADER, "could not convert Map to Shipment")
                        .build();
            }
            return handleShipment(capabilityId, template);
        }
        else if (o instanceof Shipment) {
            return handleShipment(capabilityId, (Shipment) o);
        }
        else {
            return Response.serverError()
                    .header(ERROR_HEADER, "body type " + o.getClass().getName() + " not supported")
                    .build();
        }
    }
    
    private Response handleShipment(String capabilityId, Shipment template) {
            Shipment shipment = Shipment.builder()
                    .id(nextId(capabilityId))
                    .loadLocation(template.getLoadLocation())
                    .unloadLocation(template.getUnloadLocation())
//                    .weight(template.getWeight())
//                    .volume(template.getVolume())
                    .status(TransportStatus.NEW)
                    .build();
            return Response.ok()
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .entity(shipment)
                    .build();
    }
    
    private String nextId(String capabilityId) {
        return String.valueOf(++NEXT_SHIPMENT_ID);
    }

}
