/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.dispatch;

import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.ApiException;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.api.Shipment;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.mockito.Mockito.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author johan
 */
public class DispatchAdapterTest {
    private static final String FILENAME1 = "shipment1.csv";

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    
    @Test
    public void testSupports() throws Exception {
        DispatchAdapter instance = new DispatchAdapter(mock(ApiClient.class), mock(DirConfig.class), mock(CSVMapper.class), Collections.emptyMap());
        
        Path filePath = resourcePath("/data/" + FILENAME1);
        BasicFileAttributes attrs = Files.readAttributes(filePath, BasicFileAttributes.class);
        
        assertTrue(instance.supports(filePath));
        assertTrue(instance.supports(filePath, attrs));
    }
    
    @Test
    public void testHandle() throws Exception {
        testFolder.newFolder("done");
        testFolder.newFolder("failed");
        testFolder.newFolder("log");
        DirConfig dirs = new DirConfig(testFolder.getRoot().toString());
        Path inFile = dirs.getIncomingPath().resolve(FILENAME1);
        Path doneFile = dirs.getDonePath().resolve(FILENAME1);
        
        Files.copy(resourcePath("/data/" + FILENAME1), inFile);
        assertTrue(Files.exists(inFile));
                
        ApiClient apiClient = mock(ApiClient.class);
        when(apiClient.dispatchShipment(anyString(), any(Shipment.class))).thenReturn(mock(Shipment.class));
        
        CSVMapper mapper = mock(CSVMapper.class);
        when(mapper.read(any(Path.class))).thenReturn(Collections.singletonList(mock(ShipmentWrapper.class)));
        
        DispatchAdapter instance = new DispatchAdapter(apiClient, dirs, mapper, Collections.emptyMap());
        instance.handle(inFile);
        
        //moved from incoming to done:
        assertTrue(Files.exists(doneFile));
        assertFalse(Files.exists(inFile));
        
        verify(mapper, times(1)).read(any(Path.class));
        verify(apiClient, times(1)).dispatchShipment(anyString(), any(Shipment.class));
    }
    
    @Test
    public void testHandle_apiError() throws Exception {
        testFolder.newFolder("done");
        testFolder.newFolder("failed");
        testFolder.newFolder("log");
        DirConfig dirs = new DirConfig(testFolder.getRoot().toString());
        Path inFile = dirs.getIncomingPath().resolve(FILENAME1);
        Path failedFile = dirs.getFailedPath().resolve(FILENAME1);
        
        Files.copy(resourcePath("/data/" + FILENAME1), inFile);
        assertTrue(Files.exists(inFile));
        
        ApiClient apiClient = mock(ApiClient.class);
        when(apiClient.dispatchShipment(anyString(), any(Shipment.class))).thenThrow(new ApiException(404, ""));
        
        CSVMapper mapper = mock(CSVMapper.class);
        when(mapper.read(any(Path.class))).thenReturn(Collections.singletonList(mock(ShipmentWrapper.class)));
        
        DispatchAdapter instance = new DispatchAdapter(apiClient, dirs, mapper, Collections.emptyMap());
        instance.handle(inFile);
        
        //moved from incoming to failed:
        assertTrue(Files.exists(failedFile));
        assertFalse(Files.exists(inFile));
        
        verify(mapper, times(1)).read(any(Path.class));
        verify(apiClient, times(1)).dispatchShipment(anyString(), any(Shipment.class));
    }
    
    @Test
    public void testHandle_readError() throws Exception {
        testFolder.newFolder("done");
        testFolder.newFolder("failed");
        testFolder.newFolder("log");
        DirConfig dirs = new DirConfig(testFolder.getRoot().toString());
        Path inFile = dirs.getIncomingPath().resolve(FILENAME1);
        Path failedFile = dirs.getFailedPath().resolve(FILENAME1);
        
        Files.copy(resourcePath("/data/" + FILENAME1), inFile);
        assertTrue(Files.exists(inFile));
        
        ApiClient apiClient = mock(ApiClient.class);
        when(apiClient.dispatchShipment(anyString(), any(Shipment.class))).thenReturn(mock(Shipment.class));
        
        CSVMapper mapper = mock(CSVMapper.class);
        when(mapper.read(any(Path.class))).thenThrow(new RuntimeException());
        
        DispatchAdapter instance = new DispatchAdapter(apiClient, dirs, mapper, Collections.emptyMap());
        instance.handle(inFile);
        
        //moved from incoming to failed:
        assertTrue(Files.exists(failedFile));
        assertFalse(Files.exists(inFile));
        
        verify(mapper, times(1)).read(any(Path.class));
        verifyZeroInteractions(apiClient);
    }
    
    private Path resourcePath(String s) {
        return Paths.get(getClass().getResource(s).getFile());
    }

}
