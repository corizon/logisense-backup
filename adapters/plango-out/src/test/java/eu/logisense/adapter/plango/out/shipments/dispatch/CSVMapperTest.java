/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.dispatch;

import eu.logisense.api.LoadLocation;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author johan
 */
public class CSVMapperTest {
    private static final double DELTA = .000001;
    
    /**
     * Test of read method, of class CSVMapper.
     */
    @Test
    public void testRead() {
        System.out.println("read");
        
        Path csv = resourcePath("/data/shipment1.csv");
        
        CSVMapper instance = new CSVMapper();

        List<ShipmentWrapper> result = instance.read(csv);
        assertEquals(2, result.size());

        // 1st line: check all attribute values
        //
        ShipmentWrapper w1 = result.get(0);
        assertEquals("PARTNER1",w1.getToPartnerId());
        assertNotNull(w1.getShipment());
        
        Shipment s1 = w1.getShipment();
        assertNotNull(s1.getLoadLocation());
        assertNotNull(s1.getUnloadLocation());
        assertNull(s1.getId());
        assertNull(s1.getName());
        assertNull(s1.getHref());
//        assertEquals(65.43, s1.getWeight(), DELTA);
//        assertEquals(1.1, s1.getVolume(), DELTA);
        assertEquals(TransportStatus.NEW, s1.getStatus());
        assertEquals(LocalDateTime.of(2016, 2, 1, 23,59), s1.getDeadline());
        
        LoadLocation l1 = s1.getLoadLocation();
        assertEquals("Kitchens, Inc.", l1.getName());
        assertEquals("street 1", l1.getAddress());
        assertEquals("city 1", l1.getCity());
        assertEquals("1234AB", l1.getPostalcode());
        assertEquals("NL", l1.getCountry());
        assertEquals(LocalDateTime.of(2015, 12, 31, 23,59), l1.getTimeFrameFrom());
        assertEquals(LocalDateTime.of(2015, 12, 31, 23,59), l1.getTimeFrameTo());

        UnloadLocation ul1 = s1.getUnloadLocation();
        assertEquals("consumer 2", ul1.getName());
        assertEquals("street 2", ul1.getAddress());
        assertEquals("city 2", ul1.getCity());
        assertEquals("9999ZZ", ul1.getPostalcode());
        assertEquals("NL", ul1.getCountry());
        assertEquals(LocalDateTime.of(2016, 1, 31, 23, 59), ul1.getTimeFrameFrom());
        assertEquals(LocalDateTime.of(2016, 1, 31, 23, 59), ul1.getTimeFrameTo());
        
        // 2nd line: check fallback values for empty date/time fields
        //
        ShipmentWrapper w2 = result.get(1);
        assertNotNull(w2.getToPartnerId());
        assertNotNull(w2.getShipment());

        Shipment s2 = w2.getShipment();
        assertNotNull(s2.getLoadLocation());
        assertNotNull(s2.getUnloadLocation());
        LoadLocation o2 = s2.getLoadLocation();
        UnloadLocation d2 = s2.getUnloadLocation();
        
        assertEquals(LocalDateTime.MAX, s2.getDeadline());
        assertEquals(LocalDateTime.MAX, o2.getTimeFrameFrom());
        assertEquals(LocalDateTime.MAX, o2.getTimeFrameTo());
        assertEquals(LocalDateTime.MAX, d2.getTimeFrameFrom());
        assertEquals(LocalDateTime.MAX, d2.getTimeFrameTo());
    }
    
    private Path resourcePath(String s) {
        return Paths.get(getClass().getResource(s).getFile());
    }

}