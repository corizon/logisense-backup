/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.dispatch;

import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.adapter.plango.out.DirWatcher;
import eu.logisense.adapter.plango.out.StubServer;
import eu.logisense.api.session.client.LoginSequence;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import static org.mockito.Mockito.mock;

/**
 *
 * @author johan
 */
public class Demo {
    private static final Logger LOGGER = Logger.getLogger(Demo.class.getName());
    
    public static final Map<String,String> CLIENT_TO_CAPABILITY;
    static {
        Map<String,String> map = new HashMap<>();
        map.put("PARTNER1", "123");
        map.put("PARTNER2", "456");
        CLIENT_TO_CAPABILITY = Collections.unmodifiableMap(map);
    }

    public static void main(String[] args) {
        DirConfig dirConfig = new DirConfig("/tmp/logisense/plango-out");

        if (!dirConfig.isValid()) {
            LOGGER.severe("invalid dir config, exiting");
            System.exit(-1);
        }

        ApiClient apiClient = new ApiClient(StubServer.DEFAULT_CONFIG, mock(LoginSequence.class));
        DispatchAdapter dispatcher = new DispatchAdapter(apiClient, dirConfig, new CSVMapper(), CLIENT_TO_CAPABILITY);
        //TODO: optionally wrap dispatcher in a WriteDetector
        DirWatcher watcher = new DirWatcher(dirConfig.getIncomingPath(), dispatcher);
        watcher.start(false);
    }
}
