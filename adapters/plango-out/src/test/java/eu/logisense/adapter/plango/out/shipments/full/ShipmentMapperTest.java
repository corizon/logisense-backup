/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.full;

import eu.logisense.adapter.plango.out.shipments.full.model.PGClient;
import eu.logisense.adapter.plango.out.shipments.full.model.PGDebitor;
import eu.logisense.adapter.plango.out.shipments.full.model.PGLine;
import eu.logisense.adapter.plango.out.shipments.full.model.PGLoadLocation;
import eu.logisense.adapter.plango.out.shipments.full.model.PGLocation;
import eu.logisense.adapter.plango.out.shipments.full.model.PGOrder;
import eu.logisense.adapter.plango.out.shipments.full.model.PGShipment;
import eu.logisense.api.Shipment;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author johan
 */
public class ShipmentMapperTest {

    @Test
    public void testMap() {
        PGShipment pgShipment = makeShipment();
        ShipmentMapper instance = new ShipmentMapper();
        Shipment shipment = instance.map(pgShipment);
        
        assertNotNull(shipment);
        
        assertNotNull(shipment.getOrder());
        assertNotNull(shipment.getLoadLocation());
        assertNotNull(shipment.getUnloadLocation());
        
        assertNotNull(shipment.getOrder().getDebitor());
        assertNotNull(shipment.getOrder().getLines());
        assertNotNull(shipment.getOrder().getOrderedBy());
        assertNotNull(shipment.getOrder().getOrigin());
        assertNotNull(shipment.getOrder().getDestination());
        
        assertEquals(pgShipment.getLines().length, shipment.getOrder().getLines().size());
        
        // padded id strings:
        assertEquals(pgShipment.getOrder().getOrderedBy().getDebtorNumber().trim(), shipment.getOrder().getOrderedBy().getDebtorNumber());
        assertEquals(pgShipment.getOrder().getExternalId().trim(), shipment.getOrder().getExternalId());
    }
    
    // setup object tree similar to NP.xml contents:
    private PGShipment makeShipment() {
        PGShipment.PGShipmentBuilder sb = PGShipment.builder()
                .deadline("2016-03-01   T   16:00:00")
                .shippedUnits(9.00);
        
        PGOrder.PGOrderBuilder ob = PGOrder.builder()
                .externalId("  586876") //needs trim
                .loadingList("DK-4692811")
                .debitor(PGDebitor.builder()
                        .externalId("990003144")
                        .name("INTERSTIL A/S")
                        .build())
                .orderedBy(PGClient.buildClient()
                        .debtorNumber("  133514") //needs trim
                        .externalId("990003144")
                        .name("INTERSTIL A/S")
                        .build())
                .origin(PGLocation.buildLocation()
                        .address1("EGESKOVVEJ")
                        .address2("15")
                        .city("HORSENS")
                        .country("DK")
                        .externalId("990003144")
                        .name("INTERSTIL A/S")
                        .postalcode("8700")
                        .build())
                .destination(PGLocation.buildLocation() //FIXME: missing in sample file, values copied from unload
                        .address1("PLANETENBAAN")
                        .address2("16")
                        .city("MAARSSEN")
                        .country("nl")
                        .externalId("990069721")
                        .name("HUISTENTHUIS.NL")
                        .phone("114760001")
                        .postalcode("3606AK")
                        .build())
                .receiver(PGClient.buildClient()
                        .externalId("0")
                        .build());
        
        sb.loadLocation(PGLoadLocation.builder()
                .address1("ORIONVEJ")
                .address2("18")
                .city("HORSENS")
                .country("DK")
                .email("invoice@lgtlogistics.dk")
                .externalId("9035854")
                .name("LGT LOGISTICS")
                .phone("76264411")
                .postalcode("8700")
                .timeFrameFrom("2016-03-01T08:00:00")
                .timeFrameTo("2016-03-01T17:00:00")
                .build());
        sb.unloadLocation(PGLoadLocation.builder()
                .address1("PLANETENBAAN")
                .address2("16")
                .city("MAARSSEN")
                .country("nl")
                .externalId("990069721")
                .name("HUISTENTHUIS.NL")
                .phone("114760001")
                .postalcode("3606AK")
                .timeFrameFrom("2016-03-01T09:00:00")
                .timeFrameTo("2016-03-01T16:00:00")
                .build());
        
        PGLine line1 = PGLine.builder()
                .amount(4.00)
                .description(" ")
                .paymentOnDeliveryAmount(0.0)
                .type("COLLI")
                .volume(.9)
                .weight(34.0)
                .build();
        PGLine line2 = PGLine.builder()
                .amount(3.00)
                .description(" ")
                .paymentOnDeliveryAmount(0.0)
                .type("COLLI")
                .height(.2)
                .length(.2)
                .width(.2)
                .weight(67.0)
                .build();
        PGLine line3 = PGLine.builder()
                .amount(2.00)
                .description(" ")
                .paymentOnDeliveryAmount(0.0)
                .type("CUSTOM")
                .height(.2)
                .length(.2)
                .width(.2)
                .weight(58.0)
                .build();
        
        sb.lines(new PGLine[] { line1, line2, line3 });
        sb.order(ob.build());
        
        return sb.build();
    }
}
