/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.full.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author johan
 */
@NoArgsConstructor
public class PGOrder {
    
    @Getter
    @Setter
    private String externalId;

    @Getter
    @Setter
    private String invoiceReference;

    @Getter
    @Setter
    private String loadingList;

    @Getter
    @Setter
    private String otherReference;
    
    @Getter
    @Setter
    private PGLocation origin;
    
    @Getter
    @Setter
    private PGLocation destination;
    
    @Getter
    @Setter
    private PGDebitor debitor; // client subset (extid,name) + price
    
    @Getter
    @Setter
    private PGClient orderedBy; // client subset (extid,name) + debtorNumber
    
    @Getter
    @Setter
    private PGClient receiver; // client subset (extid,name)

    @Builder
    public PGOrder(String externalId, String invoiceReference, String loadingList, String otherReference, PGLocation origin, PGLocation destination, PGDebitor debitor, PGClient orderedBy, PGClient receiver) {
        this.externalId = externalId;
        this.invoiceReference = invoiceReference;
        this.loadingList = loadingList;
        this.otherReference = otherReference;
        this.origin = origin;
        this.destination = origin;
        this.debitor = debitor;
        this.orderedBy = orderedBy;
        this.receiver = receiver;
    }
        
}
