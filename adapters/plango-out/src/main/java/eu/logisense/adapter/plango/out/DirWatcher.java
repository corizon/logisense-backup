/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardWatchEventKinds.*;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Watches a directory for newly created files which are passed to a <code>FileHandler</code> for processing when supported.
 * 
 * Can optionally scan the directory first to allow for processing of files already present.
 * 
 * @author johan
 */
public class DirWatcher {
    private static final Logger LOGGER = Logger.getLogger(DirWatcher.class.getName());
    
    private final Path dir;
    private final FileHandler fileHandler;
    private final List<Runnable> onStarted = new ArrayList<>();
    
    public DirWatcher(Path dir, FileHandler fileHandler) {
        this.dir = dir;
        this.fileHandler = fileHandler;
    }

    public void start(boolean doScan) {        
        LOGGER.info("starting DirWatcher Thread");
        new Thread(() -> {
            if (doScan) scan();
            watch();
        }, "DirWatcher").start();
    }
    
    protected void onStarted(Runnable r) {
        onStarted.add(r);
    }
    
    protected void scan() {
        LOGGER.finest("start scanning");
        try {
            Files.find(dir, 1, fileHandler::supports).forEach(fileHandler::handle);
//            Files.walk(dir, 1).filter(fileHandler::supports).forEach(fileHandler::handle);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
    protected void watch() {
        LOGGER.finest("start watching");
        
        try {
            WatchService watcher = FileSystems.getDefault().newWatchService();
            dir.register(watcher, ENTRY_CREATE);
            
            LOGGER.finest("registered watch service");
            
            try {
                onStarted.forEach(r -> r.run());
            } catch (Exception ignore) {}
            
            // process events:
            for (;;) {
                LOGGER.finest("loop");
                
                WatchKey key;
                try {
                    key = watcher.take();
                } catch (InterruptedException x) {
                    return;
                }
                
                LOGGER.finest("took a key");
                
                key.pollEvents().forEach((event) -> {
                    WatchEvent.Kind<?> kind = event.kind();
                    
                    if (kind == OVERFLOW) {
                        //TODO: handle overflow
                        LOGGER.warning("encountered OVERFLOW event");
                    }
                    else if (kind == ENTRY_CREATE) {
                        WatchEvent<Path> ev = cast(event);
                        Path name = ev.context();
                        LOGGER.log(Level.FINEST, "ENTRY_CREATE event for: {0}", name.toString());
                        Path file = dir.resolve(name);
                        if (fileHandler.supports(file)) {
                            fileHandler.handle(file);
                        }
                    }
                });
                
                if (!key.reset()) {
                    LOGGER.severe("invalid watchKey, exiting");
                    break;
                }
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "IO Exception", ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }
}
