/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.adapter.plango.out.shipments.full.model;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author johan
 */
@NoArgsConstructor
public class PGLoadLocation extends PGLocation {

    // can be used for both load and and unload location for now
    
    @Getter
    @Setter
    private String timeFrameFrom;
    
    @Getter
    @Setter
    private String timeFrameTo;

    @Builder
    public PGLoadLocation(String timeFrameFrom, String timeFrameTo, String address1, String address2, String city, String country, String email, String externalId, String name, String phone, String postalcode) {
        super(address1, address2, city, country, email, externalId, name, phone, postalcode);
        this.timeFrameFrom = timeFrameFrom;
        this.timeFrameTo = timeFrameTo;
    }

}
