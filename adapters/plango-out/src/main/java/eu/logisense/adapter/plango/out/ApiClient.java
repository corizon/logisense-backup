/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule; // 2.7.x
import com.fasterxml.jackson.datatype.jsr310.JSR310Module; // 2.4.x
import com.opower.rest.client.generator.core.ClientResponseFailure;
import eu.logisense.api.Client;
import eu.logisense.api.ClientService;
import eu.logisense.api.ExternalSyncService;
import eu.logisense.api.Location;
import eu.logisense.api.LocationService;
import eu.logisense.api.RideService;
import eu.logisense.api.Shipment;
import eu.logisense.api.ShipmentService;
import eu.logisense.api.session.client.LoginSequence;
import eu.logisense.api.session.client.TokenProvider;
import eu.logisense.api.util.ClientUtil;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.openide.util.Lookup;

/**
 *
 * @author johan
 */
public class ApiClient {
    
    private final ApiConfig config;
    private final ClientService clientService;
    private final LocationService locationService;
    private final RideService rideService;
    private final ShipmentService shipmentService;
    private final ExternalSyncService syncService;
    private final TokenProvider tokenProvider = Lookup.getDefault().lookup(TokenProvider.class);
    
    public ApiClient(ApiConfig config, LoginSequence loginSequence) {
        this.config = config;
        loginSequence.start();
        clientService = ClientUtil.proxy(ClientService.class, config.getBaseUri());
        locationService = ClientUtil.proxy(LocationService.class, config.getBaseUri());
        rideService = ClientUtil.proxy(RideService.class, config.getBaseUri());
        shipmentService = ClientUtil.proxy(ShipmentService.class, config.getBaseUri());
        syncService = ClientUtil.proxy(ExternalSyncService.class, config.getBaseUri());
    }

    public ClientService getClientService() {
        return clientService;
    }
    
    public RideService getRideService() {
        return rideService;
    }
    
    public ShipmentService getShipmentService() {
        return shipmentService;
    }
    
    public ExternalSyncService getSyncService() {
        return syncService;
    }
    
    public Shipment dispatchShipment(String capabilityId, Shipment template) throws ApiException {
        Objects.requireNonNull(capabilityId);
        Objects.requireNonNull(template);
        
        ResteasyClient client = new ResteasyClientBuilder().build();
        client.register(JacksonContextResolver.class);
        
        try {
            //FIXME: proxy creation fails on missing `@Consumes`
            // (but adding it would limit supported types while server should just forward any type it receives)
//            UsageService proxy = client.target(config.getAppUrl()).proxy(UsageService.class);
//            Response resp = proxy.usePost(capabilityId, template);
            Invocation.Builder requestBuilder = client
                    .target(config.getResourceUri("/use/" + capabilityId))
                    .request();
            if (tokenProvider != null) {
                requestBuilder.header("Authorization", "Bearer " + tokenProvider.getBearerToken());
            }
            Response response = requestBuilder
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.json(template));
            
            if (response.getStatus() == Status.OK.getStatusCode()) {
                response.bufferEntity();
                Object entity = response.readEntity(Object.class);

                //FIXME: lose the maps, also see stub usage service
                if (entity instanceof Map) {
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.registerModule(new JSR310Module());
                    return mapper.convertValue(entity, Shipment.class);
                }
                else if (entity instanceof Shipment) {
                    return (Shipment) entity;
                }
                else {
                    throw new IllegalStateException("unexpected response body type: " + entity.getClass().getName());
                }
            }
            else { 
                throw new ApiException(response.getStatus(), response.getHeaderString(ApiConfig.ERROR_HEADER));
            }
        } finally {
            client.close();
        }
        
    }
    

    public Client postClient(Client client) throws ApiException, IOException {
        try {
            // TODO: insert/update strategy; assume initial load for now
            return clientService.postClient(client);
        } catch(ClientResponseFailure ex) {
            throw new ApiException(ex.getResponse().getStatus(), ex);
        }
    }
    
    public Location postLocation(Location location) throws ApiException, IOException {
        try {
            // TODO: insert/update strategy; assume initial load for now
            return locationService.postLocation(location);
        } catch(ClientResponseFailure ex) {
            throw new ApiException(ex.getResponse().getStatus(), ex);
        }
    }
    
    public Shipment postShipment(Shipment shipment) throws ApiException, IOException {
        try {
            // TODO: insert/update strategy; assume initial load for now
            return syncService.postShipment(shipment);
        } catch(ClientResponseFailure ex) {
            throw new ApiException(ex.getResponse().getStatus(), ex);
        }
    }
    
}
