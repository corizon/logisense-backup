/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.full;

import eu.logisense.adapter.plango.out.shipments.full.model.PGClient;
import eu.logisense.adapter.plango.out.shipments.full.model.PGDebitor;
import eu.logisense.adapter.plango.out.shipments.full.model.PGLine;
import eu.logisense.adapter.plango.out.shipments.full.model.PGLoadLocation;
import eu.logisense.adapter.plango.out.shipments.full.model.PGLocation;
import eu.logisense.adapter.plango.out.shipments.full.model.PGOrder;
import eu.logisense.adapter.plango.out.shipments.full.model.PGShipment;
import eu.logisense.api.Client;
import eu.logisense.api.Debitor;
import eu.logisense.api.Destination;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Location;
import eu.logisense.api.Order;
import eu.logisense.api.OrderLine;
import eu.logisense.api.OrderLineType;
import eu.logisense.api.Origin;
import eu.logisense.api.Shipment;
import eu.logisense.api.UnloadLocation;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author johan
 */
public class ShipmentMapper {
    
    private final Pattern whitespace = Pattern.compile("[\\s\\p{Z}]"); //"\\p{javaSpaceChar}"

    public Shipment map(PGShipment s) {
        Shipment.ShipmentBuilder sb = Shipment.builder()
                .deadline(toDatetime(s.getDeadline()))
                .shippedUnits(s.getShippedUnits() == null ? null : s.getShippedUnits().intValue())
                .note(s.getNote())
                .loadLocation(toLoadLocation(s.getLoadLocation()))
                .unloadLocation(toUnloadLocation(s.getUnloadLocation()));
        
        PGOrder o = s.getOrder();
        
        Order.OrderBuilder ob = Order.builder()
                .debitor(toDebitor(o.getDebitor()))
                .destination(toDestination(o.getDestination()))
                .externalId(trim(o.getExternalId()))
                .invoiceReference(o.getInvoiceReference())
                .loadingList(o.getLoadingList())
                .name("") // fixme: null -> internal server error
                .orderedBy(toClient(o.getOrderedBy()))
                .origin(toOrigin(o.getOrigin()))
                .otherReference(o.getOtherReference());

        doLines(s.getLines(), ob);
        
        sb.order(ob.build());
        
        return sb.build();
    }
    
    protected void doLines(PGLine[] pgLines, Order.OrderBuilder ob) {
        List<OrderLine> lines = new ArrayList<>();
        
        if (pgLines != null) {
            for (PGLine l : pgLines) {
                OrderLine ol = OrderLine.builder()
                        .amount(l.getAmount() == null ? null : l.getAmount().intValue())
                        .description(l.getDescription())
                        .height(l.getHeight())
                        .length(l.getLength())
//                        .name(l.getName())
                        .paymentOnDeliveryAmount(l.getPaymentOnDeliveryAmount())
                        .paymentOnDeliveryCurrency(l.getPaymentOnDeliveryCurrency())
                        .remarks(l.getRemarks())
                        .type(toLineType(l.getType()))
                        .weight(l.getWeight())
                        .width(l.getWidth())
                        .build();
                dimFix(ol, l.getVolume());
                lines.add(ol);
            }
        }
        ob.lines(lines);
    }
    
    protected Location toLocation(PGLocation l) {
        return Location.buildLocation()
                .address(l.getAddress1() + " " + l.getAddress2())
                .city(l.getCity())
                .country(l.getCountry())
                .email(l.getEmail())
                .externalId(l.getExternalId())
                .name(l.getName())
                .phone(l.getPhone())
                .postalcode(l.getPostalcode())
                .build();
    }
    
    protected Origin toOrigin(PGLocation l) {
        if (l == null) return null;
        return Origin.builder()
                .address(l.getAddress1() + " " + l.getAddress2())
                .city(l.getCity())
                .country(l.getCountry())
                .email(l.getEmail())
                .externalId(trim(l.getExternalId()))
                .name(l.getName())
                .phone(l.getPhone())
                .postalcode(l.getPostalcode())
                .build();
    }

    protected Destination toDestination(PGLocation l) {
        if (l == null) return null;
        return Destination.builder()
                .address(l.getAddress1() + " " + l.getAddress2())
                .city(l.getCity())
                .country(l.getCountry())
                .email(l.getEmail())
                .externalId(trim(l.getExternalId()))
                .name(l.getName())
                .phone(l.getPhone())
                .postalcode(l.getPostalcode())
                .build();
    }
    
    protected LoadLocation toLoadLocation(PGLoadLocation l) {
        return LoadLocation.builder()
                .address(l.getAddress1() + " " + l.getAddress2())
                .city(l.getCity())
                .country(l.getCountry())
                .email(l.getEmail())
                .externalId(trim(l.getExternalId()))
                .name(l.getName())
                .phone(l.getPhone())
                .postalcode(l.getPostalcode())
                .timeFrameFrom(toDatetime(l.getTimeFrameFrom()))
                .timeFrameTo(toDatetime(l.getTimeFrameTo()))
                .build();
    }
    
    protected UnloadLocation toUnloadLocation(PGLoadLocation l) {
        return UnloadLocation.builder()
                .address(l.getAddress1() + " " + l.getAddress2())
                .city(l.getCity())
                .country(l.getCountry())
                .email(l.getEmail())
                .externalId(trim(l.getExternalId()))
                .name(l.getName())
                .phone(l.getPhone())
                .postalcode(l.getPostalcode())
                .timeFrameFrom(toDatetime(l.getTimeFrameFrom()))
                .timeFrameTo(toDatetime(l.getTimeFrameTo()))
                .build();
    }
    
    protected Debitor toDebitor(PGDebitor d) {
        return Debitor.builder()
                .externalId(trim(d.getExternalId()))
                .name(d.getName())
                .price(d.getPrice())
                .build();
    }

    protected Client toClient(PGClient c) {
        return Client.builder()
                .debtorNumber(trim(c.getDebtorNumber()))
                .externalId(trim(c.getExternalId()))
                .name(c.getName())
                .build();
    }
    
    protected String trim(String s) {
        return s == null ? null : s.trim();
    }
    
    protected LocalDateTime toDatetime(String s) {
        if (s == null || s.isEmpty()) return null;
        String replaced = whitespace.matcher(s).replaceAll("");
        return LocalDateTime.parse(replaced, DateTimeFormatter.ISO_DATE_TIME);
    }
    
    protected OrderLineType toLineType(String s) {
        if ("COLLI".equals(s)) return OrderLineType.CUSTOM;
        return OrderLineType.valueOf(s);
    }
 
    /**
     * Try to determine individual dimensions from volume and pallet size (assuming EURO_PALLET when unknown).
     * 
     * @param orderLine
     * @param volume 
     */
    protected void dimFix(OrderLine orderLine, Double volume) {
        if (volume == null || volume == 0.0) return;
        
        //assumming w/l/h are either all set or all empty
        if (orderLine.getHeight() == null) {
            OrderLineType dimType = orderLine.getType();
            if (dimType == null || dimType == OrderLineType.CUSTOM) {
                dimType = OrderLineType.EURO_PALLET;
            }
            orderLine.setLength(dimType.length);
            orderLine.setWidth(dimType.width);
            orderLine.setHeight(volume / (dimType.length * dimType.width)); //todo: use line.amount as well?
        }
    }
}
