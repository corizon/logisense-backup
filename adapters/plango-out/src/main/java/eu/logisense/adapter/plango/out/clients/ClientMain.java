/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.adapter.plango.out.clients;

import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.ApiConfig;
import eu.logisense.adapter.plango.out.Config;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.adapter.plango.out.DirWatcher;
import eu.logisense.adapter.plango.out.auth.ServiceLoginSequence;
import java.util.logging.Logger;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ClientMain {
    private static final Logger LOGGER = Logger.getLogger(ClientMain.class.getName());

    public static void main(String[] args) {
        String incomingPath = Config.getValue(Config.PROP_INCOMING_PATH);
        String baseUri = Config.getValue(Config.PROP_LOGISENSE_BASE_URI);
        
        DirConfig dirConfig = new DirConfig(incomingPath);
        if (!dirConfig.isValid()) {
            LOGGER.severe("invalid dir config, exiting");
            System.exit(-1);
        }

        ApiClient apiClient = new ApiClient(new ApiConfig(baseUri), new ServiceLoginSequence());
        ClientImportAdapter clientImporter = new ClientImportAdapter(apiClient, dirConfig);
        //TODO: optionally wrap importer in a WriteDetector
        DirWatcher watcher = new DirWatcher(dirConfig.getIncomingPath(), clientImporter);
        watcher.start(false);
    }
}
