/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.dispatch;

import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.ApiException;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.adapter.plango.out.FileHandler;
import eu.logisense.api.Shipment;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author johan
 */
public class DispatchAdapter implements FileHandler {
    private static final Logger LOGGER = Logger.getLogger(DispatchAdapter.class.getName());
    
    private final ApiClient apiClient;
    private final DirConfig dirConfig;
    private final CSVMapper csvMapper;
    //translate local plan&go client/partner ids to capability/provider ids:
    private final Map<String,String> clientToCapability;

    public DispatchAdapter(ApiClient apiClient, DirConfig dirConfig, CSVMapper csvMapper, Map<String,String> clientToCapability) {
        this.apiClient = apiClient;
        this.dirConfig = dirConfig;
        this.csvMapper = csvMapper;
        this.clientToCapability = clientToCapability;
    }
    
    @Override
    public void handle(Path filePath) {
        // TODO: need locks? how will plan&go put files in the out dir?
        // is there a risk it's still writing when DirWatcher receives an ENTRY_CREATE event?
        
        LOGGER.log(Level.INFO, "processing file {0}", filePath.getFileName());
        
        List<ShipmentWrapper> wrappers;
        try {
            wrappers = csvMapper.read(filePath);
        } catch(Exception ex) {
            LOGGER.log(Level.SEVERE, "failed to read shipments from CSV", ex);
            dirConfig.moveFailed(filePath);
            return;
        }
        
        Map<ShipmentWrapper,Shipment> itemsSucceeded = new HashMap<>();
        Map<ShipmentWrapper,Exception> itemsFailed = new HashMap<>();
        
        wrappers.forEach(w -> {
            LOGGER.log(Level.FINEST, "dispatching to partner {0}", w.getToPartnerId());
            LOGGER.log(Level.FINEST, "shipment template details: ", w.getShipment());
            
            try {
                Shipment shipment = postShipment(w);
                LOGGER.log(Level.INFO, "received new shipment id {0}", shipment.getId());
                itemsSucceeded.put(w, shipment);
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
                itemsFailed.put(w, ex);
            }
        });
        
        if (!itemsFailed.isEmpty()) {
            dirConfig.moveFailed(filePath);
        } else {
            dirConfig.moveDone(filePath);
        }
        //TODO: add process context per item (src csv: file+linenumber; result: id or exception)
        // collect done/failed items and write to new file(s); 
    }

    @Override
    public boolean supports(Path p) {
        return Files.isRegularFile(p) && hasCsvExtension(p);
    }

    @Override
    public boolean supports(Path p, BasicFileAttributes a) {
        return a.isRegularFile() && hasCsvExtension(p);
    }
    
    protected boolean hasCsvExtension(Path p) {
        return p.getFileName().toString().toUpperCase().endsWith(".CSV");
    }
    
    protected Shipment postShipment(ShipmentWrapper w) throws ApiException {
//        if (!CLIENT_TO_CAPABILITY.containsKey(w.getToPartnerId())) throw new ApiException(400, "");
        String capabilityId = getCapabilityId(w.getToPartnerId());
        Shipment shipment = apiClient.dispatchShipment(capabilityId, w.getShipment());
        return shipment;
    }
    
    protected String getCapabilityId(String partnerId) {
        return clientToCapability.get(partnerId);
    }

}
