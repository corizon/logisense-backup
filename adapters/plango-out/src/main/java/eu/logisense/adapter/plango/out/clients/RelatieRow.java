/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.clients;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author johan
 */
@JacksonXmlRootElement(localName = RelatieRow.ROOT_ELEMENT)
public class RelatieRow {
    public static final String ROOT_ELEMENT =  "row";
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "adreskode")
    private String addressCode;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "naam")
    private String name;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "naam_deel_2")
    private String namePart2;
    
    // includes the house number (unlike post address, which uses a separate attribute)
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "woonadres")
    private String homeAddress;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "telefoon")
    private String phoneNumber;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "postkode")
    private String postalCode;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "woonplaats")
    private String city;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "landkode")
    private String countryCode;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "debiteur")
    private String debtorNumber;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "crediteur")
    private String creditorNumber;
    
    // might contain multiple addresses separated by commas
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "Relatie_e_mail")
    private String emailAddress;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "post_adres")
    private String postAddress;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "post_woonplaats")
    private String postCity;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "Url_www_adres")
    private String homePageUrl;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "post_huisnummer")
    private String postHouseNumber;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "post_postkode")
    private String postPostalCode;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "land_omschrijving")
    private String countryName;
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "Relatienummer")
    private String relationNumber;
    
}
