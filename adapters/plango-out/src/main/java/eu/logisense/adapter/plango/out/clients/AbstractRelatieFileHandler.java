/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.clients;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.adapter.plango.out.FileHandler;
import eu.logisense.api.Client;
import eu.logisense.api.ApprovalStatus;
import eu.logisense.api.Location;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author johan
 */
public abstract class AbstractRelatieFileHandler implements FileHandler {
    private static final Logger LOGGER = Logger.getLogger(AbstractRelatieFileHandler.class.getName());
    
    protected final ApiClient apiClient;
    protected final DirConfig dirConfig;

    public AbstractRelatieFileHandler(ApiClient apiClient, DirConfig dirConfig) {
        this.apiClient = apiClient;
        this.dirConfig = dirConfig;
    }
    
    @Override
    public void handle(Path filePath) {
        LOGGER.log(Level.INFO, "handling {0}", filePath);
        int totalCount = 0, failCount = 0;
        XMLStreamReader sr = null;
        
        try (InputStream inStream = Files.newInputStream(filePath);
                Writer logWriter = dirConfig.getLogWriter(filePath)) {
            
            XMLInputFactory f = XMLInputFactory.newFactory();
            sr = f.createXMLStreamReader(inStream);
            XmlMapper mapper = new XmlMapper();
            
            if (!sr.hasNext()) {
                LOGGER.warning("no root element found");
                dirConfig.moveFailed(filePath);
                return;
            }
            
            sr.next(); // "csvimport" root
            if (!RelatieImport.ROOT_ELEMENT.equals(sr.getLocalName())) {
                LOGGER.log(Level.WARNING, "invalid root element name: {0}", sr.getLocalName());
                dirConfig.moveFailed(filePath);
                return;
            }
            
            while (sr.hasNext()) {
                sr.next();
                
                if (sr.isStartElement() && RelatieRow.ROOT_ELEMENT.equals(sr.getLocalName())) {
                    RelatieRow row = mapper.readValue(sr, RelatieRow.class);
                    
                    boolean rowSuccess = handleRow(row, logWriter);
                    
                    totalCount++;
                    if (!rowSuccess) failCount++;
                    if (totalCount % 1000 == 0) LOGGER.log(Level.INFO, "progress: {0} rows", totalCount);
                }
            }
            
            if (failCount > 0)
                dirConfig.moveFailed(filePath);
            else          
                dirConfig.moveDone(filePath);
                
        } catch (RuntimeException | XMLStreamException | IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            dirConfig.moveFailed(filePath);
        } finally {
            if (sr != null) {
                try {
                    sr.close(); // not autoclosable; does not close underlying stream;
                } catch (XMLStreamException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.INFO, "processed {0} rows, {1} failed", new Object[]{totalCount, failCount});
    }
    
    // create or update a client or location:
    public abstract boolean handleRow(RelatieRow row, Writer logWriter);
    
    protected boolean hasXmlExtension(Path p){
        return p.getFileName().toString().toUpperCase().endsWith(".XML");
    }
    
    //
    // TODO: extract mapper class
    //
    
    protected Client client(RelatieRow row) {
        String name = clientName(row);
        
        Client.ClientBuilder cb = Client.builder();
        cb.name(name);
        
        // FIXME: home/post variants sharing address code as external id; skipping post location for now
        Location home = homeLocation(row, name);
//        Location post = postLocation(row, name);
        
//        cb.locations(Arrays.asList(home, post));
        cb.locations(Arrays.asList(home));
        cb.debtorNumber(row.getDebtorNumber());
        cb.creditorNumber(row.getCreditorNumber());
        cb.externalId(row.getRelationNumber());
        //status: assuming already approved when importing from plan&go
        cb.status(ApprovalStatus.APPROVED);
        
        return cb.build();
    }
    
    protected Location location(RelatieRow row) {
        return homeLocation(row, null);
    }
    
    private String clientName(RelatieRow row) {
        StringBuilder sb = new StringBuilder();
        if (!isEmpty(row.getName())) sb.append(row.getName());
        if (!isEmpty(row.getName()) && !isEmpty(row.getNamePart2())) sb.append("\n");
        if (!isEmpty(row.getNamePart2())) sb.append(row.getNamePart2());
        return sb.toString();
    }
    
    private Location homeLocation(RelatieRow row, String name) {
        Location.LocationBuilder hlb = Location.buildLocation();
        hlb.name(isEmpty(name) ? clientName(row) : name);
        hlb.address(row.getHomeAddress()); // includes houseNumber
        hlb.postalcode(row.getPostalCode());
        hlb.city(row.getCity());
        hlb.country(row.getCountryCode());
        hlb.email(row.getEmailAddress());
        hlb.phone(row.getPhoneNumber());
        hlb.status(ApprovalStatus.APPROVED);
        hlb.externalId(row.getAddressCode());
        
        return hlb.build();
    }
    
    private Location postLocation(RelatieRow row, String name) {
        Location.LocationBuilder plb = Location.buildLocation();
        plb.name(isEmpty(name) ? clientName(row) : name);
        plb.address(row.getPostAddress() + " " + row.getPostHouseNumber());
        plb.postalcode(row.getPostPostalCode());
        plb.city(row.getPostCity());
        plb.country(row.getCountryCode());
        plb.status(ApprovalStatus.APPROVED);
        plb.externalId(row.getAddressCode());

        return plb.build();
    }
    
    private boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

}
