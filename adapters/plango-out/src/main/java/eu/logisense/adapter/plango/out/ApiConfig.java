/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Objects;

/**
 *
 * @author johan
 */
public class ApiConfig {
    public static final String ERROR_HEADER = "x-error";

    private final URL baseUrl;
    
    //TODO: authentication, keycloak, 
    
    public ApiConfig(String protocol, String host, int port, String app) {
        this(url(protocol, host, port, app));
    }
    
    public ApiConfig(String baseUrl) {
        this(url(baseUrl));
    }
    
    public ApiConfig(URL baseUrl) {
        Objects.requireNonNull(baseUrl);
        this.baseUrl = baseUrl;
    }

    public String getProtocol() {
        return baseUrl.getProtocol();
    }

    public String getHost() {
        return baseUrl.getHost();
    }

    public int getPort() {
        return baseUrl.getPort();
    }
    
    public URI getBaseUri() {
        return URI.create(baseUrl.toString());
    }
    
    public URI getResourceUri(String resource) {
        return URI.create(baseUrl.toString() + resource);
    }
    
    static URL url(String protocol, String host, int port, String app) {
        try {
            return new URL(protocol, host, port, app);
        } catch (MalformedURLException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
    static URL url(String s) {
        try {
            return new URL(s);
        } catch (MalformedURLException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
}
