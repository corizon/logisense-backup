/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.clients;

import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.ApiException;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.api.Location;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author johan
 */
public class LocationImportAdapter extends AbstractRelatieFileHandler {
    private static final Logger LOGGER = Logger.getLogger(LocationImportAdapter.class.getName());

    public LocationImportAdapter(ApiClient apiClient, DirConfig dirConfig) {
        super(apiClient, dirConfig);
    }

    @Override
    public boolean handleRow(RelatieRow row, Writer logWriter) {
        StringBuilder logLine = new StringBuilder();
                
        try {
            logLine.append(row.getRelationNumber()).append("\t").append(row.getAddressCode()).append("\t");
            Location template = location(row);
            Location result = apiClient.postLocation(template);
            LOGGER.log(Level.FINE, "received new location id {0}", result.getId());
            logLine.append("created ").append(result.getId());
            return true;
        } catch (ApiException | IOException | RuntimeException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            logLine.append(ex.getMessage());
            return false;
        } finally {
            try {
                logLine.append("\n");
                logWriter.append(logLine.toString());
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public boolean supports(Path filePath, BasicFileAttributes a) {
        return a.isRegularFile() && commonSupports(filePath);
    }

    @Override
    public boolean supports(Path filePath) {
        return Files.isRegularFile(filePath) && commonSupports(filePath);
    }
    
    protected boolean commonSupports(Path filePath) {
        return filePath.getFileName().toString().startsWith("Relaties") && hasXmlExtension(filePath);
    }
    
}
