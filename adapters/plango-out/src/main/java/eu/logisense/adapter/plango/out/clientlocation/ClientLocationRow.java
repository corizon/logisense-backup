/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.adapter.plango.out.clientlocation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
@JacksonXmlRootElement(localName = ClientLocationRow.ROOT_ELEMENT)
public class ClientLocationRow {

    public static final String ROOT_ELEMENT = "clientLocations";

    @Getter
    @Setter
    @JacksonXmlProperty(localName = "id")
    private String id;
    

    @Getter
    @Setter
    @JacksonXmlProperty(localName = "id_PG")
    private String id_PG;

    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "name")
    private String name;    
    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "address")
    private String address;
    

    @Getter
    @Setter
    @JacksonXmlProperty(localName = "address_no")
    private String address_no;

    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "zipcode")
    private String zipcode;
    

    @Getter
    @Setter
    @JacksonXmlProperty(localName = "city")
    private String city;

    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "country")
    private String country;
    

    @Getter
    @Setter
    @JacksonXmlProperty(localName = "phone")
    private String phone;

    
    @Getter
    @Setter
    @JacksonXmlProperty(localName = "email")
    private String email;
}
