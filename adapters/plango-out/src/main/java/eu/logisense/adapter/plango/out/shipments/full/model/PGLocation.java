/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.adapter.plango.out.shipments.full.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author johan
 */
@NoArgsConstructor
public class PGLocation {

    // main diff from logisense-api: address split in 2 parts (street, housenumber + extension)
    @Getter
    @Setter
    private String address1;
    
    @Getter
    @Setter
    private String address2;
    
    @Getter
    @Setter
    private String city;
    
    @Getter
    @Setter
    private String country;
    
    @Getter
    @Setter
    private String email;
    
    @Getter
    @Setter
    private String externalId;
    
    @Getter
    @Setter
    private String name;
    
    @Getter
    @Setter
    private String phone;
    
    @Getter
    @Setter
    private String postalcode;

    @Builder(builderMethodName = "buildLocation")
    public PGLocation(String address1, String address2, String city, String country, String email, String externalId, String name, String phone, String postalcode) {
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.country = country;
        this.email = email;
        this.externalId = externalId;
        this.name = name;
        this.phone = phone;
        this.postalcode = postalcode;
    }

}
