/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.auth;

import eu.logisense.adapter.plango.out.Config;
import eu.logisense.api.session.client.Connections;
import eu.logisense.api.session.client.LoginSequence;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.keycloak.OAuthErrorException;
import org.keycloak.adapters.ServerRequest;
import org.keycloak.common.VerificationException;

/**
 *
 * @author johan
 */
public class ServiceLoginSequence extends LoginSequence {
    private static final Logger LOGGER = Logger.getLogger(ServiceLoginSequence.class.getName());
    
    @Override
    protected void init() {
    }

    @Override
    protected void activateLogin() {
        try {
            KeycloakSessionImpl.getKeycloak().login();
            loginCallback.onSuccess();
        } catch (IOException | ServerRequest.HttpFailure | VerificationException | OAuthErrorException | RuntimeException ex) {
            LOGGER.log(Level.SEVERE, "failed to login: {0}", ex);
            loginCallback.onFailure();
        }
    }

    @Override
    protected void resume() {
        Connections.create(Config.getValue(Config.PROP_LOGISENSE_BASE_URI));
    }

    @Override
    protected void exit() {
        LOGGER.info("exiting");
        System.exit(-1);
    }

}
