/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.auth;

import eu.logisense.api.session.Session;
import eu.logisense.api.session.client.Connection;
import static eu.logisense.api.session.client.Connection.PROP_ACTIVE;
import eu.logisense.api.session.client.ConnectionFactory;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author johan
 */
public class ConnectionImpl implements Connection{
    private static final Logger LOGGER = Logger.getLogger(ConnectionImpl.class.getName());

    private final URL url;
    private final BooleanProperty activeProperty = new SimpleBooleanProperty(false);
    private final Session session = new KeycloakSessionImpl();

    public ConnectionImpl(String baseUri) {
        try {
            this.url = new URL(baseUri);
        } catch (MalformedURLException ex) {
            throw new IllegalArgumentException(ex);
        }
        activeProperty.set(true);
    }

    @Override
    public void addActivationListener(final PropertyChangeListener listener) {
        activeProperty.addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
            listener.propertyChange(new PropertyChangeEvent(activeProperty, PROP_ACTIVE, oldValue, newValue));
        });
    }
    
    @Override
    public boolean isActive() {
        return activeProperty.get();
    }

    @Override
    public Session getSession() {
        return activeProperty.get() ? session : null;
    }

    @Override
    public URL getURL() {
        return url;
    }
    
    @ServiceProvider(service = ConnectionFactory.class, position = 100)
    public static class FactoryImpl implements ConnectionFactory {
        
        @Override
        public Connection create(String baseUri) {
            return new ConnectionImpl(baseUri);
        }
    }
    
}
