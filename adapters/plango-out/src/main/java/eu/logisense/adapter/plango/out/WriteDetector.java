/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <code>FileHandler</code> decorator that needs to detect whether a newly
 * created or modified file is still being written to and delegate processing
 * when writing is complete.
 *
 * Assuming the <code>supports()</code> methods don't need file contents and
 * will only use name, extention and attributes.
 * 
 * @author johan
 */
public class WriteDetector implements FileHandler {
    private static final Logger LOGGER = Logger.getLogger(WriteDetector.class.getName());
    
    public static final int DEFAULT_INTERVAL = 2000;
    // max tries / total time?
    
    private final FileHandler delegate;
    private final int interval;
    
    public WriteDetector(FileHandler delegate) {
        this.delegate = Objects.requireNonNull(delegate);
        this.interval = DEFAULT_INTERVAL;
    }

    @Override
    public boolean supports(Path filePath, BasicFileAttributes a) {
        return delegate.supports(filePath, a);
    }

    @Override
    public boolean supports(Path filePath) {
        return delegate.supports(filePath);
    }

    @Override
    public void handle(Path filePath) {
        // 1) wait until we can get an exclusive lock on the file,
        //    (but looks like this is possible even while another process is writing, see tests)
//        lockStrategy(filePath);
        // or 2) wait until file contents (size/checksum) stop changing,
        sizeStrategy(filePath);
//        checksumStrategy(filePath);//TODO
        // or 3) wait until file reaches a minimum age
        //       see e.g. Spring Integration's LastModifiedFileListFilter
        //       http://docs.spring.io/spring-integration/reference/html/files.html
        //       link has more suggestions, though these require changes in the writing process (like renaming when finished)
        // or 4) ...
    }
            
    private void lockStrategy(Path filePath) {
        try (FileChannel fc = FileChannel.open(filePath, StandardOpenOption.WRITE)) {
            FileLock lock = fc.tryLock(); //requires writable channel
            while (lock == null) {
                Thread.sleep(interval);
                lock = fc.tryLock();
            } 
            lock.release();
            delegate.handle(filePath);
        } catch (IOException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
    private void sizeStrategy(Path filePath) {
        try {
            long prevSize = -1;
            long size = Files.size(filePath);
            while (size != prevSize) {
                Thread.sleep(interval);
                prevSize = size;
                size = Files.size(filePath);
            }
            delegate.handle(filePath);
        } catch (IOException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

}
