/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.full.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author johan
 */
@NoArgsConstructor
public class PGLine {

    @Getter
    @Setter
    private Double amount; //can be Integer if decimal part is removed from import, i.e. "      4" instead of "      4.00"
    
    @Getter
    @Setter
    private String description;
    
    @Getter
    @Setter
    private Double height;
    
    @Getter
    @Setter
    private Double length;
    
    @Getter
    @Setter
    private Double paymentOnDeliveryAmount;
    
    @Getter
    @Setter
    private String paymentOnDeliveryCurrency;
    
    @Getter
    @Setter
    private String remarks;
    
    @Getter
    @Setter
    private String type;
    
    @Getter
    @Setter
    private Double volume;
    
    @Getter
    @Setter
    private Double weight;
    
    @Getter
    @Setter
    private Double width;

    @Builder
    public PGLine(Double amount, String description, Double height, Double length, Double paymentOnDeliveryAmount, String paymentOnDeliveryCurrency, String remarks, String type, Double volume, Double weight, Double width) {
        this.amount = amount;
        this.description = description;
        this.height = height;
        this.length = length;
        this.paymentOnDeliveryAmount = paymentOnDeliveryAmount;
        this.paymentOnDeliveryCurrency = paymentOnDeliveryCurrency;
        this.remarks = remarks;
        this.type = type;
        this.volume = volume;
        this.weight = weight;
        this.width = width;
    }

}
