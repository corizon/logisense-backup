/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.logisense.adapter.plango.out.clientlocation;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.logisense.adapter.plango.out.ApiClient;
import eu.logisense.adapter.plango.out.DirConfig;
import eu.logisense.adapter.plango.out.FileHandler;
import eu.logisense.api.Client;
import eu.logisense.api.ApprovalStatus;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author Timon Veenstra <timon@limetri.eu>
 */
public class ClientLocationFileHandler implements FileHandler {
    private static final Logger LOGGER = Logger.getLogger(ClientLocationFileHandler.class.getName());

    protected final ApiClient apiClient;
    protected final DirConfig dirConfig;

    public ClientLocationFileHandler(ApiClient apiClient, DirConfig dirConfig) {
        this.apiClient = apiClient;
        this.dirConfig = dirConfig;
    }

    @Override
    public void handle(Path filePath) {
        LOGGER.log(Level.INFO, "handling {0}", filePath);
        int totalCount = 0, failCount = 0;
        XMLStreamReader sr = null;

        try (InputStream inStream = Files.newInputStream(filePath);
                Writer logWriter = dirConfig.getLogWriter(filePath)) {
            
            XMLInputFactory f = XMLInputFactory.newFactory();
            sr = f.createXMLStreamReader(inStream);
            XmlMapper mapper = new XmlMapper();

            if (!sr.hasNext()) {
                LOGGER.warning("no root element found");
                dirConfig.moveFailed(filePath);
                return;
            }

            sr.next(); // "ExportObjectCardVM" root
            if (!ClientLocationImport.ROOT_ELEMENT.equals(sr.getLocalName())) {
                LOGGER.log(Level.WARNING, "invalid root element name: {0}", sr.getLocalName());
                dirConfig.moveFailed(filePath);
                return;
            }

            while (sr.hasNext()) {
                sr.next();

                if (sr.isStartElement() && ClientLocationRow.ROOT_ELEMENT.equals(sr.getLocalName())) {
                    ClientLocationRow row = mapper.readValue(sr, ClientLocationRow.class);
                    boolean rowSuccess = handleRow(row, logWriter);
                    
                    totalCount++;
                    if (!rowSuccess) failCount++;
                    if (totalCount % 1000 == 0) LOGGER.log(Level.INFO, "progress: {0} rows", totalCount);
                }
            }

            if (failCount > 0)
                dirConfig.moveFailed(filePath);
            else          
                dirConfig.moveDone(filePath);

        } catch (RuntimeException | XMLStreamException | IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            dirConfig.moveFailed(filePath);
        } finally {
            if (sr != null) {
                try {
                    sr.close();
                } catch (XMLStreamException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.INFO, "processed {0} rows, {1} failed", new Object[]{totalCount, failCount});
    }

    public boolean handleRow(ClientLocationRow row, Writer logWriter) {
        StringBuilder logLine = new StringBuilder();
        
        try {
            logLine.append(row.getId()).append("\t").append(row.getId_PG()).append("\t");
            
            // map imported row to client w/ locations:
            Client template = client(row);

            Client client = apiClient.getClientService().getClient(template.getId());
            if (client != null) {
                client.setStatus(ApprovalStatus.APPROVED);
                client.setExternalId(template.getExternalId());

                apiClient.getClientService().updateClient(client.getId(),client);
                logLine.append("updated");
                return true;
            } else {
                LOGGER.log(Level.WARNING, "client with id {0} not found in logisense", template.getId());
                logLine.append("client id not found in logisense");
                return false;
            }
        
        } catch (RuntimeException ex) { 
            LOGGER.log(Level.SEVERE, null, ex);
            logLine.append(ex.getMessage());
            return false;
        } finally {
            try {
                logLine.append("\n");
                logWriter.append(logLine.toString());
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public boolean supports(Path filePath, BasicFileAttributes a) {
        return a.isRegularFile() && commonSupports(filePath);
    }

    @Override
    public boolean supports(Path filePath) {
        return Files.isRegularFile(filePath) && commonSupports(filePath);
    }

    protected boolean commonSupports(Path filePath) {
        return filePath.getFileName().toString().startsWith("exportClients") && hasXmlExtension(filePath);
    }

    protected boolean hasXmlExtension(Path p) {
        return p.getFileName().toString().toUpperCase().endsWith(".XML");
    }

    //
    // TODO: extract mapper class
    //
    protected Client client(ClientLocationRow row) {

        Client.ClientBuilder cb = Client.builder();
        cb.id(row.getId());
        cb.externalId(row.getId_PG());
        cb.status(ApprovalStatus.APPROVED);

        return cb.build();
    }

}
