/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;

/**
 *
 * @author johan
 */
public class DirConfig {
    private static final Logger LOGGER = Logger.getLogger(DirConfig.class.getName());
    
    public static final String DEFAULT_DONE_DIR = "done";
    public static final String DEFAULT_FAIL_DIR = "failed";
    public static final String DEFAULT_LOG_DIR = "log";
    
    /**
     * Directory to watch for incoming files to process.
     */
    @Getter
    private final Path incomingPath;
    
    /**
     * Files that were processed successfully should be moved here.
     */
    @Getter
    private final Path donePath;
    
    /**
     * Files that have any failure during processing should be moved here.
     * Even if part of the file was processed successfully and is already saved/committed to the database.
     * 
     */
    @Getter
    private final Path failedPath;
    
    /**
     * Contains details logs, preferably one per processed file.
     */
    @Getter
    private final Path logPath;
    
    public DirConfig(String incoming) {
        this.incomingPath = Paths.get(incoming);
        
        this.donePath = this.incomingPath.resolve(DEFAULT_DONE_DIR);
        this.failedPath = this.incomingPath.resolve(DEFAULT_FAIL_DIR);
        this.logPath = this.incomingPath.resolve(DEFAULT_LOG_DIR);
    }
    
    public boolean isValid() {
        return isValidDir(incomingPath)
                && isValidDir(donePath)
                && isValidDir(failedPath)
                && isValidDir(logPath);
    }
    
    /**
     * Creates a writable log for a single file being processed.
     * 
     * @param filePath
     * @return
     * @throws IOException 
     */
    public Writer getLogWriter(Path filePath) throws IOException {
        String logName = filePath.getFileName().toString() + "." + new Date().getTime() + ".log";
        Path logFile = getLogPath().resolve(logName);
        return Files.newBufferedWriter(logFile);
    }
    
    public static boolean isValidDir(Path path) {
        if (!Files.exists(path)) {
            LOGGER.log(Level.SEVERE, "path does not exist: {0}", path);
            return false;
        }
        if (!Files.isDirectory(path)) {
            LOGGER.log(Level.SEVERE, "not a directory: {0}", path);
            return false;
        }
        return true;
    }
    
    public void moveDone(Path filePath) {
        moveInto(donePath, filePath);
    }
    
    public void moveFailed(Path filePath) {
        moveInto(failedPath, filePath);
    }
    
    private void moveInto(Path dirPath, Path filePath) {
        try {
            Files.move(filePath, dirPath.resolve(filePath.getFileName()), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
}
