/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.logisense.adapter.plango.out.shipments.full.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author johan
 */
@NoArgsConstructor
public class PGShipment {
    public static final String ROOT_ELEMENT = "shipments";
    
    @Getter
    @Setter
    private String deadline;
    
    @Getter
    @Setter
    private PGLoadLocation loadLocation;
    
    @Getter
    @Setter
    private String note;
    
    @Getter
    @Setter
    private PGOrder order;
    
    @Getter
    @Setter
    private PGLoadLocation unloadLocation;
    
    @Getter
    @Setter
    @JacksonXmlElementWrapper(useWrapping = false)
    private PGLine[] lines; // lines not on order as in logisense-api
    
    //
    // line totals
    //
    
    @Getter
    @Setter
    private Double shippedUnits; // sum(lines.amount)

    @Getter
    @Setter
    @Deprecated
    @JsonIgnore
    private Double volume;

    @Getter
    @Setter
    @Deprecated
    @JsonIgnore
    private Double weight;

    @Builder
    public PGShipment(String deadline, PGLoadLocation loadLocation, String note, PGOrder order, PGLoadLocation unloadLocation, PGLine[] lines, Double shippedUnits) {
        this.deadline = deadline;
        this.loadLocation = loadLocation;
        this.note = note;
        this.order = order;
        this.unloadLocation = unloadLocation;
        this.lines = lines;
        this.shippedUnits = shippedUnits;
    }
    
}
