/**
 * Copyright (C) 2013-2016 Corizon BV. All rights reserved.
 * This license is granted for any contributor who benefits in one way or another
 * of the open source software from Corizon. The software may be redistributed
 * and/or modified under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the license,
 * or (at your option) any later version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the foss license exception.
 * https://www.corizon.nl/foss-exception.
 *
 * LogiSense is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License along
 * with LogiSense. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.logisense.adapter.plango.out.shipments.dispatch;

import au.com.bytecode.opencsv.CSVReader;
import eu.logisense.api.LoadLocation;
import eu.logisense.api.Shipment;
import eu.logisense.api.TransportStatus;
import eu.logisense.api.UnloadLocation;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author johan
 */
public class CSVMapper {
    private static final Logger LOGGER = Logger.getLogger(CSVMapper.class.getName());
    
    public List<ShipmentWrapper> read(Path filePath) {
        List<ShipmentWrapper> wrappers = new ArrayList<>();
        
        // if (!Files.probeContentType(filePath).equals("text/plain")) { ...
        
        try (Reader reader = Files.newBufferedReader(filePath);
                CSVReader csvReader = new CSVReader(reader, ';', '"', 1);) {
            
            String[] line;
            while ((line = csvReader.readNext()) != null) {
                String toPartnerId = line[0];
                
                LocalDateTime etd = isBlank(line[13]) ? LocalDateTime.MAX : LocalDateTime.parse(line[13]+".23.59", DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm"));
                LoadLocation load = LoadLocation.builder()
                        .name(line[3])
                        .address(line[4])
                        .city(line[5])
                        .postalcode(line[6])
                        .country(line[7])
                        .latitude(0.0)
                        .longitude(0.0)
                        .timeFrameFrom(etd)
                        .timeFrameTo(etd)
                        .build();
                LocalDateTime eta = isBlank(line[14]) ? LocalDateTime.MAX : LocalDateTime.parse(line[14], DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm"));
                UnloadLocation unload = UnloadLocation.builder()
                        .name(line[8])
                        .address(line[9])
                        .city(line[10])
                        .postalcode(line[11])
                        .country(line[12])
                        .timeFrameFrom(eta)
                        .timeFrameTo(eta)
                        .latitude(0.0)
                        .longitude(0.0)
                        .build();
                Shipment shipment = Shipment.builder()
//                        .weight(Double.valueOf(line[1]))
                        //FIXME map to order line
//                        .volume(Double.valueOf(line[2]))
                        .loadLocation(load)
                        .unloadLocation(unload)
                        .status(TransportStatus.NEW)
                        .deadline(isBlank(line[15]) ? LocalDateTime.MAX : LocalDateTime.parse(line[15]+".23.59", DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm")))
                        .build();

                ShipmentWrapper w = new ShipmentWrapper(shipment, toPartnerId);
                wrappers.add(w);
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "IO Exception", ex);
            throw new RuntimeException(ex);
        }
        
        return wrappers;
    }
    
    private static boolean isBlank(String s) {
        return s == null || s.trim().isEmpty();
    }
    
//    public void write(Path filePath, List<ShipmentWrapper>) {}
    //TODO: write variants w/ done/failed process context (ids from partner, error messages, )
}
