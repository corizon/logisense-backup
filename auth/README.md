# Authentication

## Setup

### Keycloak server

install keycloak-1.8.1.Final

- [download](http://keycloak.jboss.org/downloads.html) `keycloak-1.8.1.Final.zip`
- unzip anywhere
- open a terminal in the `bin` subdirectory
- import demo realm via server startup parameters:  
    the demo realm json file is included in same directory as this readme;  
    update the path for the `keycloak.migration.file` parameter, and run:

```
./standalone.sh -Dkeycloak.migration.action=import -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=<.../PATH/TO>/logisensedemorealm.json -Dkeycloak.migration.strategy=OVERWRITE_EXISTING
```

subsequent startups can use just `./standalone.sh`


### Maven build

add a profile to your `settings.xml`:

(values match imported file from previous step)

```
        <profile>
            <id>logisense-target-localhost</id>
            <properties>
                <logisense.realm.public.key>MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApnwjweHERIUZTTMjAALYACwbWKM9UEBo+h91trkWsGJ05z88O4GU4xNmv7q+XWb6hdtPhhn2iSyzoDhzYMmMOvEaQAz4Am+jqB7OLFl95JuCHnyHy0EnXuF12L6uMx8XgckqEBgv+xbyWsXvc1ibtgapBxlsGfZIDN+clMrbTtvDC2u0luPYHQwbI9OLUZtqxx+Nmv32smw/xzHSqgLe43ZD0EPjVSXT5DEHahGCqr0yyavvPI0qHc+YjbWxrhqbe+16LG2XJGukGkepXibcWTqqlQ+1kCQXkFcOFzGLY1LTd1545i69Pey25g3mdnSkEOon3DXayoMKd8iVR0lUdQIDAQAB</logisense.realm.public.key>
                <logisense.auth.url>http://localhost:8080/auth</logisense.auth.url>
                <logisense.world.name>localhost</logisense.world.name>
                <logisense.world.application>world-${logisense.world.name}</logisense.world.application>
                <logisense.world.role>${logisense.world.application}-user</logisense.world.role>
                <logisense.plango-out.secret>76be90ee-bd79-46a2-bb27-c0154137d194</logisense.plango-out.secret>
            </properties>
        </profile>
```

activate the profile when building logisense-parent (and when rebuilding the rcp-session and server-web modules)


## Run authenticated rich client

- start keycloak server: `.../path/to/keycloak-server/bin/standalone.sh`
- start logisense server-web via wildfly-swarm
- start logisense rcp-app
- use demo credentials: `demo/demo`


//TODO: apps
